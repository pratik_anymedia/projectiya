-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 24, 2015 at 04:49 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projecti_ya2015`
--
CREATE DATABASE IF NOT EXISTS `projecti_ya2015` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `projecti_ya2015`;

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

DROP TABLE IF EXISTS `advertisements`;
CREATE TABLE IF NOT EXISTS `advertisements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `thumb_path` varchar(255) NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `status` tinyint(2) NOT NULL,
  `role` int(11) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `advertisements`
--

INSERT INTO `advertisements` (`id`, `title`, `description`, `thumb_path`, `file_path`, `url`, `status`, `role`, `created`, `modified`, `slug`) VALUES
(1, 'CS/IT Project with source code', 'CS/IT Project with source code at projectiya.com', './assets/uploads/advertise/thumb/13eef4f727d0baf0a9ebf0f4e12f04fe.jpg', './assets/uploads/advertise/13eef4f727d0baf0a9ebf0f4e12f04fe.jpg', '', 1, 0, '2015-10-24 12:27:36', '0000-00-00 00:00:00', 'csit-project-with-source-code'),
(2, 'sadd', 'asdasd', './assets/uploads/advertise/thumb/d1150dea1933fbab4bf833c3deb2107d.png', './assets/uploads/advertise/d1150dea1933fbab4bf833c3deb2107d.png', 'http://www.dummy.com', 1, 1, '2015-10-24 01:14:48', '0000-00-00 00:00:00', 'sadd');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `blog_title` text NOT NULL,
  `blog_slug` varchar(200) NOT NULL DEFAULT '',
  `blog_content` longtext NOT NULL,
  `file_path` varchar(200) NOT NULL,
  `thumb_image` varchar(200) NOT NULL,
  `comment_count` bigint(20) NOT NULL,
  `blog_status` tinyint(1) NOT NULL DEFAULT '1',
  `blog_category` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `blog_title`, `blog_slug`, `blog_content`, `file_path`, `thumb_image`, `comment_count`, `blog_status`, `blog_category`, `created`, `updated`) VALUES
(1, 'this is my ', 'this-is-my', '&lt;h1&gt;&lt;strong&gt;dsadsdytut&lt;/strong&gt;&lt;/h1&gt;\r\n&lt;p&gt;&lt;strong&gt;kjdkjhkjdf skhkdfsv.lkjdskj lj&lt;/strong&gt;&lt;/p&gt;', './assets/uploads/blog/3023b819db5d70ecc5c529f80ecef630.jpg', './assets/uploads/blog/thumb/3023b819db5d70ecc5c529f80ecef630.jpg', 0, 1, '', '2015-07-05 06:40:04', '2015-07-31 03:06:33'),
(2, 'Top 40 Java Web Services Interview Questions And Answers', 'top-40-java-web-services-interview-questions-and-answers', '&lt;p&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;Main characteristics of the Web Services&amp;nbsp; are :&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;1. Interoperability&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;2. Extensibility&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;3. Machine processable descriptions.&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;for example in simple words , when we call somebody so the&lt;/span&gt;&lt;span style=&quot;margin: 0px; padding: 0px; border: 0px; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: inherit; line-height: 22px; vertical-align: baseline; color: #2f2e2e; letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff2cc;&quot;&gt;&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;person dialing and calling&lt;/span&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;is the&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;margin: 0px; padding: 0px; border: 0px; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: inherit; line-height: 22px; vertical-align: baseline; color: #2f2e2e; letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff2cc;&quot;&gt;client&amp;nbsp; application&lt;/span&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;, while&lt;/span&gt;&lt;span style=&quot;margin: 0px; padding: 0px; border: 0px; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: inherit; line-height: 22px; vertical-align: baseline; color: #2f2e2e; letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff2cc;&quot;&gt;&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;person receiving the call&lt;/span&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;is&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;margin: 0px; padding: 0px; border: 0px; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: inherit; line-height: 22px; vertical-align: baseline; color: #2f2e2e; letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff2cc;&quot;&gt;server application&lt;/span&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;and&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;margin: 0px; padding: 0px; border: 0px; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: inherit; line-height: 22px; vertical-align: baseline; color: #2f2e2e; letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff2cc;&quot;&gt;&quot;hello&quot;&lt;/span&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;word is the protocol as&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;margin: 0px; padding: 0px; border: 0px; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: inherit; line-height: 22px; vertical-align: baseline; color: #2f2e2e; letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff2cc;&quot;&gt;similar to HTTP&lt;/span&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;request .&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;margin: 0px; padding: 0px; border: 0px; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: inherit; line-height: 22px; vertical-align: baseline; letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: #38761d; background-color: #ffffff;&quot;&gt;&lt;strong&gt;Q2&lt;/strong&gt;&amp;nbsp;&lt;strong&gt;What is the difference between SOA and a web service?&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;div class=&quot;p2&quot; style=&quot;margin: 0px; padding: 0px; border: 0px; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: inherit; line-height: 22px; vertical-align: baseline; color: #2f2e2e; letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;div class=&quot;p3&quot; style=&quot;margin: 0px; padding: 0px; border: 0px; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: inherit; line-height: 22px; vertical-align: baseline; color: #2f2e2e; letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot;&gt;&lt;span class=&quot;s1&quot; style=&quot;margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;&quot;&gt;SOA (Service-Oriented Architecture)&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;is an architectural pattern that makes possible for&lt;/div&gt;\r\n&lt;div class=&quot;p3&quot; style=&quot;margin: 0px; padding: 0px; border: 0px; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: inherit; line-height: 22px; vertical-align: baseline; color: #2f2e2e; letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot;&gt;services to interact with one another independently. &lt;/div&gt;', './assets/uploads/blog/aaea11b4e6bdbd5881aae8a13ac32783.jpg', './assets/uploads/blog/thumb/aaea11b4e6bdbd5881aae8a13ac32783.jpg', 0, 1, '', '2015-07-31 03:09:21', '0000-00-00 00:00:00'),
(3, 'What You Should Do Before Pushing PHP Code to your Production GIT Repository', 'what-you-should-do-before-pushing-php-code-to-your-production-git-repository', '&lt;h3&gt;Leaving Qandidate, off to Coolblue&lt;/h3&gt;\r\n&lt;p&gt;After I had a very interesting conversation with the &lt;a href=&quot;http://php-and-symfony.matthiasnoback.nl/2015/07/meeting-the-broadway-team/&quot;&gt;developers behind the Broadway framework for CQRS and event sourcing&lt;/a&gt; the day wasn''t over for me yet. I walked about one kilometer to the north to meet &lt;a href=&quot;https://twitter.com/pderaaij&quot;&gt;Paul de Raaij&lt;/a&gt;, who is a senior developer at &lt;a href=&quot;http://www.coolblue.nl&quot;&gt;Coolblue&lt;/a&gt;, a company which sells and delivers all kinds of - mostly - electrical consumer devices. Their headquarters are very close to the new and shiny Rotterdam Central station. The company itself is doing quite well. With 1000+ employees they keep needing more office space.&lt;/p&gt;\r\n&lt;p&gt;Paul showed me around all departments and offices, which nowadays span three floors. There''s developer teams everywhere. It''s not a PHP-only company. Though PHP is well represented, there are also .NET and Delphi developers. Coolblue runs on quite a lot of software and hardware.&lt;/p&gt;\r\n&lt;h3&gt;Heritage&lt;/h3&gt;\r\n&lt;p&gt;Developers at Coolblue have learnt to call the legacy software they maintain &quot;heritage&quot;. &quot;Legacy software&quot; often has a negative sound to it, while in fact, it''s what enables the company itself to be so successful, so it doesn''t really deserve that negative image. I don''t fully agree with this approach since most of the relevant literature about this subject speaks of &quot;legacy software&quot;, which, to me personally, doesn''t mean anything bad. I''m well aware that anything I write today will be &quot;legacy&quot; tomorrow, because, literally, other people inherit that piece of code and need to maintain it. In my dictionary, &quot;legacy software&quot; isn''t a bad thing (though I know that it often is, so I understand this little play of words).&lt;/p&gt;\r\n&lt;h3&gt;New things: microservices&lt;/h3&gt;\r\n&lt;p&gt;Paul mentioned that there is an ongoing struggle amongst developers who rather want to try &quot;new&quot; things, while they feel stuck in the &quot;old&quot; way of doing things. Paul argues that it''s &lt;em&gt;always&lt;/em&gt; possible to try something &quot;new&quot; in an older project as well. The infrastructure may not be there for it yet, but introducing it might therefore be even more challenging, as well as more satisfying. I fully agree with Paul on this, and I also like to work on an older code-base and introduce new concepts to it. Anyway, in my personal experience, thinking that you''re better off working on a green-field application, because you can do everything &quot;the right way&quot;, often turns out to be quite a fallacy. I''m sure you''ll recognize this sentiment as well.&lt;/p&gt;\r\n&lt;p&gt;At Coolblue, &quot;new things&quot; currently means &lt;em&gt;event sourcing&lt;/em&gt; and &lt;em&gt;microservices&lt;/em&gt;. They have introduced several microservices so far. Microservices are one of these things Coolblue developers have been wanting to introduce for quite some time. It turned out to be not that hard, but, according to Paul, the key was to keep it small at first. They started by extracting several smaller and less crucial parts from the main application into microservices. You can read about some of their experiences on &lt;a href=&quot;http://devblog.coolblue.nl/&quot;&gt;devblog.coolblue.nl&lt;/a&gt;.&lt;/p&gt;\r\n&lt;h3&gt;New things: event sourcing&lt;/h3&gt;\r\n&lt;p&gt;Paul and others have done quite some research with regard to event sourcing as well. They haven''t taken the step yet to implement it in their software. Take a look at this &lt;a href=&quot;http://www.slideshare.net/pderaaij/cqrs-espresentation&quot;&gt;slide deck&lt;/a&gt; to get an impression of what it might look like for them when they do.&lt;/p&gt;\r\n&lt;p&gt;Paul made an interesting observation with regard to &quot;new things&quot;: there is often a mismatch between what a developer thinks of themselves, and what that same developer thinks of other developers. When listening to meetup or conference talks, you may start thinking that you''re way behind on current developments in the world of (PHP) software development. Paul at first felt the same, but noticed that when you actually talk to developers about what you''re doing, it might just as well turn out that you''re doing fine.&lt;/p&gt;\r\n&lt;h3&gt;Teams&lt;/h3&gt;\r\n&lt;p&gt;Developer teams at Coolblue are separated based on the features they work on. There is a team working on &quot;pre-cart&quot;, i.e. everything related to the presentation of the products, their categories, etc. Then there''s a &quot;post-cart&quot; team, which works on actually making the sale, payment, etc. Paul himself is moving from team to team mostly, helping everyone solve any issues that they may be facing. This way, he gets a nice overview which enables him to take knowledge from each team to other teams. This also helps preventing the same mistakes from being made in different teams.&lt;/p&gt;\r\n&lt;p&gt;Walking through the corridors, we pass a lot of &quot;team rooms&quot;. Walls are made of glass, but each team is still nicely separated from the others. They can see, but not hear each other, meaning that they can focus on what they''re working on, while still feeling part of the organization. It appears that each team consists of about&lt;/p&gt;', './assets/uploads/blog/fe57534b867c3081e8f9626b3d8ff0d6.jpg', './assets/uploads/blog/thumb/fe57534b867c3081e8f9626b3d8ff0d6.jpg', 1, 1, 'php', '2015-07-31 03:17:34', '2015-07-31 12:56:13'),
(4, 'Java 8 New Features : Lambda Expressions , Optional Class , Defender Methods With Examples', 'java-8-new-features-lambda-expressions-optional-class-defender-methods-with-examples', '&lt;p&gt;&lt;strong style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot;&gt;1. Introduction Of Optional :&lt;/strong&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;The main benefit of Optional is to&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;em style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot;&gt;&lt;strong&gt;Avoid&lt;/strong&gt;&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;null pointer exception&lt;/em&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&lt;span class=&quot;Apple-converted-space&quot;&gt;&amp;nbsp;&lt;/span&gt;: There is another class named Optional in the util package , as it is used to avoid the null pointer exception , If the value is present then it will return the true value otherwise it will show the false value &amp;nbsp;As it is boolean the value must be between false and true .&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;In java language , to access an object we use reference type .And when we are unable &amp;nbsp;to have a specific object to point to , then we set the value of the reference null , indicating it is not pointing to any object .&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;Null is a literal (also known as constant ) in java .&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;public static Cat &amp;nbsp;find(String name, List;Cat; cats) {&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&amp;nbsp; &amp;nbsp;for(Cat cat : cats) {&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; if(cat.getName().equals(name)) {&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;return cat;&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; }&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&amp;nbsp; &amp;nbsp;}&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&amp;nbsp; &amp;nbsp;return null;&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;}&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;Method signature shows that &amp;nbsp;this method may not return a value but a null reference .&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;a style=&quot;margin: 0px; padding: 0px; border: 0px; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: inherit; line-height: 22px; vertical-align: baseline; color: #a41600; text-decoration: none; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; name=&quot;more&quot;&gt;&lt;/a&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;Cat;Cat; cats = asList(new Cat(&quot;lion&quot;),&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; new Cat(&quot;snow leopard&quot;),&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; new Cat(&quot;tiger&quot;));&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;Cat found = find(&quot;mountain lion&quot;, cats);&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;//some code in between and much later on (or possibly somewhere else)...&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;String name = found.getName(); //uh oh!&lt;/span&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;br style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; /&gt;&lt;span style=&quot;color: #2f2e2e; font-family: Arial, Tahoma, Verdana; font-size: 18px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;&quot;&gt;The question we face is : Will optional helps &amp;nbsp;us to get rid of null pointer exceptions or references . then it is sadly NO !!&lt;/span&gt;&lt;/p&gt;', './assets/uploads/blog/7c48a56274287525ed22629ec580f047.jpg', './assets/uploads/blog/thumb/7c48a56274287525ed22629ec580f047.jpg', 2, 1, '', '2015-07-31 03:20:19', '2015-07-31 06:55:25');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_blog_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `comment_blog_id_` (`comment_blog_id`),
  KEY `comment_approved_date` (`comment_approved`,`comment_date`),
  KEY `comment_parent_id` (`comment_parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment_blog_id`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent_id`, `user_id`) VALUES
(2, 4, 'nisha sharma', 'nisha@gmail.com', 'http://localhost/projectiya/blog/blog_detail/4', '127.0.0.1', '2015-07-31 12:05:56', '0000-00-00 00:00:00', 'hello i am nisha this is my new comment for php blog', 0, '1', '', '', 0, 5),
(3, 3, 'nisha sharma', 'nisha@gmail.com', 'http://localhost/projectiya/blog/blog_detail/3', '127.0.0.1', '2015-07-31 12:51:15', '0000-00-00 00:00:00', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, '0', '', '', 0, 5),
(4, 4, 'uygadsjbcm', 'dsajyghnmkj@gmail.com', 'http://localhost/projectiya/blog/blog_detail/java-8-new-features-lambda-expressions-optional-class-defender-methods-with-examples', '127.0.0.1', '2015-07-31 01:15:39', '0000-00-00 00:00:00', 'cshgjknm,dsa hnjkscn kj xuhxjkasjch ubiuhasn,cmha khush jsdkj', 0, '1', '', '', 0, 5),
(5, 3, 'nisha sharma', 'nisha@gmail.com', 'http://localhost/projectiya/blog/blog_detail/what-you-should-do-before-pushing-php-code-to-your-production-git-repository', '127.0.0.1', '2015-08-03 09:28:24', '0000-00-00 00:00:00', 'this is tesykljs kljnmlkdsa uiknmlkdsj buoilkj', 0, '1', '', '', 0, 5),
(6, 4, 'Karan', 'shah.karan01@gmail.com', 'http://www.projectiya.com/blog/blog_detail/java-8-new-features-lambda-expressions-optional-class-defender-methods-with-examples', '1.22.70.173', '2015-08-27 12:02:47', '0000-00-00 00:00:00', 'Very good blog always do posing..!', 0, '0', '', '', 0, 8),
(7, 4, 'Ankit  Patidar', 'ankkuboss1@gmail.com', 'http://localhost/projectiya/blog/blog_detail/java-8-new-features-lambda-expressions-optional-class-defender-methods-with-examples', '::1', '2015-10-15 04:09:47', '0000-00-00 00:00:00', 'hgrr', 0, '1', '', '', 0, 22),
(8, 4, 'Ankit  Patidar', 'ankkuboss1@gmail.com', 'http://localhost/projectiya/blog/blog_detail/java-8-new-features-lambda-expressions-optional-class-defender-methods-with-examples', '::1', '2015-10-15 04:10:02', '0000-00-00 00:00:00', 'hi man', 0, '1', '', '', 0, 22),
(9, 4, 'Ankit  Patidar', 'ankkuboss1@gmail.com', 'http://localhost/projectiya/blog/blog_detail/java-8-new-features-lambda-expressions-optional-class-defender-methods-with-examples', '::1', '2015-10-15 04:12:39', '0000-00-00 00:00:00', 'ha bhia', 0, '1', '', '', 0, 22);

-- --------------------------------------------------------

--
-- Table structure for table `common_setting`
--

DROP TABLE IF EXISTS `common_setting`;
CREATE TABLE IF NOT EXISTS `common_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_type` tinyint(1) NOT NULL COMMENT '1=project_count,2=active user count',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `common_setting`
--

INSERT INTO `common_setting` (`id`, `attribute_type`, `status`, `last_updated`) VALUES
(1, 1, 0, '2015-10-23 14:37:17'),
(2, 2, 1, '2015-10-23 14:36:52');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE IF NOT EXISTS `contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `message` text NOT NULL,
  `reply` text NOT NULL,
  `created` date NOT NULL,
  `user_ip` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `firstname`, `lastname`, `email`, `mobile`, `message`, `reply`, `created`, `user_ip`, `status`) VALUES
(1, 'test', 'tester1', 'ritesh@gmail.com', '', 'main quetion???', 'this is the test reply for perticular question this is the test reply for perticular question this is the test reply for perticular question this is the test reply for perticular question this is the test reply for perticular question ', '2014-03-10', '::1', 1),
(2, 'ritesh', 'karpenter', 'ritesh@gmail.com', '', 'test description on test id...', 'okk', '2015-08-31', '::1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course`, `description`, `slug`, `status`, `created_date`) VALUES
(1, 'MCA', 'this is master of hkhjdfs ukhjkf kkjhnkjds hujk fijds', 'mca', 1, '2015-07-19 12:43:21'),
(2, 'BE', 'bachelor of engineering', 'be', 1, '2015-09-06 06:19:21'),
(3, 'BCA', 'bachelor of computer application', 'bca', 1, '2015-09-06 06:20:42'),
(6, 'Bsc IT', 'Bsc IT', 'bsc-it', 1, '2015-09-08 01:39:44'),
(7, 'Msc IT', 'Msc IT', 'msc-it', 1, '2015-09-08 01:40:01'),
(8, 'ME', 'ME', 'me', 1, '2015-09-08 01:41:39'),
(9, 'BTech', 'BTech', 'btech', 1, '2015-09-08 01:42:13'),
(10, 'DOEACC', 'DOEACC', 'doeacc', 1, '2015-09-08 01:52:26'),
(11, 'MBA - IT', 'MBA - IT', 'mba-it', 1, '2015-09-08 01:52:39'),
(12, 'PGDIT', 'PGDIT', 'pgdit', 1, '2015-09-08 01:52:54'),
(13, 'TYBsc', 'TYBsc', 'tybsc', 1, '2015-09-08 01:53:11'),
(14, 'Diploma', 'Diploma', 'diploma', 1, '2015-09-08 01:53:29'),
(15, 'Polytechnic', 'Polytechnic', 'polytechnic', 1, '2015-09-08 01:53:46');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(150) NOT NULL,
  `template_subject` text NOT NULL,
  `template_body` text NOT NULL,
  `template_subject_admin` varchar(255) NOT NULL,
  `template_body_admin` text NOT NULL,
  `template_created` datetime NOT NULL,
  `template_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `template_name`, `template_subject`, `template_body`, `template_subject_admin`, `template_body_admin`, `template_created`, `template_updated`) VALUES
(1, 'Change Password', 'Change your user password', '<p>Hello {fullname} Your user password successfully changed. please click on below link: {activation_key}</p>', '', '', '2014-06-11 12:48:06', '2015-07-12 05:54:05'),
(2, 'Purchase summary', 'Purchase order details On projectiya.com', '<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Hello {full_name},</p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Congratulation !!!. Your order have been placed successfully. You can check to your order detail below</p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">--</p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><strong>Order Id:<span class="Apple-converted-space">&nbsp;</span></strong>{order_id}<span class="Apple-converted-space">&nbsp;</span><br /><strong>Total Amount:</strong><span class="Apple-converted-space">&nbsp;</span>{total_amount}<span class="Apple-converted-space">&nbsp;</span><br /><strong>Download code URL:&nbsp;</strong>&nbsp;{download_url}<span class="Apple-converted-space">&nbsp;</span><br /><br /></p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Thanks to deal with us projectiya<strong>.com</strong></p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">{site_url}</p>', 'Order placed successfully on projectiya.com', '<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Hello admin ,</p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">One order have been placed successfully on projectiya.com . You can check order detail below :-</p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><strong>User Name</strong>: {full_name}</p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><strong>User email</strong><span class="Apple-converted-space">&nbsp;</span>:{email}</p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><strong>User phone</strong><span class="Apple-converted-space">&nbsp;</span>: {user_phone}</p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Project Title : {project_title}&nbsp;</p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Project Price: {project_price}</p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Thanks</p>\n<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">{site_url}</p>', '2014-06-11 03:36:56', '2015-08-26 01:32:10'),
(3, 'User Registration', 'Welcome, {full_name}!', '<table class="body-wrap" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">\n<tbody>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">&nbsp;</td>\n<td class="container" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top" width="600">\n<div class="content" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">\n<table class="main" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#fff">\n<tbody>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="alert alert-warning" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 20px; vertical-align: top; color: #fff; font-weight: bold; text-align: center; border-radius: 5px 5px 0 0; background-color: #607d8b; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#FF9F00">\n<p>Welcome, &nbsp;{full_name} in Projectiya.com</p>\n</td>\n</tr>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="content-wrap" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">\n<table style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;" width="100%" cellspacing="0" cellpadding="0">\n<tbody>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="content-block" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">\n<p style="text-align: left;">Hello&nbsp;{full_name},<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Please confirm your email address by clicking the link below.</p>\n</td>\n</tr>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="content-block" style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0px; padding: 0px 0px 20px; text-align: justify;" valign="top">We are welcome to you in the world of project. We are projectiya.com. we &nbsp;have large collection of acedamic projects. here &nbsp;you can buy &amp; sell your acedamic project.We may need to send you critical information about our service and it is important that we have an accurate email address.</td>\n</tr>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="content-block" style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0px; padding: 0px 0px 20px; text-align: center;" valign="top"><a class="btn-primary" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #fff; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #607d8b; margin: 0; border-color: #607D8B; border-style: solid; border-width: 10px 20px;" href="{activation_key}">ACTIVATE &nbsp;MY aCCOUNT</a></td>\n</tr>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="content-block" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">\n<p>Thanks You,<br />{site_name}.</p>\n<p>Follow us on facebook <a href="https://www.facebook.com/projectiya?ref=hl">@projectiya</a>.</p>\n</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody>\n</table>\n<div class="footer" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">\n<table style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;" width="100%">\n<tbody>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="aligncenter content-block" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">Powered by: CRUD Technology</td>\n</tr>\n</tbody>\n</table>\n</div>\n</div>\n</td>\n<td style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">&nbsp;</td>\n</tr>\n</tbody>\n</table>', '', '', '2014-06-11 06:51:58', '2015-09-08 01:30:31'),
(4, 'Customer Email', 'Email to Customer', '<p><strong>Hello</strong> {full_name},</p>\n<p>{message}</p>\n<p><strong>Thanks</strong></p>\n<p><strong>Derma Cessity Gold</strong></p>\n<p>&nbsp;</p>', '', '', '2014-10-07 01:18:18', '2015-05-18 02:16:26'),
(5, 'change Trainer password', 'Change your Trainer password form admin', '<table style="margin-left: 200px;" width="400px;">\n<thead>\n<tr><th colspan="2" width="100%">\n<h2>Change Password Notification</h2>\n</th></tr>\n</thead>\n<tbody>\n<tr>\n<td>\n<p>Hello {fullname},</p>\n</td>\n<td class="content" width="121"> </td>\n</tr>\n<tr>\n<td class="content" colspan="2">\n<p align="justify">This email contains instructions for sending your Account Security Password.<br /> Your password has been changed form the Admin</p>\n<br /> <strong>Your New Password is : {password} </strong></td>\n</tr>\n<tr>\n<td class="content" colspan="2">\n<p>Sincerely,</p>\n<p>Admin, Itrainer.com<br />{date}</p>\n<p>All Right Reserved</p>\n</td>\n</tr>\n</tbody>\n</table>\n<p> </p>', '', '', '2014-10-07 03:55:23', '2014-12-18 01:51:41'),
(6, 'Support email template', 'Admin Reply from Itrainer', '<p>hello {user_full_name},</p>\n<p>Subject :{subject }</p>\n<p>message:{message}</p>\n<p>Thanking you for Contact Us </p>\n<p>thanks</p>\n<p>Itrainer.com</p>\n<p><strong> </strong></p>', 'Itrainer support  message form user', '<table style="height: 222px;" width="500">\n<thead>\n<tr><th>Support Message</th></tr>\n</thead>\n<tbody>\n<tr>\n<td>\n<p>hello Admin,</p>\n<p>{user_full_name} have some query:</p>\n<p>Subject :{subject }</p>\n<p>message:{message}</p>\n<p>thanks</p>\n<p>Itrainer.com</p>\n<p> </p>\n</td>\n</tr>\n</tbody>\n</table>\n<p> </p>', '2014-10-28 11:00:21', '2014-12-18 01:50:40'),
(7, 'User account create', 'Welcome, {full_name}!', '<p>Welcome to derma cessity&nbsp;We have created your account. You can login to your account by given credential&nbsp;:</p>\n<p>user id: {email}</p>\n<p>password:{pass}</p>\n<p>&nbsp;</p>\n<p>--</p>\n<p>Thanks</p>\n<p>Dermacessity Gold</p>', '', '', '2014-12-19 05:28:33', '2015-05-15 10:48:13'),
(8, 'Forgot Password', '{site_name}', '<table class="body-wrap" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">\n<tbody>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">&nbsp;</td>\n<td class="container" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top" width="600">\n<div class="content" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">\n<table class="main" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#fff">\n<tbody>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="alert alert-warning" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 20px; vertical-align: top; color: #fff; font-weight: bold; text-align: center; border-radius: 5px 5px 0 0; background-color: #607d8b; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#FF9F00">\n<p>Projectiya.com</p>\n</td>\n</tr>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="content-wrap" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">\n<table style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;" width="100%" cellspacing="0" cellpadding="0">\n<tbody>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="content-block" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">\n<p style="text-align: left;">Hello&nbsp;{full_name},<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; If you forget your &nbsp;username or password, reset your account credentials to regain access to your account.</p>\n</td>\n</tr>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="content-block" style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0px; padding: 0px 0px 20px; text-align: justify;" valign="top">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;Below, you''ll click on link and reset your password via email associated with your account. Click on following link:</td>\n</tr>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="content-block" style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0px; padding: 0px 0px 20px; text-align: center;" valign="top"><a class="btn-primary" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #fff; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #607d8b; margin: 0; border-color: #607D8B; border-style: solid; border-width: 10px 20px;" href="{reset_url}">RESET &nbsp;PASSWORD</a></td>\n</tr>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="content-block" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">\n<p>Thanks You<br />Projectiya.com.</p>\n<p>Follow us on facebook <a href="https://www.facebook.com/projectiya?ref=hl">@projectiya</a>.</p>\n</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody>\n</table>\n<div class="footer" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">\n<table style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;" width="100%">\n<tbody>\n<tr style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">\n<td class="aligncenter content-block" style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">Powered by: CRUD Technology</td>\n</tr>\n</tbody>\n</table>\n</div>\n</div>\n</td>\n<td style="font-family: ''Helvetica Neue'',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">&nbsp;</td>\n</tr>\n</tbody>\n</table>', '', '', '2014-12-20 02:43:51', '2015-09-08 01:29:32');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `category`, `question`, `answer`, `status`, `created`) VALUES
(1, 0, 'DSDSADCSAHGXHJSKCAKSXHCNASJM', 'DSADDSAHDASIUHASCKJBASCA', 0, '2015-07-13 09:42:00');

-- --------------------------------------------------------

--
-- Table structure for table `favorite_project`
--

DROP TABLE IF EXISTS `favorite_project`;
CREATE TABLE IF NOT EXISTS `favorite_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(5) NOT NULL,
  `favorite` int(5) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `favorite_project`
--

INSERT INTO `favorite_project` (`id`, `project_id`, `user_id`, `status`, `favorite`, `created`) VALUES
(1, 9, 5, 1, 1, '2015-08-03 09:46:52'),
(2, 2, 8, 1, 1, '2015-08-27 12:08:35'),
(3, 3, 8, 1, 1, '2015-09-07 02:19:43');

-- --------------------------------------------------------

--
-- Table structure for table `image_setting`
--

DROP TABLE IF EXISTS `image_setting`;
CREATE TABLE IF NOT EXISTS `image_setting` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `image_type` varchar(1) NOT NULL DEFAULT '2' COMMENT '1=popup image,2=slider images',
  `path` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=enable,0=disable',
  `description` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `image_setting`
--

INSERT INTO `image_setting` (`id`, `image_type`, `path`, `status`, `description`) VALUES
(24, '2', './assets/backend/images/slider_images/2385cb5c4cd0091d2e61b3eb28eef1f3.jpg', 1, '<p>gfdfg</p>'),
(25, '1', './assets/backend/images/popup_images/b4a071265a986b06076dd1c8603b2f86.jpg', 1, '<p>rererw</p>'),
(26, '2', './assets/backend/images/slider_images/e41bebaf1721036074a79e0811ca2843.jpg', 1, '<p>yrfewfsdf</p>');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
CREATE TABLE IF NOT EXISTS `options` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes',
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `option_name`, `option_value`, `autoload`, `status`) VALUES
(1, 'CALL_US', '+91 111-111-1111', 'yes', 0),
(2, 'FACEBOOK_URL', 'http://www.facebook.com/', 'yes', 1),
(3, 'FACEBOOK_APPID', 'test.chapter247@gmail.com', 'yes', 0),
(4, 'FACEBOOK_SECRET', 'http://dribbble.com/12', 'yes', 0),
(5, 'TWITTER_URL', 'http://www.twitter.com/', 'yes', 1),
(6, 'TWITTER_APPID', '123456', 'yes', 0),
(7, 'TWITTER_SECRET', '+1 000-000-0000', 'yes', 0),
(8, 'GOOGLEPLUS_URL', 'ritesh@gmail.com', 'yes', 1),
(9, 'GOOGLEPLUS_APPID', 'www.pluggedin.com', 'yes', 0),
(10, 'GOOGLEPLUS_SECRET', 'https://www.pinterest.com/', 'yes', 0),
(11, 'HOME_PAGE_VIDEO', 'http://dribbble.com/', 'yes', 0),
(12, 'LOCATION_MAP', 'https://vimeo.com/', 'yes', 0),
(13, 'ADDRESS', '193 E. Gore Creek DriveVail, CO 81657', 'yes', 1),
(14, 'ZIP_CODE', '12345', 'yes', 1),
(15, 'CONTACT_US_PHONE', '+91 8103122999', 'yes', 1),
(16, 'EMAIL', 'info@sweetbasil-vail.comdsa', 'yes', 1),
(17, 'WEBSITE', 'www.projectiya.com', 'yes', 1),
(18, 'PINTEREST_URL', 'https://www.pinterest.com/dsa', 'yes', 1),
(19, 'DRIBBLE_URL', 'http://dribbble.com/sad', 'yes', 1),
(20, 'VIMEO_URL', 'https://vimeo.com/dsd', 'yes', 1),
(21, 'TRAINER_FEE', '100', 'yes', 1),
(22, 'CONTACT_US_FAX', '970-476-0137', 'yes', 1),
(23, 'LINKEDIN_URL', 'www.linkedin.com', 'yes', 1),
(24, 'COPY_RIGHT', '2015  ecommerce.  All Rights Reserved', 'yes', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `buyer_id` varchar(100) NOT NULL,
  `project_id` int(11) NOT NULL,
  `unique_id` varchar(20) NOT NULL,
  `token_id` varchar(255) NOT NULL,
  `project_title` varchar(255) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `total_amount` float(10,2) NOT NULL,
  `download_count` int(10) NOT NULL,
  `coupon_discount` float(8,2) NOT NULL,
  `coupon_code_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `receive_email` int(1) NOT NULL DEFAULT '0' COMMENT '0= No, 1=Yes',
  `transaction_id` varchar(255) NOT NULL,
  `payment_status` varchar(25) NOT NULL,
  `order_status` varchar(25) NOT NULL COMMENT '1 = Pending, 2 = In queue, 3 = In Progress, 4 = Shipping, 5 = Shipped,6=approve,7=decline,8=block',
  `other_payment_info` text NOT NULL,
  `pay_pal_payment` varchar(255) NOT NULL,
  `created` date NOT NULL,
  `is_cancelled` int(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=cancelled',
  `cancelled_info` text NOT NULL,
  `cancelled_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `buyer_id`, `project_id`, `unique_id`, `token_id`, `project_title`, `order_id`, `total_amount`, `download_count`, `coupon_discount`, `coupon_code_id`, `coupon_code`, `payment_method`, `user_name`, `user_email`, `receive_email`, `transaction_id`, `payment_status`, `order_status`, `other_payment_info`, `pay_pal_payment`, `created`, `is_cancelled`, `cancelled_info`, `cancelled_date`) VALUES
(1, 6, '34161120', 2, '0', '11rB7QNPhFzxHgZm', 'Fake Product Review Monitoring And Removal For Genuine Online Product Reviews Using Opinion Mining', '7wySHhjl', 98568544.00, 0, 0.00, 0, '', '', 'ritesh tester', 'developer.ritesh@yahoo.in', 0, '', '', '1', '', '', '2015-08-26', 0, '', '0000-00-00 00:00:00'),
(2, 6, '7673853', 2, 'fyugudsh856', 'OdyV2muA8OjbPa52', 'Fake Product Review Monitoring And Removal For Genuine Online Product Reviews Using Opinion Mining', '0zj2668e', 98568544.00, 3, 0.00, 0, '', '', 'ritesh tester', 'developer.ritesh@yahoo.in', 0, '', '', '1', '', '', '2015-08-26', 0, '', '0000-00-00 00:00:00'),
(3, 6, '99684841', 3, '0', 'YgdeEYqBWZUCAouq', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod   tempor incididunt ut labore et dolore magna aliqua', 'L6WqtWf2', 5545356.00, 0, 0.00, 0, '', '', 'ritesh tester', 'developer.ritesh@yahoo.in', 0, '', '', '1', '', '', '2015-08-26', 0, '', '0000-00-00 00:00:00'),
(4, 6, '5361894', 2, 'fyugudsh856', 'Y9rseyxNO0oAYR54', 'Fake Product Review Monitoring And Removal For Genuine Online Product Reviews Using Opinion Mining', 'dBaZH188', 98568544.00, 3, 0.00, 0, '', '', 'ritesh tester', 'developer.ritesh@yahoo.in', 0, '', '', '1', '', '', '2015-08-31', 0, '', '0000-00-00 00:00:00'),
(5, 8, '80308403', 3, 'cszhj465zx', '99eHDWxqQgobGh7B', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod   tempor incididunt ut labore et dolore magna aliqua', '4PjA3VQv', 5545356.00, 1, 0.00, 0, '', '', 'karan shah', 'shah.karan01@gmail.com', 0, '', '', '1', '', '', '2015-08-31', 0, '', '0000-00-00 00:00:00'),
(6, 0, '65305526', 2, 'fyugudsh856', 'oKVr1VB6AtRFgoN8', 'Loan Management System in java-j2ee', 'IHj41UHi', 1500.00, 3, 0.00, 0, '', '', 'karan shah', 'shah.karan01@gmail.com', 0, '', '', '1', '', '', '2015-09-07', 0, '', '0000-00-00 00:00:00'),
(7, 33, '93029854', 2, 'fyugudsh856', 'k4v8EbBRr4TA7ixN', 'Loan Management System in java-j2ee', 'X8tZifEb', 1500.00, 3, 0.00, 0, '', '', 'karan shah', 'shah.karan01@gmail.com', 0, '', '', '1', '', '', '2015-09-23', 0, '', '0000-00-00 00:00:00'),
(8, 15, '2900982', 3, 'cszhj465zx', 'dLgmHQN5L8kf1IPm', 'Online Examination Project in JSP-J2EE  with source code', 'kLblKtb2', 1500.00, 1, 0.00, 0, '', '', 'karan shah', 'shah.karan01@gmail.com', 0, '', '', '1', '', '', '2015-09-23', 0, '', '0000-00-00 00:00:00'),
(9, 1, '403993715513574924', 0, 'fyugudsh856', '7jy8n6DRvymPvxod', 'Loan Management System in java-j2ee', 'CvxAhCi9', 1500.00, 0, 0.00, 0, '', '', 'Ritesh tester', 'developer.ritesh@yahoo.in', 0, '', '', '1', '', '', '2015-09-29', 0, '', '0000-00-00 00:00:00'),
(10, 20, '403993715513575040', 0, 'fyugudsh856', 'NCZ41vYJH7pjHS7w', 'Loan Management System in java-j2ee', '6VfjOs3A', 1500.00, 0, 0.00, 0, '', '', 'ritesh tester', 'developer.ritesh@yahoo.in', 0, '', '', '1', '', '', '2015-09-29', 0, '', '0000-00-00 00:00:00'),
(11, 32, '403993715513575080', 0, 'cszhj465zx', '60HgMR57PhR69MjC', 'Online Examination Project in JSP-J2EE  with source code', 'DgAdsGgL', 1500.00, 1, 0.00, 0, '', '', 'ritesh tesr', 'developer.ritesh@yahoo.in', 0, '', '', '1', '', '', '2015-09-29', 0, '', '0000-00-00 00:00:00'),
(12, 15, '403993715513575238', 2, 'cszhj465zx', 'KjZd26SJVwRJjbD7', 'Online Examination Project in JSP-J2EE  with source code', 'EFvRrGFo', 1500.00, 1, 0.00, 0, '', '', 'karan shah', 'shah.karan01@gmail.com', 0, '', '', '1', '', '', '2015-09-29', 0, '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `limit` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `limit`, `amount`) VALUES
(1, 'Free Users', 2, 0),
(2, 'Silver', 10, 500);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_title` text NOT NULL,
  `post_slug` varchar(200) NOT NULL DEFAULT '',
  `post_content` longtext NOT NULL,
  `post_order` int(11) NOT NULL DEFAULT '0',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `post_type` varchar(20) NOT NULL DEFAULT 'post' COMMENT 'post,page',
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `page_type` varchar(255) DEFAULT NULL,
  `post_category` int(11) NOT NULL COMMENT '0=none,1=about us',
  `post_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `post_slug` (`post_slug`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_created`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `post_author`, `post_parent`, `post_title`, `post_slug`, `post_content`, `post_order`, `post_mime_type`, `post_type`, `post_status`, `page_type`, `post_category`, `post_created`, `post_updated`) VALUES
(1, 0, 0, 'about us', 'about-us', '<p class="sub-title">In the action of website creation the &ldquo;About Us&rdquo; page is unfortunately ignored which is one amongst the most significant and worthy page in a website. As the &ldquo;About Us&rdquo; page plays a very earnest role in a website. This page in itself is sufficient to arbitrate whether you are loosing or achieving a valuable customer.</p>\r\n<p>However, any new customer who seeks to avail your products or services will go through this page in order to appraise the credibility as well as the reliability of the company. Moreover, it&rsquo;s not only fetch the first impression of the company along with a face of business but also allows the visitors to be acquainted with the company&rsquo;s profile including aims, objectives, services, achievements and other worthy information. The most crucial thing is about to make the website solid-gold.</p>\r\n<p>After all, some aspects must be kept in mind in the midst of creating a website by web designers and web developers towards the enhancement of the look for &ldquo;About Us&rdquo; page, i.e.</p>\r\n<p><em><strong>Uniqueness</strong></em>: The &ldquo;About Us&rdquo; page must be something unique in such a way that it strike the visitors notice and eye-candy to hold visitors with the page. As most of the visitors do remember the entire website on account of about us page by correlating the name, history and operations of the company.</p>\r\n<p><em><strong>Relevance</strong></em>: The &ldquo;about us&rdquo; page directly communicates to the people with company&rsquo;s business, performance and anything relevant to its operation. Therefore, all the while designing &ldquo;about us&rdquo; page the text, colors and images must be relevant to the business.</p>\r\n<p><em><strong>Photo and Images</strong></em>: Human minds do grasp images in a very distinct manner with accompanying information about the product or business as images have an instant vibe and leaves a long lasting impression. So, &ldquo;about us&rdquo; page should have an image. Including the photograph of the company&rsquo;s owner or location or anything that resembles with the company&rsquo;s business or mission would be perfectly admissible.</p>\r\n<p><em><strong>Simplicity</strong></em>: The &ldquo;about us&rdquo; page which should be capable to arrest the attention of the visitors and drag them in itself. Moreover, it should facilitate the visitors to go through the entire &ldquo;about us&rdquo; page without putting even a little bit of strain on their eyes. So, The &ldquo;about us&rdquo; page should be simple, attractive and eye-slippery. However, this can be achieved by creating a central focus on the page including one or two elements as well as the text or images used must be balanced and maintain contrast.</p>\r\n<p><em><strong>Content</strong></em>: The content sports a very earnest role in &ldquo;about us&rdquo; page that directly face the audience and convey the company&rsquo;s information to pull them towards the company&rsquo;s business as well as convince them excellently. Therefore, content being implemented in the &ldquo;about us&rdquo; page must be engaging and informative rather than boring and run-of-the-mill.</p>\r\n<p>Besides all of these, I have handpicked some samples of &ldquo;about us&rdquo; page and presenting here in <strong>Free Sample of &lsquo;About Us&rsquo; page in Website for Web Designers</strong>. Go through these one by one and use to go with which allured you as well as your client.</p>', 0, '', 'page', 'publish', NULL, 0, '2014-02-08 11:15:03', '2015-08-04 05:48:02'),
(2, 0, 0, 'jdfs jjdfs hdfsjh jh dfsjhjhdfs hdfs hdfsjhhdfs kjdfs', 'jdfs-jjdfs-hdfsjh-jh-dfsjhjhdfs-hdfs-hdfsjhhdfs-kjdfs', '&lt;p&gt;fdlkjk lkfjds lkjdfshj dfsjjkdfs jdfsjhjjhjdsfh dfsjhkjdfs hjhdfs&lt;/p&gt;', 0, '', 'post', 'publish', NULL, 1, '2014-02-08 11:36:15', '2014-02-08 11:36:22'),
(3, 0, 0, 'contact us', 'contact-us', '&lt;p class=&quot;sub-title&quot;&gt;In the action of website creation the &amp;ldquo;About Us&amp;rdquo; page is unfortunately ignored which is one amongst the most significant and worthy page in a website. As the &amp;ldquo;About Us&amp;rdquo; page plays a very earnest role in a website. This page in itself is sufficient to arbitrate whether you are loosing or achieving a valuable customer.&lt;/p&gt;\r\n&lt;p&gt;However, any new customer who seeks to avail your products or services will go through this page in order to appraise the credibility as well as the reliability of the company. Moreover, it&amp;rsquo;s not only fetch the first impression of the company along with a face of business but also allows the visitors to be acquainted with the company&amp;rsquo;s profile including aims, objectives, services, achievements and other worthy information. The most crucial thing is about to make the website solid-gold.&lt;/p&gt;\r\n&lt;p&gt;After all, some aspects must be kept in mind in the midst of creating a website by web designers and web developers towards the enhancement of the look for &amp;ldquo;About Us&amp;rdquo; page, i.e.&lt;/p&gt;\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Uniqueness&lt;/strong&gt;&lt;/em&gt;: The &amp;ldquo;About Us&amp;rdquo; page must be something unique in such a way that it strike the visitors notice and eye-candy to hold visitors with the page. As most of the visitors do remember the entire website on account of about us page by correlating the name, history and operations of the company.&lt;/p&gt;\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Relevance&lt;/strong&gt;&lt;/em&gt;: The &amp;ldquo;about us&amp;rdquo; page directly communicates to the people with company&amp;rsquo;s business, performance and anything relevant to its operation. Therefore, all the while designing &amp;ldquo;about us&amp;rdquo; page the text, colors and images must be relevant to the business.&lt;/p&gt;\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Photo and Images&lt;/strong&gt;&lt;/em&gt;: Human minds do grasp images in a very distinct manner with accompanying information about the product or business as images have an instant vibe and leaves a long lasting impression. So, &amp;ldquo;about us&amp;rdquo; page should have an image. Including the photograph of the company&amp;rsquo;s owner or location or anything that resembles with the company&amp;rsquo;s business or mission would be perfectly admissible.&lt;/p&gt;\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Simplicity&lt;/strong&gt;&lt;/em&gt;: The &amp;ldquo;about us&amp;rdquo; page which should be capable to arrest the attention of the visitors and drag them in itself. Moreover, it should facilitate the visitors to go through the entire &amp;ldquo;about us&amp;rdquo; page without putting even a little bit of strain on their eyes. So, The &amp;ldquo;about us&amp;rdquo; page should be simple, attractive and eye-slippery. However, this can be achieved by creating a central focus on the page including one or two elements as well as the text or images used must be balanced and maintain contrast.&lt;/p&gt;\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;Content&lt;/strong&gt;&lt;/em&gt;: The content sports a very earnest role in &amp;ldquo;about us&amp;rdquo; page that directly face the audience and convey the company&amp;rsquo;s information to pull them towards the company&amp;rsquo;s business as well as convince them excellently. Therefore, content being implemented in the &amp;ldquo;about us&amp;rdquo; page must be engaging and informative rather than boring and run-of-the-mill.&lt;/p&gt;\r\n&lt;p&gt;Besides all of these, I have handpicked some samples of &amp;ldquo;about us&amp;rdquo; page and presenting here in&amp;nbsp;&lt;strong&gt;Free Sample of &amp;lsquo;About Us&amp;rsquo; page in Website for Web Designers&lt;/strong&gt;. Go through these one by one and use to go with which allured you as well as your client.&lt;/p&gt;', 0, '', 'page', 'publish', NULL, 0, '2014-02-15 08:38:31', '2015-08-04 06:22:33'),
(4, 0, 0, 'wfdrqew', 'wfdrqew', '&lt;p&gt;&amp;nbsp;efedf&lt;/p&gt;', 0, '', 'post', 'publish', NULL, 0, '2014-02-15 08:39:07', '0000-00-00 00:00:00'),
(5, 0, 0, 'new contact1', 'new-contact1', '&lt;p&gt;fsdd fds fd s1&lt;/p&gt;', 0, '', 'page', 'publish', NULL, 0, '0000-00-00 00:00:00', '2015-10-11 10:37:36'),
(6, 0, 0, 'demo page', 'demo-page', '&lt;p&gt;&lt;span style=&quot;color: #0000ff;&quot;&gt;&lt;em&gt;&lt;strong&gt;Amdin dummy description&lt;/strong&gt;&lt;/em&gt;&lt;/span&gt;&lt;/p&gt;', 0, '', 'page', 'publish', NULL, 0, '0000-00-00 00:00:00', '2015-10-20 11:04:16'),
(7, 0, 0, 'demo', 'demo', '&lt;p&gt;sdjflskdjlkds&lt;/p&gt;', 0, '', 'page', 'unpublish', 'resource', 0, '0000-00-00 00:00:00', '2015-10-24 03:36:01');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `technology` text NOT NULL,
  `backend_technology` text NOT NULL,
  `form_pages` int(11) NOT NULL,
  `no_of_tables` int(11) NOT NULL,
  `related_projects` text NOT NULL,
  `main_file` varchar(255) NOT NULL,
  `main_image` varchar(255) NOT NULL,
  `thumb_image` varchar(255) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `video_url` varchar(255) NOT NULL,
  `course_type` text NOT NULL,
  `available_item` int(5) NOT NULL,
  `discount_item` int(5) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `price_type` int(5) NOT NULL,
  `price` float(10,2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=unpublished, 1=published',
  `doc_file` varchar(255) NOT NULL,
  `featured_project` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0 = No, 1 = Yes',
  `view_count` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `unique_id`, `user_id`, `slug`, `technology`, `backend_technology`, `form_pages`, `no_of_tables`, `related_projects`, `main_file`, `main_image`, `thumb_image`, `short_description`, `video_url`, `course_type`, `available_item`, `discount_item`, `meta_title`, `meta_keywords`, `meta_description`, `price_type`, `price`, `title`, `description`, `status`, `doc_file`, `featured_project`, `view_count`, `created`, `modified`) VALUES
(2, 'fyugudsh856', 32, 'loan-management-system-in-java-j2ee', '#3#,#6#,#8#,#9#', 'MySQL', 10, 5, '', './assets/uploads/projects/669ed7f37da5a04da9bffc826b65fda6.zip', './assets/uploads/project_files/93bdd656f1f66be0bdc3bb74e10aa259.jpg', './assets/uploads/project_files/thumb/93bdd656f1f66be0bdc3bb74e10aa259.jpg', 'Most of the bank out-sources pre-loan process to loan agencies to reduce the burden and let the agencies pickup the information from customers and verify it before it is being forwarded to the actual bank for approval of loan.', 'SADF', '#1#,#2#,#3#,#4#,#5#', 1, 0, '', '', '', 0, 1500.00, 'Loan Management System in java-j2ee', '&lt;div class=&quot;content-inner&quot;&gt;\n&lt;p&gt;Most of the bank out-sources pre-loan process to loan agencies to reduce the burden and let the agencies pickup the information from customers and verify it before it is being forwarded to the actual bank for approval of loan.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;Lending Tree is an interface which facilitates a customer to apply for a loan from on-line and to track the status from time-to-time along with aiding the loan approval agency to verify and accept/reject the customer file. Lending Tree is unique in such a way, it not only helps the customers but also the loan agency to check the pending, assign it to a departments, complete the formalities and procedures between the departments and&amp;nbsp; arrive at decisions to very fact in addition to providing a transparency system for every one.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;The customer can directly apply for a loan by selecting a bank and loan type from the list available. The application is received by loan agency who will have three departments- PickUp, Verifiaction and Legal. This system can be controlled by the administrator. First he will look at the application received and allot the application for a particular employee of pickup department. The employee will go and make a physical verification of the documents at the customers and receives the documents necessary for the loan. Then he logs into this system and forwards the application to the verification department which will verify the whereabouts of the person, his organization, his salary particulars etc. and then forwards the application with a status verified. Then application reaches the legal department. The legal department people will verify the builder details and when satisfied sends their report to the administrator.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;The administrator or final approving authority views both types of reports, Viz, the reports from verification department and the report legal department. This will help him to take a decision regarding whether to forward it to the bank or not. The same is communicated to the customer.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;The customer can at any time view the status of his application and can send any messages to the administrator and can get clarifications from him. Thus the lending tree s/w helps to simplify the loan system along with making the work easy.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;There are 5 types of users who can get immense benefits from system:&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;ul&gt;\n&lt;li&gt;The customer - seeking the loan and information related to banks and loans&lt;/li&gt;\n&lt;li&gt;The administrator of loan agency who will take track the decision of bank to approve or disapprove and also controls the overall system functionality&lt;/li&gt;\n&lt;li&gt;The PickUp department users who picks up the details and documents from customers&lt;/li&gt;\n&lt;li&gt;The verification department user who make a physical verification of the details submitted by the customer&lt;/li&gt;\n&lt;li&gt;The legal department user who verifies the legality of the documents of the builder and construction.&lt;/li&gt;\n&lt;/ul&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;Software Requirments:&lt;/strong&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;Operating System&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :Windows&lt;/p&gt;\n&lt;p&gt;Technologies&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :Java/J2ee(JDBC, Servlets, JSP)&lt;/p&gt;\n&lt;p&gt;Web Technologies&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :Html, JavaScript&lt;/p&gt;\n&lt;p&gt;Web Server&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :Tomcat&lt;/p&gt;\n&lt;p&gt;Database&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;:Oracle&lt;/p&gt;\n&lt;p&gt;Softwares&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :J2sdk6.0, Tomcat 5.0, Oracle 9i&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;Hardware Requirements:&lt;/strong&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;Hardware&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :Pentium based systems with a&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Minimum of p4&lt;/p&gt;\n&lt;p&gt;RAM&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;:256MB (min)&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; .&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;/div&gt;', 1, './assets/uploads/projects/71ddd57a0f5e361f2b8d23d8b5d612f3.doc', 1, 40, '2015-07-18 01:50:45', '2015-09-06 01:04:52'),
(3, 'cszhj465zx', 32, 'online-examination-project-in-jsp-j2ee-with-source-code', '#3#', 'MySQL', 15, 7, '', './assets/uploads/projects/b96825d9b827f9722d01909817a9ed78.zip', './assets/uploads/project_files/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', './assets/uploads/project_files/thumb/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', 'Online examination system project is a web application which is implemented in Servlet jsp platform. Online examination system Servlet jsp project tutorial and guide for developing code. Entity–relationship(er) diagrams,Data flow diagram(dfd),Sequence dia', 'SQvVIZIV6Lo', '#1#', 1, 0, '', '', '', 0, 1500.00, 'Online Examination Project in JSP-J2EE  with source code', '&lt;p&gt;Online Examination System&amp;nbsp;project could be a web portal which is developed or implemented in java domain or platform. This project is helpful for students to practice different mock examinations from this site. In current generation lots of the examinations like GRE, CAT, and MAT&amp;hellip;etc is conducted through online system. This project will help students to get practiced to online examination method by taking mock tests from this web portal. Online Examination System&amp;nbsp;portal is implemented in 2 modules student examination module and examination admin module. Admin module will add multiple courses under different branches so students can easily know about test details. Student examination module students ought to register with application and choose interested courses and participate in the online test.&lt;/p&gt;', 1, './assets/uploads/projects/7a480ca1320adde49e7eb2c567c4f278.pdf', 1, 18, '2015-08-08 03:20:36', '2015-09-06 20:54:46'),
(4, 'cszhj465zx', 5, 'project-1', '#3#', 'MySQL', 15, 7, '', './assets/uploads/projects/b96825d9b827f9722d01909817a9ed78.zip', './assets/uploads/project_files/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', './assets/uploads/project_files/thumb/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', 'Online examination system project is a web application which is implemented in Servlet jsp platform. Online examination system Servlet jsp project tutorial and guide for developing code. Entity–relationship(er) diagrams,Data flow diagram(dfd),Sequence dia', 'SQvVIZIV6Lo', '#1#', 1, 0, '', '', '', 0, 1500.00, 'project 1', '&lt;p&gt;Online Examination System&amp;nbsp;project could be a web portal which is developed or implemented in java domain or platform. This project is helpful for students to practice different mock examinations from this site. In current generation lots of the examinations like GRE, CAT, and MAT&amp;hellip;etc is conducted through online system. This project will help students to get practiced to online examination method by taking mock tests from this web portal. Online Examination System&amp;nbsp;portal is implemented in 2 modules student examination module and examination admin module. Admin module will add multiple courses under different branches so students can easily know about test details. Student examination module students ought to register with application and choose interested courses and participate in the online test.&lt;/p&gt;', 1, './assets/uploads/projects/7a480ca1320adde49e7eb2c567c4f278.pdf', 1, 12, '2015-08-08 03:20:36', '2015-09-06 20:54:46'),
(5, 'cszhj465zx', 5, 'project-2', '#3#', 'MySQL', 15, 7, '', './assets/uploads/projects/b96825d9b827f9722d01909817a9ed78.zip', './assets/uploads/project_files/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', './assets/uploads/project_files/thumb/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', 'Online examination system project is a web application which is implemented in Servlet jsp platform. Online examination system Servlet jsp project tutorial and guide for developing code. Entity–relationship(er) diagrams,Data flow diagram(dfd),Sequence dia', 'SQvVIZIV6Lo', '#1#', 1, 0, '', '', '', 0, 1500.00, 'project 2', '&lt;p&gt;Online Examination System&amp;nbsp;project could be a web portal which is developed or implemented in java domain or platform. This project is helpful for students to practice different mock examinations from this site. In current generation lots of the examinations like GRE, CAT, and MAT&amp;hellip;etc is conducted through online system. This project will help students to get practiced to online examination method by taking mock tests from this web portal. Online Examination System&amp;nbsp;portal is implemented in 2 modules student examination module and examination admin module. Admin module will add multiple courses under different branches so students can easily know about test details. Student examination module students ought to register with application and choose interested courses and participate in the online test.&lt;/p&gt;', 1, './assets/uploads/projects/7a480ca1320adde49e7eb2c567c4f278.pdf', 1, 12, '2015-08-08 03:20:36', '2015-09-06 20:54:46'),
(6, 'cszhj465zx', 5, 'project-3', '#3#', 'MySQL', 15, 7, '', './assets/uploads/projects/b96825d9b827f9722d01909817a9ed78.zip', './assets/uploads/project_files/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', './assets/uploads/project_files/thumb/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', 'Online examination system project is a web application which is implemented in Servlet jsp platform. Online examination system Servlet jsp project tutorial and guide for developing code. Entity–relationship(er) diagrams,Data flow diagram(dfd),Sequence dia', 'SQvVIZIV6Lo', '#1#', 1, 0, '', '', '', 0, 1500.00, 'project 3', '&lt;p&gt;Online Examination System&amp;nbsp;project could be a web portal which is developed or implemented in java domain or platform. This project is helpful for students to practice different mock examinations from this site. In current generation lots of the examinations like GRE, CAT, and MAT&amp;hellip;etc is conducted through online system. This project will help students to get practiced to online examination method by taking mock tests from this web portal. Online Examination System&amp;nbsp;portal is implemented in 2 modules student examination module and examination admin module. Admin module will add multiple courses under different branches so students can easily know about test details. Student examination module students ought to register with application and choose interested courses and participate in the online test.&lt;/p&gt;', 1, './assets/uploads/projects/7a480ca1320adde49e7eb2c567c4f278.pdf', 1, 12, '2015-08-08 03:20:36', '2015-09-06 20:54:46'),
(7, 'cszhj465zx', 5, 'project-4', '#3#', 'MySQL', 15, 7, '', './assets/uploads/projects/b96825d9b827f9722d01909817a9ed78.zip', './assets/uploads/project_files/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', './assets/uploads/project_files/thumb/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', 'Online examination system project is a web application which is implemented in Servlet jsp platform. Online examination system Servlet jsp project tutorial and guide for developing code. Entity–relationship(er) diagrams,Data flow diagram(dfd),Sequence dia', 'SQvVIZIV6Lo', '#1#', 1, 0, '', '', '', 0, 1500.00, 'project 4', '&lt;p&gt;Online Examination System&amp;nbsp;project could be a web portal which is developed or implemented in java domain or platform. This project is helpful for students to practice different mock examinations from this site. In current generation lots of the examinations like GRE, CAT, and MAT&amp;hellip;etc is conducted through online system. This project will help students to get practiced to online examination method by taking mock tests from this web portal. Online Examination System&amp;nbsp;portal is implemented in 2 modules student examination module and examination admin module. Admin module will add multiple courses under different branches so students can easily know about test details. Student examination module students ought to register with application and choose interested courses and participate in the online test.&lt;/p&gt;', 1, './assets/uploads/projects/7a480ca1320adde49e7eb2c567c4f278.pdf', 1, 12, '2015-08-08 03:20:36', '2015-09-06 20:54:46'),
(8, 'cszhj465zx', 15, 'project-5', '#3#', 'MySQL', 15, 7, '', './assets/uploads/projects/b96825d9b827f9722d01909817a9ed78.zip', './assets/uploads/project_files/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', './assets/uploads/project_files/thumb/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', 'Online examination system project is a web application which is implemented in Servlet jsp platform. Online examination system Servlet jsp project tutorial and guide for developing code. Entity–relationship(er) diagrams,Data flow diagram(dfd),Sequence dia', 'SQvVIZIV6Lo', '#1#', 1, 0, '', '', '', 0, 1500.00, 'project 5', '&lt;p&gt;Online Examination System&amp;nbsp;project could be a web portal which is developed or implemented in java domain or platform. This project is helpful for students to practice different mock examinations from this site. In current generation lots of the examinations like GRE, CAT, and MAT&amp;hellip;etc is conducted through online system. This project will help students to get practiced to online examination method by taking mock tests from this web portal. Online Examination System&amp;nbsp;portal is implemented in 2 modules student examination module and examination admin module. Admin module will add multiple courses under different branches so students can easily know about test details. Student examination module students ought to register with application and choose interested courses and participate in the online test.&lt;/p&gt;', 1, './assets/uploads/projects/7a480ca1320adde49e7eb2c567c4f278.pdf', 1, 12, '2015-08-08 03:20:36', '2015-09-06 20:54:46'),
(9, 'cszhj465zx', 15, 'project-6', '#3#', 'MySQL', 15, 7, '', './assets/uploads/projects/b96825d9b827f9722d01909817a9ed78.zip', './assets/uploads/project_files/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', './assets/uploads/project_files/thumb/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', 'Online examination system project is a web application which is implemented in Servlet jsp platform. Online examination system Servlet jsp project tutorial and guide for developing code. Entity–relationship(er) diagrams,Data flow diagram(dfd),Sequence dia', 'SQvVIZIV6Lo', '#1#', 1, 0, '', '', '', 0, 1500.00, 'project 6', '&lt;p&gt;Online Examination System&amp;nbsp;project could be a web portal which is developed or implemented in java domain or platform. This project is helpful for students to practice different mock examinations from this site. In current generation lots of the examinations like GRE, CAT, and MAT&amp;hellip;etc is conducted through online system. This project will help students to get practiced to online examination method by taking mock tests from this web portal. Online Examination System&amp;nbsp;portal is implemented in 2 modules student examination module and examination admin module. Admin module will add multiple courses under different branches so students can easily know about test details. Student examination module students ought to register with application and choose interested courses and participate in the online test.&lt;/p&gt;', 1, './assets/uploads/projects/7a480ca1320adde49e7eb2c567c4f278.pdf', 1, 20, '2015-08-08 03:20:36', '2015-09-06 20:54:46'),
(10, 'cszhj465zx', 5, 'project-7', '#3#', 'MySQL', 15, 7, '', './assets/uploads/projects/b96825d9b827f9722d01909817a9ed78.zip', './assets/uploads/project_files/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', './assets/uploads/project_files/thumb/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', 'Online examination system project is a web application which is implemented in Servlet jsp platform. Online examination system Servlet jsp project tutorial and guide for developing code. Entity–relationship(er) diagrams,Data flow diagram(dfd),Sequence dia', 'SQvVIZIV6Lo', '#1#', 1, 0, '', '', '', 0, 1500.00, 'project 7', '&lt;p&gt;Online Examination System&amp;nbsp;project could be a web portal which is developed or implemented in java domain or platform. This project is helpful for students to practice different mock examinations from this site. In current generation lots of the examinations like GRE, CAT, and MAT&amp;hellip;etc is conducted through online system. This project will help students to get practiced to online examination method by taking mock tests from this web portal. Online Examination System&amp;nbsp;portal is implemented in 2 modules student examination module and examination admin module. Admin module will add multiple courses under different branches so students can easily know about test details. Student examination module students ought to register with application and choose interested courses and participate in the online test.&lt;/p&gt;', 1, './assets/uploads/projects/7a480ca1320adde49e7eb2c567c4f278.pdf', 1, 16, '2015-08-08 03:20:36', '2015-09-06 20:54:46'),
(11, 'cszhj465zx', 5, 'project-8', '#3#', 'MySQL', 15, 7, '', './assets/uploads/projects/b96825d9b827f9722d01909817a9ed78.zip', './assets/uploads/project_files/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', './assets/uploads/project_files/thumb/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', 'Online examination system project is a web application which is implemented in Servlet jsp platform. Online examination system Servlet jsp project tutorial and guide for developing code. Entity–relationship(er) diagrams,Data flow diagram(dfd),Sequence dia', 'SQvVIZIV6Lo', '#1#', 1, 0, '', '', '', 0, 1500.00, 'project 8', '&lt;p&gt;Online Examination System&amp;nbsp;project could be a web portal which is developed or implemented in java domain or platform. This project is helpful for students to practice different mock examinations from this site. In current generation lots of the examinations like GRE, CAT, and MAT&amp;hellip;etc is conducted through online system. This project will help students to get practiced to online examination method by taking mock tests from this web portal. Online Examination System&amp;nbsp;portal is implemented in 2 modules student examination module and examination admin module. Admin module will add multiple courses under different branches so students can easily know about test details. Student examination module students ought to register with application and choose interested courses and participate in the online test.&lt;/p&gt;', 1, './assets/uploads/projects/7a480ca1320adde49e7eb2c567c4f278.pdf', 1, 18, '2015-08-08 03:20:36', '2015-09-06 20:54:46'),
(12, 'cszhj465zx', 5, 'project-9', '#3#,#4#,#5#', 'MySQL', 15, 7, '', './assets/uploads/projects/b96825d9b827f9722d01909817a9ed78.zip', './assets/uploads/project_files/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', './assets/uploads/project_files/thumb/82b6ea15d3d16c0c95828b0ac7179ac6.jpg', 'Online examination system project is a web application which is implemented in Servlet jsp platform. Online examination system Servlet jsp project tutorial and guide for developing code. Entity–relationship(er) diagrams,Data flow diagram(dfd),Sequence dia', 'SQvVIZIV6Lo', '#1#,#2#,#6#', 1, 0, '', '', '', 0, 1500.00, 'project 9', '&lt;p&gt;Online Examination System&amp;nbsp;project could be a web portal which is developed or implemented in java domain or platform. This project is helpful for students to practice different mock examinations from this site. In current generation lots of the examinations like GRE, CAT, and MAT&amp;hellip;etc is conducted through online system. This project will help students to get practiced to online examination method by taking mock tests from this web portal. Online Examination System&amp;nbsp;portal is implemented in 2 modules student examination module and examination admin module. Admin module will add multiple courses under different branches so students can easily know about test details. Student examination module students ought to register with application and choose interested courses and participate in the online test.&lt;/p&gt;', 1, './assets/uploads/projects/7a480ca1320adde49e7eb2c567c4f278.pdf', 1, 103, '2015-08-08 03:20:36', '2015-10-16 19:51:36');

-- --------------------------------------------------------

--
-- Table structure for table `project_rating`
--

DROP TABLE IF EXISTS `project_rating`;
CREATE TABLE IF NOT EXISTS `project_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `project_rating`
--

INSERT INTO `project_rating` (`id`, `project_id`, `user_id`, `rate`, `created`, `status`) VALUES
(1, 2, 5, '4', '2015-07-31 03:09:46', 1),
(2, 2, 8, '3', '2015-08-27 12:08:47', 1),
(3, 3, 8, '2', '2015-09-07 02:19:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_request`
--

DROP TABLE IF EXISTS `project_request`;
CREATE TABLE IF NOT EXISTS `project_request` (
  `prid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `filename` varchar(500) NOT NULL,
  PRIMARY KEY (`prid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `project_request`
--

INSERT INTO `project_request` (`prid`, `user_id`, `email`, `mobile_no`, `description`, `title`, `filename`) VALUES
(4, 32, 'pratikverma@gmail.com', '9826969247', 'ldsjflasjdflsda lsjdfl sadf;jdemo', 'this is pratik project', '0b3d97f852a31130d3ad54987f90624c.docx');

-- --------------------------------------------------------

--
-- Table structure for table `project_reviews`
--

DROP TABLE IF EXISTS `project_reviews`;
CREATE TABLE IF NOT EXISTS `project_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` int(5) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `project_reviews`
--

INSERT INTO `project_reviews` (`id`, `project_id`, `user_id`, `user_email`, `message`, `status`, `created`) VALUES
(2, 2, 5, 'nisha@gmail.com', 'this is new review message for test ', 1, '2015-08-06 03:09:48'),
(3, 3, 8, 'superadmin@projectwala.com', 'Vary nice project. i love the way how projectiya works.', 1, '2015-09-07 01:18:49');

-- --------------------------------------------------------

--
-- Table structure for table `request_payment`
--

DROP TABLE IF EXISTS `request_payment`;
CREATE TABLE IF NOT EXISTS `request_payment` (
  `rpid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `amount` varchar(100) NOT NULL,
  PRIMARY KEY (`rpid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `request_payment`
--

INSERT INTO `request_payment` (`rpid`, `user_id`, `amount`) VALUES
(1, 15, '2323'),
(2, 15, '200'),
(3, 15, '3434'),
(4, 15, '200'),
(5, 15, '333'),
(6, 15, '54354'),
(7, 15, '200');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` longtext,
  `added_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `added_on`, `updated_on`) VALUES
(1, 'admin_email', 'superadmin@bookamandir.com', '0000-00-00 00:00:00', '2015-10-20 11:09:59'),
(2, 'welcome_text', '<p><span style="color: #ff6600;">Welcome Projectiyadfgsdfgfd</span></p>', '0000-00-00 00:00:00', '2015-10-23 08:23:09'),
(3, 'noreply_email', 'noreplay@gmail.com', '0000-00-00 00:00:00', '2015-10-20 11:09:59'),
(4, 'header_email', 'header@gmail.com', '0000-00-00 00:00:00', '2015-10-20 11:09:59'),
(5, 'phone_number', '0731+123456', '0000-00-00 00:00:00', '2015-10-20 11:09:59'),
(6, 'cont_adrs', 'Indore', '0000-00-00 00:00:00', '2015-10-20 11:09:59'),
(7, 'cont_fax', '+08 (123) 456-7890', '0000-00-00 00:00:00', '2015-10-20 11:09:59'),
(8, 'cont_web', 'companyname.com', '0000-00-00 00:00:00', '2015-10-20 11:09:59'),
(9, 'copyright', 'Copyright123', '0000-00-00 00:00:00', '2015-10-20 11:09:59'),
(10, 'logo', 'icon48x48.png', '0000-00-00 00:00:00', '2015-10-20 11:09:59'),
(11, 'fb_url', 'https://www.facebook.com/', '0000-00-00 00:00:00', '2015-10-23 08:23:44'),
(12, 'twitter_url', 'https://www.twitter.com/', '0000-00-00 00:00:00', '2015-10-20 11:10:54'),
(13, 'youtube_url', NULL, '0000-00-00 00:00:00', '2015-10-20 11:10:54'),
(14, 'vimeo_url', 'https://www.vimeo.com/', '0000-00-00 00:00:00', '2015-10-20 11:10:54'),
(15, 'gplus_url', 'https://plus.google.com/', '0000-00-00 00:00:00', '2015-10-20 11:10:54'),
(16, 'trumblr_url', NULL, '0000-00-00 00:00:00', '2015-10-20 11:10:54'),
(17, 'dribbble_url', 'https://www.dribbble.com/', '0000-00-00 00:00:00', '2015-10-20 11:10:54'),
(18, 'linkedin_url', 'https://www.linkedin.com/', '0000-00-00 00:00:00', '2015-10-20 11:10:54'),
(19, 'pinterest_url', NULL, '0000-00-00 00:00:00', '2015-10-20 11:10:54'),
(20, 'show_project_count', '0', '0000-00-00 00:00:00', '2015-10-24 11:11:44'),
(21, 'show_active_user_count', '0', '0000-00-00 00:00:00', '2015-10-24 11:11:44');

-- --------------------------------------------------------

--
-- Table structure for table `technologies`
--

DROP TABLE IF EXISTS `technologies`;
CREATE TABLE IF NOT EXISTS `technologies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `technology` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `technologies`
--

INSERT INTO `technologies` (`id`, `technology`, `description`, `slug`, `status`, `created_date`) VALUES
(2, 'PHP', 'this php technology', 'php', 1, '2015-09-06 06:00:13'),
(3, 'Java', 'Java-J2EE', 'java', 1, '2015-09-06 05:59:47'),
(4, 'Android', 'this os jhkd kjhkjn gjfnlk.gnfgj hfkuyh uhjdfgks', '', 1, '2015-07-19 12:44:10'),
(5, 'DotNet,C#.net,Vb.Net', 'Dotnet', 'dotnetcnetvbnet', 1, '2015-09-06 06:01:46'),
(6, 'J2EE', 'Java  2  Enterprise Editions', '', 1, '2015-09-06 05:58:13'),
(8, 'JSP', 'Java Server Page', '', 1, '2015-09-06 06:08:08'),
(9, 'MySql', 'MySql Server', '', 1, '2015-09-06 06:08:19'),
(10, 'MS  SQL Server', 'Microsoft Sql server', '', 1, '2015-09-06 06:09:48'),
(11, 'C', 'Programing In c', '', 1, '2015-09-06 06:10:24'),
(12, 'C++', 'Programming in c++', '', 1, '2015-09-06 06:11:14'),
(13, 'Spring', 'Spring  in  java', '', 1, '2015-09-06 06:13:41'),
(14, 'Hibernate', 'Hibernate in Java', '', 1, '2015-09-06 06:16:26'),
(15, 'Struts', 'Struts In Java', '', 1, '2015-09-06 06:17:13'),
(16, 'PHP1', 'this php technology', 'php1', 1, '2015-09-06 06:00:13'),
(17, 'PHP2', 'this php technology', 'php2', 1, '2015-09-06 06:00:13'),
(18, 'PHP3', 'this php technology', 'php3', 1, '2015-09-06 06:00:13'),
(19, 'PHP4', 'this php technology', 'php4', 1, '2015-09-06 06:00:13'),
(20, 'PHP5', 'this php technology', 'php5', 1, '2015-09-06 06:00:13');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

DROP TABLE IF EXISTS `testimonial`;
CREATE TABLE IF NOT EXISTS `testimonial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_approved` int(11) NOT NULL,
  `added_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `content`, `user_id`, `is_approved`, `added_on`, `updated_on`) VALUES
(3, 'Welcome to Swingersextreme.com (SWX)\n\nSwingersextreme is a site for swingers designed by swingers who are committed to providing a professional, adult online experience for our members. Our goal at the X is to provide our members with a high quality, user friendly site that welcomes everyone and their ideas and suggestions. Our sincere hope is that you will Experience the Extreme.\n\nWant to join? Just create a simple profile, post up your photos, and soon you''ll be networking with our incredible members. Get started today!', 32, 1, '2015-10-24 02:59:39', '2015-10-24 12:59:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_role` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0= super admin,1=customers, 2=user,3=services_providers',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Inactive, 1=Active,2= Deactive by user, 3=Banned, ',
  `company_name` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `package_id` int(11) NOT NULL,
  `group_id` varchar(255) NOT NULL,
  `open_time` time NOT NULL,
  `close_time` time NOT NULL,
  `address` text NOT NULL,
  `address1` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(100) NOT NULL,
  `county` varchar(100) NOT NULL,
  `profile_photo` varchar(255) NOT NULL,
  `thumb_image` varchar(255) NOT NULL,
  `zip_code` varchar(20) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_account_no` varchar(255) NOT NULL,
  `bank_branch` varchar(255) NOT NULL,
  `bank_city` varchar(255) NOT NULL,
  `bank_ifsc_code` varchar(255) NOT NULL,
  `secret_key` varchar(255) NOT NULL,
  `new_password_key` varchar(50) DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `last_ip` varchar(40) NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` date NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_role` (`user_role`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_role`, `status`, `company_name`, `name`, `first_name`, `last_name`, `email`, `gender`, `password`, `mobile`, `phone`, `fax`, `package_id`, `group_id`, `open_time`, `close_time`, `address`, `address1`, `city`, `state`, `country`, `county`, `profile_photo`, `thumb_image`, `zip_code`, `bank_name`, `bank_account_no`, `bank_branch`, `bank_city`, `bank_ifsc_code`, `secret_key`, `new_password_key`, `new_password_requested`, `newsletter`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(1, 0, 1, 'superadmin', 'superadmin', 'super1', 'admin2', 'superadmin@projectwala.com', 'male', '3c237dc9d763e7f5fd1f0b528ad5e7235411a5b4', '12334566479', '5445465', '132312585845', 0, '', '00:00:00', '00:00:00', 'test address', '', '', '', '', '', '', '', '0', '', '0', '0.00', '', '', '', NULL, NULL, 0, '127.0.0.1', '2015-10-24 02:49:46', '2015-02-25', '2015-10-24 12:49:46'),
(32, 1, 1, '', '', 'Pratik', 'Verma', 'shah.karan01@gmail.com', '', '601f1889667efaebb33b8c12572835da3f027f78', '', '1234566', '', 2, '', '00:00:00', '00:00:00', 'Indore', '', 'Indore', '', 'india', '', '', '', '', '', '', '', '', '', '', NULL, NULL, 0, '127.0.0.1', '2015-10-24 01:59:01', '2015-10-23', '2015-10-24 11:59:01');

-- --------------------------------------------------------

--
-- Table structure for table `user_academy_information`
--

DROP TABLE IF EXISTS `user_academy_information`;
CREATE TABLE IF NOT EXISTS `user_academy_information` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `high_school_name` varchar(255) NOT NULL,
  `high_school_city` varchar(255) NOT NULL,
  `high_school_percentage` float(10,2) NOT NULL,
  `high_secondary_school_name` varchar(255) NOT NULL,
  `high_secondary_school_city` varchar(255) NOT NULL,
  `high_secondary_school_percentage` float(10,2) NOT NULL,
  `graduation_institude_name` varchar(255) NOT NULL,
  `graduation_institude_city` varchar(255) NOT NULL,
  `graduation_course_name` varchar(255) NOT NULL,
  `graduation_branch_name` varchar(255) NOT NULL,
  `graduation_aggr_percentage` float(10,2) NOT NULL,
  `post_graduation_institude_name` varchar(255) NOT NULL,
  `post_graduation_city` varchar(255) NOT NULL,
  `post_graduation_course_name` varchar(255) NOT NULL,
  `post_graduation_branch_name` varchar(255) NOT NULL,
  `post_graduation_aggr_percentage` float(10,2) NOT NULL,
  `other_course_info` text NOT NULL,
  `project_done_info` text NOT NULL,
  `skills` text NOT NULL,
  `total_experience` int(5) NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_academy_information`
--

INSERT INTO `user_academy_information` (`id`, `user_id`, `high_school_name`, `high_school_city`, `high_school_percentage`, `high_secondary_school_name`, `high_secondary_school_city`, `high_secondary_school_percentage`, `graduation_institude_name`, `graduation_institude_city`, `graduation_course_name`, `graduation_branch_name`, `graduation_aggr_percentage`, `post_graduation_institude_name`, `post_graduation_city`, `post_graduation_course_name`, `post_graduation_branch_name`, `post_graduation_aggr_percentage`, `other_course_info`, `project_done_info`, `skills`, `total_experience`, `modified`) VALUES
(1, 5, 'new high school', 'dewas', 74.30, 'max public school gwalior', 'gwalior main', 54.30, 'GSITS indore', 'indore', 'BE main', 'IT', 41.45, 'GSITS indore', 'indore main', 'ME', 'Civil', 48.40, 'this is my full information about other courses you can check this iksi', 'this is the test topic is data', 'this is the test topic is data', 6, '2015-08-06 12:52:18');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
