<?php include('inc/header.php') ?>
    <div class="page-title">
      <div class="container">
        <h2>About us</h2>
        <ul class="breadcrumb pull-right">
          <li><a href="#">Home</a></li>
          <li class="active"><a href="#">About</a></li>
        </ul>
      </div>
    </div>


    <div class="header-discription">
      <div class="container ">    
        <h2>About us</h2>
        <p>What you need to know about us and our team</p>
      </div>
    </div>

    <!--*******************content*********-->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <div class="box">
              <img src="images/about.jpg" class="img-responsive">
            </div>
          </div>
          <div class="col-md-7  content-discription">
            <h3>Who we are</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae, provident, nam, et, consectetur molestiae aspernatur ratione placeat dignissimos odio cum non eveniet adipisci voluptas doloribus fugiat maiores odit sint repellat. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, nisi, quaerat, quo excepturi accusantium sint modi nesciunt dicta distinctio quibusdam eum expedita similique est ullam accusamus quae non placeat dolores. </p> 
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus, molestiae, rerum, provident dolorum placeat minus molestias illo odit expedita iure corporis consequuntur repudiandae blanditiis reprehenderit vitae cum repellendus saepe non?</p>
          </div>
        </div> 
      </div>
    </div>
    <!-- ********************content************** -->
    

    




<?php include('inc/footer.php') ?>