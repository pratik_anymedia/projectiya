<?php include('inc/header.php'); ?>


  <div class="clearfix"></div>
    <div class="page-title">
      <div class="container">
        <h2>Project Description</h2>
        <ul class="breadcrumb pull-right">
          <li><a href="#">Home</a></li>
          <li class="active"><a href="#">Description</a></li>
        </ul>
      </div>
    </div>
<div class="container">
    <br><br><br>
    <!--lATEST pROJECT LIST-->
  <div class="col-md-9">
    <div class="Desc-warp">
      <div class="desc-innner">
        <div class="header">
        <div class="col-md-8">
          
        <h3 class="">Social Cloud Computing</h3>
        <ul>
          <li><a href="#">Posted : Projectwala </a></li>
          <li><a href="#"> <i class="fa fa-calendar"></i> 20-04-2015</a></li>
          <li><a href="#"><i class="fa fa-comment"></i> No Comments </a></li>
        </ul>
        </div>
        
        <div class="col-md-4">
          <div class="show-ratring text-center">
            <h2>4</h2>
            <div class="rating-warp">
              <ul>
              <li><i class="fa fa-star"></i></li>
              <li><i class="fa fa-star"></i></li>
              <li><i class="fa fa-star"></i></li>
              <li><i class="fa fa-star"></i></li>
              <li><i class="fa fa-star"></i></li>
            </div>
          </div>

        </div>
        <div class="clearfix"></div>
        </div>
<!--description header end here-->

        <div class="col-md-4">
          <div class="desc-img">
            <img src="images/project-info-1.jpg" class="img-responsive">
          </div>

           <!--download btn-->
        <div class="">
          <br><br><br>
          <div><a href="#" class="btn btn-danger"><i class="fa fa-download"></i> Download Now</a></div>
          <br>
          <div>
           <a href="#" class="btn btn-primary"><i class="fa fa-star"></i> Add To Favorite</a>
          </div>
        </div>


        </div>
        <div class="col-md-8">
          <div class="content-inner">
          <h4>Aim:</h4>

          <p>The main aim of this project is to combine the mobile backbone nodes, which have superior mobility and communication capability, with regular nodes, which are constrained in mobility and communication capability.</p>

          <h4>Existing System:</h4>

          <p>In the existing system, multiple mobile robots can be used to explore an area of interest more rapidly than a single mobile robot, and multiple sensors can provide simultaneous coverage of a relatively large area for an extended period of time. In many applications the data collected by these distributed platforms is best utilized after it has been aggregated, which requires communication among the robotic or sensing agents.
          </p>

          <h4>Proposed System:</h4>

          <p>In the proposed system, we develop a novel technique for maximizing this quantity in networks of fixed regular nodes using mixed-integer linear programming (MILP). The MILP-based algorithm provides a significant reduction in computation time compared to existing methods and is computationally tractable for problems of moderate size. An approximation algorithm is also developed that is appropriate for large-scale problems.</p>

          <h4>Hardware and Software Requirements:</h4>

          <h4>Software Requirements</h4>

          <ul>
          <li><span>Microsoft Windows XP Professional</span></li>
          <li><span>JDK 6.0</span></li>
          <li><span>Java Swing</span></li>
          </ul>

          <h4>Hardware Requirements</h4>

          <ul>
          <li><span>Pentium 4 processor</span></li>
          <li><span>1 GB RAM</span></li>
          <li><span>80 GB Hard Disk Space</span></li>
          </ul>
      

        </div>
        </div>
        <div class="clearfix"></div>



      </div>
    </div>
  </div>
    
    <div class="col-md-3 blog-right">
            <!-- Single button -->
          <h2 class="heading">Latest Categories</h2>
           <div class="block">
            <ul>
              <li><a href="#">PHP</a></li>
              <li><a href="#">.Net</a></li>
              <li><a href="#">Android</a></li>
              <li><a href="#">Iphone</a></li>
            </ul>
          </div>

          <h2 class="heading">Latest Categories</h2>
           <div class="block">
            <ul>
              <li><a href="#">PHP</a></li>
              <li><a href="#">.Net</a></li>
              <li><a href="#">Android</a></li>
              <li><a href="#">Iphone</a></li>
            </ul>
          </div>

          <h2 class="heading">Latest Categories</h2>
           <div class="block">
            <ul>
              <li><a href="#">PHP</a></li>
              <li><a href="#">.Net</a></li>
              <li><a href="#">Android</a></li>
              <li><a href="#">Iphone</a></li>
            </ul>
          </div>
        </div>

  

  <div class="clearfix"></div>

  <!--review warp-->
  <div class="review-warp">
    <div class="col-md-9">
      <div class="header clearfix">
        <h3 class="pull-left">Reviews</h3>
        <div class="pull-right">
          <a href="#" class="btn btn-success">Provide Rating And Review</a>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-9 review-content">


<!--review block-->
      <div class="review-block">
        <div class="media">
          <div class="media-left">
            <div class="review-date-warp">
              <div class="post-date text-center">
                <i class="fa fa-calendar"></i>
                <h4>15</h4>
                <h5>June</h5>
              </div>
            </div>
          </div>
          <div class="media-body">
            <div class="media-head-warp clearfix">
            <h4 class="media-heading"><span>Posted By : </span> Media heading</h4>
            <div class="favariote pull-right star">
              Favorite 
              <span>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
              </span>
            </div>
            </div>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            </p>
          </div>
        </div>

      </div>

<!--review block-->
      <div class="review-block">
        <div class="media">
          <div class="media-left">
            <div class="review-date-warp">
              <div class="post-date text-center">
                <i class="fa fa-calendar"></i>
                <h4>15</h4>
                <h5>June</h5>
              </div>
            </div>
          </div>
          <div class="media-body">
            <div class="media-head-warp clearfix">
            <h4 class="media-heading"><span>Posted By : </span> Media heading</h4>
            <div class="favariote pull-right star">
              Favorite 
              <span>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
              </span>
            </div>
            </div>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            </p>
          </div>
        </div>

      </div>


    </div>

  </div>

  </div>

<?php include('inc/footer.php') ?>