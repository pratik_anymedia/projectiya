    <div class="footer">
      <div class="twitter-feed socail-feed">
        <div class="container ">
        <!-- <p><i class="fa fa-twitter fa-3x"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque, magni, nostrum consectetur atque provident pariatur tenetur deleniti </p> -->
        <div class="col-md-4">
          <h3>Like Us On Socail Media </h3>
        </div>
        <div class="col-md-8">
          <ul class="socail-warp">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
          </ul>
        </div>
        <div class="clearfix"></div>
      </div>
      </div>

      <div class="container footer-content">
        <div class="row">
          <div class="col-md-4">
            <a class="navbar-brand" href="../index.html"> <span class="logo">Project Wala <i class="fa fa-bookmark"></i></span></a>
            <div class="site-description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus</div>
          </div>
          <div class="col-md-4">
            <h3 class="footer-heading">Latest Categories</h3>
            <ul class="list-inline footer-tags">
              <li><a href="">PHP</a></li>
              <li><a href="">Dot Net</a></li>
              <li><a href="">Android</a></li>
              <li><a href="">VB++</a></li>
              <li><a href="">My Sql</a></li>
              <li><a href="">Mobile Apps</a></li>
              <li><a href="">Mechanical</a></li>
              <li><a href="">Reports</a></li>
              <li><a href="">Website</a></li>
              <li><a href="">HTML</a></li>
          
            </ul>
          </div>
          <div class="col-md-4">
            <h3 class="footer-heading">Blog Posts</h3>
            <ul class="list-unstyled blog-posts">
              <li>
                 <a href="#">
                    <img src="images/blog/postOne.png" class="blog-thumbnail" alt="">
                    <h5>Creative Images are here</h5>
                    <p>Lorem Ipsum dolor sine amet consequor...</p>
                  </a>
               </li>
              <li>
                 <a href="#">
                    <img src="images/blog/postTwo.png" class="blog-thumbnail" alt="">
                    <h5>Creative Images are here</h5>
                    <p>Lorem Ipsum dolor sine amet consequor...</p>
                  </a>
               </li>
              <li>
                 <a href="#">
                    <img src="images/blog/postThree.png" class="blog-thumbnail" alt="">
                    <h5>Creative Images are here</h5>
                    <p>Lorem Ipsum dolor sine amet consequor...</p>
                  </a>
               </li>
              <ul>
        
          </div>
        </div>
      </div>

      

    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-1.10.2.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/core.js"></script>
    <script type="text/javascript">
    $(function() {
    //caches a jQuery object containing the header element
    var header = $(".fixed-head-warp");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 106) {
            header.addClass("head-modify");
        } else {
            header.removeClass("head-modify");
        }
    });
    });
    </script>
  </body>
  </html>