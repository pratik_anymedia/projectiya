<!DOCTYPE html>
<html>
<head>
  <title>ProjectWala</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap -->
  <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="./css/font-awesome.min.css" rel="stylesheet">

  <link href="./css/style.css" rel="stylesheet">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
    <div class="header-warp">
     <div class="fixed-head-warp fixed-head">
      <div class="top-box">
        <div class="container">
            <div class="col-md-6">
              <p class="support-text">For Support Call Us: +91 0123456789</p>
            </div>
            <div class="col-md-6">
             <div class="modcontent clearfix">
            
              <ul class="yt-loginform menu pull-right">
                <li class="yt-login">
                <a class="login-switch"  href="registration.php" title="">
                   Login
                </a>
                </li>
                <li class="yt-register">
                <a class="register-switch text-font" href="registration.php" onclick="showBox('yt_register_box','jform_name',this, window.event || event);return false;">
                <span class="title-link"> <span>Register</span></span>
                </a>
                </li>
                <li class="jshop-checkout">
                <a href="#">Checkout</a>
                </li>
              </ul>

            </div>

            </div>
            <div class="clearfix"></div>
          </div>
      </div>
      <nav class="navbar navbar-default" role="navigation">
        <div class="container">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"> <span class="logo">Project <strong>Wala</strong> <i class="fa fa-bookmark"></i></span></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <!--  <ul class="nav navbar-nav navbar-right navbar-social-links hidden">
            <li><a href="#">
              <span class="fa-stack ">
                <i class="fa fa-circle-o fa-stack-2x"></i>
                <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
              </span>
            </a></li>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          </ul> -->

          <ul class="nav navbar-nav navbar-right ">
            <li class="active"><a href="index.php">Home</a></li>
            <li><a href="about-us.php">About</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog</a>
            </li>
            <li><a href="contact.php">Contact</a></li>
           
          </ul>
          <div class="clearfix"></div>
        </div><!-- /.navbar-collapse -->
      </div>
    </nav>
      </div>
    </div>
      <div class="clearfix"></div>