<?php include('inc/header.php'); ?>
    <!-- carousel -->

    <div id="carousel-example-generic" class="carousel slider" data-ride="carousel">

      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol>

      <!-- Slider Content (Wrapper for slides )-->
      <div class="container">
        <div class="carousel-inner">
          <div class="item active">
            <div class="row">
              <div class="col-md-6 slider-caption animated ">
                <h1 class="animated flash text-info">Built On Creativity</h1>
                <p class="animated fadeIn">Cascade  is a  Flat , Responsive, Admin  Dashboard template. It is complete set of modern standards and top notch design. Built on twitter bootstrap 3.0 with bunch of premium extended features, strong code structure underneath and handful of useful jquery plugins , all packed in one design. Months of research and years of experience all strived to make a wonderful theme with each and every useful feature for all types of applications.Performance of each module is highly optimized for different browsers and devices.</p>
                <a href="#" class=" btn btn-info animated fadeInLeft">Live Preview</a>
                <a href="#" class=" btn btn-success animated fadeInRight">Buy Now</a>
                
              </div>
              <div class="col-md-6">
                <img src="images/slider/ipad.png" width="100%" class="animated fadeInRightBig" alt="...">
              </div>
              
            </div>
          </div>

          <div class="item ">
            <div class="row">
              <div class="col-md-6 slider-caption animated ">
                <h1 class="animated flash">Built On Creativity</h1>
                <p class="animated fadeIn">End user/client will be able to pick manually any color/styling on any element (header, main navigation, sidebar, widgets, boxes etc…) by their own choice and that makes this template perfect for your next project. Your users/clients will simply love it</p>
                <a href="#" class=" btn btn-info animated fadeInUpBig ">Live Preview</a>
                <a href="#" class=" btn btn-success animated fadeInUpBig ">Buy Now</a>
                
              </div>
              <div class="col-md-6">
                <img src="images/slider/ipad.png" class="animated fadeInRightBig" alt="...">
              </div>
              
            </div>
          </div>
        </div>

        <!-- Slider Content (Wrapper for slides )-->


        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
          <span class="fa fa-arrow-circle-o-left fa-2x"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
          <span class="fa fa-arrow-circle-o-right fa-2x"></span>
        </a>
      </div>
    </div>

    <div class="container">
    <br><br><br>
    <!--lATEST pROJECT LIST-->
    <div class="col-md-9">
    <div class="latest-list-warp">
     <!--  <h2 class="heading">Latest Projects</h2> -->
      <div class="list-block">
        <div class="col-md-1 post-date-warp">
          <div class="post-date text-center">
            <i class="fa fa-calendar"></i>
            <h4>15</h4>
            <h5>June</h5>
          </div>
          <hr>
          <div class="post-date text-center">
            <i class="fa fa-thumbs-o-up"></i>
            <h5>1000</h5>
          </div>
        </div>
        <div class="col-md-3">
          <a href="#"><img src="images/project-info-1.jpg" class="img-responsive"></a>
        </div>
        <div class="col-md-8 list-block-right">
          <a href="project-desc.php"><h4 class="title">Social Cloud Computing</h4></a>
          <div class="block-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enimadipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim</div>
          <div class="rate-warp">
            <div class="row">
            <div class="col-md-5 block">
              Favirote &nbsp;&nbsp;
              <a href="#">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              </a>
            </div>
            <div class="col-md-4 block">
              Viewd - <a href="#">10220</a>
            </div>
            <div class="col-md-3 text-right">
              <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-download"></i> Download</a>
            </div>
            </div>

          </div>
        </div>
        <div class="clearfix"></div>
      </div>

      <div class="list-block">
        <div class="col-md-1 post-date-warp">
          <div class="post-date text-center">
            <i class="fa fa-calendar"></i>
            <h4>15</h4>
            <h5>June</h5>
          </div>
          <hr>
          <div class="post-date text-center">
            <i class="fa fa-thumbs-o-up"></i>
            <h5>1000</h5>
          </div>
        </div>
        <div class="col-md-3">
          <img src="images/project-info-1.jpg" class="img-responsive">
        </div>
        <div class="col-md-8 list-block-right">
          <a href="project-desc.php"><h4 class="title">Social Cloud Computing</h4></a>
          <div class="block-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enimadipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim</div>
          <div class="rate-warp">
            <div class="row">
            <div class="col-md-5 block">
              Favirote &nbsp;&nbsp;
              <a href="#">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              </a>
            </div>
            <div class="col-md-4 block">
              Viewd - <a href="#">10220</a>
            </div>
            <div class="col-md-3 text-right">
              <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-download"></i> Download</a>
            </div>
            </div>

          </div>
        </div>
        <div class="clearfix"></div>
      </div>

      <div class="list-block">
        <div class="col-md-1 post-date-warp">
          <div class="post-date text-center">
            <i class="fa fa-calendar"></i>
            <h4>15</h4>
            <h5>June</h5>
          </div>
          <hr>
          <div class="post-date text-center">
            <i class="fa fa-thumbs-o-up"></i>
            <h5>1000</h5>
          </div>
        </div>
        <div class="col-md-3">
          <img src="images/project-info-1.jpg" class="img-responsive">
        </div>
        <div class="col-md-8 list-block-right">
          <a href="#"><h4 class="title">Social Cloud Computing</h4></a>
          <div class="block-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enimadipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim</div>
          <div class="rate-warp">
            <div class="row">
            <div class="col-md-5 block">
              Favirote &nbsp;&nbsp;
              <a href="#">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              </a>
            </div>
            <div class="col-md-4 block">
              Viewd - <a href="#">10220</a>
            </div>
            <div class="col-md-3 text-right">
              <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-download"></i> Download</a>
            </div>
            </div>

          </div>
        </div>
        <div class="clearfix"></div>
      </div>

      <div class="list-block">
        <div class="col-md-1 post-date-warp">
          <div class="post-date text-center">
            <i class="fa fa-calendar"></i>
            <h4>15</h4>
            <h5>June</h5>
          </div>
          <hr>
          <div class="post-date text-center">
            <i class="fa fa-thumbs-o-up"></i>
            <h5>1000</h5>
          </div>
        </div>
        <div class="col-md-3">
          <img src="images/project-info-1.jpg" class="img-responsive">
        </div>
        <div class="col-md-8 list-block-right">
          <a href="#"><h4 class="title">Social Cloud Computing</h4></a>
          <div class="block-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enimadipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim</div>
          <div class="rate-warp">
            <div class="row">
            <div class="col-md-5 block">
              Favirote &nbsp;&nbsp;
              <a href="#">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              </a>
            </div>
            <div class="col-md-4 block">
              Viewd - <a href="#">10220</a>
            </div>
            <div class="col-md-3 text-right">
              <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-download"></i> Download</a>
            </div>
            </div>

          </div>
        </div>
        <div class="clearfix"></div>
      </div>


    </div>
    </div>
    
    <div class="col-md-3 blog-right">
            <!-- Single button -->
          <h2 class="heading">Latest Categories</h2>
           <div class="block">
            <ul>
              <li><a href="#">PHP</a></li>
              <li><a href="#">.Net</a></li>
              <li><a href="#">Android</a></li>
              <li><a href="#">Iphone</a></li>
            </ul>
          </div>

          <h2 class="heading">Latest Categories</h2>
           <div class="block">
            <ul>
              <li><a href="#">PHP</a></li>
              <li><a href="#">.Net</a></li>
              <li><a href="#">Android</a></li>
              <li><a href="#">Iphone</a></li>
            </ul>
          </div>

          <h2 class="heading">Latest Categories</h2>
           <div class="block">
            <ul>
              <li><a href="#">PHP</a></li>
              <li><a href="#">.Net</a></li>
              <li><a href="#">Android</a></li>
              <li><a href="#">Iphone</a></li>
            </ul>
          </div>
        </div>

    </div>
   

<?php include('inc/footer.php') ?>