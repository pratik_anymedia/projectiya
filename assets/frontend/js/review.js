/*Custom Jquery*/

$(function(){
		$('#review_footer').on('click','#Review_add',function(event){
          event.preventDefault();
          var project_id = $('#product_id_add').val();
          var emailRegex = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
          var fname = $('#user_full_name').val();
          var femail = $('#user_email').val(); 
          var review_message = $('#review_message').val();
          if(fname == "")
          {
            $('#fn_error').html('Please Enter Full Name Field');
            setTimeout(function(){ $('#fn_error').html(''); }, 5000);
            return false;
          }   
          if(femail == "")
          {
            $('#em_error').html('Please Enter Email Field');
            setTimeout(function(){ $('#em_error').html(''); }, 5000);
            return false;
          }else if(!emailRegex.test(femail)){
            $('#em_error').html('Please enter valid email Address');
            setTimeout(function(){ $('#em_error').html(''); }, 5000);
            return false;
          } 
          if(review_message==''){
            $('#rev_com_error').html('Please enter review field');
            setTimeout(function(){ $('#rev_com_error').html(''); }, 5000);
            return false;
          } 
          $.post(BASEURL+'projects/add_review/',{full_name:fname,email:femail,message:review_message,project_id:project_id},function(data){          
            if(data!=''){
              if(data=='TRUE'){
                $('.alert_msg').css('display','block');
                $('.alert_msg').html('Success : your review has been sent successfully Please Wait for Approval');
                //$('#rating_review').val(0);
                setTimeout(function(){ 
                  $('.alert_msg').html(''); 
                  $('.alert_msg').css('display','none');
                  $('#review_modal').hide();
                  window.location.reload();
                },3000);
              }else if(data=='alreadyRate'){
                 alert('Opps: You have been already added Review on this Project');
              }else{
                alert('Error: Try it later :-(');

              }
            }  
          });
        });


      $('#review_modal').modal({
        show:false,
        backdrop:'static',
      });
      $('#review_modal').on('hidden.bs.modal',function(e){
          var review_message = $('#review_message').val('');
      });

	    $('#review_login').on('click',function(event) {
          event.preventDefault();
          alert('Sorry !! Please login first for write review on this project.');
          return false;
      });

});