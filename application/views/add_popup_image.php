

<div class="row">
    <div class="col-mod-12">
        <h3 class="page-header"> Setting </h3>          
    </div>
</div>

<!-- Users widget -->
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading text-primary">
                <h3 class="panel-title">Add Popup Image</h3>
            </div>
            <div class="panel-body">
                
                    
                        
                        <?php //echo validation_errors(); ?>   
                        <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal')); ?> 
                        <div class="form-body">
                            <div class="form-body">

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label"><strong>Popup Image</strong></label>
                                        <input type="file" name="popup_image">
                                      
                                        <?php echo form_error('popup_image');
                                        
                                       ?>
                                        <?php //if(!empty($page_set->file_path)){ ?>
                                        <img src="<?php //echo base_url(str_replace('./','',$page_set->file_thumb_path));    ?>" alt="">	
                                        <?php //} ?>
                                        <small>(file type: jpeg,jpg,png,gif)</small> 
                                    </div>
                                </div> 
                            </div> 

                            <div class="form-actions">       
                                <button type="submit" class="btn blue">Submit</button> 
                                <a href="<?php echo base_url('backend/projects/'); ?>" ><button class="btn btn-danger" type="button">Cancel</button></a>
                            </div>  

                            <?php echo form_close(); ?>
                        </div>
                    
              
            </div>
        </div>
    </div>  <!-- / Users widget-->

