<style type="text/css"> 
    .overlay {
        background-color: rgba(0, 0, 0, 0.2);
        z-index: 999;
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;

    }
    .ajax-loader {
        position: absolute;
        left: 50%;
        top: 50%;
        margin-left: -62px; /* -1 * image width / 2 */
        margin-top: -62px;  /* -1 * image height / 2 */
    }
</style>

<!-- carousel -->

<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left">Project</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label"><a href="<?php echo base_url(); ?>projects/project_list">Project List</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>

<div class="container">

    <br><br>
    <div class="row">
        <!-- /.col-lg-6 -->
        <!--  <div class="col-md-6">
           <div class="input-group input-group-lg">
             <input type="text" class="form-control" placeholder="Search for...">
             <span class="input-group-btn">
               <button class="btn btn-default" type="button">Go!</button>
             </span>
           </div>
         </div> -->
    </div><!-- /.row -->
    <!--lATEST pROJECT LIST-->
    <div  class="col-md-9">
        <?php msg_alert_backend(); ?>
        <div id="mydiv" class="latest-list-warp">
            <!-- <h2 class="heading">Latest Projects</h2>-->
            <?php
            
            
            if (!empty($projects)) {
                foreach ($projects as $value):
                    $this->load->view('templates/frontend/widgets/projectBox' , array('value' => $value));
                    ?>
                    

                    <?php
                endforeach;
            }else{
            ?>
            <div class="col-md-12 text-center z-depth-1" style="padding:7px;">
                <h1>No Projetcs Found </h1>
                
            </div>    
            <?php
            }
            ?> 
            <div class="container">
                <div class="row-fluid  control-group mt15">             
                    <div class="span12 ">
                        <?php if (!empty($pagination)) echo $pagination; ?>              
                    </div>
                </div>
            </div>    
        </div>
    </div>   
    <div class="col-md-3 blog-right">
        <!-- Single button -->
        <section class="links">
            <h1 class="section-heading text-highlight">
                <span class="line">Project's Technologies </span>
            </h1>
            <div class="section-content">
            <?php if (!empty($technology)) {
                foreach ($technology as $value) {
                    ?>
                    <p>
                        <span>
                            <input type="checkbox" class="technology_id" name="technology_id[]" value="<?php echo $value->id; ?>">
                        </span>
                        <span>
                            <?php echo $value->technology; ?>
                        </span>
                    </p>
                    <?php }
                }
                ?> 
            </div>        
        </section>
        
        <section class="links">
            <h1 class="section-heading text-highlight"><span class="line">Project's Course</span></h1>
                <div class="section-content">
                    <?php if (!empty($courses)) {
                        foreach ($courses as $value) { ?>
                            <p> 
                                <span>
                                    <input type="checkbox"  class="course_id" name="course_id[]" value="<?php echo $value->id; ?>">
                                </span>
                                <span> <?php echo $value->course; ?></span>
                            </p>
                    <?php }
                    }
                    ?> 
                </div>
        </section>            
        <section class="links">
            <h1 class="section-heading text-highlight"><span class="line"><?php echo $this->lang->line('FEATURED_PROJECT'); ?> </span></h1>
                <div class="section-content">
                    <?php
                    $fp_count = 0;
                    if (!empty($projects)) {
                        foreach ($projects as $key => $value) {
                            if ($fp_count == 6) {
                                break;
                            }
                            if (isset($value->featured_project) && $value->featured_project == 1) {
                                $fp_count++;
                                ?>
                                    <p>
                                        <a href="<?php echo base_url('projects/detail/' . $value->slug); ?>"><i class="fa fa-caret-right"></i><?php
                                            if (!empty($value->title)) {
                                                echo $value->title;
                                            } ?>
                                        </a>
                                    </p>
                                <?php
                            }
                        }
                    }

                    if ($fp_count == 0) { ?>
                        <p> No Record Found.    </p>
                    <?php } ?>
                </div>
                        <!--section-content-->
            </section>
    </div>
</div>

