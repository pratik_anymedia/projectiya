<div class="clearfix"></div>

<div class="page-title">
    <div class="container">
        <h2>Download</h2>
    </div>
</div>
<div class="container">

    <br><br>
    <div class="row">
        <div class="jumbotron">
            <h1>Oops!</h1>
            <p>Download link is Expired..!!</p>
            <p><a class="btn btn-primary btn-lg" href="<?php echo base_url('projects') ?>" role="button">Back to Projects</a></p>
        </div>
    </div><!-- /.row -->
</div>