 <style type="text/css">
    .rating-warp {
      padding-top: 10px;
      padding-left: 70px;
    }
    .modal-header{
      background-color: #323543;
      color:#fff;
    }
    .modal-footer{
      background-color: #323543;
      color:#fff;
    }
   .re_error{
      color:red;
   }
 </style>
  <div class="clearfix"></div>
  <div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left">Project Description</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label"><a href="#">Description</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>
<div class="container">
    <br>
    <div class="row">
    
    <div class="col-md-12">
     
       <div class="panel panel-default">
  <div class="panel-heading"> <h3>Summary of Project order</h3></div>
  <div class="panel-body">
   

       <table class="table table-bordered">
        <!--  <caption>table title and/or explanatory text</caption> -->
         <thead>
           <tr>
             <th width="30%">Title</th>
             <th width="20%">Technologies</th>
             <th width="20%">Courses</th>
             <th width="10%">Price</th>
             <th width="10%">Action</th>
           </tr>
         </thead>
         <tbody>
           <tr>
              <td><?php  if(!empty($project_info->title)) echo $project_info->title;    ?></td>
             <td> <?php if(!empty($project_info->technology)){ $Array=explode('#,#',trim($project_info->technology,'#'));  } ?>
             <?php  if(!empty($technology)):  ?>
                  <?php foreach ($technology as $value){ ?>
                  <?php if(!empty($Array)): for($i=0; $i < count($Array); $i++){ if($Array[$i]==$value->id){ echo ucfirst($value->technology).',';} } endif;  ?> 
                  <?php } ?>
                <?php endif; ?> </td>
             <td>
                         <?php if(!empty($project_info->course_type)){ $Array=explode('#,#',trim($project_info->course_type,'#'));  } ?>
<?php  if(!empty($courses)):  ?>
                  <?php foreach ($courses as $value){ ?>
                   <?php if(!empty($Array)): for($i=0; $i < count($Array); $i++){ if($Array[$i]==$value->id){ echo ucfirst($value->course).',';} } endif;  ?> 
                  <?php } ?>
                <?php endif; ?> 
             </td>
             <td> <?php   if(!empty($project_info->price)) echo  '<i class="fa fa-rupee"></i> '.$project_info->price;  ?></td>
             <td>
                <a class="btn  btn-primary" href="<?php echo base_url('order/checkout/'.$project_info->unique_id); ?>">Processed To checkout</a>
             </td>
           </tr>
          
         </tbody>
       </table>
      </div>
</div>
</div>


<div class="col-md-12 blog-main">

          <div class="blog-post">
            <h2 class="blog-post-title">Terms And Condition </h2>
            <p class="blog-post-meta"><!-- January 1, 2014 by <a href="#">Mark</a> --> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, uis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
             <ul>
              <li>Click EBS to Pay Amount by Net Banking, Debit Card and Credit Card from EBS secure online payment gateway.</li>
              <li>Click PayPal to Pay Amount by Credit Card and PayPal ID for International Student from secure PayPal gateway.</li>
              <li>All payments will be processed by our payments partner, EBS - Payment Gateway and Paypal, via their secure web interface.</li>
              <li>Payments should usually be credited within 24 hours.</li>
              <li>Please contact us if a successful payment has not been credited to your account after one day.</li>
              <li>Project Download link will be send to your email id within 24 hours after succesful payment transcation.</li>
              <li>Please note payment transcation id for all future communication.</li>

            </ul>
            <hr>
           
           <!-- <h2 class="blog-post-title">PayMent Option </h2> 
             <blockquote>
              <p>Curabitur blandit tempus porttitor. <strong>Nullam quis risus eget urna mollis</strong> ornare vel eu leo. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            </blockquote>
            <p>Dear <em>User</em> Dear Student, you can pay amount for projects using below options.</p>
             -->
            <h2 class="blog-post-title">Credit Card, Debit Card, Net Banking, And Online Payment.</h2>
            <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
           <h2 class="blog-post-title"> Deposit Cash or Online Transfer.</h2>
           <address>
              <strong> State Bank Of India</strong>Bank Account Number:<strong> 245467464688 </strong><br> 
              Account Holder Name:<strong>"Karan Kumar Shah"</strong> <br>
              Account Type:<strong>Saving</strong> <br>
              IFSC Code:<strong>SBI0023019</strong> <br>
              Mira Road, Thane, Maharashtra - 401104<br>
              <abbr title="Phone">P:</abbr> (123) 456-7890

            </address>
        

          <div class="blog-post">
            <h2 class="blog-post-title">Outside India.</h2>
           
            <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
            <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
          </div><!-- /.blog-post -->
          <div class="col-md-12 text-center">
          
             <a class="btn btn-lg btn-primary" href="<?php echo base_url('order/checkout/'.$project_info->unique_id); ?>">Processed To checkout</a>
             <a  class="btn btn-lg btn-primary"  href="<?php echo base_url('projects/')  ?>">Cancel</a>
          
          </div>
          <br>

    </div>

    </div>
    </div>
</div>

    
    
    