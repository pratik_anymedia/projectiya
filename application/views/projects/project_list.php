<style type="text/css">
    .rating-warp {
        padding-top: 10px;
        padding-left: 70px;
    }
    .modal-header{
        background-color: #323543;
        color:#fff;
    }
    .modal-footer{
        background-color: #323543;
        color:#fff;
    }
    .re_error{
        color:red;
    }
</style>
<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left">Project</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label"><a href="<?php echo base_url(); ?>projects/project_list">Project List</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>
<div class="container ">

    <!-- pROJECT LIST-->
    <div class="col-md-12 table-responsive">
        <table class="table table-flip-scroll table-hover">
            <thead class=" ">
                <tr>
                    <th>#</th>
                    <th>Project Title</th>
                    <th>Technology</th>
                    <th>Backend</th>
                    <th>Forms</th>
                    <th>No. Of Tables</th>
                    <th>Price</th>

                </tr>
            </thead>
            <tbody>
                <?php
                    $no=1;
                if (!empty($projects)) {
                    foreach ($projects as $key => $value) {
                        ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><a href="<?php echo base_url('projects/detail/' . $value->slug); ?>"><?php echo ucfirst($value->title); ?></td>
                            <td><?php
                                if (!empty($value->technology)) {
                                    $Array = explode('#,#', trim($value->technology, '#'));
                                    $count = count($Array);
                                    $technology_str = '';
                                    if (!empty($technology)) {
                                        ?>
                                        <?php
                                        foreach ($technology as $value1) {

                                            if (!empty($Array)) {
                                                for ($i = 0; $i < $count; $i++) {
                                                    if ($Array[$i] == $value1->id) {
                                                        $technology_str.= $value1->technology . ', ';
                                                    }
                                                }
                                            }
                                        }
                                        echo substr($technology_str, 0, strlen($technology_str) - 2);
                                        ?>
                                        <?php
                                    }
                                }
                                ?></td>
                              
                                    <td><?php echo $value->backend_technology ?></td>
                                    <td><?php echo $value->form_pages ?></td>
                                    <td><?php echo$value->no_of_tables ?></td>
                            <td><?php echo $value->price; ?></td>
                           
                        </tr>
                        <?php
                    $no++; }
                }
                ?>
            </tbody>
        </table>
        <div class="row-fluid  control-group mt15">             
            <div class="span12 pull-right">
                <?php if (!empty($pagination)) echo $pagination; ?>              
            </div>
        </div>
    </div>
</div>

