<style type="text/css">
    .rating-warp {
        padding-top: 10px;
        padding-left: 70px;
    }
    .modal-header{
        background-color: #323543;
        color:#fff;
    }
    .modal-footer{
        background-color: #323543;
        color:#fff;
    }
    .re_error{
        color:red;
    }
</style>
<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left">Projects</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label"><a href="<?php echo base_url(); ?>projects/project_list">Project List</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>
<div class="container">
    <!--lATEST pROJECT LIST-->
    <div class="col-md-9">
        <div class="Desc-warp">
            <div class="desc-innner">
                
                <!--description header end here-->

                <div class="col-md-4 ">
                    <div class="desc-img">
                        <?php if (!empty($project_info->main_image)) { ?>    
                            <img src="<?php echo base_url(str_replace('./', '', $project_info->main_image)); ?>" class="img-responsive img-thumbnail">
                        <?php } ?>
                    </div>

                    <!--download btn-->
                    <div><br>
                    </div>
                    <div class="block">
                        <?php
                        if (!empty($advertisements)) {
                            foreach ($advertisements as $value) {
                                ?>
                                <?php if (!empty($value->thumb_path)) { ?> 
                                    <?php if (!empty($value->url)) { ?> 
                                        <a href="<?php echo $value->url; ?>" target="_blank"  title="<?php echo $value->title; ?>"><img src="<?php echo base_url(str_replace('./', '', $value->thumb_path)); ?>"  altp="<?php echo $value->title; ?>" class="img-thumbnail"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo base_url(str_replace('./', '', $value->thumb_path)); ?>"  altp="<?php echo $value->title; ?>" class="img-thumbnail">
                                    <?php } ?>  
                                <?php } ?>
                                <?php
                            }
                        }
                        ?>        
                    </div>

                    <!-- Open Login model  -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form action="">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">User Login</h4>
                                    </div>
                                    <div class="modal-body">

                                        <div id="msg"></div>       
                                        <div class="form-group">

                                            <label for="recipient-name" style="color:#000;" class="control-label">Email:</label>

                                            <input type="email" placeholder="Email" name="email" class="form-control" id="user_email">

                                            <span style="color:red;" id="email_error"></span>

                                        </div>

                                        <div class="form-group">

                                            <label for="message-text" style="color:#000;" class="control-label">Password:</label>

                                            <input type="password" placeholder="Password"  name="password" class="form-control" id="user_password">

                                            <span style="color:red;" id="pass_error"></span>

                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <a href="<?php echo base_url('projects/buynow/' . $project_info->unique_id); ?>" id="Review_add" class="btn btn-danger">Buy Now With Registration </a>
                                        <button type="button" id="submit_apply_id"  class="btn btn-theme"><i class="fa fa-sign-in"></i> Login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 z-depth-1" style="padding: 0;">
                    <div class="content-inner">
                        <?php if (!empty($project_info->video_url)) { ?>
                            <iframe width="100%" height="250" src="//www.youtube.com/embed/<?php echo $project_info->video_url; ?>" frameborder="0" allowfullscreen></iframe>
                        <?php } ?>
                        <br><br>
                        
                    </div>
                    <div id="watch-header" class="content-inner" style="padding: 10px;">
                        <div id="watch7-headline" class="clearfix">
                            <div id="watch-headline-title">
                                <h1 class="yt watch-title-container">
                                    <span class="" title=""> <?php
                                        if (!empty($project_info->title)) {
                                            echo ucfirst($project_info->title);
                                        }
                                        ?>
                                    </span>
                                </h1><hr>
                            </div>
                            <div id="watch-headline-title">
                                <p>
                                    <a href="#">Posted : <?php if(isset($project_info->name)){ echo ucfirst($project_info->name); } ?></a>  
                                    <a href="#"> <i class="fa fa-calendar"></i> <?php echo date('d M Y', strtotime($project_info->created)); ?></a> &nbsp;&nbsp; 
                                    <a href="#"> <i class="fa fa-comment"></i> No Comments </a>
                                    <span class="btn btn-theme pull-right"> 
                                        <i class="fa fa-rupee"></i> 
                                        <?php if (!empty($project_info->price)) echo $project_info->price; ?>
                                    </span> 
                                </p>
                                <p><?php if (user_logged_in() == TRUE): ?>
                                    <input type="hidden"  id="user_id_add" name="user_id_add" value="<?php echo user_id(); ?>">
                                    <input type="hidden"  id="product_id_add" name="product_id_add" value="<?php echo $project_info->id; ?>">

                                    <div class="exemple">
                                        <div class="exemple2" data-average="0" data-id="<?php echo $project_info->id; ?>"></div>
                                    </div>
                                    <?php else: ?>
                                    <?php
                                        if (!empty($project_info->id)) {
                                            $total_rating = product_rating_count($project_info->id);
                                        } else {
                                            $total_rating = 0;
                                        }
                                    ?>
                                    <div class="exemple">
                                        <div class="showrate" data-average="<?php
                                            if (!empty($total_rating)) {
                                                echo $total_rating;
                                            } else {
                                                echo 0;
                                            }
                                            ?>" data-id="1">
                                        </div>
                                    </div>

                                    <?php endif; ?>
                                </p>
                                <p>
                                <ul class="social-icons col-md-6 col-sm-6 col-xs-12">
                                    <li>
                                        <?php if (user_logged_in() == TRUE) { ?>    
                                            <a href="<?php echo base_url('order/checkout/' . $project_info->unique_id); ?>" title="Buy Now"><i class="fa fa-shopping-cart"></i></a>
                                        <?php } else { ?>
                                            <a href="#myModal" data-toggle="modal" data-target="#myModal" title="Buy Now"><i class="fa fa-shopping-cart"></i></a>
                                        <?php } ?>
                                    </li>
                                     <?php
                                        $title = urlencode('tester');
                                        $url = urlencode(base_url() . 'projects/detail/' . trim($project_info->slug));
                                        $summary = urlencode(trim($project_info->short_description));
                                        $image = urlencode(base_url(str_replace('./', '', $project_info->main_image)));
                                    ?>
                                    <li><a onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo urlencode($project_info->title); ?>&amp;p[summary]=<?php echo $summary; ?>&amp;p[url]=<?php echo $url; ?>&amp;&p[images][0]=<?php echo $image; ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)" title="Share on Facebook"><i class="fa fa-facebook"></i></a>
                                    <li>
                                        <?php if (user_logged_in() == TRUE) { ?>
                                        <a href="javascript:;"  id="add_to_favorite" title="Like"><i class="fa fa-thumbs-o-up"></i></a>
                                    <?php } else { ?>
                                        <a href="#myModal" data-toggle="modal" data-target="#myModal" title="Like"><i class="fa fa-thumbs-o-up"></i></a> <?php } ?>
                                    </li>
                                </ul>
                                    <?php if (user_logged_in() == TRUE) { ?>    
                                        <a href="<?php echo base_url('order/checkout/' . $project_info->unique_id); ?>" title="Buy Now"><button class="btn btn-danger pull-right"  style="width:95px;  margin-top: 10px;"><i class="fa fa-shopping-cart"></i>Buy Now</button></a>
                                    <?php } else { ?>
                                        <a href="#myModal" data-toggle="modal" data-target="#myModal" title="Buy Now"><button class="btn btn-danger pull-right"  style="width:95px;  margin-top: 10px;"><i class="fa fa-shopping-cart"></i>Buy Now</button></a>
                                    <?php } ?>
                            </p>
                            </div>                                

                        </div>
                    </div>
                    
                    <div class="content-inner">
                        
                    </div>
                </div>
                <div class="col-md-8 z-depth-1" style="margin-top: 10px; padding-top: 10px;">
                    <div class="content-inner">
                     <?php
                        $tech_Id_arr = explode(',', $project_info->technology);
                            //str_replace('#', '', $a);
                            $tech_id_new = array();

                            foreach ($tech_Id_arr as $key => $value1) {
                                $tech_id_new[] = str_replace('#', '', $value1);

                            }
                            if (!empty($tech_id_new)) {
                                echo $this->lang->line('TECHNOLOGY_USED') . ':';
                                foreach ($tech_id_new as $key => $value2) {
                                    if (isset($technologies[$value2]))
                                        echo $technologies[$value2] . ', ';
                                }
                            }
                        
                        
                        if (!empty($project_info->backend_technology)) {
                            echo ucfirst(html_entity_decode($project_info->backend_technology));
                        }
                        echo '<br>';
                        if (!empty($project_info->no_of_tables)) {
                            echo $this->lang->line('DATABASE_TABLES') . ':';
                            echo ucfirst(html_entity_decode($project_info->no_of_tables));
                        }
                        echo '<br>';
                        $crs_Id_arr = explode(',', $project_info->course_type);
                            //str_replace('#', '', $a);
                            $crs_id_new = array();
                            foreach ($crs_Id_arr as $key => $value1) {
                                $crs_id_new[] = str_replace('#', '', $value1);
                            }


                            if (!empty($crs_id_new)) {
                                echo $this->lang->line('CAN_BE_USED_IN') . ':';
                                foreach ($crs_id_new as $key => $value2) {

                                    if (isset($course_arr[$value2]))
                                        echo $course_arr[$value2] . ', ';
                            }

                        }
                            echo '<br><br>';
                        ?>
                    </div>
                      
                </div>
                <div class="col-md-8 z-depth-1" style="margin-top: 10px; padding-top: 10px;">
                        <div class="content-inner">
                           <?php  
                            if (!empty($project_info->description)) {
                            echo ucfirst(html_entity_decode($project_info->description));
                        }?>
                        </div>
                    </div>  
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <!-- Single button -->
        <section class="links">
            <h1 class="section-heading text-highlight">
                <span class="line"><?php echo $this->lang->line('LATEST_TECHNOLOGY'); ?></span>
            </h1>
            <div class="section-content">

                    <?php
                    if (!empty($technology)) {
                        foreach ($technology as $value) {
                            ?>
                          <p>
                            <a href="<?php echo base_url('projects/index/?technology='.$value->id) ?>"><i class="fa fa-caret-right"></i><?php echo $value->technology; ?></a>
                          </p>
                    <?php
                        }
                    } ?>

            </div>
        </section>
        <section class="links">
            <h1 class="section-heading text-highlight">
                <span class="line"><?php echo $this->lang->line('RELATED_PROJECTS'); ?></span>
            </h1>
            <div class="section-content">
                    <?php
                        if (!empty($related_projects)) {
                            foreach ($related_projects as $key => $value) {
                                ?>
                                <p>
                                    <a href="<?php echo base_url('projects/detail/' . $value->slug); ?>"><i class="fa fa-caret-right"></i><?php
                                        if (!empty($value->title)) {
                                            echo $value->title;
                                        } ?>
                                    </a>
                                </p>
                                <?php
                            }
                        } else { ?>
                        <p><?php echo $this->lang->line('NO_RECORD_FOUND'); ?></p>
                    <?php }  ?>
            </div>
        </section>
    </div>
    <div class="clearfix"></div>
    <!--review warp-->
    <div class="review-warp">
            <?php   
                if (user_logged_in() == TRUE) {
                $user = get_user_info();
            ?>
            <div class="col-md-9 z-depth-1">
                <div class="header clearfix">
                    <h3 class="pull-left" style="padding:7px;">Reviews</h3>
                    <div class="pull-right">
                        <a href="javascript:;" class="btn btn-primary" data-toggle="modal" data-target="#review_modal" >Provide Review</a>
                    </div>
                </div>
            </div>
            <!-- modal Data -->
            <div class="modal" id="review_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close"  style="color:#fff;"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"> Provide Review</h4>
                        </div>
                        <div class="modal-body">
                            <div style="display:none;" class="alert alert_msg alert-success">
                                This is My Message
                            </div>
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Your Full Name</label>
                                    <input type="text"  id="user_full_name"  readonly="readonly"  value="<?php if (!empty($user->first_name) && !empty($user->last_name)) echo ucfirst($user->first_name . ' ' . $user->last_name); ?>"  name="user_full_name" class="form-control" id="exampleInputEmail1" placeholder="Full Name">
                                    <span id="fn_error" class="re_error"></span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Email</label>
                                    <input type="text"  id="user_email" readonly="readonly" name="user_email"  value="<?php if (!empty($user->email)) echo $user->email; ?>" class="form-control" id="exampleInputPassword1" placeholder="Email">
                                    <span id="em_error" class="re_error"></span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Review</label>
                                    <textarea class="form-control" id="review_message" name="review_message" placeholder="Write Review"></textarea>
                                    <span id="rev_com_error" class="re_error"></span>
                                </div>
                            </form>
                        </div>
                        <div id="review_footer" class="modal-footer">  

                            <button type="button" id="Review_add"  class="btn btn-theme">Submit Review</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal Data -->
            <?php }else { ?>
            <div class="col-md-9 z-depth-1" style="padding:7px;">
                <div class="header clearfix">
                    <h3 class="pull-left" >Reviews</h3>
                    <div class="pull-right">
                        <a href="javascript:;" data-toggle="modal" data-target="#myModal" class="btn btn-theme">Provide Review</a>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="clearfix"></div>
            <div class="col-md-9 review-content">

            <?php $product_view = get_project_reviews($project_info->id); ?>
            <?php if (!empty($product_view)) { ?>
            <?php   foreach ($product_view as $value) {
            $user_info = get_user_information($value->user_id);  ?> 

                <?php if (!empty($user_info)) { ?>            
                <!--review block-->
                <div class="review-block z-depth-1">
                    <div class="media">
                        <div class="media-left">
                            <div class="review-date-warp">
                                <div class="post-date text-center">
                                    <i class="fa fa-calendar"></i>
                                    <h4><?php echo date('d', strtotime($value->created)); ?></h4>
                                    <h5><?php echo date('M', strtotime($value->created)); ?></h5>
                                </div>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="media-head-warp clearfix">
                                <h4 class="media-heading"><span>Posted By : </span> <?php
                                    if (!empty($user_info->first_name)) {
                                        echo $user_info->first_name . ' ' . $user_info->last_name;
                                    }
                                    ?></h4>
                            </div>
                            <p>
                                <?php
                                if (!empty($value->message)) {
                                    echo $value->message;
                                }
                                ?>
                            </p>
                        </div>
                    </div>

                </div>
            <?php
                }
              }
            } else {
                ?>
                
            <!--review block-->
        </div>
        <div class="clearfix"></div><br>
            <div class="col-md-9 z-depth-1">
                <div class="media">
                    <h4 style="padding:7px;"> No Review Found </h4>  
                </div>
            </div>
        <?php } ?>
        <div class="clearfix"></div><br>
    </div>
</div>
