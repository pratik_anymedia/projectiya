<div class="page-title">
    <div class="container" >
        <?php  if(!empty($page)) { ?>
            
            <div class="col-md-10"><h2 class="" ><?php echo ucwords($page->post_title); ?></h2></div>
            <div class="col-md-10">
              <?php echo html_entity_decode($page->post_content);?>
            </div>
         <?php } ?>
    </div>
  </div>
