
<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left">User</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label"><a href="<?php echo base_url(); ?>user/profile"> <i class="fa fa-user"></i> Profile</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>  


<div class="container">
    <br>
    <div class="row">
        <div class="col-md-9 col-sm-9">
            <?php echo msg_alert_frontend(); ?>
            <div class="row">
                <div class="col-md-3">
                    <ul id="myTab" class="nav nav-stacked-left">
                        <li <?php if (!empty($_POST['general_info'])) {
                echo 'class="active"';
            } ?>  <?php if (!$_POST) {
                echo 'class="active"';
            } ?>><a href="#home4" data-toggle="tab">General Information</a></li>
                        <li <?php if (!empty($_POST['academic_info'])) {
                echo 'class="active"';
            } ?>  ><a href="#profile4" data-toggle="tab">Academic Information</a></li>
                        <li <?php if (!empty($_POST['bank_detail'])) {
                echo 'class="active"';
            } ?>  ><a href="#dropdown7" data-toggle="tab">Payment Detail</a></li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade in <?php if (!empty($_POST['general_info'])) {
                echo 'active';
            } ?> <?php if (!$_POST) {
                echo 'active';
            } ?>" id="home4">
<?php echo form_open_multipart(current_url()); ?>

                            <div class="form-group">
                                <label for="inputEmail3" class="control-label">First Name<span class="men">*</span></label>
                                <input type="text" class="form-control" name="first_name"  id="first_name" placeholder="First Name" value="<?php if (!empty($customer->first_name)) echo $customer->first_name; ?>">
                                <?php echo form_error('first_name'); ?><span style="color:red;" id="fn_error"></span> 
                            </div> 

                            <div class="form-group">
                                <label for="inputEmail3" class="control-label">Last Name <span class="men">*</span></label>
                                <input type="text" class="form-control" name="last_name"  id="last_name" placeholder="Last Name" value="<?php if (!empty($customer->last_name)) echo $customer->last_name; ?>">
                                <?php echo form_error('last_name'); ?><span style="color:red;" id="ln_error"></span>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword3" class="control-label"> Email <span class="men">*</span></label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?php if (!empty($customer->email)) echo $customer->email; ?>">
                                    <?php echo form_error('email'); ?><span style="color:red;" id="em_error"></span>
                            </div> 

                            <div class="form-group">
                                <label for="inputPassword3" class=" control-label"> Address 1<span class="men">*</span></label>
                                <textarea name="address"  id="address"  class="form-control"><?php if (!empty($customer->address)) echo $customer->address; ?></textarea>
                                    <?php echo form_error('address'); ?>
                                <span style="color:red;" id="ad_error"></span>    
                            </div> 

                            <div class="form-group">
                                <label for="inputPassword3" class="control-label"> Address 2 </label>
                                <textarea name="address1"  id="address1"  class="form-control"><?php if (!empty($customer->address1)) echo $customer->address1; ?></textarea>
                                        <?php echo form_error('address1'); ?><span style="color:red;" id="ad1_error"></span>
                            </div> 

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputPassword3" class="control-label">City <span class="men">*</span></label>         
                                        <input type="text" class="form-control" name="city"  id="city" placeholder="City" value="<?php if (!empty($customer->city)) echo $customer->city; ?>">
                                            <?php echo form_error('city'); ?>
                                        <span style="color:red;" id="ct_error"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputPassword3" class="control-label">State<span class="men">*</span></label>         
                                        <input type="text" class="form-control" name="state"  id="state" placeholder="State" value="<?php if (!empty($customer->state)) echo $customer->state; ?>">
                                            <?php echo form_error('state'); ?>
                                        <span style="color:red;" id="ct_error"></span>
                                    </div>
                                </div>

                            </div> 

                            <div class="form-group">
                                <label for="inputPassword3" class="control-label">Mobile No.<span class="men">*</span></label>         
                                <input type="text" class="form-control" name="mobile" maxlength="15" id="mobile" placeholder="Mobile" value="<?php if (!empty($customer->phone)) echo $customer->phone; ?>">
                                <?php echo form_error('mobile'); ?><span style="color:red;" id="ct_error"></span>      
                            </div>  

                            <div class="form-group">
                                <label for="inputPassword3" class="control-label">Upload Resume </label>         
                                <input type="file"  name="resume" >
<?php echo form_error('resume'); ?><span style="color:red;" id="ct_error"></span>      
                            </div> 

                            <div class="form-group">
                                <label for="inputPassword3" class="control-label">Zip Code <span class="men">*</span></label>
                                <input type="text" class="form-control" maxlength="6" id="zip_code" maxlength="8"  name="zip_code" value="<?php if (!empty($customer->zip_code)) echo $customer->zip_code; ?>"  id="zip_code" placeholder="Zip Code">
                                    <?php echo form_error('zip_code'); ?> 
                                <span style="color:red;" id="zc_error"></span> 
                            </div>


                            <input type="submit" name="general_info" value="Update" class="btn btn-theme ">
                            <?php echo form_close(); ?>

                        </div>
                        <div class="tab-pane fade <?php if (!empty($_POST['academic_info'])) {
                                echo 'active in';
                            } ?>" id="profile4">
                            <?php echo form_open(current_url()); ?>
                            <h3>High School (10th)</h3>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="exampleInputEmail1">School Name</label>
                                        <input type="text" name="high_school_name" value="<?php if (!empty($customer_academy_info->high_school_name)) echo $customer_academy_info->high_school_name;
                                            else echo set_value('high_school_name'); ?>" class="form-control" placeholder="School Name">
                                            <?php echo form_error('high_school_name'); ?>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="exampleInputPassword1">City</label>
                                        <input type="text" class="form-control"   value="<?php if (!empty($customer_academy_info->high_school_city)) echo $customer_academy_info->high_school_city;
                                            else echo set_value('high_school_city'); ?>" name="high_school_city"  placeholder="School City">
                                        <?php echo form_error('high_school_city'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Percentage</label>
                                <input type="text" class="form-control" name="high_school_percentage"   value="<?php if (!empty($customer_academy_info->high_school_percentage)) echo $customer_academy_info->high_school_percentage;
                                        else echo set_value('high_school_percentage'); ?>"  placeholder="10th %">
                                    <?php echo form_error('high_school_percentage'); ?>
                            </div>

                            <h3>Higher Secondary School (10+2 th)</h3>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="exampleInputEmail1">School Name</label>
                                        <input type="text" class="form-control" value="<?php if (!empty($customer_academy_info->high_secondary_school_name)) echo $customer_academy_info->high_secondary_school_name;
                                            else echo set_value('high_secondary_school_name'); ?>"  name="high_secondary_school_name"  placeholder="School Name">
                                            <?php echo form_error('high_secondary_school_name'); ?>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="exampleInputPassword1">City</label>
                                        <input type="text" class="form-control" value="<?php if (!empty($customer_academy_info->high_secondary_school_city)) echo $customer_academy_info->high_secondary_school_city;
                                            else echo set_value('high_secondary_school_city'); ?>" name="high_secondary_school_city"  placeholder="City">
                                            <?php echo form_error('high_secondary_school_city'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Percentage</label>
                                <input type="text" class="form-control"  name="high_secondary_school_percentage" value="<?php if (!empty($customer_academy_info->high_secondary_school_percentage)) echo $customer_academy_info->high_secondary_school_percentage;
                                    else echo set_value('high_secondary_school_percentage'); ?>"  placeholder="12th %">
                                    <?php echo form_error('high_secondary_school_percentage'); ?>
                            </div> 

                            <h3> Graduation / Deploma </h3>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="exampleInputEmail1"> Collage / Institude Name</label>
                                        <input type="text" class="form-control" name="graduation_institude_name" value="<?php if (!empty($customer_academy_info->graduation_institude_name)) echo $customer_academy_info->graduation_institude_name;
                                            else echo set_value('graduation_institude_name') ?>"  placeholder="Institude Name">
                                            <?php echo form_error('graduation_institude_name'); ?>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="exampleInputPassword1">City</label>
                                        <input type="text" class="form-control" value="<?php if (!empty($customer_academy_info->graduation_institude_city)) echo $customer_academy_info->graduation_institude_city;
                                            else echo set_value('graduation_institude_city') ?>" name="graduation_institude_city" placeholder="City">     
                                <?php echo form_error('graduation_institude_city'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="exampleInputEmail1">Course Name</label>
                                        <input type="text" class="form-control" name="graduation_course_name"  value="<?php if (!empty($customer_academy_info->graduation_course_name)) echo $customer_academy_info->graduation_course_name;
                                else echo set_value('graduation_course_name'); ?>"  placeholder="Course Name">
                                        <?php echo form_error('graduation_course_name'); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="exampleInputPassword1">Branch / Stream / Field </label>
                                        <input type="text" class="form-control" name="graduation_branch_name"  value="<?php if (!empty($customer_academy_info->graduation_branch_name)) echo $customer_academy_info->graduation_branch_name;
                                        else echo set_value('graduation_branch_name'); ?>"   placeholder="Branch / Stream">     
                                            <?php echo form_error('graduation_branch_name'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Aggregate Percentage</label>
                                <input type="text" class="form-control" name="graduation_aggr_percentage"  value="<?php if (!empty($customer_academy_info->graduation_aggr_percentage)) echo $customer_academy_info->graduation_aggr_percentage;
                                else echo set_value('graduation_aggr_percentage'); ?>"  placeholder="Percentage %">
                                        <?php echo form_error('graduation_aggr_percentage'); ?>
                            </div>   

                            <h3>Post Graduation</h3>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="exampleInputEmail1"> Collage / institude Name</label>
                                        <input type="text" class="form-control" name="post_graduation_institude_name"  value="<?php if (!empty($customer_academy_info->post_graduation_institude_name)) echo $customer_academy_info->post_graduation_institude_name;
                                        else echo set_value('post_graduation_institude_name'); ?>" placeholder="institude Name">
                                            <?php echo form_error('post_graduation_institude_name'); ?>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="exampleInputPassword1">City</label>
                                        <input type="text" class="form-control" name="post_graduation_city"  value="<?php if (!empty($customer_academy_info->post_graduation_city)) echo $customer_academy_info->post_graduation_city;
                                            else echo set_value('post_graduation_city'); ?>"  placeholder="City">     
                                            <?php echo form_error('post_graduation_city'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="exampleInputEmail1">Course Name</label>
                                        <input type="text" class="form-control" name="post_graduation_course_name"  value="<?php if (!empty($customer_academy_info->post_graduation_course_name)) echo $customer_academy_info->post_graduation_course_name;
                                            else echo set_value('post_graduation_course_name'); ?>"   placeholder="Course Name">
                                            <?php echo form_error('post_graduation_course_name'); ?> 
                                    </div>
                                    <div class="col-md-6">
                                        <label for="exampleInputPassword1">Branch /stream / field </label>
                                        <input type="text" class="form-control" name="post_graduation_branch_name"  value="<?php if (!empty($customer_academy_info->post_graduation_branch_name)) echo $customer_academy_info->post_graduation_branch_name;
                                            else echo set_value('post_graduation_branch_name'); ?>" placeholder="Branch Name">     
                                            <?php echo form_error('post_graduation_branch_name'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Aggregate Percentage</label>
                                <input type="text" class="form-control" name="post_graduation_aggr_percentage" value="<?php if (!empty($customer_academy_info->post_graduation_aggr_percentage)) echo $customer_academy_info->post_graduation_aggr_percentage;
                                    else echo set_value('post_graduation_aggr_percentage'); ?>" placeholder="Percentage %">
                                    <?php echo form_error('post_graduation_aggr_percentage'); ?>
                            </div>

                            <h3>Other Information</h3>
                            <div class="form-group">
                                <label for="exampleInputFile">Other Cource information</label>
                                <textarea  class="form-control"  name="other_course_info" placeholder="Other Course Information"><?php if (!empty($customer_academy_info->other_course_info)) echo $customer_academy_info->other_course_info;
                                    else echo set_value('other_course_info'); ?></textarea>
                                    <?php echo form_error('other_course_info'); ?>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Project Done (in Academy/institude/)</label>
                                <textarea  class="form-control" name="project_done_info"  placeholder="Academy Porject Information"><?php if (!empty($customer_academy_info->project_done_info)) echo $customer_academy_info->project_done_info;
                                    else echo set_value('project_done_info'); ?></textarea>
                                    <?php echo form_error('project_done_info'); ?>
                            </div> 
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="exampleInputFile">Skills</label>
                                        <textarea name="skills" class="form-control" placeholder="Skills"><?php if (!empty($customer_academy_info->skills)) echo $customer_academy_info->skills;
                                        else echo set_value('skills'); ?></textarea>
                                <?php echo form_error('skills'); ?>
                                    </div> 
                                    <div class="col-md-4">
                                        <label for="exampleInputFile">Experience (in year)</label>
                                        <input type="text" class="form-control" name="total_experience" value="<?php if (!empty($customer_academy_info->total_experience)) echo $customer_academy_info->total_experience;
                                else echo set_value('total_experience'); ?>"  placeholder="Total Experience">
                                    <?php echo form_error('total_experience'); ?>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" name="academic_info" value="Update" class="btn btn-theme">Update</button>
                            </form>
                        </div>
                        <div class="tab-pane fade <?php if (!empty($_POST['bank_detail'])) {
                                echo 'active in';
                            } ?>" id="dropdown7">
                            <?php echo form_open(current_url()); ?>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Bank Name</label>
                                <input type="text"  class="form-control" value="<?php if (!empty($customer->bank_name)) echo $customer->bank_name;
                                    else echo set_value('bank_name'); ?>"  name="bank_name" placeholder="Bank Name">
                                    <?php echo form_error('bank_name'); ?>                    
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Acc No.</label>
                                <input type="text" class="form-control" name="bank_acc_no" value="<?php if (!empty($customer->bank_account_no)) echo $customer->bank_account_no;
                                    else echo set_value('bank_acc_no'); ?>" placeholder="Account No.">
                                    <?php echo form_error('bank_name'); ?> 
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Branch</label>
                                <input type="text" class="form-control" name="bank_branch" value="<?php if (!empty($customer->bank_branch)) echo $customer->bank_branch;
                                    else echo set_value('bank_branch'); ?>" placeholder="Branch">
                                    <?php echo form_error('bank_branch'); ?> 
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">IFSC Code</label>
                                <input type="text" class="form-control" name="bank_ifsc_code" value="<?php if (!empty($customer->bank_ifsc_code)) echo $customer->bank_ifsc_code;
                                    else echo set_value('bank_ifsc_code'); ?>"  placeholder="IFSC Code">
                                    <?php echo form_error('bank_ifsc_code'); ?> 
                            </div> 
                            <div class="form-group">
                                <label for="exampleInputFile">City</label>
                                <input type="text"  value="<?php if (!empty($customer->bank_city)) echo $customer->bank_city;
                                    else echo set_value('bank_city'); ?>"  name="bank_city"  class="form-control" id="exampleInputPassword1" placeholder="City">
                                    <?php echo form_error('bank_city'); ?> 
                            </div>
                            <button type="submit" name="bank_detail" value="Update" class="btn btn-theme btn-primary">Update</button>
                            </form>
                        </div>
                        <!--   <div class="tab-pane fade" id="dropdown8">
                            <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
                          </div> -->
                    </div>

                </div>
            </div> 
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-md-3 col-sm-3" id="sidebar">   
<?php include('sidebar.php'); ?>
        </div><!--/.sidebar-offcanvas-->

    </div><!--/.sidebar-offcanvas-->
    <br>
</div>