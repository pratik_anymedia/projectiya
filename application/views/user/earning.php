
<div class="clearfix"></div>
<!--  <div class="page-title">
    <div class="container">
        <h2>  <i class="fa fa-user"></i> Total Earning  <i class="fa fa-rupee"></i> <?php echo $totalEarning; ?></h2>
      <ul class="breadcrumb pull-right">
        <li><a href="#">Uploaded Projects</a></li>
         <li class="active"><a href="#">Description</a></li> 
      </ul>
    </div>
  </div>-->
    <div class="page-wrapper">
        <div class="container">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left"><i class="fa fa-user"></i> Total Earning <i class="fa fa-rupee"></i> <?php echo $totalEarning; ?></h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label"><a href="<?php echo base_url(); ?>user/upload_project"> <i class="fa fa-user"></i> Uploaded Projects</a></li>
                    </ul>
                </div>    
            </header>
        </div>
    </div>  


   <?php 
          if(!empty($_SERVER['QUERY_STRING']))
            $QUERY_STRING = "0?".$_SERVER['QUERY_STRING'];
          else
            $QUERY_STRING ='';
        ?>

  <div class="container">
  <br>
    <div class="row">

 <?php //echo msg_alert_frontend(); ?>

         
      <div class="col-md-9 col-sm-9 table-responsive">
          <table class="table users-table table-condensed table-hover" >
           <thead>
            <tr>
              <th>#</th>
                <th>Project Title</th>
                <th>Buyer Name</th>
                <th>Price</th>
                <th class="hidden-phone">Order Date</th>
                
            </tr>
         </thead>
         <tbody>
         <?php 
            //print_r($allEarning);
         if(!empty($allEarning)):
              $i=0;
                foreach($allEarning as $value){ $i++; ?>
                <tr class="gradeX">
                    <td><?php echo $i."." ;?></td>
                    <td><?php echo character_limiter($value->title,30); ?></td>
                    <td><?php echo $value->order_user; ?></td>
                    <td class="to_hide_phone"><?php echo $value->price; ?></td> 
                    <td><?php echo date('d-m-Y',strtotime($value->order_date)); ?></td>
                </tr>

                <?php } ?>

            <?php else: ?>
              <tr>
                <th colspan="7"> <center>No Projects found.</center></th>
              </tr>
            <?php endif; ?>

</tbody>
</table>
       <br>
      </div><!--/.col-xs-12.col-sm-9-->
        <div class="col-md-3 col-sm-3" id="sidebar">
             <?php include('sidebar.php'); ?>
        </div><!--/.sidebar-offcanvas-->
      </div>
  </div>