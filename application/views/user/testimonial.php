<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left"><i class="fa fa-user"></i> Testimonial</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label">
                        Write Testimonial
                    </li>
                </ul>
            </div>    
        </header>
    </div>
</div>

  <div class="container">
  <br>
    <div class="row">
      <div class="col-md-9 col-sm-9">
        <?php echo msg_alert_frontend(); ?>

        <div class=" col-md-12">
          <h3>Write Testimonial</h3>
          <?php echo form_open(); ?>
            
          
          <div class="form-group">
            <div class="col-md-12">   
              <label class="control-label"><strong>Testimonial</strong></label>
              <textarea cols="100" rows="5" name="testimonial" class="form-control"><?php echo (set_value('description'))?set_value('description'):(isset($userTestimonial->content)?trim($userTestimonial->content):'') ?></textarea>           
              <?php echo form_error('testimonial'); ?>
            </div> 
          </div> 

               <div class="form-actions"> 
                  <div class="col-md-12">
                     <button type="submit" class="btn btn-theme">Submit Testimonial</button> 
                  </div>
                </div> 
                <br> 
             <?php echo form_close(); ?>
             </div>
    
      </div><!--/.col-xs-12.col-sm-9-->
        <div class="col-md-3 col-sm-3" id="sidebar">
             <?php include('sidebar.php'); ?>
        </div><!--/.sidebar-offcanvas-->
      </div>
  </div>