
<div class="clearfix"></div>
    <div class="page-wrapper">
        <div class="container">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left"><i class="fa fa-user"></i> Register</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label"><a href="<?php echo base_url();?>user/register">Registraion</a></li>
                        <li class="active"><a href="<?php echo base_url(); ?>user/login">Login</a></li>
                    </ul>
                </div>   
            </header>
        </div>
    </div>

  <div class="container">

	<div class="register-warp">
	  <div class="col-md-7">
	  	<div class="register-inner">
	  	<h3>Register</h3>
	  	<br>
	  	<div  class="contact-form">
	  	  <?php echo form_open(current_url()); ?>
         <div class="form-group row">
          <div class="col-md-4">
            <label for="name">First Name</label>
            <input type="text" name="first_name" value="<?php echo set_value('first_name');  ?>" placeholder="First Name" class="form-control">
          <?php echo form_error('first_name'); ?>
          </div>
          <div class="col-md-4">
            <label for="name">Last Name</label>
            <input type="text" name="last_name" value="<?php echo set_value('last_name');  ?>" placeholder="Last Name" class="form-control">
          <?php echo form_error('last_name'); ?>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-8">
             <label for="name">Email</label>
            <input type="text" name="user_email" value="<?php echo set_value('user_email');  ?>" placeholder="Email" class="form-control">
         <?php echo form_error('user_email'); ?>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-8">
             <label for="name">Password</label>
            <input type="password" name="userpassword" value="<?php echo set_value('password');  ?>" placeholder="Password" class="form-control">
         <?php echo form_error('userpassword'); ?>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-8">
             <label for="name">Confirm Password</label>
            <input type="password" name="con_password" value="<?php echo set_value('con_password');  ?>" placeholder="Confirm Password"  class="form-control">
          <?php echo form_error('con_password'); ?>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-8">
             <label for="name">Mobile No</label>
            <input type="text" name="mobile_no" value="<?php echo set_value('mobile_no') ?>" placeholder="Mobail No." class="form-control">
            <?php echo form_error('mobile_no'); ?>
          </div>
        </div><br>
        
        <div class="form-group row">
            <div class="col-md-8">
                <input type="submit" value="Register" class="btn btn-theme">
            </div> 
        </div> 
       <?php echo form_close(); ?>
        </div>

	  </div>
	  </div>

	  <div class="col-md-5">

	  	<h3>Login</h3>
	  	<br>
	  	<div  class="contact-form">
      <?php echo msg_alert_frontend(); ?>
	    <?php echo form_open(base_url('user/login')); ?>
         <div class="form-group row">
          <div class="col-md-10">
            <label for="name">Email</label>
            <input type="text" name="email" value="<?php echo set_value('email'); ?>" placeholder="Email" class="form-control">
         <?php echo form_error('email'); ?>
          </div>
          </div>
         <div class="form-group row">
          <div class="col-md-10">
            <label for="name">Password</label>
            <input type="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="Password" class="form-control">
         <?php echo form_error('password'); ?>
          </div>
        </div>
        <div class="form-group row">
            <div class="col-md-8">
                <p><a href="#">Forgot Password</a></p>
                <input type="submit" value="Login" class="btn btn-theme">
            </div>
        </div>
        <?php echo form_close(); ?>
      </div>

	  </div>

	</div>

</div>