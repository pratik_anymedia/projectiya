<style type="text/css">
  #forgot_password_form{   
     margin: 100px 100px;
  }
</style>
<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left"><i class="fa fa-user"></i> User</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label"><a href="#">Forgot Password</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>

  <div class="container">
  <br>
    <div class="row">
      <div class="col-md-6 col-sm-6">
       <h2>Forget Your Password</h2> 
       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
       cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
       proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>  
       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
       cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
       proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
         
      <br>
      </div><!--/.col-xs-12.col-sm-9-->
        <div class="col-md-6 col-sm-6 text-center" id="sidebar">
          <?php echo msg_alert_frontend(); ?>
            <?php echo form_open(current_url(),array('id'=>'forgot_password_form')); ?>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email Address <small>(which is include our services )</small></label>
                      <input type="text"  class="form-control" value="<?php echo set_value('email'); ?>"  name="email" placeholder="Register User Email">
                      <?php echo form_error('email'); ?>                    
                    </div>
                    
                    <button type="submit" name="submit" value="send_request" class="btn btn-default btn-primary">Send Request</button>
                </form>
        </div><!--/.sidebar-offcanvas-->
      </div>
  </div>