<style type="text/css" media="screen">

    #tab_images_uploader_container{

      margin: 80px 215px;
    }
    .myrow{
      margin-top:40px;
     
    }
  


</style>
<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left">User</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label"><a href="<?php echo base_url(); ?>user/upload_project"> <i class="fa fa-user"></i> Upload Project</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>  

  <div class="container">
  <br>
    <div class="row">
      <div class="col-md-9 col-sm-9">
        <?php echo msg_alert_frontend(); ?>

        <ul id="myTab" class="nav nav-tabs">
          <li <?php if(!$this->session->userdata('project_file')){ echo 'class="active"'; } ?> ><a href="#home2" data-toggle="tab"> Upload Zip </a></li>
          <li <?php if($this->session->userdata('project_file')){ echo 'class="active"'; } ?> ><a href="#profile2" data-toggle="tab">Project General Info</a></li>
        </ul>
        <div id="myTabContent" class="tab-content">


       
          <div class="tab-pane fade <?php if(!$this->session->userdata('project_file')){ echo 'in active'; } ?> " id="home2">  
            <?php  ?>

            <?php if(!$this->session->userdata('project_file')){ ?>
            <div  class="col-md-12 text-center">
            <h3>Upload Files </h3>
            <p>Please upload your project source file in zip or rar format .</p>
             <div id="mulitplefileuploader">Upload Project zip / rar</div>
             <div id="status"></div>
            </div>


            <?php } else { ?>

            
            <div  class="col-md-12 text-center">
              <h3>Upload Files </h3>
              <p>Your source file uploading process has been completed </p>
              <span>if you want to Reupload files to <a href="<?php echo base_url('user/reupload_file/'); ?>" class="btn btn-xs btn-primary" title="Click Here"> Click Here</a>  </span>
            </div>

           <?php  

              //$test =  $this->session->userdata('project_file'); print_r($test); echo $test['file_path'];
               }


           ?>
           <br>
          </div>
          <div class="tab-pane fade <?php if($this->session->userdata('project_file')){ echo 'in active'; } ?>" id="profile2">
              
          <div class=" col-md-12">
          <h3>Write Project general Information</h3>
          <?php echo form_open_multipart(current_url()); ?>
            
          
          <div class="form-group">
            <div class="col-md-12">   
              <label class="control-label"><strong>Project Title</strong></label>                                    
              <input type="text" placeholder="Project Title" class="form-control" name="title" value="<?php echo set_value('title') ?>">
              <?php echo form_error('title'); ?>
            </div> 
          </div>

          <div class="form-group">
            <div class="col-md-12">   
              <label class="control-label"><strong>Short Description</strong></label>                                    
              <textarea placeholder="Short Description" class="form-control" name="short_description"><?php echo set_value('short_description') ?></textarea>
              <?php echo form_error('short_description'); ?>
            </div> 
          </div>

          <div class="form-group">
            <div class="col-md-12">   
              <label class="control-label"><strong>Description</strong></label>
              <textarea cols="100" rows="5" name="description" class="tinymce_edittor form-control"><?php echo set_value('description') ?></textarea>           
              <?php echo form_error('description'); ?>
            </div> 
          </div> 

          <div class="form-group">
            <div class="col-md-12"> 
              <label class="control-label"><strong>Technology</strong></label>                                     
              <select class="form-control" id="technology" name="technology[]"  multiple style="width:100%">
                <option value="">Select Technology</option>
                <?php  if(!empty($technology)):  ?>
                  <?php foreach ($technology as $value){ ?>
                    <option value="<?php  echo $value->id;  ?>"  ><?php  if(!empty($value->technology)) echo $value->technology;  ?></option>
                  <?php } ?>
                <?php endif; ?> 
              </select>                                 
              <?php echo form_error('technology[]'); ?>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-12"> 
              <label class="control-label"><strong>Courses</strong></label>                                     
              <select class="form-control" id="course" name="course[]"  multiple style="width:100%">
                <option value="">Select Course</option>
                <?php  if(!empty($courses)):  ?>
                  <?php foreach ($courses as $value){ ?>
                    <option value="<?php  echo $value->id;  ?>"  ><?php  if(!empty($value->course)) echo $value->course;  ?></option>
                  <?php } ?>
                <?php endif; ?> 
              </select>                                 
              <?php echo form_error('course[]'); ?>
            </div>
          </div>

            <div class="form-group">
            <div class="col-md-4">   
                      <label class="control-label"><strong>Backend Technology</strong></label>                                    
              <input placeholder="Backend Technology" class="form-control" value="<?php echo set_value('backend_technology') ?>" name="backend_technology">
              <?php echo form_error('backend_technology'); ?>
                  </div> 


                  <div class="col-md-4">   
                      <label class="control-label"><strong>Forms / Pages</strong></label>                                    
              <input placeholder="Forms / Pages" class="form-control" value="<?php echo set_value('form_pages') ?>" name="form_pages">
              <?php echo form_error('form_pages'); ?>
                  </div> 


                  <div class="col-md-4">   
                      <label class="control-label"><strong>No. Of Tables</strong></label>                                    
              <input placeholder="No.Of Tables" class="form-control" value="<?php echo set_value('no_of_tables') ?>" name="no_of_tables">
              <?php echo form_error('no_of_tables'); ?>
                  </div> 
                </div>
               <div class="clearfix"></div>


                <div class="form-group">
                 <div class="col-md-12">   
                    <label class="control-label"><strong>Price</strong></label>                                    
                    <input type="text" placeholder="Price" class="form-control" name="price" value="<?php echo set_value('price'); ?>">
                    <?php echo form_error('price'); ?>
                  </div> 
                </div>


                <div class="form-group">
                  <div class="col-sm-12">
                  <label class="control-label"><strong>Featured Image</strong></label>
                    <input type="file" name="featured_image">
                      <?php echo form_error('featured_image'); ?>
                 
                    <small>(file_type: jpeg,jpg,png,gif)</small> 
                  </div>
                </div> 

                <div class="form-group">
                  <div class="col-sm-12">
                  <label class="control-label"><strong>Document File</strong></label>
                    <input type="file" name="doc_file">
                      <?php echo form_error('doc_file'); ?>
                    
                    <small>(file_type:pdf,doc,docx,ppt)</small> 
                  </div>
                </div>

              <div class="form-group">
                  <div class="col-md-12"> 
                      <label class="control-label"><strong>video key(id)</strong></label>                                     
              <a style="text-decoration:none;" class="input-group demo-input-group">
                <span class="input-group-addon">https://www.youtube.com/watch?v=</span>
                <input class="form-control" type="text" name='video_url'  placeholder="video key (id)">
              </a>                                
              <?php //echo form_error('status'); ?>
                </div>
              </div> 

               <div class="form-actions"> 
                  <div class="col-md-12">
                     <button type="submit" class="btn btn-theme">Submit Project</button> 
                  </div>
                </div> 
                <br> 
             <?php echo form_close(); ?>
             </div>
          </div>
         
        </div>
    
      </div><!--/.col-xs-12.col-sm-9-->
        <div class="col-md-3 col-sm-3" id="sidebar">
             <?php include('sidebar.php'); ?>
        </div><!--/.sidebar-offcanvas-->
      </div>
  </div>