
<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left">Request For Payment</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label">
                        <a href="<?php echo base_url(); ?>user/upload_project">Uploaded Projects</a>
                    </li>
                </ul>
            </div>    
        </header>
    </div>
</div>
<?php
if (!empty($_SERVER['QUERY_STRING']))
    $QUERY_STRING = "0?" . $_SERVER['QUERY_STRING'];
else
    $QUERY_STRING = '';
?>

<div class="container">
    <br>
    <div class="row">

        <div class="col-md-2 col-sm-2">
        </div>    
        <div class="col-md-4 col-sm-4">
            <?php echo msg_alert_frontend(); ?>
            <form class="form-horizontal" action="<?php echo base_url('user/requestForPayment') ?>" method="post" role="form">

                <div class="form-group">
                    <label for="withdrawal">Account Number :  <?php echo $userDetails->bank_account_no; ?></label> 

                </div>   
                <div class="form-group">
                    Total Balance : <i class="fa fa-rupee"></i> <label for="withdrawal" id="totalBalance"><?php echo $totalEarning; ?></label> 

                </div>  
                <div class="form-group">
                    <label for="withdrawal">Withdrawal Amount</label> 
                    <input  class="form-control" type="text" style="width:100%;" name="withdrawal_amount" id="txt_name"  placeholder="Withdrawal Amount">
                    <?php echo form_error('withdrawal_amount'); ?> 
                </div> 

                <div class="form-group">
                    <label for="withdrawal">Current Balance</label> 
                    <i class="fa fa-rupee"></i><label for="withdrawal" id="currentBalance"> 0.00</label> 
                </div> 

                <button class="btn btn-primary pull-right" type="submit">Send</button>
            </form>
            <br>
        </div><!--/.col-xs-12.col-sm-9-->
        <div class="col-md-3 col-sm-3">
        </div>
        <div class="col-md-3 col-sm-3" id="sidebar">
            <?php include('sidebar.php'); ?>
        </div><!--/.sidebar-offcanvas-->
    </div>
</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.3.min.js" ></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#txt_name").keyup(function () {
            var userTyped = $(this).val();
            var totalBalance = $('#totalBalance').text()
            var currentBalance = totalBalance - userTyped;

            if (currentBalance > 0) {
                $('#currentBalance').text(currentBalance);
            } else {

                 $(this).val(totalBalance);

                $('#currentBalance').text("0.00");
            }

        });


    })
</script>