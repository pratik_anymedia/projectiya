
<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left"><i class="fa fa-user"></i> User</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label"><a href="#">Project Review Edit</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>
  <div class="container">
  <br>
    <div class="row">
      <div class="col-md-9 col-sm-9">
         <?php echo msg_alert_frontend(); ?>
          <?php echo form_open(current_url()); ?>
              <div class="form-group">
                <label for="exampleInputEmail1">Project title</label>
                <input type="text"  class="form-control"  readonly="readonly" value="<?php echo character_limiter(get_project_title($reviews->project_id),50); ?>"  name="projet_title" placeholder="Project Title ">              
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">User Email</label>
                <input type="text" class="form-control" name="bank_branch"  readonly="readonly" value="<?php  if(!empty($reviews->user_email)) echo $reviews->user_email;  ?>" placeholder="User Email">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Review Message</label>
                <textarea cols="100" rows="5" class="form-control" name="review_message" ><?php if(!empty($reviews->message)) echo $reviews->message;  ?></textarea>
                <?php echo form_error('review_message'); ?> 
              </div>
              <button type="submit" name="bank_detail" value="Update" class="btn btn-default btn-primary">Update</button>
          </form>
      <br>
      </div><!--/.col-xs-12.col-sm-9-->
        <div class="col-md-3 col-sm-3" id="sidebar">
            <?php include('sidebar.php'); ?>
        </div><!--/.sidebar-offcanvas-->
      </div>
  </div>