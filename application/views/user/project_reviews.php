
<div class="clearfix"></div>
    <div class="page-wrapper">
        <div class="container">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left"><i class="fa fa-user"></i> User</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label"><a href="<?php echo base_url(); ?>user/project_reviews"> <i class="fa fa-user"></i> Project Reviews</a></li>
                    </ul>
                </div>    
            </header>
        </div>
    </div>  
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-9 table-responsive">
         <?php echo msg_alert_frontend(); ?>
          <table class="table users-table table-condensed table-hover" >
           <thead>
            <tr>
               <th width="5%"  >#</th>
               <th width="30%" >Project Title</th>
               <th width="15%" >Username</th>
               <th width="30%" >Review</th>
               <th  width="15%">Action</th>
            </tr>
         </thead>
         <tbody>
         <?php $i=0; if(!empty($reviews)){  foreach($reviews as $value){  $i++;   ?>
            <tr>
                <td class="visible-lg"><?php echo $i.'.';?></td>
                <td class="visible-lg"><?php echo character_limiter(get_project_title($value->project_id),50); ?></td>
                <td class="visible-lg"><?php if(!empty($customer->first_name)) echo ucfirst($customer->first_name.' '.$customer->last_name);    ?></td>
                <!-- <td class="visible-lg"><?php //if(!empty($customer->email)) echo $customer->email; ?></td> -->
                <td><?php if(!empty($value->message)) echo character_limiter($value->message,70);    ?></td>
                <td>  
                  <a href="<?php echo base_url('user/project_reviews_edit/'.$value->id); ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                  <a href="<?php echo base_url('user/project_review_delete/'.$value->id); ?>" onclick="return confirm('Do you want to delete it');" class="btn btn-xs btn-warning"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
        <?php  } }else{ ?>
            <tr class="text-center">
              <td colspan="6">No Review Found</td >
            </tr>
        <?php } ?>

</tbody>
</table>
       <br>
      </div><!--/.col-xs-12.col-sm-9-->
        <div class="col-md-3 col-sm-3" id="sidebar">
             <?php include('sidebar.php'); ?>
        </div><!--/.sidebar-offcanvas-->
      </div>
  </div>
  