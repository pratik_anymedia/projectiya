
<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left"><i class="fa fa-user"></i> User</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label"><a href="<?php echo base_url(); ?>user/dashboard"> <i class="fa fa-user"></i> Dashboard</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>  

  <div class="container">
  <br>
    <div class="row">
      <div class="col-md-9 col-sm-9">
          
          <div class="row">
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
               <div class="text-center">
                  <i class="fa fa-book fa-4x"></i>
                </div> 
                <div  class="caption text-center">
                  <h3><?php echo $totalProject; ?></h3>
                  <p>Project</p>
                  <p><a href="<?php echo base_url('user/projects');?>" class="btn btn-theme" role="button">View</a></p>
                </div>
              </div>
            </div>
         

        
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                 <div class="text-center">
                  <i class="fa fa-rupee fa-4x"></i>
                </div> 
                <div class="caption text-center">
                  <h3><?php echo $totalEarning; ?></h3>
                  <p>Earning</p>
                  <p><a href="<?php echo base_url('user/earning'); ?>" class="btn btn-theme" role="button">View</a></p>
                </div>
              </div>
            </div>
       
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <div class="text-center">
                  <i class="fa fa-shopping-cart fa-4x"></i>
                </div>  
                <div class="caption text-center">
                  <h3><?php echo $totalBuy; ?></h3>
                  <p>Buy</p>
                  <p><a href="#" class="btn btn-theme" role="button">View</a></p>
                </div>
              </div>
            </div>
          </div> 
      </div><!--/.col-xs-12.col-sm-9-->
      <div class="col-md-3 col-sm-3" id="sidebar">
          <?php include('sidebar.php'); ?>
      </div><!--/.sidebar-offcanvas-->
      </div><!--/.sidebar-offcanvas-->
  </div>