
<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left"> <i class="fa fa-user"></i> User</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label">
                        <a href="<?php echo base_url(); ?>user/change_password">Change Password</a>
                    </li>
                </ul>
            </div>    
        </header>
    </div>
</div>

  <div class="container">
      <br>
      <br>
      <div class="col-md-9 col-sm-9">
         <h2> <i class="fa fa-list"></i> Change Password  </h2>

         <hr>
        <?php echo msg_alert_frontend(); ?> 
         <?php echo form_open(current_url()); ?>
         
          <div class="form-group">

                <label for="inputEmail3" class="control-label">Old Password</label>

       

                  <input type="password" name="oldpassword" id="oldpassword"  placeholder="Old Password" class="form-control">                                 

                  <?php echo form_error('oldpassword'); ?>

              
              </div>



              <div class="form-group">

                <label for="inputPassword3" class="control-label">New Password</label>

           

                  <input type="password" name="newpassword" id="newpassword"  placeholder="New Password" class="form-control">                                 

                  <?php echo  form_error('newpassword'); ?>

              
              </div>



              <div class="form-group">

                <label for="inputPassword3" class="control-label">Confirm Password</label>

               

                  <input type="password" name="confpassword" id="confpassword"  placeholder="Confirm Password" class="form-control">                                 

                  <?php echo  form_error('confpassword'); ?>


              </div>



       
              <div class="form-group">
                  <button type="submit" class="btn btn-theme btn-info">Update</button>
              </div>
       <?php echo form_close(); ?>

             <br>
             <br>
      </div><!--/.col-xs-12.col-sm-9-->
      <div class="col-md-3 col-sm-3" id="sidebar"> 
           <?php include('sidebar.php'); ?>
      </div><!--/.sidebar-offcanvas-->
  </div>