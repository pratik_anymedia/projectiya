<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Request For Project</h4>
            </div>
            <div class="modal-body">
                <?php if ($this->session->flashdata('req_msg_success')): ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $this->session->flashdata('req_msg_success'); ?>
                    </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('req_msg_error')): ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $this->session->flashdata('req_msg_error'); ?>
                    </div>
                <?php endif; ?>

                <form method="post" action="<?php echo base_url('user/projectRequest'); ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Email Address:</label>
                        <input type="email" name="email" class="form-control" id="recipient-name">
                        <?php echo form_error('email'); ?> 
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Mobile No:</label>
                        <input type="text" name="mobile_no" class="form-control" id="recipient-name">
                        <?php echo form_error('mobile_no'); ?> 
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Project Title:</label>
                        <input type="text" name="project_title"class="form-control" id="recipient-name">
                        <?php echo form_error('project_title'); ?> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Description:</label>
                        <textarea class="form-control" name="description" id="message-text"></textarea>
                        <?php echo form_error('description'); ?> 
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Document:</label>
                        <input type="file" name="document" class="form-control" id="recipient-name">
                        <?php echo form_error('document'); ?> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send Request</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>