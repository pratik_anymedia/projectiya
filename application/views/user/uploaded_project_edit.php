<style type="text/css" media="screen">
  #tab_images_uploader_container{
    margin: 80px 215px;
  }
  .myrow{
    margin-top:40px;
  }
</style>
<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left"><i class="fa fa-user"></i> User</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label"><a href="#"> Upload Project</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>

  <div class="container">
  <br>
    <div class="row">
      <div class="col-md-9 col-sm-9">
        <?php echo msg_alert_frontend(); ?>

        <ul id="myTab" class="nav nav-tabs">
          <li <?php //if(!$this->session->userdata('project_file')){ echo 'class="active"'; } ?> class="active" ><a href="#home2" data-toggle="tab"> Upload Project Edit  </a></li>
         <!--  <li <?php //if($this->session->userdata('project_file')){ echo 'class="active"'; } ?> ><a href="#profile2" data-toggle="tab">Project General Info</a></li> -->
          <!-- <li><a href="#profile3" data-toggle="tab">Project detail</a></li> -->
          <!-- <li class="dropdown">
            <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1">
              <li><a href="#dropdown3" tabindex="-1" data-toggle="tab">@fat</a></li>
              <li><a href="#dropdown4" tabindex="-1" data-toggle="tab">@mdo</a></li>
            </ul>
          </li> -->
        </ul>
        <div id="myTabContent" class="tab-content">


       
          
          <div class="tab-pane fade in active <?php //if($this->session->userdata('project_file')){ echo 'in active'; } ?>" id="profile2">
              
          <div class=" col-md-12">
          <h3>Write Project general Information</h3>
          <?php echo form_open_multipart(current_url()); ?>
            
          
          <div class="form-group">
            <div class="col-md-12">   
                      <label class="control-label"><strong>Project Title</strong></label>                                    
              <input type="text" placeholder="Project Title" class="form-control" name="title" value="<?php if(!empty($project->title))  echo $project->title; else echo set_value('title'); ?>">
              <?php echo form_error('title'); ?>
                  </div> 
                </div>

                <div class="form-group">
            <div class="col-md-12">   
                      <label class="control-label"><strong>Short Description</strong></label>                                    
              <textarea placeholder="Short Description" class="form-control" name="short_description"><?php  if(!empty($project->short_description))  echo $project->short_description; else  echo set_value('short_description') ?></textarea>
              <?php echo form_error('short_description'); ?>
                  </div> 
                </div>

        
                <div class="form-group">
            <div class="col-md-12">   
                      <label class="control-label"><strong>Description</strong></label>
                      <textarea cols="100" rows="5" name="description" class="tinymce_edittor form-control"><?php  if(!empty($project->description))  echo $project->description; else  echo set_value('description') ?></textarea>           
              <?php echo form_error('description'); ?>
                  </div> 
                </div> 
                    <?php if(!empty($project->technology)){ $Array=explode('#,#',trim($project->technology,'#'));  } ?>
              <div class="form-group">
                  <div class="col-md-12"> 
                      <label class="control-label"><strong>Technology</strong></label>                                     
              <select class="form-control" id="technology" name="technology[]"  multiple style="width:100%">
                <option value="">Select Technology</option>
                <?php  if(!empty($technology)):  ?>
                  <?php foreach ($technology as $value){ ?>
                    <option value="<?php  echo $value->id;  ?>" <?php if(!empty($Array)): for($i=0; $i < count($Array); $i++){ if($Array[$i]==$value->id){ echo 'selected="selected"';} } endif;  ?>  ><?php  if(!empty($value->technology)) echo $value->technology;  ?></option>
                  <?php } ?>
                <?php endif; ?> 
              </select>                                 
              <?php echo form_error('technology'); ?>
                  </div>
                </div>
                
                    <?php if(!empty($project->course_type)){ $Array=explode('#,#',trim($project->course_type,'#'));  } ?>
                <div class="form-group">
                  <div class="col-md-12"> 
                      <label class="control-label"><strong>Courses</strong></label>                                     
              <select class="form-control" id="course" name="course[]"  multiple style="width:100%">
                <option value="">Select Course</option>
                <?php  if(!empty($courses)):  ?>
                  <?php foreach ($courses as $value){ ?>
                    <option value="<?php  echo $value->id;  ?>" <?php if(!empty($Array)): for($i=0; $i < count($Array); $i++){ if($Array[$i]==$value->id){ echo 'selected="selected"';} } endif;  ?>   ><?php  if(!empty($value->course)) echo $value->course;  ?></option>
                  <?php } ?>
                <?php endif; ?> 
              </select>                                 
              <?php echo form_error('course'); ?>
                  </div>
                </div>


                <div class="form-group">
                <div class="col-md-12">   
                      <label class="control-label"><strong>Price</strong></label>                                    
              <input type="text" placeholder="Price" class="form-control" name="price" value="<?php if(!empty($project->price))  echo $project->price; else  echo set_value('price'); ?>">
              <?php echo form_error('price'); ?>
                  </div> 
                </div>

                <div class="form-group">
            <div class="col-md-4">   
                      <label class="control-label"><strong>Backend Technology</strong></label>                                    
              <input placeholder="Backend Technology" class="form-control" value="<?php if(!empty($project->backend_technology)){  echo $project->backend_technology;     }  ?>" name="backend_technology">
              <?php echo form_error('backend_technology'); ?>
                  </div> 
                  <div class="col-md-4">   
                      <label class="control-label"><strong>Forms / Pages</strong></label>                                    
              <input placeholder="Forms / Pages" class="form-control" value="<?php if(!empty($project->form_pages)){  echo $project->form_pages; }  ?>" name="form_pages">
              <?php echo form_error('form_pages'); ?>
                  </div> 
                  <div class="col-md-4">   
                      <label class="control-label"><strong>No. Of Tables</strong></label>                                    
              <input placeholder="No.Of Tables" class="form-control" value="<?php if(!empty($project->no_of_tables)){  echo $project->no_of_tables;     }  ?>" name="no_of_tables">
              <?php echo form_error('no_of_tables'); ?>
                  </div> 
                </div>
<div class="clearfix"></div>
<br>
                <div class="form-group">
                  <div class="col-sm-12">
                  <label class="control-label"><strong>Project Source Files</strong></label>
                    <input type="file" name="userfile">
                      <?php echo form_error('userfile'); ?>
                     <?php if(!empty($project->doc_file)){ ?>
                      <a class="btn btn-info" href="<?php echo base_url('user/download_source_file/'.$project->id);   ?>" title="">Download Source File</a>
                    <?php } ?>
                    <small>(file_type: zip,rar,ppt,pptx,docx,doc)</small> 
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                  <label class="control-label"><strong>Featured Image</strong></label>
                    <input type="file" name="featured_image">
                      <?php echo form_error('featured_image'); ?>
                    <?php if(!empty($project->thumb_image)){ ?>
                      <img src="<?php echo base_url(str_replace('./','',$project->thumb_image));   ?>" alt="">  
                    <?php } ?>
                    <small>(file_type: jpeg,jpg,png,gif)</small> 
                  </div>
                </div> 

                <div class="form-group">
                  <div class="col-sm-12">
                  <label class="control-label"><strong>Document File</strong></label>
                    <input type="file" name="doc_file">
                      <?php echo form_error('doc_file'); ?>
                    <?php if(!empty($project->doc_file)){ ?>
                      <a class="btn btn-info" href="<?php echo base_url('user/download_doc_file/'.$project->id);   ?>" title="">Download Document File</a>
              
                    <?php } ?>
                    <small>(file_type:pdf,doc,docx,ppt)</small> 
                  </div>
                </div>

              <div class="form-group">
                  <div class="col-md-12"> 
                      <label class="control-label"><strong>video key(id)</strong></label>                                     
              <a style="text-decoration:none;" class="input-group demo-input-group">
                <span class="input-group-addon">https://www.youtube.com/watch?v=</span>
                <input class="form-control" type="text" name='video_url' value="<?php if(!empty($project->video_url))  echo $project->video_url; else  echo set_value('video_url'); ?>"  placeholder="video key (id)">
              </a>                                
              <?php //echo form_error('status'); ?>
                  </div>
                </div> 


               <div class="form-actions"> 
                  <div class="col-md-12">
                     <button type="submit" class="btn btn-primary">Submit Project</button> 
                  </div>
                </div> 
                <br> 
             <?php echo form_close(); ?>
             </div>
          </div>
         
        </div>
    
      </div><!--/.col-xs-12.col-sm-9-->
        <div class="col-md-3 col-sm-3" id="sidebar">
             <?php include('sidebar.php'); ?>
        </div><!--/.sidebar-offcanvas-->
      </div>
  </div>