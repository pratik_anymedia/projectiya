
<?php 

    $segment2 = $this->uri->segment(2);

    $segment3 = $segment2."/".$this->uri->segment(3);

    if($segment2 == 'dashboard' || $segment2 == '') $dashboard='active'; else $dashboard='';

    if($segment2 == 'products' || $segment3 == 'products/edit') $myproduct='active'; else $myproduct='';

    if($segment3 == 'customers/add') $cust='active'; else $cust='';

 ?>
      <div class="footer">
                © 2015 <a href="#">ProjectWala.com</a>
              </div>

            </div> <!-- /.content -->

            <!-- .right-sidebar -->
            <div class="right-sidebar right-sidebar-hidden">
              <div class="right-sidebar-holder">

                <!-- @Demo part only The part from here can be removed till end of the @demo  -->
                <a href="screens.html" class="btn btn-danger btn-block">Logout </a>


                <h4 class="page-header text-primary text-center">
                  Theme Options
                  <a  href="#"  class="theme-panel-close text-primary pull-right"><strong><i class="fa fa-times"></i></strong></a>
                </h4>

                <ul class="list-group theme-options">
                  <li class="list-group-item" href="#"> 
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="fixedNavbar" value=""> Fixed Top Navbar
                      </label>
                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="fixedNavbarBottom" value=""> Fixed Bottom Navbar
                      </label>
                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="boxed" value=""> Boxed Version
                      </label>
                    </div>

                    <div class="form-group backgroundImage hidden" >
                      <label class="control-label">Paste Image Url and then hit enter</label>
                      <input type="text" class="form-control" id="backgroundImageUrl" />
                    </div>
        <!-- 
        <div class="checkbox">
          <label>
            <input type="checkbox" id="responsive" value=""> Disable Responsiveness
          </label>
        </div> 
      -->     
    </li>
    <li class="list-group-item" href="#">Predefined Themes
      <ul class="list-inline predefined-themes"> 
        <li><a class="badge" style="background-color:#54b5df" data-color-primary="#54b5df" data-color-secondary="#2f4051" data-color-link="#FFFFFF"> &nbsp; </a></li>
        <li><a class="badge" style="background-color:#d85f5c" data-color-primary="#d85f5c" data-color-secondary="#f0f0f0" data-color-link="#474747"> &nbsp; </a></li>
        <li><a class="badge" style="background-color:#3d4a5d" data-color-primary="#3d4a5d" data-color-secondary="#edf0f1" data-color-link="#777e88"> &nbsp; </a></li>
        <li><a class="badge" style="background-color:#A0B448" data-color-primary="#A0B448" data-color-secondary="#485257" data-color-link="#AFB5AA"> &nbsp; </a></li>
        <li><a class="badge" style="background-color:#2FA2D1" data-color-primary="#2FA2D1" data-color-secondary="#484D51" data-color-link="#A5B1B7"> &nbsp; </a></li>
        <li><a class="badge" style="background-color:#2f343b" data-color-primary="#2f343b" data-color-secondary="#525a65" data-color-link="#FFFFFF"> &nbsp; </a></li>
      </ul>
    </li>
    <li class="list-group-item" href="#">Change Primary Color
      <div class="input-group colorpicker-component bscp" data-color="#54728c" data-color-format="hex" id="colr">
        <span class="input-group-addon"><i style="background-color: #54728c"></i></span>
        <input type="text" value="#54728c" id="primaryColor" readonly class="form-control" />

      </div>
    </li>
    <li class="list-group-item" href="#">Change LeftSidebar Background
      <div class="input-group colorpicker-component bscp" data-color="#f9f9f9" data-color-format="hex" id="Scolr">
        <span class="input-group-addon"><i style="background-color: #f9f9f9"></i></span>
        <input type="text" value="#f9f9f9" id="secondaryColor" readonly class="form-control" />

      </div>
    </li>
    <li class="list-group-item" href="#">Change Leftsidebar Link Color
      <div class="input-group colorpicker-component bscp" data-color="#54728c" data-color-format="hex" id="Lcolr">
        <span class="input-group-addon"><i style="background-color: #54728c"></i></span>
        <input type="text" value="#54728c" id="linkColor" readonly class="form-control" />

      </div>
    </li>
    <li class="list-group-item" href="#">Change RightSidebar Background
      <div class="input-group colorpicker-component bscp" data-color="#f9f9f9" data-color-format="hex" id="Rcolr">
        <span class="input-group-addon"><i style="background-color: #f9f9f9"></i></span>
        <input type="text" value="#f9f9f9" id="rightsidebarColor" readonly class="form-control" />

      </div>
    </li>
  </li>
</ul>
<!-- /.@Demo part only The part to here can be removed   -->
<div id="bic_calendar_right" class="bg-white"></div>

<h4 class="page-header text-primary">Current Projects </h4>

<div class="list-group projects">
  <a class="list-group-item" href="#">  
    <p> Archon <span class="pull-right label bg-success">90%</span></p>
    <div class="progress progress-mini">
      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
        <span class="sr-only">90% Complete</span>
      </div>
    </div>

  </a>
  <a class="list-group-item" href="#">  
    <p> Banshee <span class="pull-right label bg-warning">40%</span></p>
    <div class="progress progress-mini">
      <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
        <span class="sr-only">40% Complete</span>
      </div>
    </div>

  </a>
  <a class="list-group-item" href="#">  
    <p> Cascade <span class="pull-right label bg-primary">40%</span></p>
    <div class="progress progress-mini">
      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
        <span class="sr-only">75% Complete</span>
      </div>
    </div>
  </a>
</div>
<h4 class="page-header">Contacts</h4>
<div class="list-group contact-list">
  <a class="list-group-item">
    <img src="images/profiles/one.png" class="chat-user-avatar" alt="">
    Jane Doe
    <i class="fa fa-circle"></i>
  </a>
  <a class="list-group-item contact">
    <img src="images/profiles/two.png" class="chat-user-avatar" alt="">
    Jenny
    <i class="fa fa-circle online"></i>
  </a>
  <a class="list-group-item contact">
    <img src="images/profiles/three.png" class="chat-user-avatar" alt="">
    Vijay
    <i class="fa fa-circle busy"></i>
  </a>
  <a class="list-group-item">
    <img src="images/profiles/four.png" class="chat-user-avatar" alt="">
    Nikky
    <i class="fa fa-circle offline"></i>
  </a>
  <a class="list-group-item contact">
    <img src="images/profiles/five.png" class="chat-user-avatar" alt="">
    John
    <i class="fa fa-circle"></i>
  </a>
  <a class="list-group-item contact">
    <img src="images/profiles/six.png" class="chat-user-avatar" alt="">
    Anusha
    <i class="fa fa-circle"></i>
  </a>
  <a class="list-group-item">
    <img src="images/profiles/seven.png" class="chat-user-avatar" alt="">
    Antony
    <i class="fa fa-circle offline"></i>
  </a>
  <a class="list-group-item contact">
    <img src="images/profiles/eight.png" class="chat-user-avatar" alt="">
    Fana
    <i class="fa fa-circle busy"></i>
  </a>
  <a class="list-group-item contact">
    <img src="images/profiles/nine.png" class="chat-user-avatar" alt="">
    Chris
    <i class="fa fa-circle offline"></i>
  </a>
  <a class="list-group-item">
    <img src="images/profiles/ten.png" class="chat-user-avatar" alt="">
    Sandy
    <i class="fa fa-circle online"></i>
  </a>
  <a class="list-group-item contact">
    <img src="images/profiles/eleven.png" class="chat-user-avatar" alt="">
    Ajay
    <i class="fa fa-circle"></i>
  </a>
  <a class="list-group-item contact">
    <img src="images/profiles/twelve.png" class="chat-user-avatar" alt="">
    Sanju
    <i class="fa fa-circle"></i>
  </a>
</div>
</div>


</div>  <!-- /.right-sidebar-holder -->
</div>  <!-- /.right-sidebar -->


</div> <!-- /.box-holder -->
</div><!-- /.site-holder -->



<!-- Load JS here for Faster site load =============================-->

<script src="<?php echo BACKEND_THEME_URL ?>js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/less-1.5.0.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/bootstrap.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/bootstrap-toggle.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/bootstrap-select.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/bootstrap-switch.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery.tagsinput.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery.placeholder.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/bootstrap-typeahead.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/application.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/moment.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery.sortable.js"></script>
<script type="text/javascript" src="<?php echo BACKEND_THEME_URL ?>js/jquery.gritter.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery.nicescroll.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/prettify.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery.noty.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/bic_calendar.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery.accordion.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/skylo.js"></script>



<script src="<?php echo BACKEND_THEME_URL ?>js/bootstrap-progressbar.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/bootstrap-progressbar-custom.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/bootstrap-colorpicker-custom.js"></script>



<!-- Core Jquery File  =============================-->
<script src="<?php echo BACKEND_THEME_URL ?>js/core.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>select2/dist/js/select2.js"></script>

<script>
  $(function() {
    $('.toggle-one').bootstrapToggle();
  })
</script>
<script  type="text/javascript" src="<?php echo base_url() ?>assets/backend/tinymce/tinymce.min.js"></script>



<script type="text/javascript">

    var BASEURL = "<?php echo base_url(); ?>";

</script>

<script type="text/javascript">

tinymce.init({

    //selector: "textarea",

    selector: ".tinymce_edittor",

     menubar: false,

    plugins:[

                "advlist autolink lists link image charmap print preview anchor media",

                "searchreplace visualblocks code fullscreen",

                "insertdatetime table contextmenu paste textcolor",

                "image",      

            ],

    image_advtab: true,

    toolbar: "insertfile undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | preview code ",

    file_browser_callback: function(field_name, url, type, win) {

            if(type=='image') $('#my_form input').click();

    }

});



    // tinymce.init({

    //     //selector: "textarea",

    //     selector: ".tinymce_edittor",

    //     relative_urls : false,

    //    remove_script_host : false,

    //    convert_urls : true,

    //     menubar: false,

    //    // language : 'he_IL',

    //    // theme_advanced_buttons1_add : "ltr,rtl"

    //     height :450,

    //     plugins: [

    //         "advlist autolink lists link image charmap print preview anchor media",

    //         "searchreplace visualblocks code fullscreen",

    //         "insertdatetime table contextmenu paste textcolor directionality",

    //     ],

    //     image_advtab: true,

    //     toolbar: "insertfile undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | preview code ",     

    //      file_browser_callback : elFinderBrowser,  



    // });

//  function elFinderBrowser (field_name, url, type, win) {

//   tinymce.activeEditor.windowManager.open({

//     file: '<?php echo base_url("elfinders") ?>',// use an absolute path!'<?php echo base_url("/assets/theme/assets/global/plugins/") ?>'

//     title: 'File Manager',

//     width: 900,

//     height: 450,

//     resizable: 'yes'

//   }, {

//     setUrl: function (url) {

//       win.document.getElementById(field_name).value = url;

//     }

//   });

//   return false;

// }

</script>
<script type="text/javascript">
  $("#technology").select2();
</script>
<script type="text/javascript">
  $("#course").select2();
</script>

</body>
</html>