<?php
$segment2 = $this->uri->segment(2);
$segment3 = $segment2 . "/" . $this->uri->segment(3);
if ($segment2 == 'dashboard' || $segment2 == '')
    $dashboard = 'active';
else
    $dashboard = '';
if ($segment2 == 'messages' || $segment2 == '')
    $messages = 'active';
else
    $messages = '';
if ($segment2 == 'users' || $segment3 == 'users/add' || $segment3 == 'users/edit')
    $user = 'active';
else
    $user = '';
if ($segment2 == 'email_templates' || $segment3 == 'email_templates/add' || $segment3 == 'email_templates/edit' || $segment3 == 'email_templates/view')
    $email_templates = 'active';
else
    $email_templates = '';


if ($segment2 == 'slider_images' || $segment3 == 'slider_images/add' || $segment3 == 'slider_images/edit' || $segment3 == 'slider_images/view')
    $slider_images = 'active';
else
    $slider_images = '';


if ($segment2 == 'setting' || $segment3 == '')
    $setting = 'active';
else
    $setting = '';
if ($segment2 == 'technologies' || $segment3 == 'technologies/add' || $segment3 == 'technologies/edit' || $segment3 == 'technologies/status')
    $tech = 'active';
else
    $tech = '';
if ($segment2 == 'advertisements' || $segment3 == 'advertisements/add' || $segment3 == 'advertisements/edit')
    $advertise = 'active';
else
    $advertise = '';
if ($segment2 == 'categories' || $segment3 == 'categories/add' || $segment3 == 'products/product_page_setting' || $segment3 == 'categories/edit' || $segment2 == 'products' || $segment3 == 'products/add' || $segment3 == 'products/import' || $segment3 == 'products/edit')
    $product = 'active';
else
    $product = '';
if ($segment2 == 'courses' || $segment3 == 'courses/import' || $segment3 == 'courses/add' || $segment3 == 'courses/edit')
    $course = 'active';
else
    $course = '';
if ($segment2 == 'projects' || $segment3 == 'projects/add' || $segment3 == 'projects/edit')
    $projects = 'active';
else
    $projects = '';
if ($segment2 == 'groups' || $segment3 == 'groups/add' || $segment3 == 'groups/edit')
    $group = 'active';
else
    $group = '';
if ($segment2 == 'zones' || $segment3 == 'zones/add' || $segment3 == 'zones/edit')
    $zone = 'active';
else
    $zone = '';
if ($segment2 == 'coupons' || $segment3 == 'coupons/add' || $segment3 == 'coupons/edit')
    $coupon = 'active';
else
    $coupon = '';
if ($segment2 == 'blog' || $segment3 == 'blog/add' || $segment3 == 'blog/edit' || $segment3 == 'blog/blog_comments')
    $blog = 'active';
else
    $blog = '';
if ($segment2 == 'orders' || $segment3 == 'orders/add' || $segment3 == 'orders/edit')
    $order = 'active';
else
    $order = '';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title> Projectiya | Superadmin Panel </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Loading Bootstrap -->
        <link href="<?php echo BACKEND_THEME_URL ?>css/bootstrap.css" rel="stylesheet">
        <!-- Loading Stylesheets -->    
        <link href="<?php echo BACKEND_THEME_URL ?>css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo BACKEND_THEME_URL ?>css/style.css" rel="stylesheet" type="text/css"> 
        <link href="<?php echo BACKEND_THEME_URL ?>less/style.less" rel="stylesheet"  title="lessCss" id="lessCss">
        <!-- Loading Custom Stylesheets -->    
        <link href="<?php echo BACKEND_THEME_URL ?>css/bootstrap-toggle.css" rel="stylesheet">
        <link href="<?php echo BACKEND_THEME_URL ?>css/custom.css" rel="stylesheet">
        <link href="<?php echo BACKEND_THEME_URL ?>select2/dist/css/select2.css" rel="stylesheet">
        <link rel="shortcut icon" href="<?php echo BACKEND_THEME_URL ?>images/favicon.ico">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <![endif]-->
        <style type="text/css">
            .error{
                color:red;
            }
        </style>
        <script src="<?php echo BACKEND_THEME_URL ?>js/jquery-1.10.2.min.js"></script>
    </head>
    <body>
        <div class="site-holder">
            <!-- this is a dummy text -->
            <!-- .navbar -->
            <nav class="navbar" role="navigation">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url('backend/dashboard'); ?>"><i class="fa fa-list btn-nav-toggle-responsive text-white"></i> <span class="logo">Pro<strong>jec</strong>Tiya <i class="fa fa-bookmark"></i></span></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav user-menu navbar-right ">

                        <li><a href="#" class="user dropdown-toggle" data-toggle="dropdown"><span class="username"><img src="<?php echo BACKEND_THEME_URL ?>images/profiles/administrator.png" class="user-avatar" alt=""> Super Admin </span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url('superadmin/profile') ?>"><i class="fa fa-user"></i> My Profile</a></li>
                                <!-- <li><a href="#"><i class="fa fa-envelope"></i> Inbox</a></li> -->
                                <li><a href="<?php echo base_url('superadmin/change_password') ?>"><i class="fa fa-cogs"></i> Change Password</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url('superadmin/logout') ?>" class="text-danger"><i class="fa fa-lock"></i> Logout</a></li>
                            </ul>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav> <!-- /.navbar -->

            <!-- .box-holder -->
            <div class="box-holder">

                <!-- .left-sidebar -->
                <div class="left-sidebar">
                    <div class="sidebar-holder">
                        <ul class="nav  nav-list">
                            <!-- sidebar to mini Sidebar toggle -->
                            <li class="nav-toggle">
                                <button class="btn  btn-nav-toggle text-primary"><i class="fa fa-angle-double-left toggle-left"></i> </button>
                            </li>

                            <li class="<?php if (!empty($dashboard)) { ?> active <?php } ?>">
                                <a href="<?php echo base_url('backend/dashboard'); ?>" data-original-title="Dashboard"><i class="fa fa-dashboard"></i><span class="hidden-minibar"> Dashboard </span></a>
                            <li>

                            <li class="submenu <?php if (!empty($projects)) { ?> active <?php } ?>">
                                <a class="dropdown" href="#" data-original-title="Pages"><i class="fa fa-book"></i><span class="hidden-minibar"> Manage Projects  <span class="badge bg-danger pull-right"><?php echo get_no_of_products(); ?></span>
                                </a>
                                <ul <?php if (!empty($projects)) { ?>  style="display:block;" <?php } else { ?> style="display:none;"  <?php } ?> >
                                    <li><a href="<?php echo base_url('backend/projects/'); ?>" data-original-title="Project List"><i class="fa fa-eye"></i><span> Project List</span></a></li>
                                    <li><a href="<?php echo base_url('backend/projects/add'); ?>" data-original-title=" Add Project "><i class="fa fa-plus"></i><span> Add Project </span></a></li>
                                    <li><a href="<?php echo base_url('backend/projects/requests'); ?>" data-original-title="Project Request"><i class="fa fa-tasks"></i><span> Project Requests </span></a></li>
                                </ul>
                            </li>

                            <li class="submenu <?php if (!empty($tech)) { ?> active <?php } ?>">
                                <a class="dropdown" href="#" data-original-title="Manage Technologies"><i class="fa fa-chain"></i><span class="hidden-minibar"> Manage Technologies  </span>
                                </a>
                                <ul <?php if (!empty($tech)) { ?>  style="display:block !important;" <?php } else { ?> style="display:none;"  <?php } ?> >
                                    <li><a href="<?php echo base_url('backend/technologies/'); ?>" data-original-title="Technology List"><i class="fa fa-eye"></i><span> Technology List</span></a></li>
                                    <li><a href="<?php echo base_url('backend/technologies/add'); ?>" data-original-title="Add Project"><i class="fa fa-plus"></i><span> Add Technology </span></a></li>
                                </ul>
                            </li>	

                            <li class="submenu <?php if (!empty($course)) { ?> active <?php } ?>">
                                <a class="dropdown" href="#" data-original-title="Manage Courses"><i class="fa fa-rocket"></i><span class="hidden-minibar"> Manage Courses </span>
                                </a>
                                <ul <?php if (!empty($course)) { ?>  style="display:block !important;" <?php } else { ?> style="display:none;"  <?php } ?> >
                                    <li><a href="<?php echo base_url('backend/courses/'); ?>" data-original-title="Course List"><i class="fa fa-eye"></i><span> Course List</span></a></li>
                                    <li><a href="<?php echo base_url('backend/courses/add'); ?>" data-original-title="Add Project"><i class="fa fa-plus"></i><span> Add Course </span></a></li>
                                </ul>
                            </li>

                            <li class="submenu <?php if (!empty($user)) { ?> active <?php } ?>">
                                <a class="dropdown" href="#" data-original-title="Manage Users"><i class="fa fa-user"></i><span class="hidden-minibar"> Manage Users  <span class="badge bg-danger pull-right"><?php echo get_no_of_uers(); ?> </span>
                                </a>
                                <ul <?php if (!empty($user)) { ?>  style="display:block;" <?php } else { ?> style="display:none;"  <?php } ?>>
                                    <li><a href="<?php echo base_url('backend/users/'); ?>" data-original-title="User List"><i class="fa fa-eye"></i><span> User List</span></a></li>
                                    <li><a href="<?php echo base_url('backend/users/add'); ?>" data-original-title="Chat"><i class="fa fa-plus"></i><span> Add User </span></a></li>
                                </ul>
                            </li>
                            <li class="<?php if (!empty($order)) { ?> active <?php } ?>">
                                <a href="<?php echo base_url('backend/orders/'); ?>" data-original-title=""><i class="fa fa-envelope-o"></i><span class="hidden-minibar">Manage orders<span class="badge bg-success pull-right"><?php //echo get_orders_count(); ?> </span></a>
                            </li>	

                            <li class="submenu <?php if (!empty($blog)) { ?> active <?php } ?>">
                                <a class="dropdown" href="#" data-original-title="Manage Users"><i class="fa fa-comment"></i><span class="hidden-minibar"> Manage Blogs  </span>
                                </a>
                                <ul <?php if (!empty($blog)) { ?>  style="display:block;" <?php } else { ?> style="display:none;"  <?php } ?>>
                                    <li><a href="<?php echo base_url('backend/blog/'); ?>" data-original-title="User List"><i class="fa fa-eye"></i><span>Post List</span></a></li>
                                    <li><a href="<?php echo base_url('backend/blog/add'); ?>" data-original-title="Chat"><i class="fa fa-plus"></i><span> Add Post </span></a></li>
                                </ul>
                            </li> 



                            <li class="submenu <?php if (!empty($email_templates)) { ?> active <?php } ?>">
                                <a class="dropdown" href="#" data-original-title="Manage Users"><i class="fa fa-briefcase"></i><span class="hidden-minibar"> Manage Template </span>
                                </a>
                                <ul <?php if (!empty($email_templates)) { ?>  style="display:block;" <?php } else { ?> style="display:none;"  <?php } ?>>
                                    <li><a href="<?php echo base_url('backend/email_templates/'); ?>" data-original-title="User List"><i class="fa fa-eye"></i><span>Template List</span></a></li>
                                    <li><a href="<?php echo base_url('backend/email_templates/add'); ?>" data-original-title="Chat"><i class="fa fa-plus"></i><span> Add Template </span></a></li>
                                </ul>
                            </li>
                            <li class="submenu <?php if (!empty($faqs)) { ?> active <?php } ?>">
                                <a class="dropdown" href="#" data-original-title="Manage Users"><i class="fa fa-info-circle"></i><span class="hidden-minibar"> Manage faqs </span>
                                </a>
                                <ul <?php if (!empty($faqs)) { ?>  style="display:block;" <?php } else { ?> style="display:none;"  <?php } ?>>
                                    <li><a href="<?php echo base_url('backend/faqs/'); ?>" data-original-title="User List"><i class="fa fa-eye"></i><span>faqs List</span></a></li>
                                    <li><a href="<?php echo base_url('backend/faqs/add'); ?>" data-original-title="Chat"><i class="fa fa-plus"></i><span> Add Faqs </span></a></li>
                                </ul>
                            </li>
                            <li class="submenu <?php if (!empty($slider_images)) { ?> active <?php } ?>">
                                <a class="dropdown" href="#" data-original-title="Manage Users"><i class="fa fa-image"></i><span class="hidden-minibar"> Slider Images</span>
                                </a>
                             <ul <?php if (!empty($slider_images)) { ?>  style="display:block;" <?php } else { ?> style="display:none;"  <?php } ?>>
                                    <li><a href="<?php echo base_url('backend/slider_images/'); ?>" data-original-title="User List"><i class="fa fa-eye"></i><span>Slider Image List</span></a></li>
                                    <li><a href="<?php echo base_url('backend/slider_images/add'); ?>" data-original-title="Chat"><i class="fa fa-plus"></i><span> Add Slider Image </span></a></li>
                                </ul></li>

                            <li class="submenu <?php if (!empty($faqs)) { ?> active <?php } ?>">
                                <a class="dropdown" href="#" data-original-title="Manage Users"><i class="fa fa-picture-o"></i><span class="hidden-minibar"> Manage Advertisements </span>
                                </a>
                                <ul <?php if (!empty($faqs)) { ?>  style="display:block;" <?php } else { ?> style="display:none;"  <?php } ?>>
                                    <li><a href="<?php echo base_url('backend/advertisements/'); ?>" data-original-title="User List"><i class="fa fa-eye"></i><span>Advertisements List</span></a></li>
                                    <li><a href="<?php echo base_url('backend/advertisements/add'); ?>" data-original-title="Chat"><i class="fa fa-plus"></i><span> Add Advertisements </span></a></li>
                                </ul>
                                <ul <?php if (!empty($slider_images)) { ?>  style="display:block;" <?php } else { ?> style="display:none;"  <?php } ?>>
                                    <li><a href="<?php echo base_url('backend/slider_images/'); ?>" data-original-title="User List"><i class="fa fa-eye"></i><span>Slider Image List</span></a></li>
                                    <li><a href="<?php echo base_url('backend/slider_images/add'); ?>" data-original-title="Chat"><i class="fa fa-plus"></i><span> Add Slider Image </span></a></li>
                                </ul>
                            </li>

                            <li class="submenu <?php if (!empty($pages)) { ?> active <?php } ?>">
                                <a class="dropdown" href="#" data-original-title="Manage Users"><i class="fa fa-file-o"></i><span class="hidden-minibar"> Pages </span>
                                </a>
                                <ul <?php if (!empty($pages)) { ?>  style="display:block;" <?php } else { ?> style="display:none;"  <?php } ?>>
                                    <li><a href="<?php echo base_url('backend/pages/'); ?>" data-original-title="Page List"><i class="fa fa-eye"></i><span>Page List</span></a></li>
                                    <li><a href="<?php echo base_url('backend/pages/add'); ?>" data-original-title="Chat"><i class="fa fa-plus"></i><span> Add Page </span></a></li>
                                </ul>
                            </li>
                            
                            
                            

                            <li class="<?php if (!empty($messages)) { ?> active <?php } ?>">
                                <a href="<?php echo base_url('backend/messages/'); ?>" data-original-title=""><i class="fa fa-envelope-o"></i><span class="hidden-minibar">Manage Message <span class="badge bg-success pull-right"><?php echo get_pending_contact_msg(); ?> </span></a>
                            </li>

<!--                            <li class="<?php //if (!empty($option)) {   ?> active <?php //}   ?>">
                                <a href="<?php //echo base_url('superadmin/option');   ?>" data-original-title="option"><i class="fa fa-dashboard"></i><span class="hidden-minibar"> Setting </span></a>
                            <li>-->
                                <!--start-->
                            <li class="submenu <?php if (!empty($setting)) { ?> active <?php } ?>">
                                <a class="dropdown" href="#" data-original-title="setting"><i class="fa fa-briefcase"></i><span class="hidden-minibar"> Setting </span>
                                </a>
                                <ul <?php if (!empty($setting)) { ?>  style="display:block;" <?php } else { ?> style="display:none;"  <?php } ?>>
                                    <li><a href="<?php echo base_url('superadmin/settings'); ?>" data-original-title="User List"><i class="fa fa-pencil"></i><span>Settings</span></a></li>
                                    <li><a href="<?php echo base_url('superadmin/add_popup_image'); ?>" data-original-title="Chat"><i class="fa fa-plus"></i><span> Add Popup Image </span></a></li>
                                    <li><a href="<?php echo base_url('superadmin/view_popup_image'); ?>" data-original-title="Chat"><i class="fa fa-eye"></i><span> View Popup Image </span></a></li>
                                    
                                </ul>
                            </li>
                            <!--end-->

                        </ul>
                    </div>
                </div> <!-- /.left-sidebar -->

                <!-- .content -->
                <div class="content">
