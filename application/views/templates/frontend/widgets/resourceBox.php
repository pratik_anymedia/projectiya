<div class="container">
    <div class="row">
        <div class="z-depth-1">
            <div class="col-md-12">
                <!--  Horizontal News Box -->
                <div class="news horizontal">
                    <div class="col-md-12 no-padding">
                        <!-- News Description -->
                        <div class="news-description">
                            <div class="news-time">
                                <h4 class="title"><?php echo ucwords($value->post_title); ?></h4>
                            </div>
                            <div class="exemple collapsable">
                                <?php echo $value->post_content; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
