
<div class="z-depth-1">
    <div class="news horizontal no-border">
        <div class="col-md-4 no-padding no-padding">
            <!-- News Image -->
            <div class="news-image">
                <a href="<?php echo base_url('projects/detail/' . $value->slug); ?>">
                    <img alt="news Image" class="responsive-img" src="<?php echo base_url(str_replace('./', '', $value->main_image)); ?>">
                </a>
            </div>
        </div>
        <div class="col-md-8  no-padding">
            <!-- News Description -->
            <div class="news-description">
                <div class="news-time">
                    <i class="fa fa-eye"></i> <?php echo $value->view_count; ?> Views &nbsp;
                    <i class="fa fa-thumbs-o-up"></i>
                    <?php
                    $total_like = get_total_like_of_project($value->id);
                    echo ($total_like > 0) ? $total_like : 'No'
                    ?> Likes


                </div>
                <div class="news-title">
                    <a href="<?php echo base_url('projects/detail/' . $value->slug); ?>"> <?php
                        if (!empty($value->title)) {
                            echo ucwords($value->title);
                        }
                        ?>

                    </a> 
                    <?php if (isset($value->price)) { ?>
                        <div>
                            <label class="" id="price_tag"><i class="fa fa-inr"></i><?php echo $value->price; ?></label>
                        </div>
                    <?php } ?>
                </div>
                <div class="news-content"><p><?php
                        if (!empty($value->short_description)) {
                            echo $this->lang->line('DESCRIPTION') . ': ';
                            echo character_limiter($value->short_description, 100);
                        }
                        ?></p>
                    <div class="news-time">
                        <?php
                        $tech_Id_arr = explode(',', $value->technology);
                        $tech_id_new = array();

                        foreach ($tech_Id_arr as $key => $value1) {
                            $tech_id_new[] = str_replace('#', '', $value1);
                        }


                        if (!empty($tech_id_new)) {
                            $tecs = array();
                            echo $this->lang->line('TECHNOLOGY_USED') . ':';
                            foreach ($tech_id_new as $key => $value2) {
                                if (isset($technologies[$value2]))
                                    $tecs[] = $technologies[$value2];
                            }
                            echo implode(', ', $tecs);
                        }
                        echo '<br>';
                        if (!empty($value->backend_technology)) {
                            echo $this->lang->line('BACKEND_TECHNOLOGY') . ':';
                            echo $value->backend_technology;
                        }
                        echo '<br>';
                        if (!empty($value->no_of_tables)) {
                            echo $this->lang->line('DATABASE_TABLES') . ':';
                            echo $value->no_of_tables;
                        }
                        echo '<br>';

                        $crs_Id_arr = explode(',', $value->course_type);
                        $crs_id_new = array();
                        foreach ($tech_Id_arr as $key => $value1) {
                            $crs_id_new[] = str_replace('#', '', $value1);
                        }


                        if (!empty($crs_id_new)) {
                            echo $this->lang->line('CAN_BE_USED_IN') . ':';
                            $crs = array();
                            foreach ($crs_id_new as $key => $value2) {

                                if (isset($course_arr[$value2]))
                                    $crs[] = $course_arr[$value2];
                            }
                            echo implode(', ', $crs);
                        }
                        echo '<br>';
                        ?>
                    </div>   
                    <?php
                    if (!empty($value->id)) {
                        $total_rating = product_rating_count($value->id);
                    } else {
                        $total_rating = 0;
                    }
                    ?>
                    <div class="exemple">
                        <div class="showrate" data-average="<?php
                        if (!empty($total_rating)) {
                            echo $total_rating;
                        } else {
                            echo 0;
                        }
                        ?>" data-id="1"></div>
                    </div>
                </div>
                <div class="boxbtn">
                    <a class="btn btn-theme"  href="<?php echo base_url('projects/detail/' . $value->slug); ?>" title="Watch Video"><i class="fa fa-video-camera"></i> <span class="hidden-sm hidden-xs">Watch Video</span></a>
                    <a href="<?php echo base_url('projects/download_doc_file/' . $value->unique_id); ?>" title="Download" class="btn btn-danger"><i class="fa fa-download"></i> <span class="hidden-sm hidden-xs">Download</span></a>
                </div>
            </div>
        </div>
    </div>
</div>