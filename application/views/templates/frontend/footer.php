<?php
$segment2 = $this->uri->segment(1);
$segment3 = $segment2 . "/" . $this->uri->segment(2);
if ($segment2 == 'about_us')
    $about = 'active';
else
    $about = '';
if ($segment2 == 'contact_us')
    $contact = 'active';
else
    $contact = '';
if ($segment2 == 'blog' || $segment2 == 'blog/blog_detail')
    $blog = 'active';
else
    $blog = '';
if ($segment2 == 'projects' || $segment2 == 'projects/detail')
    $project = 'active';
else
    $project = '';
?>

 <!-- ******FOOTER****** --> 
    <footer class="footer">
        <div class="footer-content">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-3 col-sm-4 about">
                        <div class="footer-col-inner">
                            <h3><span class="logo">ProjecTiya <i class="fa fa-bookmark"></i></span></h3>
                            <ul>
                                <?php
                                if (!empty($pages) && is_array($pages)) {
                                    foreach ($pages as $key => $value) {
                                        ?>
                                <li><a href="<?php echo base_url(); ?>common_content/index/<?php echo $value->id; ?>"><i class="fa fa-caret-right"></i><?php echo ucwords($value->post_title); ?></a></li>
                                <?php
                                    }
                                }
                                ?>	  
                            </ul>
                        </div><!--//footer-col-inner-->
                    </div><!--//foooter-col-->
                    <div class="footer-col col-md-4 col-sm-8 newsletter">
                        <div class="footer-col-inner">
                            <h3>Technologies</h3>
                            <!--<p>Subscribe to get our weekly newsletter delivered directly to your inbox</p>-->
                            <ul class="list-inline footer-tags">
                                <?php if (!empty($technology)) {
                                    foreach ($technology as $value) { ?>
                                    <li><a href="#"><?php echo $value->technology; ?></a></li>
                                    <?php }
                                } ?>  
                            </ul>

                        </div><!--//footer-col-inner-->
                    </div><!--//foooter-col--> 
                    <div class="footer-col col-md-5 col-sm-12 contact">
                        <div class="footer-col-inner">
                            <h3>Tutorials</h3>
                            <div class="row">
                                <ul class="list-unstyled blog-posts">
                                    <?php $Latest_blog = get_latest_blogs();
                                    if (!empty($Latest_blog)) {
                                        foreach ($Latest_blog as $value) { ?>
                                    <li>
                                        <a href="<?php echo base_url() ?>blog/blog_detail/<?php echo $value->blog_slug; ?>">
                                            <img src="<?php echo base_url(str_replace('./', '', $value->thumb_image)); ?>" class="image-thumbnail" width="60px" alt="">
                                            <h5><?php echo character_limiter(ucfirst($value->blog_title), 30); ?></h5>
                                            <h6><?php if (!empty($value->blog_content)) {
                                                echo character_limiter(strip_tags(html_entity_decode($value->blog_content)), 50); } ?>
                                            </h6>
                                        </a>
                                    </li>
                                        <?php }
                                    } ?>
                                </ul>  
                            </div> 
                        </div><!--//footer-col-inner-->            
                    </div><!--//foooter-col-->   
                </div>   
            </div>        
        </div><!--//footer-content-->
        <div class="bottom-bar">
            <div class="container">
                <div class="row">
                    <?php if(isset($SETTINGS['copyright']) ): ?> <small class="copyright col-md-6 col-sm-12 col-xs-12"><?php echo $SETTINGS['copyright'] ?><a href="#"></a></small><?php endif; ?>
                    <ul class="social pull-right col-md-6 col-sm-12 col-xs-12">
                        <?php if(isset($SETTINGS['fb_url']) ): ?>
                            <li><a href="<?php echo $SETTINGS['fb_url'] ?>"><i class="fa fa-facebook"></i></a></li>
                            <?php endif; ?>

                            <?php if(isset($SETTINGS['twitter_url']) ): ?>    
                            <li><a href="<?php echo $SETTINGS['twitter_url'] ?>"><i class="fa fa-twitter"></i></a></li>
                            <?php endif; ?>

                            <?php if(isset($SETTINGS['gplus_url']) ): ?>
                            <li><a href="<?php echo $SETTINGS['gplus_url'] ?>"><i class="fa fa-google-plus"></i></a></li>
                            <?php endif; ?>

                            <?php if(isset($SETTINGS['linkedin_url']) ): ?>
                            <li><a href="<?php echo $SETTINGS['linkedin_url'] ?>"><i class="fa fa-linkedin"></i></a></li>
                            <?php endif; ?>

                            <?php if(isset($SETTINGS['vimeo_url']) ): ?>
                            <li><a href="<?php echo $SETTINGS['vimeo_url'] ?>"><i class="fa fa-vimeo-square "></i></a></li>
                            <?php endif; ?>

                            <?php if(isset($SETTINGS['youtube_url']) ): ?>
                            <li><a href="<?php echo $SETTINGS['youtube_url'] ?>"><i class="fa fa-youtube"></i></a></li>
                            <?php endif; ?>

                            <?php if(isset($SETTINGS['trumblr_url']) ): ?>
                            <li><a href="<?php echo $SETTINGS['trumblr_url'] ?>"><i class="fa fa-tumblr-square"></i></a></li>
                            <?php endif; ?>
                            <?php if(isset($SETTINGS['dribbble_url']) ): ?>
                            <li><a href="<?php echo $SETTINGS['dribbble_url'] ?>"><i class="fa fa-dribbble"></i></a></li>
                            <?php endif; ?>
                            <?php if(isset($SETTINGS['pinterest_url']) ): ?>
                            <li><a href="<?php echo $SETTINGS['pinterest_url'] ?>"><i class="fa fa-pinterest"></i></a></li>
                            <?php endif; ?>
                        
                    </ul><!--//social-->
                </div><!--//row-->
            </div><!--//container-->
        </div><!--//bottom-bar-->
    </footer><!--//footer-->
<?php if(isset($popup)):
    echo $popup;
endif; ?>
    <?php $this->load->view('user/project_request'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo FRONTEND_THEME_URL ?>js/jquery-1.10.2.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo FRONTEND_THEME_URL ?>js/bootstrap.min.js"></script>
    <script src="<?php echo FRONTEND_THEME_URL ?>js/core.js"></script>
    <script src="<?php echo FRONTEND_THEME_URL ?>js/review.js"></script>
    <script src="<?php echo FRONTEND_THEME_URL ?>ajaxfileupload/jquery.uploadfile.min.js"></script>
    <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/hide_paragraphs.js"></script><!-- Social Icons -->
    <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>jRating/jquery/jRating.jquery.js"></script>

    <script src="<?php echo FRONTEND_THEME_URL ?>plupload/js/plupload.full.min.js" type="text/javascript"></script>
    <script src="<?php echo BACKEND_THEME_URL ?>select2/dist/js/select2.js"></script>
    
    
    
    <script type="text/javascript">
        var BASEURL = '<?php echo base_url(); ?>';
    </script>
    <script type="text/javascript">
        $(function () {
            var header = $(".fixed-head-warp");
            $(window).scroll(function () {
                var scroll = $(window).scrollTop();

                if (scroll >= 106) {
                    header.addClass("head-modify");
                } else {
                    header.removeClass("head-modify");
                }
            });
        });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#submit_comment').on('click', function (event) {
                event.preventDefault();
                $.get(BASEURL + 'blog/check_user_login', {check: 'check'}, function (data) {
                    if (data == 'FALSE') {
                        $('#login_error').css('display', 'block');
                        $('#login_error').html('Please Login first for leave comment');
                        setTimeout(function () {
                            $('#login_error').html('');
                            $('#login_error').css('display', 'none');
                        }, 5000);
                        return false;
                    } else {
                        $('#comment_submission_form').submit();
                        return true;
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.exemple2').jRating({
                length: 5,
                rateMax: 5,
                decimalLength: 0,
                onSuccess: function (element, rate) {
                    if (element == 'TRUE') {
                        alert('Success : your rate has been saved :)');
                    } else if (element == 'alreadyRate') {
                        alert('Opps: You have already rated this');
                    } else {
                        alert('Error : please retry');
                    }
                },
                onError: function () {
                    alert('Error : please retry');
                }
            });
            $(".showrate").jRating({
                isDisabled: true,
                rateMax: 5
            });
        });
    </script> 
    <script type="text/javascript">
        $(document).ready(function () {
            $('#add_to_favorite').on('click', function (event) {
                event.preventDefault();
                var user_id = $('#user_id_add').val();
                var product_id = $('#product_id_add').val();
                if (user_id != '' && product_id != '') {
                    $.post(BASEURL + 'projects/add_to_favorite', {user: user_id, pro_id: product_id}, function (data) {
                        if (data == 'TRUE') {
                            alert('Success : Your action has been successfully done :)');
                        } else if (data == 'alreadyfavorite') {
                            alert('Opps: You have already added this Project');
                        } else {
                            alert('Error : please retry');
                        }
                    });
                } else {
                    alert('Error : Please Retry');
                }
            });
            $('#for_add_login').on('click', function (event) {
                event.preventDefault();
                alert('Sorry !! Please login first for add to favorite this project.');
            });
        });
    </script>

    <script type="text/javascript">
        function pro_fbshare(id) {
            window.open("http://www.facebook.com/share.php?u=<?php echo base_url(); ?>projects/fbshare/" + id, "Facebook_Share", "menubar=1,resizable=1,width=900,height=500");
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var settings = {
                url: BASEURL + "user/project_file_upload/",
                dragDrop: true,
                fileName: "myfile",
                allowedTypes: "zip,rar,pdf,doc,docx,ppt,pptx,xml,ppt",
                returnType: "json",
                multiple: false,
                onSuccess: function (files, data, xhr)
                {
                    if (data != '') {
                        var length = files.length;
                        $('.ajax-upload-dragdrop').css('display', 'none');
                        $("#status").append("<div>Please click on Done button to next process</div>");

                    }
                },
                showDelete: true,
                deleteCallback: function (data, pd)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        $.post(BASEURL + "user/project_file_delete/", {op: "delete", name: data[i]},
                        function (resp, textStatus, jqXHR)
                        {
                            //Show Message  
                            //alert(resp);
                            if (resp) {
                                $("#status").append("<div>File Deleted</div>");
                                setTimeout(function () {
                                    window.location.reload();
                                }, 3000);
                            }
                        });
                    }
                    pd.statusbar.hide(); //You choice to hide/not.
                }
            }
            var uploadObj = $("#mulitplefileuploader").uploadFile(settings);
        });
    </script>
    <script type="text/javascript">
        $("#technology").select2();
    </script>
    <script type="text/javascript">
        $("#course").select2();
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.blog-right').on('change', 'input[type=checkbox]', function (event) {
                event.preventDefault();
                var tech_ids = [];
                var course_ids = [];
                $("input[name^='technology_id']:checked:enabled").each(function (i) {
                    tech_ids[i] = $(this).val();
                });
                $("input[name^='course_id']:checked:enabled").each(function (i) {
                    course_ids[i] = $(this).val();
                });
                //alert(course_ids+' / '+tech_ids);
                if (tech_ids.length > 0 || course_ids.length > 0) {
                    $('#mydiv').append('<div class="overlay" ><center><img class="ajax-loader " src="<?php echo base_url(); ?>assets/frontend/images/ajax_loder.gif" alt="loading.."></center></div>');
                    $.post(BASEURL + 'projects/get_projects', {course: course_ids, tech: tech_ids}, function (data) {
                        if (data != '') {
                            if (data != 'NoProjectFound') {
                                $('#mydiv').html('');
                                $('#mydiv').append(data);
                                $(".showrate").jRating({
                                    isDisabled: true,
                                    rateMax: 5
                                });
                            } else {
                                $('#mydiv').html('');
                                $('#mydiv').append('<div class="col-md-12 text-center"><h1>No Projetcs Found </h1> <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>');
                            }
                        }
                    });
                    window.location=BASEURL+"projects/?";
                } else {
                    $.get(BASEURL + 'projects/get_latest_projects', {latest: 'latest'}, function (data) {
                        if (data != '') {
                            if (data != 'NoProjectFound') {
                                $('#mydiv').html('');
                                $('#mydiv').append(data);
                                $(".showrate").jRating({
                                    isDisabled: true,
                                    rateMax: 5
                                });
                            } else {
                                $('#mydiv').html('');
                                $('#mydiv').append('<div class="col-md-12 text-center"><h1>No Projetcs Found </h1> <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>');
                            }
                        }
                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        $('#submit_apply_id').on('click', function () {
            var emailRegex = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
            var email = document.getElementById("user_email").value;
            var password = document.getElementById("user_password").value;
            if (email == "") {
                $('#email_error').html('Please Enter Email Address Field');
                setTimeout(function () {
                    $('#email_error').html('');
                }, 5000);
                return false;
            } else if (!emailRegex.test(email)) {
                $('#email_error').html('Please Enter valid Email Address');
                setTimeout(function () {
                    $('#email_error').html('');
                }, 5000);
                return false;
            }

            if (password == "") {
                $('#pass_error').html('Please Enter Password Field');
                setTimeout(function () {
                    $('#pass_error').html('');
                }, 5000);
                return false;
            }

            $.post(BASEURL + 'user/user_login/', {email: email, password: password}).done(function (data) {
                if (data == 'already_login') {
                    $('#email').val('');
                    $('#password').val('');
                    $('#myModal').modal('hide');
                    document.location.reload(true);
                }
                if (data == 'successfully') {
                    $('#email').val('');
                    $('#password').val('');
                    $('#myModal').modal('hide');
                    document.location.reload(true);
                }

                if (data == 'login_fail') {
                    $('#email').val('');
                    $('#password').val('');
                    $('#msg').addClass('alert');
                    $('#msg').addClass('alert-danger');
                    $('#msg').html('Please check login process again');
                    setTimeout(function () {
                        $('#myModal').modal('hide');
                        document.location.reload(true);
                    }, 3000);
                }

                if (data == 'login_fail_cred') {
                    $('#email').val('');
                    $('#password').val('');
                    $('#msg').addClass('alert');
                    $('#msg').addClass('alert-danger');
                    $('#msg').html('Invalid Login Credentials');
                    setTimeout(function () {
                        $('#myModal').modal('hide');
                        document.location.reload(true);
                    }, 3000);
                }

            });
        });

        $('#myModal').on('hidden.bs.modal', function () {
            $('#user_email').val('');
            $('#user_password').val('');
        });

        setTimeout(function () {
            $('#myModal').modal('show');
        }, 200);

    <?php if ($this->session->flashdata('req_msg_error')): ?>   
        $('#exampleModal').modal('show');
    <?php endif; ?>
</script>
<!--  This is view More -->
<script>
    
    $(function () {
      $(".collapsable").hideParagraphs();
    });
</script>
                        
                     <!-- New Footer Start -->
    <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/bootstrap-hover-dropdown.min.js"></script> 
    <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/back-to-top.js"></script>
    <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/main.js"></script> 
    <script src="<?php echo FRONTEND_THEME_URL ?>js/jquery.counterup.min.js"></script>
    <script src="<?php echo FRONTEND_THEME_URL ?>js/jquery.easing.1.3.min.js"></script>
    <script src="<?php echo FRONTEND_THEME_URL ?>js/owl.carousel.min.js"></script>
    <script src="<?php echo FRONTEND_THEME_URL ?>js/wow.min.js"></script>
    <script src="<?php echo FRONTEND_THEME_URL ?>js/html5tooltipsjs.js"></script>
    <script src="//www.googleadservices.com/pagead/conversion.js"></script>
    <script>
      //function to fix height of iframe!
      var calcHeight = function() {
        var headerDimensions = $('.preview__header').height();
        $('.full-screen-preview__frame').height($(window).height() - headerDimensions);
      }

      $(document).ready(function() {
        calcHeight();
      });

      $(window).resize(function() {
        calcHeight();
      }).load(function() {
        calcHeight();
      });
    </script>
</body>
</html>