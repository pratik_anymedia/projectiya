<?php
$segment2 = $this->uri->segment(1);
$segment3 = $segment2 . "/" . $this->uri->segment(2);
if ($segment2 == '')
    $home = 'active';
else
    $home = '';
if ($segment2 == 'user/login')
    $sell = 'active';
else
    $sell = '';
if ($segment2 == 'projects')
    $project = 'active';
else
    $project = '';
if ($segment2 == 'contact_us')
    $contact = 'active';
else
    $contact = '';
if ($segment2 == 'blog' || $segment3 == 'blog/blog_detail')
    $blog = 'active';
else
    $blog = '';
if ($segment2 == 'projects' || $segment3 == 'projects/detail')
    $project = 'active';
else
    $project = '';
?>
<script type="text/javascript">
    html5tooltips.refresh();
    
    html5tooltips([
      {
        animateFunction: "spin",
        color: "#FF0000",
        contentText: "Refresh",
        stickTo: "right",
        targetSelector: "#refresh"
      }
      
]);
</script>
<!DOCTYPE html>
<html>
    <head>

        <title>Projectiya | Buy Final Year BE,MCA,BCA,BSC-IT Project Online</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="<?php echo FRONTEND_THEME_URL ?>bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <!--<link href="<?php echo FRONTEND_THEME_URL ?>css/style.css" rel="stylesheet">-->
        <link href="<?php echo FRONTEND_THEME_URL ?>ajaxfileupload/uploadfile.css" rel="stylesheet">
        <link href="<?php echo FRONTEND_THEME_URL ?>css/blog.css" rel="stylesheet">
        <link href="<?php echo BACKEND_THEME_URL ?>select2/dist/css/select2.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo FRONTEND_THEME_URL ?>jRating/jquery/jRating.jquery.css" type="text/css" />
        <!-- New Header is added -->
        <link rel="stylesheet" href="<?php echo FRONTEND_THEME_URL ?>css/font-awesome.css"><!-- ok means he-->
        <link rel="stylesheet" href="<?php echo FRONTEND_THEME_URL ?>fonts/fontawesome-webfont.eot"><!-- ok means he-->
        <link rel="stylesheet" href="<?php echo FRONTEND_THEME_URL ?>css/flexslider.css"><!-- ok means he-->
        <link id="theme-style" rel="stylesheet" href="<?php echo FRONTEND_THEME_URL ?>css/styles.css"><!-- ok means he-->
        <link rel="stylesheet" href="<?php echo FRONTEND_THEME_URL ?>css/pe-icon-7-stroke.css"><!-- ok-->
        <link rel="stylesheet" href="<?php echo FRONTEND_THEME_URL ?>css/animate.css"><!-- ok means he-->
        <link rel="stylesheet" href="<?php echo FRONTEND_THEME_URL ?>css/owl.carousel.css"><!-- ok -->
        <link rel="stylesheet" href="<?php echo FRONTEND_THEME_URL ?>css/responsive.css"><!-- ok -->
        <!--<link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-precomposed.png">-->
        <link rel="shortcut icon" type="image/png" href="<?php echo FRONTEND_THEME_URL ?>images/fav-icon.png"/> 
        
        

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--
          [if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
          <![endif]
        -->
        <style type="text/css"> 
            .overlay {
                background-color: rgba(0, 0, 0, 0.2);
                z-index: 999;
                position: absolute;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;

            }
            .ajax-loader {
                position: absolute;
                left: 50%;
                top: 50%;
                margin-left: -62px; /* -1 * image width / 2 */
                margin-top: -62px;  /* -1 * image height / 2 */
            }
        </style>

    </head>
    <body class="home-page">
        <div id="feedback">
                <a href="#" data-toggle="modal" data-target="#exampleModal">Request Project</a>
        </div>
        <div class="clearfix"></div>
    <div class="wrapper">
        <!-- ******HEADER****** --> 
        <header class="header">  
            <div class="top-bar">
                <div class="container"> 
                    <div class="col-md-3">
                    <ul class="social-icons col-md-6 col-sm-6 col-xs-12 ">
                        <?php if (!empty($SETTINGS['show_project_count']) && $SETTINGS['show_project_count'] == 1) { ?>
                            <li><a href="<?php echo base_url('projects/project_list');?>" data-tooltip="Total Projects" data-tooltip-stickto="right" data-tooltip-color="scale" data-tooltip-animate-function="foldin"><?php echo $total_project_count; ?></a></li>
                        <?php } ?>                                     
                        <?php if (!empty($SETTINGS['show_active_user_count']) && $SETTINGS['show_active_user_count'] == 1) { ?>
                            <li> <a data-tooltip="Total Users" data-tooltip-stickto="right" data-tooltip-color="scale" data-tooltip-animate-function="foldin"><?php echo $active_user_count; ?></a></li>
                        <?php } ?>
                    </ul>
                    
                    </div> 
                        
                    <form action="<?php echo base_url('projects/index'); ?>" class="pull-right search-form" method="get" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search Your Idea...">
                        </div>
                        <button type="submit" class="btn btn-theme">Go</button>
                    </form>         
                </div>      
            </div><!--//to-bar-->
            <div class="header-main container">
                <h1 class="logo col-md-4 col-sm-4">
                    <?php if(isset($SETTINGS['logo'])) { ?> <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/uploads/logo/<?php  echo $SETTINGS['logo']; ?>" alt="Logo" /></a> <?php  } ?>
                </h1><!--//logo-->           
                <div class="info col-md-8 col-sm-8">
                    <ul class="menu-top navbar-right hidden-xs">
                        <?php if (!$this->session->userdata('user_info')) { ?>
                            <li class="divider"><a class="login-switch" href="<?php echo base_url('user/login'); ?>" title="">Sell</a>
                            </li>
                            <li class="divider"><a class="login-switch" href="<?php echo base_url('projects/'); ?>" title="">Buy</a>
                            </li>
                            <li class="divider">
                                <a class="login-switch" href="<?php echo base_url('user/login'); ?>" title="">
                                    Login
                                </a>
                            </li>
                            <li class="divider">
                                <a class="register-switch text-font" href="<?php echo base_url('user/register'); ?>" onclick="showBox('yt_register_box', 'jform_name', this, window.event || event);
                                        return false;">
                                    <span class="title-link"><span>Register</span></span>
                                </a>
                            </li>
                        <?php } else { ?>
                            <?php $user_info = get_user_info(); ?> 
                            <li class="divider"><a class="login-switch" href="<?php echo base_url('user/upload_project/'); ?>" title="">Sell</a>
                            </li>
                            <li> 
                            <li class="divider"><a class="login-switch" href="<?php echo base_url('projects/'); ?>" title="">Buy</a>
                            </li>
                            <li class="divider">
                                <a class="login-switch" href="<?php echo base_url('user/dashboard'); ?>" title="">
                                    Welcome , <?php if(isset($user_info)){ echo $user_info->first_name.' '.$user_info->last_name; } ?>
                                </a>
                            </li> 
                            <li class="divider">
                                <a class="login-switch" href="<?php echo base_url('user/logout'); ?>" title="">
                                    Logout
                                </a>
                            </li>

                        <?php } ?>
                        <li class="jshop-checkout">
                            <a href="#">Checkout</a>
                        </li>
                    </ul><!--//menu-top-->
                    <br />
                    <div class="contact pull-right">
                        <p class="phone"><i class="fa fa-phone"></i>Call us today <span class="blink_me"><?php echo get_option_value('CONTACT_US_PHONE'); ?></span></p> 
                        <?php if(isset($SETTINGS['admin_email']) ): ?><p class="email"><i class="fa fa-envelope"></i><a href="#"><?php echo $SETTINGS['admin_email'];?></a></p><?php endif;?>
                    </div><!--//contact-->
                </div><!--//info-->
            </div><!--//header-main-->
            <nav class="main-nav" role="navigation">
            <div class="container">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button><!--//nav-toggle-->
                    </div><!--//navbar-header-->            
                    <div class="navbar-collapse collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class=" nav-item <?php if (!empty($home)): echo 'active'; endif; ?>">
                                <a href="<?php echo base_url(''); ?>">Home</a></li>
                            <li class="nav-item <?php if (!empty($project)): echo 'active'; endif; ?>" >
                                <a href="<?php echo base_url('projects'); ?>">Buy Projects </a>

                            </li>
                            <?php if (!$this->session->userdata('user_info')) { ?>
                            <li class="nav-item <?php if (!empty($sell)): echo 'active'; endif; ?>" >
                                <a href="<?php echo base_url('user/login'); ?>">Sell Projects</a>
                            </li>
                            <?php }else { ?>
                            <li class="nav-item <?php if (!empty($sell)): echo 'active'; endif; ?>">
                                <a href="<?php echo base_url('user/upload_project/'); ?>">Sell Projects </a>
                            </li>
                            <?php } ?>
                            <li class="nav-item <?php if (!empty($blog)): echo 'active'; endif; ?> ">
                                <a href="<?php echo base_url('blog/'); ?>">Tutorials</a>
                            </li>

                            <li class="nav-item <?php if (!empty($resource)): echo 'active'; endif; ?> ">
                                <a href="<?php echo base_url('resources');?>">Resources </a>
                            </li><!--//dropdown-->
                        </ul><!--//nav-->
                    </div><!--//navabr-collapse-->
                </div><!--//container-->
            </nav><!--//main-nav-->
        </header><!--//header-->
    <div class="clearfix"></div>
