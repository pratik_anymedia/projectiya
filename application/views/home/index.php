<script language="JavaScript1.2">

    var howOften = 2; //number often in seconds to rotate
    var current = 0; //start the counter at 0
    var current_bottom = 0; //start the counter at 0
    var ns6 = document.getElementById && !document.all; //detect netscape 6

    //place your images, text, etc in the array elements here
    var items = new Array();

    <?php $i=0; foreach($advertisements as $banner): ?>
    items[<?php echo $i ?>] = "<a href='<?php echo $banner->url ?>' ><img class='img-responsive' alt='banner' src='<?php echo base_url().$banner->file_path;?>'  /></a>"; //a linked image
    <?php $i++; endforeach; ?>

    function rotater() {
        document.getElementById("ad_container").innerHTML = items[current];
        current = (current == items.length - 1) ? 0 : current + 1;
        setTimeout("rotater()", howOften * 1000);
    }
    window.onload = rotater;
</script>


<!-- carousel -->
<!-- ******CONTENT****** --> 
<div class="content container">
    <div id="promo-slider" class="slider flexslider">
        <ul class="slides">
            <?php
                    $slider_active = 1;
                    foreach ($slider_images as $key => $slider) {
                        if ($slider_active == 1) {
                            $class_active = 'active';
                        } else {
                            $class_active = '';
                        }
                        $slider_active++;
                ?>
            <li class="<?php echo $class_active; ?>">
                <img src="<?php echo base_url() . $slider->path; ?>"  alt="" />
            </li>
             <?php } ?>
        </ul>
    </div>
    
    <?php if(isset($SETTINGS['welcome_text'])): ?>
    <section class="promo box box-dark">        
        <div class="col-md-9">
        <h1 class="section-heading"><?php  echo (isset($SETTINGS['welcome_text']))?$SETTINGS['welcome_text']:"Buy & Sell Your Acadmic Project at Projectiya.com"; ?></h1>
        </div>  
    </section>
    <!--promo-->
    <?php endif; ?>
    <div class="container">
        <!--lATEST PROJECT LIST-->
        <div  class="col-md-9">
            <div id="mydiv" class="latest-list-warp">
                <!-- <h2 class="heading">Latest Projects</h2>-->
                <?php
                    if (!empty($projects)) {
                        foreach ($projects as $value):
                            $this->load->view('templates/frontend/widgets/projectBox' , array('value' => $value));
                        endforeach;
                    }
                ?>
                <a href="<?php echo base_url('projects'); ?>" class="btn btn-theme btn-block loadMore">Load More</a>
            </div>
        </div>

        <div class="col-md-3">
            <!-- Single button -->
            <div id="ad_container" ></div><br>
            <section class="links">
                <h1 class="section-heading text-highlight"><span class="line"><?php echo $this->lang->line('FEATURED_PROJECT'); ?> </span></h1>
                    <div class="section-content">
                        <?php
                        $fp_count = 0;
                        if (!empty($projects)) {
                            foreach ($projects as $key => $value) {
                                if ($fp_count == 6) {
                                    break;
                                }
                                if (isset($value->featured_project) && $value->featured_project == 1) {
                                    $fp_count++;
                                    ?>
                                        <p>
                                            <a href="<?php echo base_url('projects/detail/' . $value->slug); ?>"><i class="fa fa-caret-right"></i><?php
                                                if (!empty($value->title)) {
                                                    echo $value->title;
                                                } ?>
                                            </a>
                                        </p>
                                    <?php
                                }
                            }
                        }

                        if ($fp_count == 0) { ?>
                            <p> No Record Found.    </p>
                        <?php } ?>
                    </div>
                            <!--section-content-->
                </section>
                            <!--links-->
            <div id="ad_container"></div>
        </div>
    </div>
</div>
