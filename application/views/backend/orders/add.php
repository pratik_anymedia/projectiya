<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
							  <i class="fa fa-shopping-cart"></i>Order <span class="hidden-480">
								<!-- | Dec 27, 2013 7:16:25 --> </span>
							</div>
							<div class="actions">
								<a href="#" class="btn default yellow-stripe">
									<i class="fa fa-angle-left"></i>
									<span class="hidden-480"> Back </span>
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="tabbable">
								<ul class="nav nav-tabs nav-tabs-lg">
									<li class="active">
										<a href="#tab_1" data-toggle="tab"> Customers </a>
									</li >
									<li>
										<a href="#tab_2" data-toggle="tab">
											Products 
										<!-- <span class="badge badge-success"> 4 </span> -->
										</a>
									</li>
									<li>
										<a href="#tab_3" data-toggle="tab">
										View Cart </a>
									</li>
									<li>
										<a href="#tab_4" data-toggle="tab">
										Shipments 
										<!-- <span class="badge badge-danger"> 2 </span> -->
										</a>
									</li>
									<li>
										<a href="#tab_5" data-toggle="tab">
										History </a>
									</li>
								</ul>
								<div class="tab-content">
								<?php if(!empty($customer_info) && !empty($cust_info_id)){ ?>
									<div class="tab-pane <?php if(!empty($customer_info) && !empty($cust_info_id)){  echo 'active';   } ?>" id="tab_1">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-user"></i>Customer Information
 										</div>
										<!-- <div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div> -->
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form action="#" class="horizontal-form">
											<div class="form-body">
												<h3 class="form-section">Person Info</h3>
												<div class="row">
													<div class="col-md-6 ">
														<div class="form-group">
															<label>Company Name </label>
															<input type="text"  name="company_name" placeholder="Company Name"  value="<?php if(!empty($customer_info->company_name)){ echo $customer_info->company_name;   } ?>"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">First Name</label>
															<input type="text" id="firstName" class="form-control" value="<?php if(!empty($customer_info->first_name)){ echo $customer_info->first_name;   } ?>" placeholder="First Name">
															<!-- <span class="help-block">
															This is inline help </span> -->
														</div>
													</div>
													<!--/span-->
													<div class="col-md-4">
														<!-- <div class="form-group has-error"> -->
															<label class="control-label">Last Name</label>
															<input type="text" id="lastName" value="<?php if(!empty($customer_info->last_name)){ echo $customer_info->last_name;   } ?>"   class="form-control" placeholder="Last Name">
															<!-- <span class="help-block">
															This field has error. </span> -->
													<!-- 	</div> -->
													</div>
													<div class="col-md-4">
														<!-- <div class="form-group has-error"> -->
														<div class="form-group">
															<label class="control-label">Email</label>
															<input type="text" class="form-control" name="email" value="<?php if(!empty($customer_info->email)){ echo $customer_info->email;   } ?>" placeholder="Email">
															<!-- <span class="help-block">Select your gender </span> -->
														</div>
														<!-- </div> -->
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Address</label>
															<input type="text" class="form-control" name="address" value="<?php if(!empty($customer_info->address)){ echo $customer_info->address;   } ?>" placeholder="Email">
															<!-- <span class="help-block">Select your gender </span> -->
														</div>
													</div>
													<!--/span-->
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">City</label>
															<input type="text" class="form-control" name="city" value="<?php if(!empty($customer_info->city)){ echo $customer_info->city;   } ?>" placeholder="Email">
														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Zip Code</label>
															<input type="text" class="form-control" name="zip_code" value="<?php if(!empty($customer_info->zip_code)){ echo $customer_info->zip_code;   } ?>" placeholder="Email">
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">Country</label>
																<select class="select2_category form-control" data-placeholder="Choose a Category" tabindex="1">
																	<option value="" label="Select country..." selected="selected">Select country ... </option>
											                        <?php $get_country_list =get_country_array(); if (!empty($get_country_list)): ?>
												                        <?php foreach ($get_country_list as $key => $value): ?>
												                        	<option value="<?php echo $key ?>" <?php if(!empty($customer_info->country)){ if($customer_info->country==$key){ echo 'selected="selected"'; }    }  ?>  ><?php echo $value; ?></option>
												                        <?php endforeach; ?>
											                        <?php endif; ?>		
																</select>
															<span style="color:red;" id="con_error"></span> 
									              			<?php echo form_error('country'); ?>
														</div>

													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">State</label>
															<input type="text" class="form-control" name="state" value="<?php if(!empty($customer_info->state)){ echo $customer_info->state;   } ?>"  placeholder="State">
														</div>
													</div>
													<!--/span-->
												</div>

												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">Phone</label>
															<input type="text" class="form-control" name="phone" value="<?php if(!empty($customer_info->phone)){ echo $customer_info->phone;   } ?>" placeholder="Phone">	
															<span style="color:red;" id="con_error"></span> 
									              			<?php //echo form_error('country'); ?>
														</div>

													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">Fax</label>
															<input type="text" class="form-control" name="fax" value="<?php if(!empty($customer_info->fax)){ echo $customer_info->fax;   } ?>" placeholder="Fax">
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<h3 class="form-section">Shipping Address</h3>
												<div class="row">
													<div class="col-md-6 ">
														<div class="form-group">
															<label> Same As Above Information </label>
															<span  <?php if($customer_info->sametobill==1){ echo 'class="checked"';  } ?>>
															<input type="checkbox" name="same_to_bill" placeholder="Company Name"   <?php if($customer_info->sametobill==1){ echo 'checked="checked"';  } ?>  class="form-control">
														</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6 ">
														<div class="form-group">
															<label>Company Name </label>
															<input type="text"  name="ship_company_name" placeholder="Company Name" value="<?php if(!empty($customer_info->ship_company_name)){ echo $customer_info->ship_company_name;   } ?>"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">First Name</label>
															<input type="text" id="firstName" name="ship_first_name" class="form-control" value="<?php if(!empty($customer_info->ship_first_name)){ echo $customer_info->ship_first_name;   } ?>" placeholder="First Name">
															<span class="help-block">
															This is inline help </span>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-4">
														<div class="form-group has-error">
															<label class="control-label">Last Name</label>
															<input type="text" id="lastName" name="ship_last_name"  value="<?php if(!empty($customer_info->ship_last_name)){ echo $customer_info->ship_last_name;   } ?>"   class="form-control" placeholder="Last Name">
															<span class="help-block">
															This field has error. </span>
														</div>
													</div>
													<div class="col-md-4">
														<!-- <div class="form-group has-error"> -->
														<div class="form-group">
															<label class="control-label">Email</label>
															<input type="text" class="form-control" name="ship_email"  value="<?php if(!empty($customer_info->ship_email)){ echo $customer_info->ship_email;   } ?>" placeholder="Email">
															<span class="help-block">Select your gender </span>
														</div>
														<!-- </div> -->
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Address</label>
															<input type="text" class="form-control" name="ship_address" value="<?php if(!empty($customer_info->ship_address)){ echo $customer_info->ship_address;   } ?>" placeholder="Email">
															<span class="help-block">Select your gender </span>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">City</label>
															<input type="text" class="form-control" name="city" value="" placeholder="Email">
														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Zip Code</label>
															<input type="text" class="form-control" name="zip_code" value="" placeholder="Email">
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">Country</label>
																<select class="select2_category form-control" data-placeholder="Choose a Category" tabindex="1">
																	<option value="" label="Select country..." selected="selected">Select country ... </option>
											                        <?php $get_country_list =get_country_array(); if (!empty($get_country_list)): ?>
												                        <?php foreach ($get_country_list as $key => $value): ?>
												                        	<option value="<?php echo $key ?>" <?php if(!empty($_POST['country'])){ if($_POST['country']==$key){ echo 'selected="selected"'; }    }  ?>  ><?php echo $value; ?></option>
												                        <?php endforeach; ?>
											                        <?php endif; ?>		
																</select>
															<span style="color:red;" id="con_error"></span> 
									              			<?php echo form_error('country'); ?>
														</div>

													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">State</label>
															<input type="text" class="form-control" name="state" value=""  placeholder="State">
														</div>
													</div>
													<!--/span-->
												</div>

												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">Phone</label>
															<input type="text" class="form-control" name="phone" value="" placeholder="Phone">	
															<span style="color:red;" id="con_error"></span> 
									              			<?php //echo form_error('country'); ?>
														</div>

													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">Fax</label>
															<input type="text" class="form-control" name="fax" value="" placeholder="Fax">
														</div>
													</div>
													<!--/span-->
												</div>
												
												
											</div>
											<div class="form-actions right">
												<button type="button" class="btn default">Cancel</button>
												<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>
							</div>
							<?php } ?>

							<div class="tab-pane <?php if(empty($customer_info) && empty($cust_info_id)){  echo 'active';   } ?>" id="tab_1">

										<div class="row">

										<div class="col-md-12 col-sm-12">
										<div class="table-container">
										<?php echo form_open(base_url('backend/orders/add/customer'),array('id'=>'customer_search_form') ); ?>
										<div id="customer_error" class="text-center" style="color:red;font-size:14px;"></div>
											<table class="table table-striped table-bordered table-hover" id="datatable_invoices">
												<thead>
													<tr role="row" class="heading">
														<th width="10%">
															# Customer Id
														</th>
														<th width="15%">
															First Name
														</th>
														<th width="15%">
															Last Name
														</th>
														<th width="20%">
															Email
														</th>
														<th width="15%">
															Company Name
														</th>
													
														<th width="20%">
															 Actions
														</th>
													</tr>
												</thead>
											<tbody>
											<tr role="row" class="filter">
												<td>
												<input type="text" placeholder="Id" id="customer_id" class="form-control form-filter input-sm" name="customer_id">
												</td>
												<td>
													<input type="text" id="customer_first_name"  placeholder="First Name" class="form-control form-filter input-sm" name="customer_first_name">
												</td>
												<td>
													<input type="text" id="customer_last_name" placeholder="Last Name" class="form-control form-filter input-sm" name="customer_last_name">
												</td>
												<td>
												
													<input type="text" id="customer_email" placeholder="Email" class="form-control form-filter input-sm" name="customer_email">

												</td>
												<td>
													<div class="margin-bottom-5">
														<input type="text" id="customer_company_name" class="form-control form-filter input-sm" name="customer_company_name" placeholder="Company Name"/>
													<span style=""> </span>
													</div>
													<!-- <input type="text" class="form-control form-filter input-sm" name="order_invoice_amount_to" placeholder="To"/> -->
												</td>
										    <!--<td>
													<select name="order_invoice_status" class="form-control form-filter input-sm">
														<option value="">Select...</option>
														<option value="pending">Pending</option>
														<option value="paid">Paid</option>
														<option value="canceled">Canceled</option>
													</select>
												</td> -->
												<td>
													<!-- <div class="margin-bottom-5"> -->
													<button id="custmer_serach" type="submit" class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
														<!-- <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button> -->
													<a id="add_new_customer" class="btn btn-sm red filter-cancel" href="javascript:;"><i class="fa fa-plus"></i> Customer </a>
													<!-- </div> -->
												</td>
											</tr>
											
											
											</tbody>
											</table>
											<?php echo form_close();  ?>
										</div>

										<?php if(!empty($customers)): ?>
										<div class="portlet grey-cascade box">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-users"></i>Search Customers List
												</div>
											<!-- 	<div class="actions">
													<a href="#" class="btn btn-default btn-sm">
													<i class="fa fa-pencil"></i> Edit </a>
												</div> -->
											</div>
											<div class="portlet-body">
												<div class="table-responsive">
													<table class="table table-hover table-bordered table-striped">
													<thead>
													<tr>
														<th> Customer Id</th>
														<th> First Name</th>
														<th> Last Name </th>
														<th> Email</th>
														<th> Company Name </th>
														<th> Active</th>
													</tr>
													</thead>
													<tbody>
												<?php foreach($customers as $value){ ?> 
 														
													<tr>
														<td><a href="#"><?php if(!empty($value->customer_id)) echo $value->customer_id; ?></a></td>
														<td><?php if(!empty($value->first_name)) echo $value->first_name; ?></td>
														<td><?php if(!empty($value->first_name)) echo $value->first_name; ?></td>
														<td><?php if(!empty($value->email)) echo $value->email; ?></td>
														<td><?php if(!empty($value->company_name)) echo $value->company_name; ?></td>
														<td><a href="<?php echo base_url('backend/orders/add/cust_info/'.$value->id);  ?>" class="label label-sm label-success">Select</a></td>
													</tr>
												<?php  }  ?>	
													
													</tbody>
													</table>
												</div>
											</div>
										</div>
												<?php  endif;  ?>

											</div>
										</div>
										<!-- <div class="row">
											<div class="col-md-6">
											</div>
											<div class="col-md-6">
												<div class="well">
													<div class="row static-info align-reverse">
														<div class="col-md-8 name">
															 Sub Total:
														</div>
														<div class="col-md-3 value">
															 $1,124.50
														</div>
													</div>
													<div class="row static-info align-reverse">
														<div class="col-md-8 name">
															 Shipping:
														</div>
														<div class="col-md-3 value">
															 $40.50
														</div>
													</div>
													<div class="row static-info align-reverse">
														<div class="col-md-8 name">
															 Grand Total:
														</div>
														<div class="col-md-3 value">
															 $1,260.00
														</div>
													</div>
													<div class="row static-info align-reverse">
														<div class="col-md-8 name">
															 Total Paid:
														</div>
														<div class="col-md-3 value">
															 $1,260.00
														</div>
													</div>
													<div class="row static-info align-reverse">
														<div class="col-md-8 name">
															 Total Refunded:
														</div>
														<div class="col-md-3 value">
															 $0.00
														</div>
													</div>
													<div class="row static-info align-reverse">
														<div class="col-md-8 name">
															 Total Due:
														</div>
														<div class="col-md-3 value">
															 $1,124.50
														</div>
													</div>
												</div>
											</div>
										</div> -->
									</div>
									<div class="tab-pane" id="tab_2">
										<div class="table-container">
											<div class="table-actions-wrapper">
												<span>
												</span>
												<select class="table-group-action-input form-control input-inline input-small input-sm">
													<option value="">Select...</option>
													<option value="pending">Pending</option>
													<option value="paid">Paid</option>
													<option value="canceled">Canceled</option>
												</select>
												<button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> Submit</button>
											</div>
											<table class="table table-striped table-bordered table-hover" id="datatable_invoices">
											<thead>
											<tr role="row" class="heading">
												<th width="5%">
													<input type="checkbox" class="group-checkable">
												</th>
												<th width="5%">
													 Invoice&nbsp;#
												</th>
												<th width="15%">
													 Bill To
												</th>
												<th width="15%">
													 Invoice&nbsp;Date
												</th>
												<th width="10%">
													 Amount
												</th>
												<th width="10%">
													 Status
												</th>
												<th width="10%">
													 Actions
												</th>
											</tr>
											<tr role="row" class="filter">
												<td>
												</td>
												<td>
													<input type="text" class="form-control form-filter input-sm" name="order_invoice_no">
												</td>
												<td>
													<input type="text" class="form-control form-filter input-sm" name="order_invoice_bill_to">
												</td>
												<td>
													<div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
														<input type="text" class="form-control form-filter input-sm" readonly name="order_invoice_date_from" placeholder="From">
														<span class="input-group-btn">
														<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
														</span>
													</div>
													<div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
														<input type="text" class="form-control form-filter input-sm" readonly name="order_invoice_date_to" placeholder="To">
														<span class="input-group-btn">
														<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
														</span>
													</div>
												</td>
												<td>
													<div class="margin-bottom-5">
														<input type="text" class="form-control form-filter input-sm" name="order_invoice_amount_from" placeholder="From"/>
													</div>
													<input type="text" class="form-control form-filter input-sm" name="order_invoice_amount_to" placeholder="To"/>
												</td>
												<td>
													<select name="order_invoice_status" class="form-control form-filter input-sm">
														<option value="">Select...</option>
														<option value="pending">Pending</option>
														<option value="paid">Paid</option>
														<option value="canceled">Canceled</option>
													</select>
												</td>
												<td>
													<div class="margin-bottom-5">
														<button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
													</div>
													<button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
												</td>
											</tr>
											</thead>
											<tbody>
											</tbody>
											</table>
										</div>
									</div>
									<div class="tab-pane" id="tab_3">
										<div class="table-container">
											<table class="table table-striped table-bordered table-hover" id="datatable_credit_memos">
											<thead>
											<tr role="row" class="heading">
												<th width="5%">
													Credit&nbsp;Memo&nbsp;#
												</th>
												<th width="15%">
													Bill To
												</th>
												<th width="15%">
													Created&nbsp;Date
												</th>
												<th width="10%">
													Status
												</th>
												<th width="10%">
													Actions
												</th>
											</tr>
											</thead>
											<tbody>
											</tbody>
											</table>
										</div>
									</div>
									<div class="tab-pane" id="tab_4">
										<div class="table-container">
											<table class="table table-striped table-bordered table-hover" id="datatable_shipment">
											<thead>
											<tr role="row" class="heading">
												<th width="5%">
													 Shipment&nbsp;#
												</th>
												<th width="15%">
													 Ship&nbsp;To
												</th>
												<th width="15%">
													 Shipped&nbsp;Date
												</th>
												<th width="10%">
													 Quantity
												</th>
												<th width="10%">
													 Actions
												</th>
											</tr>
											<tr role="row" class="filter">
												<td>
													<input type="text" class="form-control form-filter input-sm" name="order_shipment_no">
												</td>
												<td>
													<input type="text" class="form-control form-filter input-sm" name="order_shipment_ship_to">
												</td>
												<td>
													<div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
														<input type="text" class="form-control form-filter input-sm" readonly name="order_shipment_date_from" placeholder="From">
														<span class="input-group-btn">
														<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
														</span>
													</div>
													<div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
														<input type="text" class="form-control form-filter input-sm" readonly name="order_shipment_date_to" placeholder="To">
														<span class="input-group-btn">
														<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
														</span>
													</div>
												</td>
												<td>
													<div class="margin-bottom-5">
														<input type="text" class="form-control form-filter input-sm" name="order_shipment_quantity_from" placeholder="From"/>
													</div>
													<input type="text" class="form-control form-filter input-sm" name="order_shipment_quantity_to" placeholder="To"/>
												</td>
												<td>
													<div class="margin-bottom-5">
														<button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
													</div>
													<button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
													<button class="btn btn-sm green filter-cancel"><i class="fa fa-times"></i> Add New </button>
												</td>
											</tr>
											</thead>
											<tbody>
											</tbody>
											</table>
										</div>
									</div>
									<div class="tab-pane" id="tab_5">
										<div class="table-container">
											<table class="table table-striped table-bordered table-hover" id="datatable_history">
											<thead>
												<tr role="row" class="heading">
													<th width="25%">
														 Datetime
													</th>
													<th width="55%">
														 Description
													</th>
													<th width="10%">
														 Notification
													</th>
													<th width="10%">
														 Actions
													</th>
												</tr>
												<tr role="row" class="filter">
													<td>
														<div class="input-group date datetime-picker margin-bottom-5" data-date-format="dd/mm/yyyy hh:ii">
															<input type="text" class="form-control form-filter input-sm" readonly name="order_history_date_from" placeholder="From">
															<span class="input-group-btn">
															<button class="btn btn-sm default date-set" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
														<div class="input-group date datetime-picker" data-date-format="dd/mm/yyyy hh:ii">
															<input type="text" class="form-control form-filter input-sm" readonly name="order_history_date_to" placeholder="To">
															<span class="input-group-btn">
															<button class="btn btn-sm default date-set" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>
													</td>
													<td>
														<input type="text" class="form-control form-filter input-sm" name="order_history_desc" placeholder="To"/>
													</td>
													<td>
														<select name="order_history_notification" class="form-control form-filter input-sm">
															<option value="">Select...</option>
															<option value="pending">Pending</option>
															<option value="notified">Notified</option>
															<option value="failed">Failed</option>
														</select>
													</td>
													<td>
														<div class="margin-bottom-5">
															<button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
														</div>
														<button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
													</td>
												</tr>
											</thead>
											<tbody>
											</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>