<div class="row">
	<div class="col-md-12 ">
		<div class="portlet box blue">    
			<div class="portlet-title"> 
				<div class="caption">            
					<i class="fa fa-sitemap"></i> Edit Category      
				</div>       
				<div class="tools">   
					<a href="" class="collapse"></a>
				</div>
			</div>

			<div class="portlet-body form">   
				<form role="form" method="post" action="<?php echo current_url() ?>" class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<div class="col-md-12"> 
			                <label class="control-label"><strong>Category Name</strong></label>                                     
							<input type="text" placeholder="Category Name" class="form-control" name="category_name" value="<?php if(!empty($category->category)) echo $category->category;?>">
							<?php echo form_error('category_name'); ?>
			            	</div> 
			            </div> 
         
            
			            <div class="form-group">
							<div class="col-md-12">
			                <label class="control-label"><strong>Status</strong></label>							                                     
							<select class="form-control" name="status" id="remote" style="width:100%">
								<option value="1" <?php if($category->status==1) echo 'selected="selected"'; ?>>Active</option>
		              			<option value="0" <?php if($category->status==0) echo 'selected="selected"'; ?>>Deactive</option>
							</select>                                 
							<?php echo form_error('status'); ?>
			            </div>
			            </div>
					</div> 
					<div class="form-actions">       
						<button type="submit" class="btn blue">Submit</button> 
						<a href="<?php echo base_url('backend/categories/'); ?>" ><button class="btn btn-danger" type="button">Cancel</button></a>
					</div>  
				</form>    
			</div>
		</div>
	</div>
</div>
