      <div class="row">
            <div class="col-mod-12">  
               <h3 class="page-header"> Orders </h3>
            </div>
      </div>
      <?php
        if(!empty($_SERVER['QUERY_STRING']))
            $QUERY_STRING = "0?".$_SERVER['QUERY_STRING'];
        else
            $QUERY_STRING ='';
      ?>
          <!-- Users widget -->
      <div class="row">
       <div class="col-md-12">
        <div class="panel">
         <div class="panel-heading text-primary">
          <h3 class="panel-title"><i class="fa fa-user"></i> List        <a class=" btn btn-xs btn-info" href="<?php echo base_url('backend/users/add');  ?>"  style="margin-top:3px;"class="btn btn-small">Add User</a>   </h3>
        </div>
      <div class="panel-body">
          <?php  echo msg_alert_backend();  ?>
        <div class="pull-right">


                  <form class="form-inline" action="<?php echo base_url('backend/users/index/id/asc/') ?>" role="form">

                      <div class="form-group">
                          <select style="width:100%;"  class="form-control" name="search_by"> <option value="">Select Field  </option>  <option value="first_name" <?php if(!empty($_GET['search_by']) && $_GET['search_by']=='first_name') echo 'selected'?>>First Name </option><option value="last_name" <?php if(!empty($_GET['search_by']) && $_GET['search_by']=='last_name') echo 'selected'?>>Last Name</option> <option value="email" <?php if(!empty($_GET['search_by']) && $_GET['search_by']=='email') echo 'selected'?>>Email </option>  </select> 
                      </div>

                      <div class="form-group">
                          <input  class="form-control" type="text" style="width:100%;" name="search_query"  placeholder="Search" value="<?php if(!empty($_GET['search_query'])) echo $_GET['search_query'] ?>">
                      </div>

                      <button class="btn btn-primary" type="submit">Search</button>

                      <a class="btn btn-warning" href="<?php echo base_url('backend/users/');  ?>"  style="margin-top:3px;"class="btn btn-small">Reset</a> 

                  </form>
        </div>
      <br>
   <div class="clearfix"></div>

       <form action="<?php echo  base_url('backend/orders/multi_delete_orders/');   ?>" id="order_delete_form" method="post" accept-charset="utf-8">

       <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">

          <thead>

            <tr>

              <th width="5%"><input type="checkbox" name="check_all" id="all_order_check"></th>                                           

              <th width="10%" >Order id</th> 

              <th width="20%">User Name</th>                      

              <th width="10%">Order Date</th>

              <th width="10%">Total</th>

<!--              <th width="10%">Status</th>                    -->

              <th width="10%">Action</th>

             <!--  <th width="10%">Delete</th> -->

            </tr>

          </thead>

          <tbody>

          <?php  if(!empty($orders)){ $i = $offset; foreach($orders as $row){ ?> 

            <tr>

                <input type="hidden" name="type" value="superadmin">

                  <td><input type="checkbox" name="checkall[]" value="<?php echo $row->id;?>" class="check_order"></td>

                  <td><?php echo $row->order_id ?></td>

                  <td><?php if(!empty($row->first_name)|| !empty($row->last_name)){  echo $row->first_name.' '.$row->last_name; }     ?></td>

                  <td><?php if(!empty($row->order_created)) echo date('m/d/Y', strtotime($row->order_created)); else echo date('m/d/Y', strtotime($row->created)); ?></td>

                  <td><?php if(!empty($row->total_amount)){  echo  $row->total_amount; } ?></td>

               
<!--                  <td>

                    <select  id="order_status_<?php echo $row->id; ?>"  class="get_status"  name="order_status">

                          <option value="1_id_<?php echo $row->id; ?>"  <?php if(!empty($row->order_status) && $row->order_status==1){ ?> selected="selected"  <?php } ?>    >Pending</option>    

                          <option value="2_id_<?php echo $row->id; ?>"  <?php if(!empty($row->order_status) && $row->order_status==2){ ?> selected="selected"  <?php } ?>  >Approved</option>    

                          <option value="3_id_<?php echo $row->id; ?>"  <?php if(!empty($row->order_status) && $row->order_status==3){ ?> selected="selected"  <?php } ?>  >Shipped</option>    

                          <option value="4_id_<?php echo $row->id; ?>"  <?php if(!empty($row->order_status) && $row->order_status==4){ ?> selected="selected"  <?php } ?> >Completed</option>   

                          <option value="5_id_<?php echo $row->id; ?>"  <?php if(!empty($row->order_status) && $row->order_status==5){ ?> selected="selected"  <?php } ?> >On Hold</option>   

                    </select>

                  </td>-->

                  <td>

                    <div class="btn-group"> 

                     

                        <a href="<?php echo base_url() ?>backend/orders/order_view/<?php echo $row->id; ?>" class="btn btn-xs btn-warning" rel="tooltip" data-placement="top" data-original-title="View Order"><i class="fa fa-eye"></i></a>

                      

                        <a href="<?php echo base_url() ?>backend/orders/delete_orders/<?php echo $row->id; ?>" onclick="return confirm('are you sure?');" class="btn btn-xs btn-danger" rel="tooltip" data-placement="top" data-original-title="Delete Order"><i class="fa fa-trash-o"></i></a>

                    </div>

                 </td>

              

              </tr>

                  

                  <?php $i++; } } else { ?>

          <tr>



            <td colspan="8" style="text-align: center;font-style: italic;"><h3>No orders found</h3></td>

          </tr>

          <?php } ?>  

          <tr>

            <td colspan="8"><input type="submit" id="delete_order" name="delete"  value="Delete" class="btn-info btn"></td>

           <!--  <td colspan="6"><input type="submit" name="message" value="Message" class="btn-info btn"></td> -->

          </tr>

                </tbody>

              </table>

          </form>    
      </div>
    </div>
  </div>
</div>  <!-- / Users widget-->

<script>
    $(document).ready(function () {
        $(document).on('click', "#all_order_check", function () {
            $(".check_order").prop('checked', $(this).prop('checked'));
        });

    });

</script>