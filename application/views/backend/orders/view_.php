<style type="text/css" media="screen">
    hr {
        margin-top: 0px; 
        margin-bottom: 0px; 
        border: 0; 
        border-top: 1px solid #151515;
    }
</style>
<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

<!-- /.modal -->
<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<!-- BEGIN STYLE CUSTOMIZER -->

<!-- END STYLE CUSTOMIZER -->
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Order View 
            <small>Details</small>
        </h3>

        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row"> 


    <div class="col-md-12">
        <?php echo msg_alert_backend(); ?>
        <!-- Begin: life time stats -->
        <div class="portlet">
            <div  class="panel-body"> 
            <div class="portlet-body">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-lg">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab">
                                Details </a>
                        </li>
                    </ul><br>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <?php echo form_open(current_url()); ?>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet yellow-crusta box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                 <span style="font-weight:5px; font-size: 15px; text-decoration: underline; text-transform: uppercase">Order Details</span>
                                            </div>
                                            <!-- <div class="actions">
                                                    <a href="#" class="btn btn-default btn-sm">
                                                    <i class="fa fa-pencil"></i> Edit </a>
                                            </div> -->
                                        </div>

                                        <div class="portlet-body">
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Order #<span class="pull-right">:</span>
                                                </div>
                                                <div class="col-md-7 value">
                                                    <?php
                                                    if (!empty($order->order_id)) {
                                                        echo $order->order_id;
                                                    }
                                                    ?><!-- <span class="label label-info label-sm">
                                                        Email confirmation was sent </span> -->
                                                </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Order Date & Time<span class="pull-right">:</span>
                                                </div>
                                                <div class="col-md-7 value"> 

                                                    <?php
                                                    if (!empty($order->created)) {
                                                        echo date('M D Y', strtotime($order->created));
                                                    }
                                                    ?>
                                                </div>
                                            </div>
<!--       No Use status                                    <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Order Status:
                                                </div>
                                                <div class="col-md-7 value">

                                                    <select  id="order_status"  class="status"  name="order_status">
                                                        <option value="1"  <?php if (!empty($order->order_status) && $order->order_status == 1) { ?> selected="selected"  <?php } ?>    >Pending</option>    
                                                        <option value="2"  <?php if (!empty($order->order_status) && $order->order_status == 2) { ?> selected="selected"  <?php } ?>  >Approved</option>    
                                                        <option value="3"  <?php if (!empty($order->order_status) && $order->order_status == 3) { ?> selected="selected"  <?php } ?>  >Shipped</option>    
                                                        <option value="4"  <?php if (!empty($order->order_status) && $order->order_status == 4) { ?> selected="selected"  <?php } ?> >Completed</option>   
                                                        <option value="5"  <?php if (!empty($order->order_status) && $order->order_status == 5) { ?> selected="selected"  <?php } ?> >On Hold</option>   
                                                    </select>
                                                </div>
                                            </div>-->
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Grand Total<span class="pull-right">:</span>
                                                </div>
                                                <div class="col-md-7 value">
                                                    <?php
                                                    if (!empty($order->total_amount)) {
                                                        echo '$ ' . number_format($order->total_amount, 2);
                                                    }
                                                    ?>
                                                </div>
                                            </div><br>
                                            <div class="row static-info">
                                                <div class="col-md-7 name">
                                                    <span style="font-weight:5px; font-size: 15px; text-decoration: underline; text-transform: uppercase">Payment Information</span>
                                                </div>
                                                <div class="col-md-5 value">
                                                    <?php
                                                    if (!empty($order->payment_method) && $order->payment_method == 'by_card') {
                                                        echo 'Credit Card';
                                                    }
                                                    ?>
                                                    <?php
                                                    if (!empty($order->payment_method) && $order->payment_method == 'paypal_express_pay') {
                                                        echo 'Paypal Express Pay';
                                                    }
                                                    ?>
                                                    <?php
                                                    if (!empty($order->payment_method) && $order->payment_method == 'account_pay') {
                                                        echo 'Account Pay';
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Download Count <span class="pull-right">:</span>
                                                </div>
                                                <div class="col-md-7 value">
                                                    <?php
                                                    if (!empty($order->download_count)) {
                                                        echo $order->download_count;
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                             <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Coupon Discount <span class="pull-right">:</span>
                                                </div>
                                                <div class="col-md-7 value">
                                                    <?php
                                                    if (!empty($order->coupon_discount)) {
                                                        echo $order->coupon_discount;
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Coupon Code <span class="pull-right">:</span>
                                                </div>
                                                <div class="col-md-7 value">
                                                    <?php
                                                    if (!empty($order->coupon_code)) {
                                                        echo $order->coupon_code;
                                                    }
                                                    ?>
                                                </div>
                                            </div><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet blue-hoki box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <span style="font-weight:5px; font-size: 15px; text-decoration: underline; text-transform: uppercase">Customer Information</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Customer Name <span class="pull-right">:</span>
                                                </div>
                                                <div class="col-md-7 value">
                                                    <?php
                                                    if (!empty($order->user_name)) {
                                                        echo $order->user_name ;
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Email <span class="pull-right">:</span>
                                                </div>
                                                <div class="col-md-7 value">
                                                    <?php
                                                    if (!empty($order->user_email)) {
                                                        echo $order->user_email;
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                          
                            <?php echo form_close(); ?>	
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="portlet grey-cascade box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <span style="font-weight:5px; font-size: 15px; text-decoration: underline; text-transform: uppercase">Shopping Cart</span>
                                            </div>
                                            <!-- <div class="actions">
                                                    <a href="#" class="btn btn-default btn-sm">
                                                    <i class="fa fa-pencil"></i> Edit </a>
                                            </div> -->
                                        </div>
                                        <div class="portlet-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Project title
                                                            </th>
                                                            <th>
                                                                Item Status
                                                            </th>
                                                            
                                                            <th>
                                                                Price
                                                            </th>
                                                           
                                                            <!-- <th>
                                                                     Tax Amount
                                                            </th> -->
                                                    <!-- 	<th>
                                                                     Tax Percent
                                                            </th> -->
                                                            <!-- <th>
                                                                     Discount Amount
                                                            </th> -->
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        if (!empty($ordered_project)): $i = 1;
                                                            foreach ($ordered_project as $value) {
                                                                ?>

                                                                <tr>
                                                                    <td>
                                                                        <a target="_blank" href="<?php echo base_url('projects/detail/' . url_title($value->title, '-', TRUE)); ?>">
                                                                            <?php
                                                                            if (!empty($value->title)) {
                                                                                echo ucfirst($value->title);
                                                                            }
                                                                            ?>
                                                                        </a>
                                                                    </td>
                                                                    <td>
                                                                        <?php if (!empty($value->id)) {
                                                                            ?> 															
                                                                            <?php if (!empty($value)) { ?>
                                                                                <?php if (!empty($value->avilable_item) && $product->avilable_item == 1) { ?>
                                                                                    <span class="label label-sm label-success">
                                                                                        Available				
                                                                                    </span>
                                                                                <?php } else { ?>
                                                                                    <span class="label label-sm label-danger">
                                                                                        Not  Available			 	
                                                                                    </span>

                                                                                    <?php
                                                                                }
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </td>

                                                                    <td>
                                                                        <?php
                                                                        if (!empty($value->price)) {
                                                                            echo number_format($value->price, 2);
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                    
                                                                </tr>
                                                                <?php
                                                                $i++;
                                                            } else:
                                                            ?>
                                                            <tr>
                                                                <td colspan="6"><h2>oops !! Project Not Found</h2></td>	
                                                            </tr>
                                                        <?php endif; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <div class="well">
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-8 name">
                                                Sub Total:
                                            </div>
                                            <div class="col-md-3 value">
                                                <?php
                                                if (!empty($order->gross_amount)) {
                                                    echo '$ ' . number_format($order->gross_amount, 2);
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-8 name">
                                                Shipping:
                                            </div>
                                            <div class="col-md-3 value">
                                                <?php
                                                if (!empty($order->shipping_amount)) {
                                                    echo '$ ' . number_format($order->shipping_amount, 2);
                                                }
                                                ?>
                                            </div>
                                        </div>	<hr>
                                        <!-- <div class="row static-info align-reverse">
                                                <div class="col-md-8 name">
                                                         Grand Total:
                                                </div>
                                                <div class="col-md-3 value">
                                        <?php
                                        if (!empty($order->total_amount)) {
                                            echo '$ ' . number_format($order->total_amount, 2);
                                        }
                                        ?>
                                                         
                                                </div>
                                        </div> -->
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-8 name">
                                                Redeem Points (Refunded):
                                            </div>
                                            <div class="col-md-3 value">
                                                <?php
                                                if (!empty($order->redeem_amount)) {
                                                    echo '- $ ' . number_format($order->redeem_amount, 2);
                                                }
                                                ?>
                                            </div>
                                        </div>	<hr>
                                        <!-- <div class="row static-info align-reverse">
                                                <div class="col-md-8 name">
                                                         Total Refunded:
                                                </div>
                                                <div class="col-md-3 value">
                                                         $0.00
                                                </div>
                                        </div> -->
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-8 name">
                                                Grand Total:
                                            </div>
                                            <div class="col-md-4 value">
                                                <?php
                                                if (!empty($order->total_amount)) {
                                                    echo '$ ' . number_format($order->total_amount, 2);
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<!-- END PAGE CONTENT-->
