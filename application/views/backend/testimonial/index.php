      <div class="row">
            <div class="col-mod-12">  
               <h3 class="page-header"> Testimonial </h3>
            </div>
      </div>
      <?php
        if(!empty($_SERVER['QUERY_STRING']))
            $QUERY_STRING = "0?".$_SERVER['QUERY_STRING'];
        else
            $QUERY_STRING ='';
      ?>
          <!-- Users widget -->
      <div class="row">
       <div class="col-md-12">
        <div class="panel">
         <div class="panel-heading text-primary">
          
        </div>
      <div class="panel-body">
          <?php  echo msg_alert_backend();  ?>
        <div class="pull-right">


                  <form class="form-inline" action="<?php echo base_url('backend/testimonial/index/id/asc/') ?>" role="form">

                      <div class="form-group">
                          <select style="width:100%;"  class="form-control" name="search_by"> <option value="">Select Field  </option>  <option value="first_name" <?php if(!empty($_GET['search_by']) && $_GET['search_by']=='first_name') echo 'selected'?>>First Name </option><option value="last_name" <?php if(!empty($_GET['search_by']) && $_GET['search_by']=='last_name') echo 'selected'?>>Last Name</option> <option value="email" <?php if(!empty($_GET['search_by']) && $_GET['search_by']=='email') echo 'selected'?>>Email </option>  </select> 
                      </div>

                      <div class="form-group">
                          <input  class="form-control" type="text" style="width:100%;" name="search_query"  placeholder="Search" value="<?php if(!empty($_GET['search_query'])) echo $_GET['search_query'] ?>">
                      </div>

                      <button class="btn btn-primary" type="submit">Search</button>

                      <a class="btn btn-warning" href="<?php echo base_url('backend/testimonial/');  ?>"  style="margin-top:3px;"class="btn btn-small">Reset</a> 

                  </form>
        </div>
      <br>
   <div class="clearfix"></div>

       <form action="<?php echo  base_url('backend/testimonial/multi_delete/');   ?>" id="testimonial_delete_form" method="post" accept-charset="utf-8">

       <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">

          <thead>

            <tr>

              <th width="5%"><input type="checkbox" name="check_all" id="all_testimonial_check"></th>                                           

              <th width="10%" >Id</th> 

              <th width="20%">User Name</th>                      

              <th width="10%">Testimonial</th>
              <th width="10%">Date</th>


              <th width="10%">Action</th>

             <!--  <th width="10%">Delete</th> -->

            </tr>

          </thead>

          <tbody>

          <?php  if(!empty($testimonials)){ $i = $offset; foreach($testimonials as $row){ ?> 

            <tr>

                <input type="hidden" name="type" value="superadmin">

                  <td><input type="checkbox" name="checkall[]" value="<?php echo $row->id;?>" class="check_testimonial"></td>

                  <td><?php echo $row->id ?></td>

                  <td><?php if(!empty($row->first_name)|| !empty($row->last_name)){  echo $row->first_name.' '.$row->last_name; }     ?></td>

                  <td><?php if(!empty($row->content)){  echo  $row->content; } ?></td>
                  
                  <td><?php echo date('m/d/Y', strtotime($row->added_on)); ?></td>

                  

               


                  <td>

                    <div class="btn-group"> 

                     
                        <?php if($row->is_approved): ?>
                        <a href="<?php echo base_url() ?>backend/testimonial/unapprove/<?php echo $row->id; ?>" class="btn btn-xs btn-warning" rel="tooltip" data-placement="top" data-original-title="Unapprove"><i class="fa fa-times"></i></a>
                        <?php else: ?>
                        <a href="<?php echo base_url() ?>backend/testimonial/approve/<?php echo $row->id; ?>" class="btn btn-xs btn-warning" rel="tooltip" data-placement="top" data-original-title="Approve"><i class="fa fa-check"></i></a>
                        <?php endif?>
                      

                        <a href="<?php echo base_url() ?>backend/testimonial/delete/<?php echo $row->id; ?>" onclick="return confirm('are you sure?');" class="btn btn-xs btn-danger" rel="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>

                    </div>

                 </td>

              

              </tr>

                  

                  <?php $i++; } } else { ?>

          <tr>



            <td colspan="8" style="text-align: center;font-style: italic;"><h3>No Testimonial found</h3></td>

          </tr>

          <?php } ?>  

          <tr>

            <td colspan="8"><input type="submit" id="delete_order" name="delete"  value="Delete" class="btn-info btn"></td>

           <!--  <td colspan="6"><input type="submit" name="message" value="Message" class="btn-info btn"></td> -->

          </tr>

                </tbody>

              </table>

          </form>    
      </div>
    </div>
  </div>
</div>  <!-- / Users widget-->

<script>
    $(document).ready(function () {
        $(document).on('click', "#all_testimonial_check", function () {
            $(".check_testimonial").prop('checked', $(this).prop('checked'));
        });

    });

</script>