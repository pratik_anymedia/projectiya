<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Settings</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            
        <div class="panel-body">
          <div class="panel panel-default">
              <div class="panel-body">
                <?php  echo msg_alert_backend();  ?>    
              <div class="tab-content">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#siteSettings" data-toggle="tab">Site Settings</a> </li>
                    <li><a href="#contactSettings" data-toggle="tab">Contact</a></li>
                    <li><a href="#socialSettings" data-toggle="tab">Social</a></li>
                    <li><a href="#commonSettings" data-toggle="tab">Common Setting</a></li>
                    <!--<li><a href="#contactAddressSettings" data-toggle="tab">Contact Address</a></li>-->
                </ul>
                  
                  <div class="tab-pane fade in active" id="siteSettings">
                    <h4>Basic Site Settings</h4>
                    <hr/>
                    <form action="<?php echo base_url('superadmin/saveBasicSettings');?>" method="post" id="" enctype="multipart/form-data" class="">
                        <div class="form-group">
                            <label for="admin_email">Admin Email</label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'admin_email',
                                    'id'            => 'admin_email',
                                    'value'         => set_value('admin_email') ? set_value('admin_email') : (isset($SETTINGS['admin_email']) ? $SETTINGS['admin_email'] : ''),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'placeholder'   => 'Enter Admin Email',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>
                        
                        <div class="form-group"> 
                            <label for="welcome_text">Welcome Text </label>
                            <div class="controls"> 
                                <?php $data = array( 'name' => 'welcome_text',
                                    'id' => 'welcome_text', 
                                    'value' => set_value('welcome_text') ? set_value('welcome_text') : (isset($SETTINGS['welcome_text']) ? $SETTINGS['welcome_text'] : ''), 
                                    'class' => 'form-control tinymce_edittor',
                                    'rows' => '20' ); 
                                echo form_textarea($data); ?>
                            </div> 
                        </div> 
                        
                        <div class="form-group">
                            <label for="noreply_email">No Reply Email</label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'noreply_email',
                                    'id'            => 'noreply_email',
                                    'value'         => set_value('noreply_email') ? set_value('noreply_email') : (isset($SETTINGS['noreply_email']) ? $SETTINGS['noreply_email'] : ''),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'placeholder'   => 'Enter No-Reply Email',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>
                        <div class="form-group">
                            <label for="header_email">Header Email</label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'header_email',
                                    'id'            => 'header_email',
                                    'value'         => set_value('header_email') ? set_value('header_email') : (isset($SETTINGS['header_email']) ? $SETTINGS['header_email'] : ''),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'placeholder'   => 'Enter Email for Site Header',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>

                        <div class="form-group">
                            <label for="phone_number">Phone Number</label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'phone_number',
                                    'id'            => 'phone_number',
                                    'value'         => set_value('phone_number') ? set_value('phone_number') : (isset($SETTINGS['phone_number']) ? $SETTINGS['phone_number'] : ''),
                                    'placeholder'   => 'Enter Phone Number',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>
                        <div class="form-group">
                            <label for="cont_adrs">Contact Address </label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'cont_adrs',
                                    'id'            => 'cont_adrs',
                                    'value'         => set_value('cont_adrs') ? set_value('cont_adrs') : (isset($SETTINGS['cont_adrs']) ? $SETTINGS['cont_adrs'] : ''),
                                    'placeholder'   => 'Enter Contact Address',
                                    'class'         => 'form-control',
                                    'rows'			=> 4
                                );

                                echo form_textarea($data); ?>

                            </div>    
                        </div>
                        
                        <div class="form-group">
                            <label for="cont_fax">Contact Fax </label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'cont_fax',
                                    'id'            => 'cont_fax',
                                    'value'         => set_value('cont_fax') ? set_value('cont_fax') : (isset($SETTINGS['cont_fax']) ? $SETTINGS['cont_fax'] : ''),
                                    'placeholder'   => 'Enter Contact Fax Number ',
                                    'class'         => 'form-control'
                                    
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>
                        <div class="form-group">
                            <label for="cont_web">Contact Web</label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'cont_web',
                                    'id'            => 'cont_web',
                                    'value'         => set_value('cont_web') ? set_value('cont_web') : (isset($SETTINGS['cont_web']) ? $SETTINGS['cont_web'] : ''),
                                    'placeholder'   => 'Enter Contact Web ',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>
                        <div class="form-group">
                            <label for="copyright">Copy Right </label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'copyright',
                                    'id'            => 'copyright',
                                    'value'         => set_value('copyright') ? set_value('copyright') : (isset($SETTINGS['copyright']) ? $SETTINGS['copyright'] : ''),
                                    'placeholder'   => 'Enter Copyright',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>
                        <div class="form-group">
                            <label for="logo">Upload Logo</label>
                            <?php 
                                if(isset($SETTINGS['logo']) && $SETTINGS['logo']!=''):
                            ?>
                            <div class="row">
                                <div class="col-md-2 " style="margin-bottom:10px;">
                                    <img src="<?php echo base_url().'assets/uploads/logo/'.$SETTINGS['logo'] ?>" class="img-thumbnail">
                                </div>
                            </div>
                            <?php
                                endif;
                            ?>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'logo',
                                    'id'            => 'logo',
                                    'value'         => set_value('logo') ? set_value('logo') : (isset($SETTINGS['logo']) ? $SETTINGS['logo'] : ''),
                                    'class'         => 'form-control'
                                );

                                echo form_upload($data); ?>

                            </div>    
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <button type="submit" class="btn btn-default"><i class="fa fa-check"></i> Save</button>
                            </div>    
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade in " id="contactSettings">
                    <h4>Contact Page Settings</h4>
                    <hr/>
                    <form action="<?php echo base_url('superadmin/saveContactSettings');?>" method="post" class="basicForm">
                        <div class="form-group">
                            <label for="contact_email">Send Email To</label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'contact_email',
                                    'id'            => 'contact_email',
                                    'value'         => set_value('contact_email') ? set_value('contact_email') : (isset($SETTINGS['contact_email']) ? $SETTINGS['contact_email'] : ''),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'placeholder'   => 'Enter contact_email Email',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>
                        <div class="form-group">
                            <label for="contact_subject">Subject Line</label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'contact_subject',
                                    'id'            => 'contact_subject',
                                    'value'         => set_value('contact_subject') ? set_value('contact_subject') : (isset($SETTINGS['contact_subject']) ? $SETTINGS['contact_subject'] : ''),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'placeholder'   => 'Enter Subject Line',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <button type="submit" class="btn btn-default"><i class="fa fa-check"></i> Save</button>
                            </div>    
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade in " id="socialSettings">
                    <h4>Social Site Settings</h4>
                    <hr/>
                    <form action="<?php echo base_url('superadmin/saveSocialSettings');?>" method="post" class="basicForm">
                        <div class="form-group">
                            <label for="fb_url">Facebook Page Url </label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'fb_url',
                                    'id'            => 'fb_url',
                                    'value'         => set_value('fb_url') ? set_value('fb_url') : (isset($SETTINGS['fb_url']) ? $SETTINGS['fb_url'] : ''),
                                    'placeholder'   => 'Enter Facebook Page Url',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>
                        <div class="form-group">
                            <label for="twitter_url">Twitter Page Url </label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'twitter_url',
                                    'id'            => 'twitter_url',
                                    'value'         => set_value('twitter_url') ? set_value('twitter_url') : (isset($SETTINGS['twitter_url']) ? $SETTINGS['twitter_url'] : ''),
                                    'placeholder'   => 'Enter Twitter Page Url',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>
                        <div class="form-group">
                            <label for="linkedin_url">Linked In Page Url </label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'linkedin_url',
                                    'id'            => 'linkedin_url',
                                    'value'         => set_value('linkedin_url') ? set_value('linkedin_url') : (isset($SETTINGS['linkedin_url']) ? $SETTINGS['linkedin_url'] : ''),
                                    'placeholder'   => 'Enter Linked In Url',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>

                        <div class="form-group">
                            <label for="dribbble_url">Dribbble Page Url </label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'dribbble_url',
                                    'id'            => 'dribbble_url',
                                    'value'         => set_value('dribbble_url') ? set_value('dribbble_url') : (isset($SETTINGS['dribbble_url']) ? $SETTINGS['dribbble_url'] : ''),
                                    'placeholder'   => 'Enter Dribbble Page Url',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>
                        <div class="form-group">
                            <label for="pinterest_url">Pinterest Page Url </label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'pinterest_url',
                                    'id'            => 'pinterest_url',
                                    'value'         => set_value('pinterest_url') ? set_value('pinterest_url') : (isset($SETTINGS['pinterest_url']) ? $SETTINGS['pinterest_url'] : ''),
                                    'placeholder'   => 'Enter Pinterest Page Url',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>

                        <div class="form-group">
                            <label for="vimeo_url">Vimeo Page Url </label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'vimeo_url',
                                    'id'            => 'vimeo_url',
                                    'value'         => set_value('vimeo_url') ? set_value('vimeo_url') : (isset($SETTINGS['vimeo_url']) ? $SETTINGS['vimeo_url'] : ''),
                                    'placeholder'   => 'Enter Vimeol Page Url',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>
                        <div class="form-group">
                            <label for="youtube_url">Youtube Page Url </label>
                            <div class="controls">
                                <?php $data = array(
                                    'id'            => 'youtube_url',
                                    'value'         => set_value('youtube_url') ? set_value('youtube_url') : (isset($SETTINGS['youtube_url']) ? $SETTINGS['youtube_url'] : ''),
                                    'placeholder'   => 'Enter Youtube Page Url',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>

                        <div class="form-group">
                            <label for="trumblr_url">Trumblr Page Url </label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'trumblr_url',
                                    'id'            => 'trumblr_url',
                                    'value'         => set_value('trumblr_url') ? set_value('trumblr_url') : (isset($SETTINGS['trumblr_url']) ? $SETTINGS['trumblr_url'] : ''),
                                    'placeholder'   => 'Enter Trumblr Page Url',
                                    'class'         => 'form-control'
                                );

                                echo form_input($data); ?>

                            </div>    
                        </div>

                        <div class="form-group">
                            <label for="gplus_url">Google Plus Page Url </label>
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'gplus_url',
                                    'id'            => 'gplus_url',
                                    'value'         => set_value('gplus_url') ? set_value('gplus_url') : (isset($SETTINGS['gplus_url']) ? $SETTINGS['gplus_url'] : ''),
                                    'placeholder'   => 'Enter Google Plus Page Url',
                                    'class'         => 'form-control'
                                );
                                echo form_input($data); ?>

                            </div>    
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <button name="submit" type="submit" class="btn btn-default"><i class="fa fa-check"></i> Save</button>
                            </div>    
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade in " id="commonSettings">
                    <h4>Common Settings</h4>
                    <hr/>
                    <form action="<?php echo base_url('superadmin/saveCommonSettings');?>" method="post" class="basicForm">
                        <div class="form-group">
                            <label class="control-label"><strong><?php echo $SETTINGS['show_project_count']; ?>Show Project count</strong></label>                                    
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'show_project_count',
                                    'id'            => 'show_project_count',
                                    'type'          => 'checkbox',
                                    'class'         => 'toggle-one',
                                    'value'       => 1,
                                    'checked'     => set_value('show_project_count')?set_value('show_project_count'):(isset($SETTINGS['show_project_count']) ? $SETTINGS['show_project_count'] : '')
                                );

                                echo form_checkbox($data); ?>

                            </div>    
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label"><strong><?php echo $SETTINGS['show_active_user_count']; ?>Show Active user count</strong></label>                                    
                            <div class="controls">
                                <?php $data = array(
                                    'name'          => 'show_active_user_count',
                                    'id'            => 'show_active_user_count',
                                    'value'       => 1,
                                    'checked'     => set_value('show_active_user_count')?set_value('show_active_user_count'):(isset($SETTINGS['show_active_user_count']) ? $SETTINGS['show_active_user_count'] : ''),
                                    'type'          => 'checkbox',
                                    'class'         => 'toggle-one'
                                );

                                echo form_checkbox($data); ?>

                            </div>    
                        </div>
                        
                        <div class="form-group">
                            <div class="controls">
                                <button type="submit" class="btn btn-default"><i class="fa fa-check"></i> Save</button>
                            </div>    
                        </div>
                    </form>
                </div>  
                  
<!--                  <div class="tab-pane fade in active" id="contactAddressSettings">
                      <h4>Contact Address Settings</h4>
                      <hr/>
                      <form action="<?php echo base_url('admin/saveContactAddressSettings');?>" method="post" class="basicForm">
                          <div class="form-group">
                              <label for="cont_adrs">Contact Address </label>
                              <div class="controls">
                                  <?php $data = array(
                                      'name'          => 'cont_adrs',
                                      'id'            => 'cont_adrs',
                                      'value'         => set_value('cont_adrs') ? set_value('cont_adrs') : (isset($ANY_SETTINGS['cont_adrs']) ? $ANY_SETTINGS['cont_adrs'] : ''),
                                      'placeholder'   => 'Enter Contact Address',
                                      'class'         => 'form-control'
                                  );

                                  echo form_input($data); ?>

                              </div>    
                          </div>
                          <div class="form-group">
                              <div class="controls">
                                  <button name="submit" type="submit" class="btn btn-default"><i class="fa fa-check"></i> Save</button>
                              </div>    
                          </div>
                      </form>
                  </div>-->
              </div>
                  </div>
          </div>
        </div>
        </div>
    </div>
</div>