	</div>

	</div>

	<!-- END CONTENT -->

</div>

<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->

<div class="page-footer">

	<div class="page-footer-inner">

		 2015 &copy; Drema Cessity.

	</div>

	<div class="page-footer-tools">

		<span class="go-top">

		<i class="fa fa-angle-up"></i>

		</span>

	</div>

</div>

<?php 



  $segment2 = $this->uri->segment(2);

  $segment3 = $segment2."/".$this->uri->segment(3);

  if($segment2 == 'dashboard' || $segment2 == '') $dashboard='active'; else $dashboard='';

  if($segment2 == 'products' || $segment3 == 'products/edit') $myproduct='active'; else $myproduct='';

  if($segment3 == 'customers/add') $cust='active'; else $cust='';





 ?>

<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->

<!--[if lt IE 9]>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/respond.min.js"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/excanvas.min.js"></script> 

<![endif]-->

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>

<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jquery-slimscroll/jquery.slimscroll.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jquery.cokie.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<?php if($dashboard=='active'): ?>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>

<?php endif; ?>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>

<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="<?php  echo BACKEND_THEME_URL ?>global/scripts/metronic.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>admin/layout/scripts/layout.js" type="text/javascript"></script>

<?php if($dashboard=='active'): ?>

<script src="<?php  echo BACKEND_THEME_URL ?>admin/pages/scripts/index.js" type="text/javascript"></script>

<?php endif; ?>

<script src="<?php  echo BACKEND_THEME_URL ?>admin/pages/scripts/tasks.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php  echo BACKEND_THEME_URL ?>global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="<?php  echo BACKEND_THEME_URL ?>global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/plupload/js/plupload.full.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/scripts/datatable.js"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/select2/select2.min.js" type="text/javascript" ></script>

<script src="<?php  echo BACKEND_THEME_URL ?>admin/pages/scripts/ecommerce-products-edit.js"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>admin/pages/scripts/components-form-tools.js"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>admin/pages/scripts/components-dropdowns.js"></script>

<script type="text/javascript" src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jquery-validation/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jquery-validation/js/additional-methods.min.js"></script>

<script type="text/javascript" src="<?php  echo BACKEND_THEME_URL ?>global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

<script src="<?php  echo BACKEND_THEME_URL ?>admin/pages/scripts/form-wizard.js"></script>

<script type="text/javascript" src="<?php  echo BACKEND_THEME_URL ?>global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->

<script  type="text/javascript" src="<?php echo base_url() ?>assets/tinymce/tinymce.min.js"></script>



<script type="text/javascript">

    var BASEURL = "<?php echo base_url(); ?>";

</script>

<script type="text/javascript">

// tinymce.init({

//     //selector: "textarea",

//     selector: ".tinymce_edittor",

//      menubar: false,

//     plugins:[

//                 "advlist autolink lists link image charmap print preview anchor media",

//                 "searchreplace visualblocks code fullscreen",

//                 "insertdatetime table contextmenu paste textcolor",

//                 "image",      

//             ],

//     image_advtab: true,

//     toolbar: "insertfile undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | preview code ",

//     file_browser_callback: function(field_name, url, type, win) {

//             if(type=='image') $('#my_form input').click();

//     }

// });



    tinymce.init({

        //selector: "textarea",

        selector: ".tinymce_edittor",

        relative_urls : false,

       remove_script_host : false,

       convert_urls : true,

        menubar: false,

       // language : 'he_IL',

       // theme_advanced_buttons1_add : "ltr,rtl"

        height :450,

        plugins: [

            "advlist autolink lists link image charmap print preview anchor media",

            "searchreplace visualblocks code fullscreen",

            "insertdatetime table contextmenu paste textcolor directionality",

        ],

        image_advtab: true,

        toolbar: "insertfile undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | preview code ",     

         file_browser_callback : elFinderBrowser,  



    });

 function elFinderBrowser (field_name, url, type, win) {

  tinymce.activeEditor.windowManager.open({

    file: '<?php echo base_url("elfinders") ?>',// use an absolute path!'<?php echo base_url("/assets/theme/assets/global/plugins/") ?>'

    title: 'File Manager',

    width: 900,

    height: 450,

    resizable: 'yes'

  }, {

    setUrl: function (url) {

      win.document.getElementById(field_name).value = url;

    }

  });

  return false;

}

</script>

<script type="text/javascript">

  $('.get_status').on('change',function(){

    var get_id = $(this).attr('id');

    var get_status = $('#'+get_id).val();

    var res = get_status.split("_");

    $.post(BASEURL+'backend/orders/change_status/',{status:res[0],order_id:res[2]}, function(data){

        if(data!=''){

            if(data=='successfully'){

                alert('Order Status has been updated Successfully'); 

                window.location.reload();

            }else if(data=='failed'){

                alert('Your Operation has been failed'); 

                window.location.reload();

            }

        }else{

            alert('Your Operation has been failed'); 

            window.location.reload();

        }

    });



  });

</script>





<script>

jQuery(document).ready(function() {    

   Metronic.init(); // init metronic core componets

   Layout.init(); // init layout

  <?php if($dashboard=='active'): ?>

      Index.init();   

      Index.initDashboardDaterange();

      Index.initJQVMAP(); // init index page's custom scripts

      Index.initCalendar(); // init index page's custom scripts

      Index.initCharts(); // init index page's custom scripts

      Index.initChat();

      Index.initMiniCharts();

      Index.initIntro();

      Tasks.initDashboardWidget();

  <?php endif; ?>

  EcommerceProductsEdit.init();

  ComponentsDropdowns.init();

  FormWizard.init();

});

</script>

<script type="text/javascript">

    $('.price_option').on('change', function() {

        var type  = $('input[name="price_type"]:checked').val();

        if(type!=''){

          if(type==1){

            $('#price_tag_1').css('display','block');

            $('#price_tag_2').css('display','none');

            $('.main_group').html(' ');



          }else if(type==2){

            $('#price_tag_2').css('display','block');

            $('#price_tag_1').css('display','none');



          }

        }

    });

    // $('.price_option').on('change', '.selector', function(event) {

    //   event.preventDefault();

    //   /* Act on the event */

    // });

    $('#btn_other_price').on('click',function(){

        alert('test'); 

    }); 

</script>



<script type="text/javascript">

  $('#content_use_only').click(function(){  

      var content = $(this).val();

      

      if($(this).is(':checked')){

        if($('#image_use_only').is(':checked')){

          $('#image_use_only').attr('checked',false);

          $('#image_use_only').parent('span').removeClass('checked');

        }

      }



  });

  $('#image_use_only').click(function(){  

      var content = $(this).val();

      if($(this).is(':checked')){

        if($('#content_use_only').is(':checked')){

            $('#content_use_only').attr('checked',false);

            $('#content_use_only').parent('span').removeClass('checked');

        }

      }

  });

</script>

<script type="text/javascript">

  $(document).ready(function() {

      $('#coupon_type').on('change',function(){

          var type =  $(this).val();

          if(type==2){

              $('#fixed_amt').css('display','block');

              $('#fixed_amt').find('input[type="text"]').val('100.00');

          } else {

              $('#fixed_amt').css('display','none');

              $('#fixed_amt').find('input[type="text"]').val('');

          }

      });

  });

</script>

<script type="text/javascript">

  $(function(){

    $('[data-toggle="tooltip"]').tooltip();

  });

</script>

<script type="text/javascript">



<?php $fast='';   if($segment3=='' && $segment2=='products'){

  $fast='raw'; 

} ?>

<?php if($segment2!='customers' && $segment2!='groups' && $segment2!='newsletter' && $segment2!='products'&& $segment2!='coupons' && $segment2!='orders' ): ?>

  $('body').find('table').on('click','input[type="checkbox"]',function() {

      var current_id =  $(this).attr('id');

      $('input[type="checkbox"]').each(function(){

         this.checked = false;

         $(this).parent('span').removeClass('checked');

      });

      $('#'+current_id).attr('checked','checked');

      $('#'+current_id).parent('span').addClass('checked');

  });

<?php endif; ?>

<?php if($segment3=='products/edit' || $segment3=='products/add'): ?>

  $('body').find('table').on('click','input[type="checkbox"]',function() {

      var current_id =  $(this).attr('id');

      $('input[type="checkbox"]').each(function(){

         this.checked = false;

         $(this).parent('span').removeClass('checked');

      });

     $('#'+current_id).attr('checked','checked');

    $('#'+current_id).parent('span').addClass('checked');

  });

<?php endif; ?>

</script>

<script type="text/javascript">

    $('#multi_order_export').on('change',function(event) {

        var export_status= $(this).val();

        if(export_status!=''){

          if(export_status=='all_order'){



            //var action = $(this).val() == "people" ? "user" : "content";

            $("#order_delete_form").attr("action", "http://23.235.208.13/~dermac8/products/backend/orders/export/");

            $("#order_delete_form").submit();

            // $.post(BASEURL+'backend/orders/selected_export/',{param:'all'}, function(data){

            //     alert('Completed');

            // });

          }else if(export_status=='selected_order'){



              var checkedNum = $('input[name="checkall[]"]:checked').length;

              if(!checkedNum){

                alert('Please select At least one order to Export ?');

                return false;

              }else{

                var val = [];

                $('input[name="checkall[]"]:checked').each(function(i){

                    val[i] = $(this).val();

                i++; });

                $("#order_delete_form").attr("action", "http://23.235.208.13/~dermac8/products/backend/orders/export/");

                $("#order_delete_form").submit();



              }  

          }else if(export_status=='stamp_csv'){



              var checkedNum = $('input[name="checkall[]"]:checked').length;

              if(!checkedNum){

                // alert('Please select At least one order to Export ?');

                // return false;



                $("#order_delete_form").attr("action", "http://23.235.208.13/~dermac8/products/backend/orders/stamp_export/");

                $("#order_delete_form").submit();

              }else{

                var val = [];

                $('input[name="checkall[]"]:checked').each(function(i){

                    val[i] = $(this).val();

                i++; });

                $("#order_delete_form").attr("action", "http://23.235.208.13/~dermac8/products/backend/orders/stamp_export/");

                $("#order_delete_form").submit();



              }  

          }

        }

    });





</script>

<script type="text/javascript">

  $('input[name="discount_item_view"]').on('switchChange.bootstrapSwitch',function(){       

      var val = $(this).val();

      var pro_id= $(this).attr('id');

      if(pro_id!=''){   

        $.post(BASEURL+'backend/products/get_discount_item/',{val:val,proid:pro_id}, function(data) {

          if(data!=''){

            if(data=='success'){

              alert('Discount item status has been changed.');

              window.location.reload();

            }else{

              alert('your action has been failed.');

              window.location.reload();

            }

          }     

        });

      }

  });

  $('input[name="featured_product_view"]').on('switchChange.bootstrapSwitch',function(){       

      var val = $(this).val();

      var pro_id= $(this).attr('id');

      if(pro_id!=''){   

        $.post(BASEURL+'backend/products/get_featured_item/',{val:val,proid:pro_id}, function(data) {

          if(data!=''){

            if(data=='success'){

              alert('Featured Product status has been changed.');

              window.location.reload();

            }else{

              alert('your action has been failed.');

              window.location.reload();

            }

          }     

        });

      }

  });

  $('input[name="avilable_item_view"]').on('switchChange.bootstrapSwitch',function(){       

      var val = $(this).val();

      var pro_id= $(this).attr('id');

      if(pro_id!=''){

        $.post(BASEURL+'backend/products/get_available_item/',{val:val,proid:pro_id}, function(data, textStatus, xhr) {

          if(data!=''){

            if(data=='success'){

              alert('Available item status has been changed.');

              window.location.reload();

            }else{

              alert('your action has been failed.');

              window.location.reload();

            }

          }     

        });

      }

  });

</script>

<script type="text/javascript">

  $('#country').on('change',function(event) {

      event.preventDefault();

      var name = $(this).val();

          $.post(BASEURL+'backend/customers/state/',{country_name:name}).done(function(data){

            var data1= $.parseJSON(data);

            if(data1.resp!='NoFound'){

                $('#state').html('');

                $('#label').html('');

                $('#label').html(data1.label);

                //$('#state').append('<option value="">Select State</option>');

                $('#state').append(data1.resp);

            } else { 

              $('#state').html('');

              $('#state').append('<option value="">Select State</option>');

            }

          });   

    });

  

    $('#ship_country').on('change',function(event){

      event.preventDefault();

      var name = $(this).val();

        $.post(BASEURL+'backend/customers/state/',{country_name:name}).done(function(data){

          var data1= $.parseJSON(data);

          if(data1.resp!='NoFound'){

              $('#ship_state').html('');

              $('#ship_label').html('');

              $('#ship_label').html(data1.label);

              //$('#ship_state').append('<option value="">Select State</option>');



              $('#ship_state').append(data1.resp);

          } else { 

            $('#ship_state').html('');

            $('#ship_state').append('<option value="">Select State</option>');

          }

        });   

    });



</script>



<script type="text/javascript">

    $('#for_child').on('click',function(){

         if($(this).is(':checked')){

            $('#for_label').fadeIn('slow');

            $('#for_add').fadeIn('slow');

         } else {

            $('#for_label').fadeOut('slow');

            $('#for_add').fadeOut('slow');

         }   

    });

    $('#submit_btn').on('click',function(){

        if($('#for_child').is(':checked')){

          var text = $('#label_state_province').val();

          if(text==''){

            $('#text_error').html('Please enter label field.');

            return false;

          }

        } 

    });

      $(document).ready(function() {

          var max_fields  = 50; //maximum input boxes allowed

          var wrapper     = $(".main_content"); //Fields wrapper

          var add_button  = $("#add_field_button"); //Add button ID



          var x = 1; //initlal text box count

          var u = 0;

          $(add_button).click(function(e){ //on add input button click

              e.preventDefault();

              if(x < max_fields){ //max input box allowed

                  u=x+1;

                  x++; //text box increment

                  $(wrapper).append('<div class="col-md-offset-4 col-md-5 col-sm-5"><div class="input-group"><input  type="text" class="form-control" name="state[]" Placeholder="State / Province Name"><a  class="input-group-addon remove_field"  href="#"><span class="glyphicon glyphicon-remove"></span></a></div><br></div>'); //add input box

              }

          });

         

          $(wrapper).on("click",".remove_field", function(e){ //user click on remove text

              e.preventDefault(); 

              $(this).closest('div').parent('div').remove(); x--;

          })

      });



      $(document).ready(function() {

          var max_fields  = 50; //maximum input boxes allowed

          var wrapper     = $(".main_group"); //Fields wrapper

          var add_button  = $("#add_field_button"); //Add button ID

          var x = 1; //initlal text box count

          var u = 0;

          $(add_button).click(function(e){ //on add input button click

              e.preventDefault();

              if(x < max_fields){ //max input box allowed

                  u=x+1;

                  x++; //text box increment

                  $(wrapper).append('<div class="form-group"><label class="col-md-2 control-label"> &nbsp;</label><div class="col-md-4"><input type="text" class="form-control" id="price_label" name="price_label[]"  placeholder="Price label"></div><div class="col-md-4"><input type="text" class="form-control" id="price" name="price_for[]" placeholder="Price" ></div><div class="col-md-2"><a  class="btn input-group-addon remove_field"  href="#"><span class="glyphicon glyphicon-remove"></span></a></div></div>'); //add input box

              }

          });

         

          $(wrapper).on("click",".remove_field", function(e){ //user click on remove text

              e.preventDefault(); 

              $(this).closest('div').parent('div').remove(); x--;

          })

      });

    



</script>



<script type="text/javascript">

    function updateConTextArea(){

        var allcon = [];

        $('#country_div :checked').each(function () {

            allcon.push($(this).val());

        });

        $('#con_div').val(allcon);

    }

    $(function(){

        $('#country_div input').click(function(event){

          var con_slug = $(this).val();

          if($(this).is(':checked')){

          

            $.post(BASEURL+'backend/zones/get_state/',{ slug:con_slug },function(data){

              if(data!=''){

                  if(data!='NoFound'){

                    $('#sta_div').html('');

                    $('#sta_div').html(data);

                  }else{

                    $('#sta_div').html('');

                    $('#sta_div').html('<h2 class="text-center">No State Found</h2>');

                  }

              }

            });

<?php if($segment3 == 'zones/edit'){ ?>

    var zone_id = $('#zone_id').val();

<?php }else{ ?>

    var zone_id = '';

<?php }   ?>  



    $.post(BASEURL+'backend/zones/get_con_info/',{country:con_slug,zone_id:zone_id},function(data){

            if(data!=''){ 

              if(data=='success'){

                updateConTextArea();

              }else{

                alert(con_slug);

                alert('Allready added in another zone');

                $('#'+con_slug).prop('checked',false);

                $('#'+con_slug).parent('span').removeClass('checked');

              }

            }  

          });

          }else{

            updateConTextArea();

          }

        });

    });

    function updateStateTextArea(){

        var array2 = [];

        var sta = $('#state_div').val();

        var array1 = sta.split(',');

        $('#sta_div :checked').each(function(){

          if($.inArray($(this).val(),array1)<0){

            array2.push($(this).val());

          }               

        });

        var array3 = array1.concat(array2);

        $('#state_div').val(array3);

    }

    $(function(){



<?php if($segment3 == 'zones/edit'){ ?>

    var zone_id = $('#zone_id').val();

<?php }else{ ?>

    var zone_id = '';

<?php }   ?> 



      $('body').find('#sta_div').on('click','input', function(){

        var sta = $(this).val();

        //alert(sta);

        $.post(BASEURL+'backend/zones/get_sta_info/',{state:sta,zone_id:zone_id},function(data){

          if(data!=''){ 

            if(data=='success'){

              updateStateTextArea();

            }else{

              //alert(con_slug);

              alert('Allready added in another zone');

              $('#'+sta).prop('checked',false);

              $('#'+sta).parent('span').removeClass('checked');

            }

          }  

        });

        

      });

      // $('body').find('#sta_div').on(function(){

      //     alert($(this).val());

      //     updateStateTextArea();

      // });

    });

</script>

<script type="text/javascript">

    function updateZipTextArea(){

        var allcon1 = [];

        var str =$('#zip_code').val();

        var zip_code = $('#zipcode_div').val();

        var test = zip_code.split(',');

        if(test!=''){

          $.each(test,function(i,element){

            if(test[i]!=str){

                allcon1.push(test[i]);

            }else{

                alert('Zip Code is allready exist');

            }    

          });

        }

        allcon1.push(str);

        $('#zipcode_div').val(allcon1);

        $('#zip_code').val('');

    }



    $('#add_zip').on('click',function(){

      var str = $('#zip_code').val();

      if($.isNumeric(str)){

        if(str.length<=4){

           alert('Plese Enter valid Zip/Postal Code');

        }else{

          <?php if($segment3 == 'zones/edit'){ ?>

              var zone_id = $('#zone_id').val();

          <?php }else{ ?>

              var zone_id = '';

          <?php }   ?> 

          $.post(BASEURL+'backend/zones/get_zip_info/',{zip_code:str,zone_id:zone_id},function(data){

          if(data!=''){ 

            if(data=='success'){

              //updateStateTextArea();

              updateZipTextArea();

            }else{

              //alert(con_slug);

              alert('Allready added in another zone');

              $('#zip_code').val(' ');

              //$('#'+sta).parent('span').removeClass('checked');

            }

          }  

        });

          

        } 

      } else{

           alert('Please Enter Numeric Value');

      }      

    });

    $('#remove_zip').click(function(){

        var all=[];

        var zip_code = $('#zipcode_div').val();

        var test = zip_code.split(',');

        var last = test.length-1;

        $.each(test,function(i,element){

          //alert(i+' '+last);

          if(i!=last){

              all.push(test[i]);

          }    

        });

        $('#zipcode_div').val(all);

    });



    $('#remove_last').click(function(){

        var all=[];

        var state_code = $('#state_div').val();

        var test = state_code.split(',');

        var last = test.length-1;

          $.each(test,function(i,element){

            //alert(i+' '+last);

            if(i!=last){

                all.push(test[i]);

            }    

          });

        $('#state_div').val(all);

    });



</script>



<script type="text/javascript">

  $('#myModal').modal({

    keyboard: false,

    backdrop: 'static',

    show:false

  });

  $('#save_changes').on('click',function(){

      var method_first = $("#method_first").val();

      var method_second = $("#method_second").val();

      var method_third = $("#method_third").val();

      var method_fourth = $("#method_fourth").val();

      var method_fifth = $("#method_fifth").val();

      var method_sixth = $("#method_sixth").val();

      var method_seven = $("#method_seven").val();

      var discount_type = $("#discount_type").val();

      var zone_id = $("#zone_id").val();

      var low_price = $("#low_price").val();

      var high_price = $("#high_price").val();

      var first_price = $("#method_one_price").val();

      var second_price = $("#method_sec_price").val();

      var third_price  = $("#method_third_price").val();

      var fourth_price = $("#method_fourth_price").val();

      var fifth_price = $("#method_fifth_price").val();

      var sixth_price = $("#method_sixth_price").val();

      var seven_price = $("#method_seven_price").val();

      if(method_first=='' && method_second=='' && method_third=='' && method_fourth=='' && method_fifth==''&& method_sixth=='' && method_seven==''){

          $('#form_error').html('Please enter method first');

          setTimeout(function(){ $('#form_error').html(''); }, 5000);

          return false;

      }

      if(low_price=="" || !$.isNumeric(low_price)){

          $('#form_error').html('Please provide numeric low price.');

          setTimeout(function(){ $('#form_error').html(''); }, 5000);

          return false;

      }

      if(high_price=="" || !$.isNumeric(high_price)){

          $('#form_error').html('Please provide numeric high price.');

          setTimeout(function(){ $('#form_error').html(''); }, 5000);

          return false;

      }

      if(parseInt(high_price)<parseInt(low_price)){

          $('#form_error').html('Please provide high price bigger then low price.');

          setTimeout(function(){ $('#form_error').html(''); }, 5000);

          return false;

      } 

      if(first_price!="" && !$.isNumeric(first_price)){

          $('#form_error').html('Please provide numeric value in any price field');

          setTimeout(function(){ $('#form_error').html(''); }, 5000);

          return false;

      }

      if(second_price!="" && !$.isNumeric(second_price)){

          $('#form_error').html('Please provide numeric value in any price field');

          setTimeout(function(){ $('#form_error').html(''); }, 5000);

          return false;

      }

      if(third_price!="" && !$.isNumeric(third_price)){

          $('#form_error').html('Please provide numeric value in any price field');

          setTimeout(function(){ $('#form_error').html(''); }, 5000);

          return false;

      }

      if(fourth_price!="" && !$.isNumeric(fourth_price)){

          $('#form_error').html('Please provide numeric value in any price field');

          setTimeout(function(){ $('#form_error').html(''); }, 5000);

          return false;

      }

      if(fifth_price!="" && !$.isNumeric(fifth_price)){

          $('#form_error').html('Please provide numeric value in any price field');

          setTimeout(function(){ $('#form_error').html(''); }, 5000);

          return false;

      } 

      if(sixth_price!="" && !$.isNumeric(sixth_price)){

          $('#form_error').html('Please provide numeric value in any price field');

          setTimeout(function(){ $('#form_error').html(''); }, 5000);

          return false;

      }

      if(seven_price!="" && !$.isNumeric(seven_price)){

          $('#form_error').html('Please provide numeric value in any price field');

          setTimeout(function(){ $('#form_error').html(''); }, 5000);

          return false;

      }

      if(first_price=='' && second_price=='' && third_price=='' && fourth_price=='' && fifth_price=='' && sixth_price=='' && seven_price==''){

          $('#form_error').html('Please provide numeric value in any one price field');

          setTimeout(function(){ $('#form_error').html(''); }, 5000);

          return false;

      }

        

      var zone_id_discount =discount_type+'_'+zone_id;  



      $.post(BASEURL+'backend/zones/variation_add/',{discount_zone:zone_id_discount,low_price:low_price,high_price:high_price,first_price:first_price,second_price:second_price,third_price:third_price,fourth_price:fourth_price,fifth_price:fifth_price,sixth_price:sixth_price,seven_price:seven_price},function(data){

        /*optional stuff to do after success */

        if(data!=''){

          if(data=='success'){

            alert('variation added sucessfully');

            $('#myModal').hide();

            window.location.reload();

          }else if(data=='failed'){

            alert('action has been failed..');

            $('#myModal').hide();

            window.location.reload();



          }

        }

      });

      



  });

</script>





<script type="text/javascript">





    function all_file() {

 <?php  if($segment3=='products/edit'){ ?>

      var fast=1; 

    <?php }else{ ?>

      var fast=0;

    <?php } ?>  

        $.post( BASEURL+'backend/products/all_file/',{'param1':'file' }, function(data) {

           if(data!=''){

                $('#tablebody').html('');

                $('#tablebody').append(data);

                if(fast==1){

                  //alert('test');

                 var l=0;

                var i=0;

                $('.is_feature').each(function(){

                  var id = $(this).attr('id');

                 

                  if($(this).is(':checked')){



                      

                    if(i==0){

                     

                     //  alert('test');

                     // $(this).attr('checked','checked');

                     // $(this).parent('span').addClass('checked');

                      i++;

                    }else{

                      this.checked = false;

                     $(this).parent('span').removeClass('checked');

                    }

                  }else{

                   this.checked = false;

                   $(this).parent('span').removeClass('checked');

                  }

                  

                l++;

                });

                var checkedNum = $('.is_feature:checked').length;

                //alert(checkedNum);

                // $('#'+current_id).attr('checked','checked');

                // $('#'+current_id).parent('span').addClass('checked');

            }

           }else{

            return false;

           }

        });

    }

    function remove_image(id){



    <?php  if($segment3=='products/edit'){ ?>

      var fast=1; 

    <?php }else{ ?>

      var fast=0;

    <?php } ?>  

      $.post( BASEURL+'backend/products/image_remove/',{'image_id':id} , function(data) {

          if(data!=''){

            $('#tablebody').html('');

            $('#tablebody').append(data);

            if(fast==1){

                   var id='';

                  var l=0;

                 

              var i=0;

                $('.is_feature').each(function(){

                  if(l==0){

                     id = $(this).attr('id');

                    

                  }

                  if($(this).is(':checked')){



                      

                    if(i==0){

                     //var id = $(this).attr('id');

                      // alert('test');

                     // $(this).attr('checked','checked');

                     // $(this).parent('span').addClass('checked');

                      i++;

                    }else{

                      this.checked = false;

                     $(this).parent('span').removeClass('checked');

                    }

                  }else{

                   this.checked = false;

                   $(this).parent('span').removeClass('checked');

                  }

                  

              l++;

                });

                var checkedNum = $('.is_feature:checked').length;

                if(checkedNum==0){

                  $('#'+id).attr('checked','checked');

                  $('#'+id).parent('span').addClass('checked');

                  

                }

            }

          }

          else{

            return false;

          }

      });

    }



    function delete_image(id){

      <?php  if($segment3=='products/edit'){ ?>

      var fast=1; 

    <?php }else{ ?>

      var fast=0;

    <?php } ?> 

      $.post( BASEURL+'backend/products/delete_image/',{'image_id':id} , function(data) {

          if(data!=''){ 

             var id='';

              $('#tablebody1').html('');

              $('#tablebody1').append(data);

              if(fast==1){

                // var checkedNum = $('.is_feature:checked').length;

                // alert(checkedNum);

               

              var l=0;

              var i=0;

                $('.is_feature').each(function(){

                 if(l==0){

                    id = $(this).attr('id');

                    

                  }



                  if($(this).is(':checked')){



                      

                    if(i==0){

                     

                     // $(this).attr('checked','checked');

                     // $(this).parent('span').addClass('checked');

                      i++;

                    }else{

                      this.checked = false;

                     $(this).parent('span').removeClass('checked');

                    }

                  }else{

                   this.checked = false;

                   $(this).parent('span').removeClass('checked');

                  }

                  

              l++;

                });

                 var checkedNum = $('.is_feature:checked').length;

                if(checkedNum==0){

                

                  $('body').find('table').find('#'+id).attr('checked','checked');

                  $('body').find('table').find('#'+id).parent('span').addClass('checked');

                  

                }

                

            }

          }

          else{

            return false;

          }

      });



    }

</script>

<script type="text/javascript">

  $("#multipale_category").select2();

   $('#select2_sample2').select2({

        placeholder: "Select Related Products",

        allowClear: true

    });

</script>





<script type="text/javascript">

  $(document).ready(function() {

      $('#free_shiping').on('change',function(event) {

        event.preventDefault();

        if($(this).is(':checked')){

          $('#ship_date').css('display','block');

        }else{

          $('#ship_date').css('display','none');

        }

      });

  });

</script>



<script type="text/javascript">

    $(document).ready(function() {

      $('#all_customer_check').on('change',function(event){

        event.preventDefault();    

        if($(this).is(":checked")){

          $(".check_customer").closest('span').addClass('checked');

          $('.check_customer').attr('checked','checked');

        } else {

          $(".check_customer").closest('span').removeClass('checked');

          $('.check_customer').removeAttr('checked');

        } 

      });  

    });



    $(document).ready(function() {

      $('#multi_order_status').on('change',function(event){

        var status = $(this).val(); 

        var val = [];

        if(status!=''){

          event.preventDefault();    

          var checkedNum = $('input[name="checkall[]"]:checked').length;

          if(!checkedNum){

            alert('Please select At least one order to chaneg status ?');

            return false;

          }else{

          $('input[name="checkall[]"]:checked').each(function(i){

            val[i] = $(this).val();

          i++; });

          $.post( BASEURL+'backend/orders/change_multiple_status/',{'status':status,ids:val} , function(data)  {

          if(data!=''){

            if(data=='successfully'){

              alert('orders status has Updated sucessfully');

              window.location.reload();

            }else if(data=='failed'){

              alert('Action has been failed..');   

              window.location.reload();

            }

          }

          });

            

          }

        }  



      });  

    });



    $(document).ready(function() {

      $('#check_all_news').on('change',function(event){

        event.preventDefault();    

        if($(this).is(":checked")){

          $(".check_newsletter").closest('span').addClass('checked');

          $('.check_newsletter').attr('checked','checked');

        } else {

          $(".check_newsletter").closest('span').removeClass('checked');

          $('.check_newsletter').removeAttr('checked');

        } 

      });  

    }); 



    $(document).ready(function() {

      $('#check_all_subscriber').on('change',function(event){

        event.preventDefault();    

        if($(this).is(":checked")){

          $(".check_sub").closest('span').addClass('checked');

          $('.check_sub').attr('checked','checked');

        } else {

          $(".check_sub").closest('span').removeClass('checked');

          $('.check_sub').removeAttr('checked');

        } 

      });  

    });



    $(document).ready(function() {

      $('#all_coupon_check').on('change',function(event){

        event.preventDefault();    

        if($(this).is(":checked")){

          $(".check_coupon").closest('span').addClass('checked');

          $('.check_coupon').attr('checked','checked');

        } else {

          $(".check_coupon").closest('span').removeClass('checked');

          $('.check_coupon').removeAttr('checked');

        } 

      });  

    });



    $('#delete_coupon').on('click',function(event) {

        event.preventDefault();

        var status_cust = $(this).val();      

        var checkedNum = $('input[name="checkall[]"]:checked').length;

        if(!checkedNum){

          alert('Please select At least one coupon whom you want to delete ?');

          return false;

        }else{

          if(status_cust!=''){

            $('#coupon_delete_form').submit();

          }else{

            return false;

          }

        }

    }); 



    $(document).ready(function() {

      $('#all_order_check').on('change',function(event){

        event.preventDefault();    

        if($(this).is(":checked")){

          $(".check_order").closest('span').addClass('checked');

          $('.check_order').attr('checked','checked');

        } else {

          $(".check_order").closest('span').removeClass('checked');

          $('.check_order').removeAttr('checked');

        } 

      });  

    });



    $('#delete_order').on('click',function() {

        

        var status_cust = $(this).val();      

        var checkedNum = $('input[name="checkall[]"]:checked').length;

       

        if(!checkedNum){

          alert('Please select At least one order whom you want to delete ?');

          return false;

        }else{

          if(status_cust!=''){



            $("#order_delete_form").attr("action", "http://23.235.208.13/~dermac8/products/backend/orders/multi_delete_orders/");

            $('#order_delete_form').submit();

          }else{

            return false;

          }

        }

    }); 



    $('#delete_subscriber').on('click',function(event) {

        event.preventDefault();

            

        var checkedNum = $('input[name="sub_id[]"]:checked').length;

        if(!checkedNum){

          alert('Please select At least one subscriber whom you want to delete ?');

          return false;

        }else{

          

            $('#sub_delete_form').submit();

        }

    }); 



    $(document).ready(function() {

      $('#all_customer').on('change',function(){       

        if($(this).is(":checked")){

          $(".group_customer").closest('span').addClass('checked');

          $('.group_customer').attr('checked','checked');

        } else {

          

          $(".group_customer").closest('span').removeClass('checked');

          $('.group_customer').removeAttr('checked');

        } 

      });  

    }); 



    $(document).ready(function() {

      $('#all_product').on('change',function(){       

        if($(this).is(":checked")){

          $(".group_product").closest('span').addClass('checked');

          $('.group_product').attr('checked','checked');

        } else {

          

          $(".group_product").closest('span').removeClass('checked');

          $('.group_product').removeAttr('checked');

        } 

      });  

    });



    $('#delete_cust').on('click',function(event) {

        event.preventDefault();

        var status_cust = $(this).val();      

        var checkedNum = $('input[name="checkall[]"]:checked').length;

        if(!checkedNum){

          alert('Please select At least one customer whom you want to delete in group?');

          return false;

        }else{

          if(status_cust!=''){

            $('#customer_delete_form').submit();

          }else{

            return false;

          }

        }

    }); 



    $('#send_news_letter').on('click',function(event) {

        event.preventDefault();

        // var status_cust = $(this).val();      

        var checkedNum = $('input[name="checkall[]"]:checked').length;

        if(!checkedNum){

          alert('Please select At least one user whom you want to send newsletter.');

          return false;

        }else{

          // if(status_cust!=''){

          //alert(checkedNum);

          $('#send_newsletter_form').submit();

          // }else{

          //   return false;

          // }

        }

    }); 



    $('#delete_product').on('click',function(event) {

        event.preventDefault();

        var status_cust = $(this).val();      

        var checkedNum = $('input[name="checkall[]"]:checked').length;

        if(!checkedNum){

          alert('Please select At least one Product whom you want to delete ?');

          return false;

        }else{

          if(status_cust!=''){

            $('#product_delete_form').submit();

          }else{

            return false;

          }

        }

    }); 



    $('#delete_group_cust').on('click',function(event) {

        event.preventDefault();

        var customers=[];

        var status_cust=$(this).val(); 

        var group_id = $('#group_id').val();     

        var checkedNum = $('input[name="checkall[]"]:checked').length;

        if(!checkedNum){

          alert('Please select At least one customer whom you want to delete?');

          return false;

        }else{

          if(status_cust!=''){

            $('input[name="checkall[]"]:checked').each(function() { //loop through each checkbox

                var id = $(this).attr('id');  //select all checkboxes with class "checkbox1"  

                customers.push(id);           

            });

            if(customers!=''){

               $.post(BASEURL+'backend/groups/customer_group_delete/',{ids:customers,group_id:group_id},function(data){              

                 if(data!=''){

                    if(data=='group_no'){

                      alert('your process has been failed.please try again.');

                      window.location.reload();

                    }else if(data=='Allready'){

                      alert('one or more customer are not found in group.');

                      window.location.reload();

                    }else if(data=='success'){

                      alert('Your action has been completed successfully.');

                      window.location.reload();

                    }

                 }

               });

            }

            

          }else{

            return false;

          }

        }

    }); 



    $('#add_to_group').on('click',function(event) {

        event.preventDefault();

        var custs=[];

        var status_cust=$(this).val();      

        var checkedNum = $('input[name="checkall[]"]:checked').length;

        if(checkedNum){

          $('input[name="checkall[]"]:checked').each(function() { //loop through each checkbox

              var id = $(this).attr('id');  //select all checkboxes with class "checkbox1"  

              custs.push(id);           

          });

          if(custs!=''){

              $('#add_group_form').modal('show');

          }

        }else{

          alert('Please select At least one customer whom you want to add in group?');

          return false;

        }  

   }); 

    $('#add_group_form').modal({

      backdrop: 'static',

      show:false

    });

    $('#send_mail_group').modal({

      backdrop:'static',

      show:false

    });

    $('#send_mail_form').modal({

      backdrop: 'static',

      show:false

    });

    $('#add_group_form').on('hidden.bs.modal', function (e){

        $('#add_group').val('');

    })



    $('#save_in_group').on('click',function(){

       var customers=[];

       var group_id = $('#add_group').val();

       if(group_id!=''){

            $('input[name="checkall[]"]:checked').each(function() { //loop through each checkbox

                var id = $(this).attr('id');  //select all checkboxes with class "checkbox1"  

                customers.push(id);           

            });

            if(customers!=''){

               $.post(BASEURL+'backend/customers/add_to_group/',{ids:customers,group_id:group_id},function(data){              

                 if(data!=''){

                    if(data=='group_no'){

                      alert('your process has been failed.please try again');

                      window.location.reload();

                    }else if(data=='Allready'){

                      alert('one or more customer are already assiged to a group. one customer can be assigned to one group.');

                      window.location.reload();

                    }else if(data=='success'){

                      alert('Your action has been completed successfully.');

                      window.location.reload();

                    }

                 }

               });

            }



       }else{

          alert('Please select group first.');

          return false; 

       }

    }); 

    $('.remove_from_group').on('click',function(){

      var group_id = $('#group_id').val();

      var cust_id  = $(this).attr('data');

      if(group_id!='' && cust_id!=''){

        $.post(BASEURL+'backend/groups/remove_from_group/',{group_id:group_id,cust_id:cust_id},function(data){              

           if(data!=''){

              if(data=='problem'){

                alert('your action has been failed.');

                window.location.reload();

              }else if(data=='success'){

                alert('Your action has been completed successfully.');

                window.location.reload();

              }

           }

        });

      }

    }); 



    $('#customer_group').on('click',function(){

        // event.preventDefault();

        var custs=[];

        var group_id =$('#group_id').val();      

        var checkedNum = $('input[name="checkall[]"]:checked').length;

        if(checkedNum){

          $('input[name="checkall[]"]:checked').each(function() { //loop through each checkbox

              var id = $(this).attr('id');  //select all checkboxes with class "checkbox1"  

              custs.push(id);           

          });

          if(custs!=''){

              $.post(BASEURL+'backend/groups/customer_group/',{ids:custs,group_id:group_id},function(data){              

                 if(data!=''){

                    if(data=='group_no'){

                      //alert('your process has been failed.please try again');

                     window.location.href=BASEURL+'backend/groups/index/0/3';

                    }else if(data=='Allready'){

                      //alert('your action has been failed.');

                      window.location.href=BASEURL+'backend/groups/index/0/2';

                    }else if(data=='success'){

                      //alert('Your action has been completed successfully.');

                      window.location.href=BASEURL+'backend/groups/index/0/1';

                    }

                 }

               });

          }

        }else{

          alert('Please select At least one customer whom you want to add in group?');

          return false;

        }  

   }); 



    $('#send_email_to_all').on('click',function(){

        var custs=[];

        var status_cust=$(this).val();      

        var checkedNum = $('input[name="checkall[]"]:checked').length;

        if(checkedNum){

          $('input[name="checkall[]"]:checked').each(function() { //loop through each checkbox

              var id = $(this).attr('id');  //select all checkboxes with class "checkbox1"  

              custs.push(id);           

          });

          if(custs!=''){

              $('#send_mail_form').modal('show');

          }

        }else{

          alert('Please select At least one customer whom you want to add in group ?');

          return false;

        }  

    }); 



    $('#send_mail_form').on('hidden.bs.modal',function (e){

        $('#email_subject').val('');

        $('#email_message').val('');

    }); 



    $('#send_mail').on('click',function(){

       var customers=[];



       var subject = $('#email_subject').val();

       var message = $('#email_message').val();

       ///alert(subject);

        if(subject==''){

          $('#form_email').html('Please fill subject field');

          setTimeout(function(){ $('#form_email').html(''); }, 5000);

          return false;

        }

        if(message==''){

          $('#form_message').html('Please fill subject field');

          setTimeout(function(){ $('#form_message').html(''); }, 5000);

          return false;

        }



       if(subject!='' && message!=''){

            $('input[name="checkall[]"]:checked').each(function() { //loop through each checkbox

                var id = $(this).attr('id');  //select all checkboxes with class "checkbox1"  

                customers.push(id);           

            });

            if(customers!=''){

               $.post(BASEURL+'backend/customers/send_email_customers/',{ids:customers,subject:subject,message:message},function(data){              

                 if(data!=''){

                    if(data=='problem'){

                      alert('one or more email address are not valid');

                      window.location.reload();

                    }else if(data=='success'){

                      alert('Your action has been completed successfully.');

                      window.location.reload();

                    }

                 }

               });

            }



       }else{

          alert('Please select group first.');

          return false; 

       }

    }); 









</script>



<script type="text/javascript">

  $('.email_group').on('click',function(){

      var group_id = $(this).attr('data');

      var group_name  = $('#email_group_'+group_id).val();

          group_name= group_name.toUpperCase();

      // alert(group_id+','+group_name);

      if(group_id!='' &&  group_name!=''){

        $('#myModalLabel').html('');

        $('#group_name_email').val(group_name);

        $('#group_id_email').val(group_id);

        $('#myModalLabel').html('Send Email To '+group_name);

        $('#send_mail_group').modal('show');

      }

  });

  $('#send_mail_to_group').on('click',function(){

        var customers=[];

        var subject = $('#email_subject').val();

        var message = $('#email_message').val();

        var group_id = $('#group_id_email').val();

        var group_name = $('#group_name_email').val();

        

        if(subject==''){

          $('#form_email').html('Please fill subject field');

          setTimeout(function(){ $('#form_email').html(''); }, 5000);

          return false;

        }

        if(message==''){

          $('#form_message').html('Please fill subject field');

          setTimeout(function(){ $('#form_message').html(''); }, 5000);

          return false;

        }



       if(subject!='' && message!=''){        

            if(group_id!='' && group_name!=''){

               $.post(BASEURL+'backend/groups/send_email_groups/',{group_id:group_id,group_name:group_name,subject:subject,message:message},function(data){              

                 if(data!=''){

                    if(data=='problem'){

                      alert('one or more email address are not valid');

                      window.location.reload();

                    }else if(data=='success'){

                      alert('Your action has been completed successfully.');

                      window.location.reload();

                    }

                 }

               });

            }

       }else{

          alert('Please select group first.');

          return false; 

       }

    }); 





</script>





<script type="text/javascript">

  $(document).ready(function() {

      var $tabs = $('.tabbable li');

      $('.prev_tab').on('click',function(){

          $tabs.filter('.active').prev('li').find('a[data-toggle="tab"]').tab('show');

      });

      $('.pro_button').on('click',function(){

        var id = $(this).attr('id');

        if(id=='first'){

          var name  = document.getElementById("name").value;

          var description = document.getElementById("description").value;

          var short_description = document.getElementById("short_description").value;

          var price = document.getElementById("price").value;

          var category = document.getElementById("multipale_category").value;        



          if(name == "")

          {

            $('#n_error').html('Please enter products name field');

            setTimeout(function(){ $('#n_error').html(''); }, 5000);

            return false;

          }

          if(description=="")

          {

            $('#de_error').html('Please enter description field');

            setTimeout(function(){ $('#de_error').html(''); }, 5000);

            return false;

          }

          if(short_description=="")

          {

            $('#sd_error').html('Please enter short description field');

            setTimeout(function(){ $('#sd_error').html(''); }, 5000);

            return false;

          }

          if(category=="")

          {

            $('#ct_error').html('Please enter category field');

            setTimeout(function(){ $('#ct_error').html(''); }, 5000);

            return false;

          }

          // if(price=="")

          // {

          //   $('#pr_error').html('Please enter price field');

          //   setTimeout(function(){ $('#pr_error').html(''); }, 5000);

          //   return false;

          // }

        } 

        if(id=='second'){

          //$('#myTextarea').val(content);

          //var value = tinymce.get('clinical_description').getContent();

          // var cli_des = tinymce.get('clinical_description').getContent();

          // var sci_des =  tinymce.get('science_description').getContent();

          // var use_des  = tinymce.get('use_description').getContent();

          // if(cli_des == "")

          // {

          //   $('#cde_error').html('Please enter clinical description field');

          //   setTimeout(function(){ $('#cde_error').html(''); }, 5000);

          //   return false;

          // }

          // if(sci_des=="")

          // {

          //   $('#sde_error').html('Please enter science description field');

          //   setTimeout(function(){ $('#sde_error').html(''); }, 5000);

          //   return false;

          // }

          // if(use_des=="")

          // {

          //   $('#ude_error').html('Please enter use description field');

          //   setTimeout(function(){ $('#ude_error').html(''); },5000);

          //   return false;

          // }

          

        } 

        $tabs.filter('.active').next('li').find('a[data-toggle="tab"]').tab('show');

      });          

  });



  $(document).ready(function() {

    var $tabs = $('#add_tab');

    $('#add_tab').tabs({

        fx: {

            opacity: 'toggle'

        }

    });

    $('.tab_button').on('click', function() {

      var tab_no = $(this).attr('data-tab');  

      var id = $(this).attr('id');

      if(id=='first'){

          var re = /^[ A-Za-z0-9_./]*$/;

          var emailRegex = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;

          var fname  = document.getElementById("first_name").value;

          var lname  = document.getElementById("last_name").value;

          var femail = document.getElementById("email").value; 

          <?php if(!empty($cust)): ?>     

          var pass    = document.getElementById("password").value;

          var con_pass= document.getElementById("con_password").value;  

          <?php endif; ?>  

          var address = document.getElementById("address").value;     

          var address1= document.getElementById("address1").value;     

          var city    = document.getElementById("city").value;     

          var zip_code= document.getElementById("zip_code").value;     

          var city    = document.getElementById("city").value;     

          var country = document.getElementById("country").value;     

          var state   = document.getElementById("state").value;

          var phone   = document.getElementById("phone").value;     

          if(fname == "")

          {

            $('#fn_error').html('Please enter first name field');

            setTimeout(function(){ $('#fn_error').html(''); }, 5000);

            return false;

          }else if (!(/^\S{0,}$/.test(fname))){



              $('#fn_error').html('First name connot contain white space');

              $('#first_name').val('');

              setTimeout(function(){ $('#fn_error').html(''); }, 5000);

              return false;

          }

          if(lname=="")

          {

            $('#ln_error').html('Please enter last name field');

            setTimeout(function(){ $('#ln_error').html(''); }, 5000);

            return false;



          }else if (!(/^\S{0,}$/.test(lname))){

              $('#ln_error').html('Last name connot contain white space');

              $('#last_name').val('');

              setTimeout(function(){ $('#ln_error').html(''); }, 5000);

              return false;

          }

          if(femail=="")

          {

            $('#em_error').html('Please enter email address field');

            setTimeout(function(){ $('#em_error').html(''); }, 5000);

            return false;



          }else if(!emailRegex.test(femail)){

            $('#em_error').html('Please enter valid email address');

            setTimeout(function(){ $('#em_error').html(''); }, 5000);

            return false;

          }

     <?php if(!empty($cust)): ?>     

          if(pass=="")

          {

             

              $('#ps_error').html('Please Enter Password Field');

              setTimeout(function(){ $('#ps_error').html(''); }, 5000);

              return false;

          }else if (!(/^\S{5,}$/.test(pass))){



              $('#ps_error').html('Password connot contain white space and less then 5 digit');

              $('#password').val('');

              setTimeout(function(){ $('#ps_error').html(''); }, 5000);

              return false;

          }

          if(con_pass=="")

          {

            $('#cps_error').html('Please enter password field');

            setTimeout(function(){ $('#cps_error').html(''); }, 5000);

            return false;

          }

          if(con_pass!=pass)

          {

              $('#cps_error').html('Confirm password does not match to password');

              setTimeout(function(){ $('#cps_error').html(''); }, 5000);

              return false;

          }

    <?php endif; ?>      

          if(address=="")

          {

              $('#ad_error').html('Please enter address field');

              setTimeout(function(){ $('#ad_error').html(''); }, 5000);

              return false;

          } 

          // if(address1=="")

          // {

          //     $('#ad1_error').html('Please enter address field');

          //     setTimeout(function(){ $('#ad1_error').html(''); }, 5000);

          //     return false;

          // }

          if(city=="")

          {

            $('#ct_error').html('Please enter city field');

            setTimeout(function(){ $('#ct_error').html(''); }, 5000);

            return false;

          }

          if(zip_code=="" || !re.test(zip_code)){

            $('#zc_error').html('Please provide alpha numeric value in zip code');

            setTimeout(function(){ $('#zc_error').html(''); }, 5000);

            return false;

          }

          if(country==''){

              $('#con_error').html('Please select country field');

              setTimeout(function(){ $('#con_error').html(''); }, 5000);

              return false;

          }

          if(phone!="" && !$.isNumeric(phone))

          {

              $('#ph_error').html('Please enter Numeric value in phone field');

              setTimeout(function(){ $('#ph_error').html(''); }, 5000);

              return false;

          }

      }

      if(id=='second'){

          // var same_to_bill = document.getElementById("same_to_bill").value;

          if(!$("#same_to_bill").is(':checked')){

            var re = /^[ A-Za-z0-9_./]*$/;

            var emailRegex = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;

            var company_name = document.getElementById("ship_company_name").value;

            var fname   = document.getElementById("ship_first_name").value;        

            var lname   = document.getElementById("ship_last_name").value;

            var femail  = document.getElementById("ship_email").value;  

            var address = document.getElementById("ship_address").value;     

            var address1 = document.getElementById("ship_address1").value;     

            var city    = document.getElementById("ship_city").value;     

            var zip_code= document.getElementById("ship_zip_code").value;     

            var city    = document.getElementById("ship_city").value;     

            var country = document.getElementById("ship_country").value;     

            var state   = document.getElementById("ship_state").value;     

            if(fname == "")

            {

              $('#ship_fn_error').html('Please enter first name field');

              setTimeout(function(){ $('#ship_fn_error').html(''); }, 5000);

              return false;

            }else if (!(/^\S{0,}$/.test(fname))){

              $('#ship_fn_error').html('First name connot contain white space');

              $('#ship_first_name').val('');

              setTimeout(function(){ $('#ship_fn_error').html(''); }, 5000);

              return false;

            }

            if(lname=="")

            {

              $('#ship_ln_error').html('Please enter last name field');

              setTimeout(function(){ $('#ship_ln_error').html(''); }, 5000);

              return false;

            }else if (!(/^\S{0,}$/.test(lname))){

              $('#ship_ln_error').html('Last name connot contain white space');

              $('#ship_last_name').val('');

              setTimeout(function(){ $('#ship_ln_error').html(''); }, 5000);

              return false;

            }

            if(femail=="")

            {

              $('#ship_em_error').html('Please enter email address field');

              setTimeout(function(){ $('#ship_em_error').html(''); }, 5000);

              return false;



            }else if(!emailRegex.test(femail)){

              $('#ship_em_error').html('Please enter valid email address');

              setTimeout(function(){ $('#ship_em_error').html(''); }, 5000);

              return false;

            }

            if(address=="")

            {

                $('#ship_ad_error').html('Please enter address field');

                setTimeout(function(){ $('#ship_ad_error').html(''); }, 5000);

                return false;

            }

            if(city=="")

            {

              $('#ship_ct_error').html('Please enter city field');

              setTimeout(function(){ $('#ship_ship_ct_error').html(''); }, 5000);

              return false;

            }

            if(zip_code=="" || !re.test(zip_code)){



                $('#ship_zc_error').html('Please provide alpha numeric value in zip code');

                setTimeout(function(){ $('#ship_zc_error').html(''); }, 5000);

                return false;

            }

            if(country==''){

                $('#ship_con_error').html('Please select country field');

                setTimeout(function(){ $('#ship_con_error').html(''); }, 5000);

                return false;

            }

            if(state=="")

            {

                $('#ship_st_error').html('Please enter state field');

                setTimeout(function(){ $('#ship_st_error').html(''); }, 5000);

                return false;

            }

        } else {



            //var emailRegex = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;

            var company_name = document.getElementById("company_name").value;

            var fname   = document.getElementById("first_name").value;

            var lname   = document.getElementById("last_name").value;

            var femail  = document.getElementById("email").value;  

            var address = document.getElementById("address").value;     

            var address1 = document.getElementById("address1").value;     

            var city    = document.getElementById("city").value;     

            var zip_code= document.getElementById("zip_code").value;     

            var city    = document.getElementById("city").value;     

            var phone    = document.getElementById("phone").value;     

            var country = document.getElementById("country").value;     

            var state   = document.getElementById("state").value; 

      

            $('#ship_company_name').val(company_name);

            $('#ship_first_name').val(fname);

            $('#ship_last_name').val(lname);

            $('#ship_email').val(femail);

            $('#ship_address').val(address);

            $('#ship_address1').val(address1);

            $('#ship_city').val(city);

            $('#ship_zip_code').val(zip_code);

            $('#ship_country').val(country);

            $('#ship_state').val(state);

            $('#ship_phone').val(phone);



        }  



      }





      $('#add_tab li:eq('+tab_no+') a').tab('show');

    });



    $('#same_to_bill').on('change',function(event) {

      event.preventDefault();

      if(!$(this).is(':checked')){

          $('#ship_company_name').val('');

          $("#ship_company_name").removeAttr('disabled');

          $('#ship_first_name').val('');

          $("#ship_first_name").removeAttr('disabled');

          $('#ship_last_name').val('');

          $("#ship_last_name").removeAttr('disabled');

          $('#ship_zip_code').val('');

          $("#ship_zip_code").removeAttr('disabled'); 

          $('#ship_email').val('');

          $("#ship_email").removeAttr('disabled');

          $('#ship_address').val('');

          $("#ship_address").removeAttr('disabled');

          $('#ship_address1').val('');

          $("#ship_address1").removeAttr('disabled'); 

          $('#ship_fax').val('');

          $("#ship_fax").removeAttr('disabled');

          $('#ship_city').val('');

          $("#ship_city").removeAttr('disabled');

          $('#ship_country').val('');

          $("#ship_country").removeAttr('disabled');

          $('#ship_state').val('');

          $("#ship_state").removeAttr('disabled');

          $('#ship_phone').val('');

          $("#ship_phone").removeAttr('disabled');



      }else{

          var company_name = document.getElementById("company_name").value;

          var fname   = document.getElementById("first_name").value;

          var lname   = document.getElementById("last_name").value;

          var femail  = document.getElementById("email").value;  

          var address = document.getElementById("address").value;     

          var address1= document.getElementById("address1").value;

          var city    = document.getElementById("city").value;     

          var zip_code= document.getElementById("zip_code").value;     

          var city    = document.getElementById("city").value;

          var phone   = document.getElementById("phone").value;     

          var country = document.getElementById("country").value;     

          var state   = document.getElementById("state").value; 

          var fax     = document.getElementById("fax").value; 

            if(country!=''){        

              $.post(BASEURL+'backend/customers/get_state/',{ slug:country,state:state },function(data){

                if(data!=''){

                  //alert(data);

                  var data1= $.parseJSON(data);

                  if(data1.resp!='NoFound'){

                  $('#ship_state').html('');

                      $('#ship_label').html('');

                      $('#ship_label').html('');

                      $('#ship_label').html(data1.label);

                      $('#ship_label').html(data1.label);



                     // $('#ship_state').append('<option value="">Select State</option>');

                      $('#ship_state').append(data1.resp);

                  } else { 

                    $('#ship_state').html('');

                    $('#ship_state').append('<option value="">Select State</option>');

                  }

                }

              });        

            }



          $('#ship_company_name').val(company_name);

          $("#ship_company_name").attr("disabled", "disabled");

          $('#ship_first_name').val(fname);

          $("#ship_first_name").attr("disabled", "disabled");

          $('#ship_last_name').val(lname);

          $("#ship_last_name").attr("disabled", "disabled");

          $('#ship_email').val(femail);

          $("#ship_email").attr("disabled", "disabled");

          $('#ship_address').val(address);

          $("#ship_address").attr("disabled", "disabled"); 

          $('#ship_address1').val(address1);

          $("#ship_address1").attr("disabled", "disabled");

          $('#ship_city').val(city);

          $("#ship_city").attr("disabled", "disabled");

          $('#ship_zip_code').val(zip_code);

          $("#ship_zip_code").attr("disabled", "disabled");

          $('#ship_country').val(country);

          $("#ship_country").attr("disabled", "disabled");

          $('#ship_state').val(state);

          $("#ship_state").attr("disabled", "disabled");

          $('#ship_phone').val(phone);

          $("#ship_phone").attr("disabled", "disabled");

          $('#ship_fax').val(fax);

          $("#ship_fax").attr("disabled", "disabled");

      }



    });







    $('#add_new_customer').on('click',function(event){

        event.preventDefault();

        /* Act on the event */

        var first_name = $('first_name').val();

        var last_name = $('last_name').val();

        var email = $('email').val();

        var company_name = $('company_name').val();

        $.post( BASEURL+'backend/orders/add_new_customer/',{'first_name':first_name,'last_name':last_name,'email':email,'company_name':company_name },function(data){

          if(data=='create_session'){    

              window.location=BASEURL+'backend/products/add';

          }else{

            return false;

          }

        });

    });

  });

</script>



<script type="text/javascript">

  $('#show_password').on('change',function(){

      if($(this).is(':checked')){

        $('#default_password').attr('type','text');

      }else{

        $('#default_password').attr('type','password');

      }

  });

</script>

<script type="text/javascript">

 

  // $(document).ready(function(){

  //   $('#customer_first').on('click',function(event) {

        

      

  //     $.post(BASEURL+"user/sign_up", { first_name:fname,last_name:lname,email:femail,password:pass,gender:gender}).done(function(data){

        

  //       if(data!=''){

  //         var option = $.parseJSON(data);

  //         if(option.Status==0){

  //           $('#em_error').html(option.msg_error);

  //           setTimeout(function(){ $('#em_error').html(''); },5000);            

  //         }    

  //         if(option.Status==1){

  //           $('#label').html(option.msg_error);

  //           $('#label').css('display','block');

  //           setTimeout(function(){ $('#label').html(''); $('#label').css('display','none');  $('form :input').val(""); },5000);            

  //         }   

  //         if(option.Status==2){            

  //           $('#label').html(option.msg_success);

  //           $('#label').css('display','block');

  //           //$('#label').addClass('in');

  //           setTimeout(function(){ $('#label').html(''); $('#label').css('display','none');  $('form :input').val(""); $('#exampleModal').modal('hide');  },5000); 

  //         }

  //       }

  //     });

  //   });

  //   $('#exampleModal').on('hide.bs.modal', function () {

  //     $('form :input').val("");

  //   })



  // });  

</script>

<script type="text/javascript">

  $("#textinput").keyup(function(){

    var value = $(this).val().replace(" ",",");

    $(this).val(value);

    var words = value.split(",");

  });

</script>





<!-- END JAVASCRIPTS -->

</body>

<!-- END BODY -->

</html>