  <div class="row">
            <div class="col-mod-12">  
               <h3 class="page-header"> Users   </h3>
            </div>
        </div>
        <?php
            if(!empty($_SERVER['QUERY_STRING']))
                $QUERY_STRING = "0?".$_SERVER['QUERY_STRING'];
            else
                $QUERY_STRING ='';
        ?>
          <!-- Users widget -->
      <div class="row">
       <div class="col-md-12">
        <div class="panel">
         <div class="panel-heading text-primary">
          <h3 class="panel-title"><i class="fa fa-user"></i> List        <a class=" btn btn-xs btn-info" href="<?php echo base_url('backend/users/add');  ?>"  style="margin-top:3px;"class="btn btn-small">Add User</a>   </h3>
        </div>
      <div class="panel-body">
          <?php  echo msg_alert_backend();  ?>

 <form class="form-inline" action="<?php echo base_url('backend/users/index/id/asc/') ?>" role="form">

              <div class="form-group">

                  <!-- <label class="sr-only" for="exampleInputEmail2">First Name</label> -->

                  <select style="width:100%;"  class="form-control" name="search_by"> <option value="">Select Field  </option>  <option value="first_name" <?php if(!empty($_GET['search_by']) && $_GET['search_by']=='first_name') echo 'selected'?>>First Name </option><option value="last_name" <?php if(!empty($_GET['search_by']) && $_GET['search_by']=='last_name') echo 'selected'?>>Last Name</option> <option value="email" <?php if(!empty($_GET['search_by']) && $_GET['search_by']=='email') echo 'selected'?>>Email </option>  </select> 

              </div>

              <div class="form-group">

                  <!-- <label class="sr-only" for="exampleInputPassword2">Enter Text</label> -->

                  <input  class="form-control" type="text" style="width:100%;" name="search_query"  placeholder="Search" value="<?php if(!empty($_GET['search_query'])) echo $_GET['search_query'] ?>">

              </div>

              <button class="btn btn-primary" type="submit">Search</button>

              <a class="btn btn-warning" href="<?php echo base_url('backend/users/');  ?>"  style="margin-top:3px;"class="btn btn-small">Reset</a> 

        </form>

      <br>
   <form action="<?php echo  base_url('backend/users/delete_customers/');   ?>" id="customer_delete_form" method="post" accept-charset="utf-8">

        <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th style="width:3%"> 
                      <input class=""  id="all_customer_check"  type="checkbox" name="check_all">
                    </th>
                    <th style="width:5%" >#</th>
                    <th style="width:12%">First Name</th>                 
                    <th style="width:12%">Last Name</th>                 
                    <th style="width:20%">Email</th>
                    <th style="width:13%">Project Histroy</th>
                    <th style="width:8%">Resume</th>
                    <th style="width:10%"><?php echo anchor("backend/users/index/status/".(($sort_order == 'asc' && $sort_by == 'status') ? 'desc' : 'asc')."/".$QUERY_STRING,'Status'); ?> <?php if($sort_order=='desc' && $sort_by == 'status'){ ?> <i class="fa fa-angle-down"></i> <?php }else{ ?> <i class="fa fa-angle-up"></i><?php } ?> </th>
                    <!--  <th style="width:10%">Created</th> -->
                    <th style="width:10%">Actions</th>
                </tr>
            </thead>
          <tbody>



                <?php if(!empty($users)):

                   $i=$offset;

                foreach($users as $value){ $i++; ?>

                <tr >

                    <td>

                      <input type="checkbox" class="check_customer" id="check_customer_<?php echo $value->id ; ?>"  name="checkall[]" value="<?php echo $value->id ; ?>">       

                    </td>

                    <td><?php echo $i."." ;?></td>

                    <td class=""><?php echo $value->first_name ; ?></td>

                    <td class=""><?php echo $value->last_name ; ?></td>

                    <td class="to_hide_phone"><?php echo $value->email;; ?></td> 

                    <td class="to_hide_phone"> <div class="btn-group"> <span  class="btn btn-sm btn-success" ><?php echo get_user_project_count($value->id);  ?></span><a class="btn btn-sm btn-primary"  href="<?php echo base_url().'backend/users/user_projects/'.$value->id ?>"> Project</a> </div> </td> 

                    <td><?php if(!empty($value->resume_file)){ echo'<a class="btn btn-success" href="'.base_url().'backend/users/download_resume_file/'.$value->id.'" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-download"></i></a>'; }else{ echo 'NA';   } ?></td>

                    <td class="to_hide_phone"><?php if($value->status){ echo 'Active'; }else{  echo 'Inactive'; } ?></td> 

                    <td class="ms">

                      <div class="btn-group"> 

                        <a href="<?php echo base_url().'backend/users/edit/'.$value->id ?>" class="btn btn-success btn-xs" ><i class="fa fa-pencil-square-o"></i></a> 

                        <i></i>

                        <a href="<?php echo base_url().'backend/users/delete/'.$value->id ?>" class="btn btn-danger btn-xs"  onclick="if(confirm('Are you sure you want to delete?')){return true;} else {return false;}" > <i class="fa fa-trash-o"></i></a> 

                      </div>

                    </td>

                </tr>

                 <?php } ?>



                <?php else: ?>

                  <tr>

                    <th colspan="9"> <center>No Customers found.</center></th>

                  </tr>

                <?php endif; ?>

                   <tr>

            <td colspan="9" ><input type="submit" id="delete_cust" name="delete"  onclick="return confirm('Do you want to delete');" value="Delete" class="btn-info btn"></td>

           <!--  <td colspan="6"><input type="submit" name="message" value="Message" class="btn-info btn"></td> -->

          </tr>

                </tbody>

          </table>

        </div>
       </form>
      </div>
    </div>
  </div>
</div>  <!-- / Users widget-->
<script>
    $(document).ready(function () {
        $(document).on('click', "#all_customer_check", function () {
            $(".check_customer").prop('checked', $(this).prop('checked'));
        });

    });

</script>