

                <div class="row">
                 <div class="col-mod-12">
                <!--  <ul class="breadcrumb">
                   <li><a href="index.html">Dashboard</a></li>
                   <li><a href="#">Tables</a></li>
                   <li class="active">Basic Tables</li>
                 </ul>
                  -->
                <!--  <div class="form-group hiddn-minibar pull-right">
                  <input type="text" class="form-control form-cascade-control nav-input-search" size="20" placeholder="Search through site" />

                  <span class="input-icon fui-search"></span>
                </div> -->

                <h3 class="page-header"> Users <i class="fa fa-user animated"></i> </h3>

            </div>
          </div>

          <!-- Users widget -->
          <div class="row">
           <div class="col-md-12">
            <div class="panel">
             <div class="panel-heading text-primary">
              <h3 class="panel-title"><i class="fa fa-user"></i> Add User
              
          </h3>
        </div>
        <div class="panel-body">
          

          <div class="panel panel-default">
            <div class="panel-heading">Add User</div>
            <div class="panel-body">
                

      <?php echo form_open_multipart(current_url(),array('class'=>'form-horizontal')); ?> 
      <div class="form-body">


					<div class="form-group">

					    <label for="inputEmail3" class="col-sm-2 control-label">First Name <span class="men">*</span></label>

					    <div class="col-sm-9">

					      <input type="text" class="form-control" name="first_name"  id="first_name" placeholder="First Name" value="<?php echo set_value('first_name')?>">

					      <?php echo form_error('first_name'); ?> <span style="color:red;" id="fn_error"></span> 

					    </div>

					</div> 



				<div class="form-group">

				    <label for="inputEmail3" class="col-sm-2 control-label">Last Name <span class="men">*</span></label>

				    <div class="col-sm-9">

				      <input type="text" class="form-control" name="last_name"  id="last_name" placeholder="Last Name" value="<?php echo set_value('last_name')?>">

				      <?php echo form_error('last_name'); ?> <span style="color:red;" id="ln_error"></span>

				    </div>

				</div>



				<div class="form-group">

				    <label for="inputPassword3" class="col-sm-2 control-label">Email <span class="men">*</span></label>

				    <div class="col-sm-9">

				      <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo set_value('email'); ?>" >

				      <?php  echo form_error('email');  ?><span style="color:red;" id="em_error"></span>

				    </div>

				</div>

				<div class="form-group">

				    <label for="inputPassword3" class="col-sm-2 control-label">Password <span class="men">*</span></label>

				    <div class="col-sm-9">

				      <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo set_value('password'); ?>" >

				      <?php  echo form_error('password');  ?><span style="color:red;" id="ps_error"></span>

				    </div>

				</div>

				<div class="form-group">

				    <label for="inputPassword3" class="col-sm-2 control-label">Confirm Password <span class="men">*</span></label>

				    <div class="col-sm-9">

				      <input type="password" class="form-control" name="con_password" id="con_password" placeholder="Confirm Password" value="<?php echo set_value('con_password'); ?>" >

				      <?php  echo form_error('con_password');  ?><span style="color:red;" id="cps_error"></span>

				    </div>

				</div> 




			  <div class="form-group">

			    <label for="inputPassword3" class="col-sm-2 control-label">Address</label>

			    <div class="col-sm-9">

			        <textarea name="address" id="address"  class="form-control"><?php echo set_value('address'); ?></textarea>

			    	<?php  echo form_error('address');  ?> <span style="color:red;" id="ad1_error"></span>

			    </div>

			  </div> 



			  <div class="form-group">

			    <label for="inputPassword3" class="col-sm-2 control-label">City <span class="men">*</span></label>

			    <div class="col-sm-9">

			      <input type="text" class="form-control" name="city"  id="city" placeholder="City" value="<?php echo  set_value('city'); ?>">

			      <?php echo form_error('city');  ?>  <span style="color:red;" id="ct_error"></span>

			    </div>
			    </div>

				  <div class="form-group">

				    <label for="inputPassword3" class="col-sm-2 control-label">Phone No.</label>

				    <div class="col-sm-9">

				       <input type="text" class="form-control" maxlength="15"  id="phone"  name="phone"  value="<?php  if(!empty($_POST['phone'])){  echo $_POST['phone'];  }  ?>" id="phone" placeholder="Phone">

				       <?php echo form_error('phone'); ?><span style="color:red;" id="ph_error"></span>

				    </div>

				  </div>





				  <div class="form-group">

				    <label for="inputtext3" class="col-sm-2 control-label">Status:</label>

				    <div class="col-sm-9">

				      <select class="form-control"  name="status">

				      	<option value="1">Active</option>

				      	<option value="0">InActive</option>

				      </select>

				    </div>

				  </div>



					<div class="form-group">

					    <div class="col-sm-offset-2 col-sm-9">

					      <button type="submit" class="btn btn-info btn-default tab_button"> Save <i class="fa fa-angle-double-right"></i></button>

					    </div>

					</div>

										

										 

										

						
            </div>
        <?php echo form_close(); ?>
            </div>
        </div>
</div>
</div>
</div>  <!-- / Users widget-->

