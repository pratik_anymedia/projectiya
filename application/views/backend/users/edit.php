

        <div class="row">
            <div class="col-mod-12">
            	<h3 class="page-header">  Project Technologies <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
            </div>
        </div>

          <!-- Users widget -->
      <div class="row">
       <div class="col-md-12">
        <div class="panel">
         <div class="panel-heading text-primary">
          <h3 class="panel-title"><i class="fa fa-rocket"></i> Edit User Info</h3>
        </div>
        <div class="panel-body">
          

          <div class="panel panel-default">
            <div class="panel-heading">Edit User</div>
            <div class="panel-body">
                
<?php echo form_open_multipart(current_url(),array('class'=>'form-horizontal')); ?> 

          <div class="form-body">
          	
			  <div class="form-group">

			    <label for="inputEmail3" class="col-sm-2 control-label">First Name <span class="men">*</span></label>

			    <div class="col-sm-9">

			      <input type="text" class="form-control" name="first_name"  id="first_name" placeholder="First Name" value="<?php if(!empty($customer->first_name)) echo $customer->first_name; ?>">

			      <?php echo form_error('first_name'); ?><span style="color:red;" id="fn_error"></span> 

			    </div>

			  </div> 


			  <div class="form-group">

			    <label for="inputEmail3" class="col-sm-2 control-label">Last Name <span class="men">*</span></label>

			    <div class="col-sm-9">

			      <input type="text" class="form-control" name="last_name"  id="last_name" placeholder="Last Name" value="<?php if(!empty($customer->last_name)) echo $customer->last_name; ?>">

			      <?php echo form_error('last_name'); ?><span style="color:red;" id="ln_error"></span>

			    </div>

			  </div>



			  <div class="form-group">

			    <label for="inputPassword3" class="col-sm-2 control-label">Email <span class="men">*</span></label>

			    <div class="col-sm-9">

			      <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?php if(!empty($customer->email)) echo $customer->email; ?>">

			      <?php  echo form_error('email');  ?><span style="color:red;" id="em_error"></span>

			    </div>

			  </div> 



			  <div class="form-group">

			    <label for="inputPassword3" class="col-sm-2 control-label">Address<span class="men">*</span></label>

			    <div class="col-sm-9">

			        <textarea name="address"  id="address"  class="form-control"><?php if(!empty($customer->address)) echo $customer->address; ?></textarea>

			    	<?php  echo form_error('address');  ?><span style="color:red;" id="ad_error"></span>

			    </div>

			  </div> 


				  <div class="form-group">

				    <label for="inputPassword3" class="col-sm-2 control-label">City <span class="men">*</span></label>

				    <div class="col-sm-9">

				      <input type="text" class="form-control" name="city"  id="city" placeholder="City" value="<?php if(!empty($customer->city)) echo $customer->city; ?>">

				      <?php echo form_error('city');  ?><span style="color:red;" id="ct_error"></span>

				    </div>

				  </div> 
				  

				  <div class="form-group">

				    <label for="inputPassword3" class="col-sm-2 control-label">Phone No.</label>

				    <div class="col-sm-9">

				       <input type="text" class="form-control"  maxlength="15" id="phone" name="phone"  value="<?php if(!empty($customer->phone)) echo $customer->phone; ?>" id="phone" placeholder="Phone">

				       <?php echo form_error('phone'); ?>

				    </div>

				  </div>


				

				  <div class="form-group">

				    <label for="inputtext3" class="col-sm-2 control-label">Status:</label>

				    <div class="col-sm-9">

				      <select class="form-control"  name="status">

				      	<option value="1" <?php if($customer->status==1) echo 'selected="selected"'; ?> >Active</option>

				      	<option value="0" <?php if($customer->status==0) echo 'selected="selected"'; ?> >InActive</option>

				      </select>

				    </div>

				  </div>



				<div class="form-group">

				    <div class="col-sm-offset-2 col-sm-9">

				      <button type="submit" id="first" data-tab="1" class="btn btn-info btn-default tab_button"> Update <i class="fa fa-angle-double-right"></i></button>

				    </div>

				</div>



          </div> 

         
        <?php echo form_close(); ?>

            </div>
            </div>
        </div>
</div>
</div>
</div>  <!-- / Users widget-->

