

    <div class="row">
        <div class="col-mod-12">  
           <h3 class="page-header"> <?php if(!empty($user->first_name) && !empty($user->last_name))  echo $user->first_name.' '.$user->last_name.'\'s';  ?>  Porjects  </h3>
        </div>
    </div>

    <!-- Users widget -->
    <div class="row">
       <div class="col-md-12">
        <div class="panel">
         <div class="panel-heading text-primary">
          <h3 class="panel-title"><i class="fa fa-user"></i> List        <a class=" btn btn-xs btn-info" href="<?php echo base_url('backend/users/');  ?>"  style="margin-top:3px;"class="btn btn-small">Back To User</a></h3>
        </div>
      <div class="panel-body">
      <?php  echo msg_alert_backend();  ?>

 
  
       <div class="table-responsive">
        <table class="table users-table table-condensed table-hover" >
           <thead>
            <tr>
              <th>#</th>
                <th>Project Title</th>
              
                <th>Price</th>
             <!--    <th>Avialable</th> -->
                <th>Status</th>
                <th class="hidden-phone">Created</th>
                <th class="hidden-phone">Actions  </th>
            </tr>
         </thead>
         <tbody>
         <?php if(!empty($projects)):
              $i=0;
                foreach($projects as $value){ $i++; ?>
                <tr class="gradeX">
                    <td><?php echo $i."." ;?></td>
                    <td><?php echo character_limiter($value->title,30); ?></td>
                    <!-- <td><a href="<?php //echo base_url('backend/projects/reviews/'.$value->id);  ?>"  class="btn btn-xs btn-success" >Review</a></td> -->
                    <td class="to_hide_phone"><?php echo $value->price; ?></td> 
                    <!-- <td class="to_hide_phone">
                      <?php //if(!empty($value->available_item)){ ?> 
                          <span class="label label-success"> Available</span>
                      <?php //} else {   ?>
                          <span class="label label-success"> Not Available</span>
                      <?php //} ?>
                    </td>  -->

                    <td class="to_hide_phone"><?php if($value->status){ echo 'Active'; }else{  echo 'Inactive'; } ?></td> 
                   
                    <td><?php echo date('d-m-Y',strtotime($value->created)); ?></td>
                    
                    <td class="ms">
                      <div class="btn-group"> 
                        <a href="#<?php //echo base_url().'user/uploaded_project_edit/'.$value->id ?>" class="btn btn-success btn-xs" ><i class="fa fa-eye"></i></a> 
                      <!--   <i></i> -->
                        <!-- <a href="<?php //echo base_url().'user/uploaded_project_delete/'.$value->id ?>" class="btn btn-danger btn-xs"  onclick="if(confirm('Are you sure you want to delete?')){return true;} else {return false;}" > <i class="fa fa-trash-o"></i></a>  -->
                      </div>
                    </td>
                </tr>

            <?php } ?>
            <?php else: ?>
              <tr>
                <th colspan="7"> <center>No Projects found.</center></th>
              </tr>
            <?php endif; ?>

          </tbody>
          </table>

        </div>
     
      </div>
    </div>
  </div>
</div>  <!-- / Users widget-->

