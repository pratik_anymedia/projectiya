        <div class="row">
         <div class="col-mod-12">
                <!--  <ul class="breadcrumb">
                   <li><a href="index.html">Dashboard</a></li>
                   <li><a href="#">Tables</a></li>
                   <li class="active">Basic Tables</li>
                 </ul>
                  -->
                <!--  <div class="form-group hiddn-minibar pull-right">
                  <input type="text" class="form-control form-cascade-control nav-input-search" size="20" placeholder="Search through site" />

                  <span class="input-icon fui-search"></span>
                </div> -->

                <h3 class="page-header">  Email Templates  </h3>
<!-- 
                <blockquote class="page-information hidden">
                 <p>
                  More styles of basic tables are available to represent static information.
                </p>
              </blockquote> -->
            </div>
          </div>

          <!-- Users widget -->
          <div class="row">
           <div class="col-md-12">
            <div class="panel">
             <div class="panel-heading text-primary">
              <h3 class="panel-title"><i class="fa fa-rocket"></i> List <a href="<?php echo base_url('backend/email_templates/add') ?>" class="btn btn-success">Add New Email Template <i class="fa fa-plus"></i> </a> 
               
          </h3>
        </div> 
        <div class="panel-body">
         
 <?php  echo msg_alert_backend();  ?>
      <br>
             
                  <div class="table-responsive">
                    <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
                      <thead>
                      <tr>

                <th width="5%">#</th>

                <th width="35%">Template name</th>

                <th width="30%">Subject</th>

                <th width="20%">Created </th>

                <th width="10%">Actions</th>

              </tr>

                      </thead>
                      <tbody>

              <?php

              if (!empty($templates)):

                $i = 0; foreach ($templates as $row) { $i++;

                  ?>

                  <tr>

                    <td><?php echo $row->id . "."; ?></td>

                    <td><a href="<?php echo base_url( 'backend/email_templates/edit/' . $row->id )?>"class="btn btn-small"  rel="tooltip" data-placement="left" data-original-title=" Edit "><?php echo $row->template_name; ?></a></td>

                    <td ><?php echo substr($row->template_subject, 0, 50); ?></td>

                    <td><?php echo date('Y-m-d', strtotime($row->template_created)); ?></td>

                    <td>

                    <a href="<?php echo base_url('backend/email_templates/edit/' . $row->id ) ?>" class="btn btn-success btn-xs" rel="tooltip" data-placement="left" data-original-title=" Edit ">

                        <i class="fa fa-pencil"></i>

                      </a>

                      <a href="<?php echo base_url('backend/email_templates/delete/' . $row->id) ?>" class="btn btn-danger btn-xs" rel="tooltip" rel="tooltip" data-placement="bottom" data-original-title="Remove" onclick="return confirm('Are you sure want to delete?');" >

                        <i class="fa fa-trash-o "></i></a>

                      </td>

                    </tr>

                    <?php } ?><?php else: ?>

                    <tr>

                      <th colspan="7">

                        <center>No Email Template Found.</center>

                      </th>

                    </tr>

                  <?php endif; ?>

                </tbody>
                    </table>
                    <div class="row-fluid  control-group mt15">             
                        <div class="span12">
                          <?php if(!empty($pagination))  echo $pagination;?>              
                        </div>
                      </div>
                  </div>
</div>
</div>
</div>
</div>  <!-- / Users widget-->

