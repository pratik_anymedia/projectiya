        <div class="row">
         <div class="col-mod-12">
                <!--  <ul class="breadcrumb">
                   <li><a href="index.html">Dashboard</a></li>
                   <li><a href="#">Tables</a></li>
                   <li class="active">Basic Tables</li>
                 </ul>
                  -->
                <!--  <div class="form-group hiddn-minibar pull-right">
                  <input type="text" class="form-control form-cascade-control nav-input-search" size="20" placeholder="Search through site" />

                  <span class="input-icon fui-search"></span>
                </div> -->

                <h3 class="page-header">  Project Technologies <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
<!-- 
                <blockquote class="page-information hidden">
                 <p>
                  More styles of basic tables are available to represent static information.
                </p>
              </blockquote> -->
            </div>
          </div>

          <!-- Users widget -->
          <div class="row">
           <div class="col-md-12">
            <div class="panel">
             <div class="panel-heading text-primary">
             <h3 class="panel-title"><i class="fa fa-rocket"></i> List  &nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo base_url('backend/technologies/add') ?>" class="btn btn-info">Add New Technology<i class="icon-plus"></i> </a>  </h3>
               
          </h3>
        </div>
        <div class="panel-body">
         
 <?php  echo msg_alert_backend();  ?>
         <table class="table users-table table-condensed table-hover" >

            <thead>

                <tr>

                  <th>#</th>

                  <th>Technology Name</th>

                  <th>Slug</th>

                  <th class="hidden-phone">Created</th>

                  <th class="hidden-phone">Actions</th>

                </tr>

                </thead>

                <tbody>

                <?php if(!empty($technologies)):

                   $i=$offset;

                foreach($technologies as $value){ $i++; ?>

                <tr class="gradeX">

                    <td><?php echo $i."." ;?></td>

                    <td class=""><?php echo $value->technology; ?></td>

                    <td class="to_hide_phone"><?php echo $value->slug; ?></td> 

                    <td class="to_hide_phone"><?php if($value->status){ echo 'Active'; }else{  echo 'Inactive'; } ?></td> 



                    <td><?php echo date('d-m-Y',strtotime($value->created_date)); ?></td>

                    <td class="ms">

                      <div class="btn-group"> 

                        <a href="<?php echo base_url().'backend/technologies/edit/'.$value->id ?>" class="btn btn-success btn-xs" ><i class="fa fa-pencil-square-o"></i></a> 

                        <i></i>

                        <a href="<?php echo base_url().'backend/technologies/delete/'.$value->id ?>" class="btn btn-danger btn-xs"  onclick="if(confirm('Are you sure you want to delete?')){return true;} else {return false;}" > <i class="fa fa-trash-o"></i></a> 

                      </div>

                    </td>

                </tr>

                 <?php } ?>

                <?php else: ?>

                  <tr>

                    <th colspan="5"> <center>No technologies found.</center></th>

                  </tr>

                <?php endif; ?>

                </tbody>

          </table>   
</div>
</div>
</div>
</div>  <!-- / Users widget-->

