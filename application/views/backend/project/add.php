<div class="row">

	<div class="col-md-12 ">

	    <?php  echo msg_alert_backend(); ?>

		<div class="portlet box blue">    

			<div class="portlet-title"> 

				<div class="caption">            

					<i class="fa fa-gears"></i> Product Setting       

				</div>       

				<div class="tools">   

					<a href="" class="collapse"></a>

				</div>

			</div>



			<div class="portlet-body form">   

				<!-- <form role="form" method="post" action="<?php //echo current_url() ?>" class="form-horizontal"> -->

				<?php echo form_open_multipart(current_url(),array('class'=>'form-horizontal')); ?>

					<div class="form-body">

						<div class="form-group">

							<div class="col-md-12">   

				                <label class="control-label"><strong>Title</strong></label>                                    

								<input type="text" placeholder="Title" class="form-control" name="title" value="<?php if(!empty($page_set->title)) echo $page_set->title; ?>">

								<?php echo form_error('title'); ?>

				            </div> 

			            </div>


			            <div class="form-group">

							<div class="col-md-12">   

				                <label class="control-label"><strong>Sub-heading</strong></label>                                    

								<textarea placeholder="Sub-heading" class="form-control" name="subheading"><?php if(!empty($page_set->subheading)) echo $page_set->subheading; ?></textarea>

								<?php echo form_error('subheading'); ?>

				            </div> 

			            </div>

			            <div class="form-group">

							<div class="col-md-12">   

				                <label class="control-label"><strong>Description</strong></label>

				                <textarea cols="100" rows="5" name="description" class="form-control"><?php if(!empty($page_set->description)) echo $page_set->description; ?></textarea>						

								<?php echo form_error('description'); ?>

				            </div> 

			            </div> 

         						
				        <div class="form-group">

			            	<div class="col-md-12"> 

				                <label class="control-label"><strong>Category</strong></label>                                     

								<select class="form-control" name="category"  style="width:100%">

									<option value="">Select Category</option>

									<?php  if(!empty($category)):  ?>

										<?php foreach ($category as $value){ ?>

											<option value="<?php  echo $value->id;  ?>" <?php  if($product->category==$value->id) echo 'selected="selected"'; ?>  ><?php  if(!empty($value->category)) echo $value->category;  ?></option>

										<?php } ?>

									<?php endif; ?>	

								</select>                                 

								<?php echo form_error('category'); ?>

			            	</div>

			            </div>


			            <div class="form-group">

							<div class="col-md-12">   

				                <label class="control-label"><strong>Price</strong></label>                                    

								<input type="text" placeholder="Title" class="form-control" name="title" value="<?php if(!empty($page_set->title)) echo $page_set->title; ?>">

								<?php echo form_error('title'); ?>

				            </div> 

			            </div>

            			

            			<div class="form-group">

			              <div class="col-sm-12">

			              <label class="control-label"><strong>Image</strong></label>

			                <input type="file" name="userfile">

			                <?php echo form_error('userfile'); ?>

			                <?php if(!empty($page_set->file_path)){ ?>

			                	<img src="<?php echo base_url(str_replace('./','',$page_set->file_thumb_path));   ?>" alt="">	

			                <?php } ?>

			                <!--<small>(file_type: jpg, png, jpeg, and image size maximum size 280 X 230 px and minimum size 270 X 220 px)</small> -->

			              </div>

			            </div>



			        

			        	<div class="form-group">

			            	<div class="col-md-12"> 

				                <label class="control-label"><strong>Status</strong></label>                                     

								<select class="form-control" name="status" id="remote" style="width:100%">

									<option value="1">Active</option>

			              			<option value="0">Deactive</option>

								</select>                                 

								<?php //echo form_error('status'); ?>

			            	</div>

			            </div> 

					</div> 

					<div class="form-actions">       

						<button type="submit" class="btn blue">Submit</button> 

						<a href="<?php echo base_url('backend/products/'); ?>" ><button class="btn btn-danger" type="button">Cancel</button></a>

					</div>  

				<?php echo form_close(); ?>

			</div>

		</div>

	</div>

</div>

