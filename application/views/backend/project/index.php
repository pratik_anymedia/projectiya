
          <div class="row">
           <div class="col-mod-12">
                <h3 class="page-header">  Projects  </h3>
            </div>
          </div>
        <?php 
          if(!empty($_SERVER['QUERY_STRING']))
            $QUERY_STRING = "0?".$_SERVER['QUERY_STRING'];
          else
            $QUERY_STRING ='';
        ?>
          <!-- Users widget -->
      <div class="row">
       <div class="col-md-12">
        <div class="panel">
         <div class="panel-heading text-primary">
          <h3 class="panel-title"><i class="fa fa-rocket"></i> List  &nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo base_url('backend/projects/add') ?>" class="btn btn-info">Add New Project<i class="icon-plus"></i> </a>  </h3>
        </div>
        <div class="panel-body">
         
         <?php  echo msg_alert_backend();  ?>

         <form class="form-inline" action="<?php echo base_url('backend/projects/index/id/asc/') ?>" role="form">
            <div class="form-group">
                <!-- <label class="sr-only" for="exampleInputEmail2">First Name</label> -->
                <select style="width:100%;"  class="form-control" name="search_by"> <option value="">Select Field  </option> <option value="title" <?php if(!empty($_GET['search_by']) && $_GET['search_by']=='title') echo'selected'?>>Project Title</option> <!-- <option value="SKU" <?php //if(!empty($_GET['search_by']) && $_GET['search_by']=='SKU') echo 'selected'?>>SKU </option> -->  </select> 
            </div>
            <div class="form-group">
                <!-- <label class="sr-only" for="exampleInputPassword2">Enter Text</label> -->
                <input  class="form-control" type="text" style="width:100%;" name="search_query"  placeholder="Search" value="<?php if(!empty($_GET['search_query'])) echo $_GET['search_query'] ?>">
            </div> 
            <div  class="form-group">
            <select name="technology" class="form-control input-xlarge select2me select2-offscreen" data-placeholder="Select..." tabindex="-1" title="">
             
                  <option value="">Select Technology</option>
                <?php  if(!empty($technology)):  ?>

                  <?php foreach ($technology as $value){ ?>
                    <option value="<?php  echo $value->id;  ?>"  <?php if(!empty($_GET['technology']) && $_GET['technology']==$value->id) echo 'selected="selected"'; ?>  ><?php  if($value->technology) echo $value->technology;  ?></option>
                  <?php } ?>
                <?php endif; ?> 
              </select>
            </div>
            <button class="btn btn-primary" type="submit">Search</button>
            <a class="btn btn-warning" href="<?php echo base_url('backend/projects/');  ?>"  style="margin-top:3px;"class="btn btn-small">Reset</a> 
      </form>
      <table class="table table-striped table-flip-scroll cf">
          <thead>
              <tr>
                <th>#</th>
                <th>Project Title</th>
                <th>Owner</th>
                <th>Review</th>
                <th>Price</th>
                <th>Avialable</th>
                <th>Status</th>
                <th class="hidden-phone">Created</th>
                <th class="hidden-phone">Actions  </th>
              </tr>
          </thead>
      <tbody>
          <?php if(!empty($projects)):
              $i=$offset;
                foreach($projects as $value){ $i++; ?>
                <tr class="gradeX">
                    <td><?php echo $i."." ;?></td>
                    <td><?php echo ucfirst(character_limiter($value->title,30)); ?></td>
                    <td><?php if(!empty($value->user_id)){ $name = get_user_name($value->user_id); if(!empty($name)) echo ucfirst($name); else echo '-'; }else{   echo 'Admin';    } ?></td>
                    <td><a href="<?php echo base_url('backend/projects/reviews/'.$value->id);  ?>"  class="btn btn-xs btn-success" >Review</a></td>
                    <td class="to_hide_phone"><?php echo $value->price; ?></td> 
                    <td class="to_hide_phone">
                      <?php if(!empty($value->available_item)){ ?> 
                          <span class="label label-success"> Available</span>
                      <?php } else {   ?>
                          <span class="label label-success"> Not Available</span>
                      <?php } ?>
                    </td> 

                    <td class="to_hide_phone"><?php if($value->status){ echo 'Active'; }else{  echo 'Inactive'; } ?></td> 
                   
                    <td><?php echo date('d-m-Y',strtotime($value->created)); ?></td>
                    
                    <td class="ms">
                      <div class="btn-group"> 
                        <a href="<?php echo base_url().'backend/projects/edit/'.$value->id ?>" class="btn btn-success btn-xs" ><i class="fa fa-pencil-square-o"></i></a> 
                        <i></i>
                        <a href="<?php echo base_url().'backend/projects/delete/'.$value->id ?>" class="btn btn-danger btn-xs"  onclick="if(confirm('Are you sure you want to delete?')){return true;} else {return false;}" > <i class="fa fa-trash-o"></i></a> 
                      </div>
                    </td>
                </tr>

                <?php } ?>

                <?php else: ?>
                  <tr>
                    <th colspan="7"> <center>No Projects found.</center></th>
                  </tr>
                <?php endif; ?>
        </tbody>
    </table>          
</div>
</div>
</div>
</div>  <!-- / Users widget-->

