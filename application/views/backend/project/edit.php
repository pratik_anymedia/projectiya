<div class="row">

	<div class="col-md-12 ">

		<div class="portlet box blue">    

			<div class="portlet-title"> 

				<div class="caption">            

					<i class="fa fa-sitemap"></i> Edit Product      

				</div>       

				<div class="tools">   

					<a href="" class="collapse"></a>

				</div>

			</div>



			<div class="portlet-body form">   

				<?php echo form_open_multipart(current_url(), array('class'=>'form-horizontal')); ?>

					<div class="form-body">

							<div class="form-group">
								<div class="col-md-12"> 
				                <label class="control-label"><strong>Product Name</strong></label>                                     
								<input type="text" placeholder="Product Name" class="form-control" name="name" value="<?php if(!empty($product->name)) echo $product->name; ?>">
								<?php echo form_error('name'); ?>
				            	</div> 
				            </div> 



				            <div class="form-group">

			            	<div class="col-md-12"> 

				                <label class="control-label"><strong>Category</strong></label>                                     

								<select class="form-control" name="category"  style="width:100%">

									<option value="">Select Category</option>

									<?php  if(!empty($category)):  ?>

										<?php foreach ($category as $value){ ?>

											<option value="<?php  echo $value->id;  ?>" <?php  if($product->category==$value->id) echo 'selected="selected"'; ?>  ><?php  if(!empty($value->category)) echo $value->category;  ?></option>

										<?php } ?>

									<?php endif; ?>	

								</select>                                 

								<?php echo form_error('category'); ?>

			            	</div>

			            </div>



				            <div class="form-group">

								<div class="col-md-12">   

					                <label class="control-label"><strong>Description</strong></label>

					                <textarea cols="100" rows="5" name="description" class="tinymce_edittor form-control"><?php if(!empty($product->description)) echo $product->description; ?></textarea>						

									<?php echo form_error('description'); ?>

					            </div> 

				            </div>



	         				<div class="form-group">

								<div class="col-md-12">   

					                <label class="control-label"><strong>Product Price</strong></label>                                    

									<input type="text" placeholder="Product Price" class="form-control" name="price" value="<?php if(!empty($product->price)) echo $product->price; ?>">

									<?php echo form_error('price'); ?>

					            </div> 

				            </div>	





					  <div class="form-group">

					    <div class="col-sm-12">

						    <label  class="control-label"><strong>Image</strong></label>

						    <input type="file" name="userfile"   >

						    <!-- <small>(file_type: jpg, png, jpeg, and image size maximum size 280 X 230 px and minimum size 270 X 220 px)</small>                                  -->

			                <?php echo  form_error('userfile'); ?>

			                <?php if(!empty($product->thumb_image)&&file_exists($product->thumb_image)): ?>

					    	<img src="<?php echo base_url(str_replace('./','',$product->thumb_image));?>"   width="65" height="65"  />

					    	<?php endif;  ?>	

					    </div>

					  </div>



		                <?php  //if(!empty($products->thumb_image)&&file_exists($products->thumb_image)): ?>

						<!--<div class="form-group">

						    <label for="inputPassword3" class="col-sm-2 control-label">Image Preview</label>

						    <div class="col-sm-10">

						   		<img src="<?php //echo base_url(str_replace('./','',$products->thumb_image));?>"  alt="" data-src="<?php //echo base_url(str_replace('./','',$products->thumb_image));?>" data-src-retina="<?php //echo base_url() ?>assets/frontend/img/icons/business_user.png" width="65" height="65"  />

						    </div>

					    </div> -->

		                <?php //endif;  ?>

         

            

				          	<div class="form-group">

				            	<div class="col-md-12"> 

					                <label class="control-label"><strong>Status</strong></label>                                     

									<select class="form-control" name="status" id="remote" style="width:100%">

										<option value="1" <?php if($product->status==1) echo 'selected="selected"'; ?> >Active</option>

				              			<option value="0" <?php if($product->status==0) echo 'selected="selected"'; ?> >Deactive</option>

									</select>                                 

									<?php echo form_error('status'); ?>

				            	</div>

				            </div>

			            

					</div> 

					<div class="form-actions">       

						<button type="submit" class="btn blue">Submit</button> 

						<a href="<?php echo base_url('backend/products/'); ?>" ><button class="btn btn-danger" type="button">Cancel</button></a>

					</div>  

				<?php echo form_close(); ?>

			</div>

		</div>

	</div>

</div>

