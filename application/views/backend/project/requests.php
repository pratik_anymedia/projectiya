
<div class="row">
    <div class="col-mod-12">
        <h3 class="page-header">  Project Requests  </h3>
    </div>
</div>
<?php
if (!empty($_SERVER['QUERY_STRING']))
    $QUERY_STRING = "0?" . $_SERVER['QUERY_STRING'];
else
    $QUERY_STRING = '';
?>
<!-- Users widget -->
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading text-primary">
                <h3 class="panel-title"><i class="fa fa-rocket"></i> List </h3>
            </div>
            <div class="panel-body">

                <?php echo msg_alert_backend(); ?>

                <form class="form-inline" action="<?php echo base_url('backend/projects/requests/id/asc/') ?>" role="form">
                    <div class="form-group">
                        <!-- <label class="sr-only" for="exampleInputEmail2">First Name</label> -->
                        <select style="width:100%;"  class="form-control" name="search_by"> 
                            <option value="">Select Field  </option> 
                            <option value="title" <?php if (!empty($_GET['search_by']) && $_GET['search_by'] == 'title') echo'selected' ?>>Request Title</option> 
                        </select> 
                    </div>
                    <div class="form-group">
                        <!-- <label class="sr-only" for="exampleInputPassword2">Enter Text</label> -->
                        <input  class="form-control" type="text" style="width:100%;" name="search_query"  placeholder="Search" value="<?php if (!empty($_GET['search_query'])) echo $_GET['search_query'] ?>">
                    </div> 

                    <button class="btn btn-primary" type="submit">Search</button>
                    <a class="btn btn-warning" href="<?php echo base_url('backend/projects/'); ?>"  style="margin-top:3px;"class="btn btn-small">Reset</a> 
                </form>
                <table class="table table-striped table-flip-scroll cf">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>File</th>
                            <th class="hidden-phone">Actions  </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($requests)):
                            $i = $offset;
                            foreach ($requests as $value) {
                                $i++;
                                ?>
                                <tr class="gradeX">
                                    <td><?php echo $i . "."; ?></td>
                                    <td><?php echo $value->title; ?></td>
                                    <td><?php echo character_limiter($value->description, 30); ?></td>
                                    <td><?php echo $value->email; ?></td>
                                    <td><?php echo $value->mobile_no; ?></td>
                                    <td><a href="<?php echo base_url().'superadmin/downloadProject/'.$value->filename; ?>"><?php echo $value->filename; ?></a></td>

                                    <td class="ms">
                                        <div class="btn-group"> 
                                            <a href="<?php echo base_url() . 'backend/projects/deleteRequest/' . $value->prid ?>" class="btn btn-danger btn-xs"  onclick="if (confirm('Are you sure you want to delete?')) {
                                                return true;
                                            } else {
                                                return false;
                                            }" > <i class="fa fa-trash-o"></i></a> 
                                        </div>
                                    </td>
                                </tr>

    <?php } ?>

                    <?php else: ?>
                            <tr>
                                <th colspan="7"> <center>No Projects found.</center></th>
                        </tr>
<?php endif; ?>
                    </tbody>
                </table>    
                <div class="row">
                    <div class="col-md-6">

                    </div>
                    <div class="col-md-6">
                        <div class="pull-right">
<?php if (!empty($pagination)) echo $pagination; ?>
                        </div>
                    </div>
                </div>     
            </div>
        </div>
    </div>
</div>  <!-- / Users widget-->

