
      <div class="row">
          <div class="col-mod-12">
              <h3 class="page-header"> Projects Reviews  </h3>
          </div>
      </div>
 
      <div class="row">
       <div class="col-md-12">
        <div class="panel">
         <div class="panel-heading text-primary">
          <h3 class="panel-title"><i class="fa fa-rocket"></i> List  &nbsp;&nbsp;&nbsp;&nbsp; <a  class="btn btn-xs btn-primary"  href="<?php  echo base_url('backend/projects/');  ?>" >Beck to projects </a>  </h3>
        </div>
        <div class="panel-body">
         
         <?php  echo msg_alert_backend();  ?>

        
      <table class="table table-bordered table-hover">
            <thead>
                <tr>    
                  <th style="width:10%">S.no.</th>
                  <th style="width:20%">Product Name</th>                 
                  <th style="width10%">Customer Name</th>                 
                  <th style="width:20%">Message</th>
                  <th style="width:10%">Status</th>
                
                  <th style="width:10%" > Created</th>
                  <th style="width:13%" > Actions </th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($reviews)):
                   $i=0;
                foreach($reviews as $value){ $i++;  $user_info = get_user_information($value->user_id);  ?>
                <tr>
                    <td class=""><?php  echo $i.'.';  ?></td>
                    <td class=""><?php if(!empty($project_info->title)) echo word_limiter($project_info->title,10); ?></td>
                    <td class=""><?php if(!empty($user_info->first_name) && !empty($user_info->last_name))  echo  ucfirst($user_info->first_name.' '.$user_info->last_name);  ?></td>                
                  
                    <td class="to_hide_phone"><?php echo  word_limiter($value->message,40); ?></td> 
                  <td>
                    <?php if($value->status){ ?> 
                      <a data-toggle="tooltip" data-placement="left" title="Active" href="<?php echo base_url().'backend/projects/change_review_status/'.$value->id.'/'.$value->project_id.'/'.$value->status; ?>"><span  class="btn btn-xs btn-success"><i class="fa fa-check-circle "></i> Active </span></a> 
                    <?php  }else{  ?>  
                      <a data-toggle="tooltip" data-placement="left" title="InActive" href="<?php echo base_url().'backend/projects/change_review_status/'.$value->id.'/'.$value->project_id.'/'.$value->status; ?>"><span  class="btn btn-xs btn-danger"><i class="fa fa-times-circle"></i> Inactive</span></a> 
                    <?php } ?> 
                    </td>
                    <td><?php echo date('d-m-Y',strtotime($value->created)); ?></td>
                    <td class="ms">
                      <div class="btn-group">
                        <a href="<?php echo base_url().'backend/projects/review_view/'.$value->id.'/'.$value->project_id; ?>" class="btn btn-warning btn-xs"   > <i class="fa fa-eye"></i></a> 
                       <i></i>
                        <a href="<?php echo base_url().'backend/projects/review_delete/'.$value->id.'/'.$value->project_id; ?>" class="btn btn-danger btn-xs"  onclick="if(confirm('Are you sure you want to delete?')){return true;} else {return false;}" > <i class="fa fa-trash-o"></i></a> 
                      </div>
                    </td>
                </tr>
                 <?php } ?>
                <?php else: ?>
                  <tr>
                    <th colspan="6"> <center>No Reviews found.</center></th>
                  </tr>
                <?php endif; ?>
   
                </tbody>
          </table> 
</div>
</div>
</div>
</div>  <!-- / Users widget-->

