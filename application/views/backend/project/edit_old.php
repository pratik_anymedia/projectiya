

        <div class="row">
         <div class="col-mod-12">
                <h3 class="page-header"> Products </h3>          
         </div>
        </div>

          <!-- Users widget -->
			<div class="row">
			<div class="col-md-12">
			<div class="panel">
			<div class="panel-heading text-primary">
			<h3 class="panel-title"><i class="fa fa-rocket"></i> Edit Product</h3>

			</div>
			<div class="panel-body">


          <div class="panel panel-default">
            <div class="panel-heading">Edit Product</div>
            <div class="panel-body">
                
      <?php echo form_open_multipart(current_url(),array('class'=>'form-horizontal')); ?> 

      <div class="form-body">

				<div class="form-body">

						<div class="form-group">

							<div class="col-md-12">   

				                <label class="control-label"><strong>Product Name</strong></label>                                    

								<input type="text" placeholder="Product Name" class="form-control" name="name" value="<?php if(!empty($product->name)) echo $product->name; ?>">

								<?php echo form_error('name'); ?>

				            </div> 

			            </div>

			            <div class="form-group">

							<div class="col-md-12">   

				                <label class="control-label"><strong>Short Description</strong></label>                                    

								<textarea placeholder="Short Description" class="form-control" name="short_description"><?php if(!empty($product->short_description)) echo $product->short_description; ?></textarea>

								<?php echo form_error('short_description'); ?>

				            </div> 

			            </div>

			                  <div class="form-group">

							<div class="col-md-12">   

				                <label class="control-label"><strong>Available Item</strong></label>                                    
				                <br>


				                <label class="checkbox-inline">
									  <input class="toggle-one"  <?php if($product->available_item==1) echo 'checked="checked"'; ?>   value="1" name="available_item" type="checkbox">
								</label>
								<?php //echo form_error('short_description'); ?>

				            </div> 

			            </div>


			            <div class="form-group">

							<div class="col-md-12">   

				                <label class="control-label"><strong>Featured Item</strong></label>                                    
				                <br>


				                <label class="checkbox-inline">
									  <input class="toggle-one" <?php if($product->featured_product==1) echo 'checked="checked"'; ?>  value="1"  name="featured_item"  type="checkbox">
								</label>
								<?php  // echo form_error('short_description'); ?>

				            </div> 

			            </div>

			            <div class="form-group">

							<div class="col-md-12">   

				                <label class="control-label"><strong>Description</strong></label>

				                <textarea cols="100" rows="5" name="description" class="form-control"><?php if(!empty($product->description)) echo $product->description; ?></textarea>						

								<?php echo form_error('description'); ?>

				            </div> 

			            </div> 

         						
				        <div class="form-group">

			            	<div class="col-md-12"> 

				                <label class="control-label"><strong>Category</strong></label>                                     

								<select class="form-control" name="category"  style="width:100%">

									<option value="">Select Category</option>

									<?php  if(!empty($category)):  ?>

										<?php foreach ($category as $value){ ?>

											<option value="<?php  echo $value->id;  ?>"   <?php if(!empty($product->category) && $value->id) echo 'selected="selected"'; ?>  ><?php  if(!empty($value->category)) echo $value->category;  ?></option>

										<?php } ?>

									<?php endif; ?>	

								</select>                                 

								<?php echo form_error('category'); ?>

			            	</div>

			            </div>


			            <div class="form-group">

							<div class="col-md-12">   

				                <label class="control-label"><strong>Price</strong></label>                                    

								<input type="text" placeholder="Price" class="form-control" name="price" value="<?php if(!empty($product->price)) echo $product->price; ?>">

								<?php echo form_error('price'); ?>

				            </div> 

			            </div>

            			

            			<div class="form-group">

			              <div class="col-sm-12">

			              <label class="control-label"><strong>Image</strong></label>

			                <input type="file" name="userfile">

			                <?php echo form_error('userfile'); ?>

			                <?php if(!empty($page_set->file_path)){ ?>

			                	<img src="<?php echo base_url(str_replace('./','',$page_set->file_thumb_path)); ?>" alt="">	

			                <?php } ?>

			                <!--<small>(file_type: jpg, png, jpeg, and image size maximum size 280 X 230 px and minimum size 270 X 220 px)</small> -->

			              </div>

			            </div>



			        

			        	<div class="form-group">

			            	<div class="col-md-12"> 

				                <label class="control-label"><strong>Status</strong></label>                                     

								<select class="form-control" name="status" id="remote" style="width:100%">

									<option <?php if($product->status==1) echo 'selected="selected"'; ?>  value="1">Active</option>

			              			<option <?php if($product->status==0) echo 'selected="selected"'; ?>  value="0">Deactive</option>

								</select>                                 

								<?php //echo form_error('status'); ?>

			            	</div>

			            </div> 

					</div> 

					<div class="form-actions">       

						<button type="submit" class="btn blue">Submit</button> 

						<a href="<?php echo base_url('backend/products/'); ?>" ><button class="btn btn-danger" type="button">Cancel</button></a>

					</div>  

        <?php echo form_close(); ?>
            </div>
            </div>
        </div>
</div>
</div>
</div>  <!-- / Users widget-->

