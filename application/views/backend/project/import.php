<div class="row">
	<div class="col-md-12">

	<div class="portlet box blue">    
			<div class="portlet-title"> 
				<div class="caption">            
					<i class="fa fa-sitemap"></i> Upload Product   <a class="btn btn-xs btn-success" href="<?php echo base_url('backend/products/download/') ?>">Sample CSV</a>   
				</div>       
				<div class="tools">   
					<a href="" class="collapse"></a>
				</div>
			</div>

			<div class="portlet-body form">
			<?php if($this->session->flashdata('msg_error_csv')): ?>
			    <div class="alert alert-danger">
					<?php echo $this->session->flashdata('msg_error_csv');  ?>
			    </div>
			<?php endif; ?>
			<?php echo msg_alert_backend(); ?>
	        <?php echo form_open_multipart(base_url('backend/products/import/'),array('class'=>'form-horizontal')); ?>
				<div class="portlet-body">
				<br>
					<div class="row">
	        <div class="col-md-10 col-md-offset-1 ">
	        			
                <div class="form-group">
                   <label for="inputPassword" class="col-sm-3 control-label">To Add Products list<small> Upload CSV File here</small></label>
					<div class="col-sm-8">
					<br>
					<input type="file" name="import_csv">
					<?php echo form_error('import_csv'); ?>   
				   </div> 
                </div>
               <!--  <div class="form-group">
                   <label for="inputPassword" class="col-sm-3 control-label">Set Password<small> Set default password for all the customer</small></label>
					<div class="col-sm-8">
					<br>
					<div class="input-group">
						<input class="form-control" type="password" id="default_password" name="default_password"   placeholder="Password">
						<span  style="text-decoration:none;" href="javascript:;" class="input-group-addon">
							<input id="show_password" type="checkbox" name="show_password" value=""> Show Password
						</span>
					</div>
					<input type="password" class="form-control" name="Password" placeholder="Default Password">
					<?php //echo form_error('password'); ?>   
				   </div> 
                </div> -->
	                
	              
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-6">
						<button type="submit" class="btn btn-primary btn-default">Upload CSV</button>
					</div>
				</div>
				</div>
	            <?php echo form_close(); ?>
			</div>			
		</div>			
		</div>			
		</div>		
	</div>	
</div>	