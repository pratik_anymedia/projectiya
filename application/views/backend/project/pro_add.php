

        <div class="row">
         <div class="col-mod-12">
                <h3 class="page-header"> Projects </h3>          
         </div>
        </div>

          <!-- Users widget -->
<div class="row">
<div class="col-md-12">
<div class="panel">
	<div class="panel-heading text-primary">
		<h3 class="panel-title"><i class="fa fa-rocket"></i>Add Project</h3>
	</div>
<div class="panel-body">
  <div class="panel panel-default">
    <div class="panel-heading">Add Product</div>
      <div class="panel-body">       
      <?php //echo validation_errors(); ?>   
      <?php echo form_open_multipart(current_url(),array('class'=>'form-horizontal')); ?> 
      <div class="form-body">
				<div class="form-body">
					<div class="form-group">
						<div class="col-md-12">   
			                <label class="control-label"><strong>Project Title</strong></label>                                    
							<input type="text" placeholder="Project Title" class="form-control" name="title" value="<?php echo set_value('title') ?>">
							<?php echo form_error('title'); ?>
			            </div> 
		            </div>

		            <div class="form-group">
						<div class="col-md-12">   
			                <label class="control-label"><strong>Short Description</strong></label>                                    
							<textarea placeholder="Short Description" class="form-control" name="short_description"><?php echo set_value('short_description') ?></textarea>
							<?php echo form_error('short_description'); ?>
			            </div> 
		            </div> 

		            <div class="form-group">
						<div class="col-md-12">   
			                <label class="control-label"><strong>Available Project</strong></label>                                    
			                <br>
			                <label class="checkbox-inline">
								  <input class="toggle-one"  value="1" name="available_item" type="checkbox">
							</label>
							<?php //echo form_error('available_item'); ?>
			            </div> 
		            </div>

		            <div class="form-group">
						<div class="col-md-12">   
			                <label class="control-label"><strong>Featured Project</strong></label>                                    
			                <br>
			                <label class="checkbox-inline">
								  <input class="toggle-one" value="1"  name="featured_item"  type="checkbox">
							</label>
							<?php //echo form_error('featured_item'); ?>
			            </div> 
		            </div>

		            <div class="form-group">
						<div class="col-md-12">   
			                <label class="control-label"><strong>Description</strong></label>
			                <textarea cols="100" rows="5" name="description" class="tinymce_edittor form-control"><?php echo set_value('description') ?></textarea>						
							<?php echo form_error('description'); ?>
			            </div> 
		            </div> 

			        <div class="form-group">
		            	<div class="col-md-12"> 
			                <label class="control-label"><strong>Technology</strong></label>                                     
							<select class="form-control" id="technology" name="technology[]"  multiple style="width:100%">
								<option value="">Select Technology</option>
								<?php  if(!empty($technology)):  ?>
									<?php foreach ($technology as $value){ ?>
										<option value="<?php  echo $value->id;  ?>"  ><?php  if(!empty($value->technology)) echo $value->technology;  ?></option>
									<?php } ?>
								<?php endif; ?>	
							</select>                                 
							<?php echo form_error('technology[]'); ?>
		            	</div>
		            </div>

		            <div class="form-group">
		            	<div class="col-md-12"> 
			                <label class="control-label"><strong>Courses</strong></label>                                     
							<select class="form-control" id="course" name="course[]"  multiple style="width:100%">
								<option value="">Select Course</option>
								<?php  if(!empty($courses)):  ?>
									<?php foreach ($courses as $value){ ?>
										<option value="<?php  echo $value->id;  ?>"  ><?php  if(!empty($value->course)) echo $value->course;  ?></option>
									<?php } ?>
								<?php endif; ?>	
							</select>                                 
							<?php echo form_error('course[]'); ?>
		            	</div>
		            </div>

		            <div class="form-group">
						<div class="col-md-4">   
			                <label class="control-label"><strong>Backend Technology</strong></label>                                    
							<input placeholder="Backend Technology" class="form-control" value="<?php echo set_value('backend_technology') ?>" name="backend_technology">
							<?php echo form_error('backend_technology'); ?>
			            </div> 
			            <div class="col-md-4">   
			                <label class="control-label"><strong>Forms / Pages</strong></label>                                    
							<input placeholder="Forms / Pages" class="form-control" value="<?php echo set_value('form_pages') ?>" name="form_pages">
							<?php echo form_error('form_pages'); ?>
			            </div> 
			            <div class="col-md-4">   
			                <label class="control-label"><strong>No. Of Tables</strong></label>                                    
							<input placeholder="No.Of Tables" class="form-control" value="<?php echo set_value('no_of_tables') ?>" name="no_of_tables">
							<?php echo form_error('no_of_tables'); ?>
			            </div> 
		            </div>


		            <div class="form-group">
        				<div class="col-md-12">   
			                <label class="control-label"><strong>Price</strong></label>                                    
							<input type="text" placeholder="Price" class="form-control" name="price" value="<?php echo set_value('price'); ?>">
							<?php echo form_error('price'); ?>
			            </div> 
		            </div>

            		<div class="form-group">
		              <div class="col-sm-12">
		              <label class="control-label"><strong>Project Source Files</strong></label>
		                <input type="file" name="userfile">
	                    <?php echo form_error('userfile'); ?>
		                
		                <small>(file_type: zip,rar,ppt,pptx,docx,doc)</small> 
		              </div>
		            </div>


		            <div class="form-group">
		              <div class="col-sm-12">
		              <label class="control-label"><strong>Featured Image</strong></label>
		                <input type="file" name="featured_image">
	                    <?php echo form_error('featured_image'); ?>
		                <?php //if(!empty($page_set->file_path)){ ?>
		                	<img src="<?php //echo base_url(str_replace('./','',$page_set->file_thumb_path));   ?>" alt="">	
		                <?php //} ?>
		                <small>(file_type: jpeg,jpg,png,gif)</small> 
		              </div>
		            </div> 

		            <div class="form-group">
		              <div class="col-sm-12">
		              <label class="control-label"><strong>Document File</strong></label>
		                <input type="file" name="doc_file">
	                    <?php echo form_error('doc_file'); ?>
		                <?php //if(!empty($page_set->file_path)){ ?>
		                	<img src="<?php //echo base_url(str_replace('./','',$page_set->file_thumb_path));   ?>" alt="">	
		                <?php //} ?>
		                <small>(file_type:pdf,doc,docx,ppt)</small> 
		              </div>
		            </div>

		        	<div class="form-group">
		            	<div class="col-md-12"> 
			                <label class="control-label"><strong>video key(id)</strong></label>                                     
							<a style="text-decoration:none;" class="input-group demo-input-group">
								<span class="input-group-addon">https://www.youtube.com/watch?v=</span>
								<input class="form-control" type="text" name='video_url'  placeholder="video key (id)">
							</a>                                
							<?php //echo form_error('status'); ?>
		            	</div>
		            </div> 

		            <div class="form-group">
		            	<div class="col-md-12"> 
			                <label class="control-label"><strong>Status</strong></label>                                     
							<select class="form-control" name="status" id="remote" style="width:100%">
								<option value="1">Active</option>
		              			<option value="0">Deactive</option>
							</select>                                 
							<?php //echo form_error('status'); ?>
		            	</div>
		            </div> 

				    </div> 

					<div class="form-actions">       
						<button type="submit" class="btn blue">Submit</button> 
						<a href="<?php echo base_url('backend/projects/'); ?>" ><button class="btn btn-danger" type="button">Cancel</button></a>
					</div>  

                <?php echo form_close(); ?>
            </div>
            </div>
        </div>
</div>
</div>
</div>  <!-- / Users widget-->

