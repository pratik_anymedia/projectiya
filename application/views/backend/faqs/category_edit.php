<div class="tab-content">
   <div id="tab_0" class="tab-pane active">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption"><i class="icon-user"></i> <span> Edit Faq Category</span></div>
         </div>
         <div class="portlet-body form">
           <!-- BEGIN FORM-->
            <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo current_url()?>">
            <div class="form-body">
                  

                   <div class="form-group">
                    <label class="col-md-3 control-label">Faq Category</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" name="faq_category" value="<?php if(!empty($category->faq_category)) echo $category->faq_category; else  echo set_value('faq_category');?>"><?php echo form_error('faq_category'); ?>
                    </div>
                </div>

              

               </div>
               <div class="form-actions fluid">
                  <div class="col-md-offset-3 col-md-9">
                     <button class="btn blue" type="submit">Update</button>
                     <a href="<?php echo base_url() . 'backend/faqs/category'; ?>" ><button class="btn btn-danger" type="button">Cancel</button></a> 

                                                   
                  </div>
               </div>
            </form>
            <!-- END FORM--> 
         </div>
      </div>
   </div>
</div>