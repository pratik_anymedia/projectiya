      <div class="row">
        <div class="col-mod-12">
          <h3 class="page-header"> faqs <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
        </div>
      </div>
<!-- Users widget -->
      <div class="row">
          <div class="col-md-12">
            <div class="panel">
              <div class="panel-heading text-primary">
                <h3 class="panel-title"><i class="fa fa-comment"></i> Edit Faqs </h3>
              </div>
              <div class="panel-body">
                <div class="panel panel-default">
                  <div class="panel-heading">Add Post</div>
                  <div class="panel-body">
                   <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal')); ?>   
    <div class="form-body">           
     <div class="form-group">      
	    <label class="col-md-2 control-label">Question</label>    
	    <div class="col-md-10">                 
	    <input type="text" placeholder="Question" class="form-control" name="question" value="<?php if (!empty($faq->question)) echo $faq->question;  else echo set_value('question');?>"><?php echo form_error('question'); ?>     
	               </div>    
 
	                       </div>  
	        <div class="form-group">        
	            <label class="col-md-2 control-label">Answer</label>  
	                <div class="col-md-10">           
	                    <div class="input-group">    
	       <textarea class="tinymce_editor form-control" rows="12" cols="100" name="answer"><?php if (!empty($faq->answer)) echo ($faq->answer); else echo set_value('answer'); ?></textarea>
	       <?php echo form_error('answer'); ?>              
	             </div>              
	         </div>      
	    </div>                        



	<div class="form-group">         
		<label class="col-sm-2 col-sm-2 control-label">Status</label>    
			<div class="col-md-10">             
				<select name="status" class="form-control"> 
					<option value="1"  <?php if (strtolower($faq->status) == 1) echo 'selected="selected"'; ?>>Publish</option>  
					<option value="0"  <?php if (strtolower($faq->status) == 0) echo 'selected="selected"'; ?>>Unpublish</option>
				</select>           
			</div>        
		</div>   
		</div> 
		<div class="form-actions text-center">   
			<button type="submit" class="btn blue">Submit</button>     
			<a href="<?php echo base_url() . 'backend/faqs/'; ?>" ><button class="btn btn-danger" type="button">Cancel</button></a> 
		</div>    
	</form>
                    <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>  <!-- / Users widget-->