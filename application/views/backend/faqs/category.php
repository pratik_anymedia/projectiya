<div class="row">   
	 <div class="col-md-12 ">        <!-- BEGIN SAMPLE FORM PORTLET-->       
	 	 <div class="portlet box blue">            
	 	 	<div class="portlet-title">                
	 	 		<div class="caption">                    
	 	 			<i class="fa fa-file"></i>Faqs Category<a href="<?php echo base_url() ?>backend/faqs/category_add" class="btn btn-xs yellow">Add New Category <i class="icon-plus"></i> </a>
	 	 		</div>                
	 	 		<div class="tools">                    
	 	 			<a href="javascript:;" class="collapse"></a>                
	 	 		</div>            
	 	 	</div>
	 	 	<div class="portlet-body">               
	 	 		 <div class="table-responsive">
	 	 		 	<table class="table table-bordered table-hover">                        
	 	 		 		<thead>                            
	 	 		 			<tr>                                
		 	 		 			<th width="5%" class="jv no_sort">#</th>
		 	 		 			<th width="40%" class="no_sort">Faqs Category</th>
		 	 		 			<th widht="20%" class="no_sort">Status</th>
		 	 		 			<th widht="10%" class="no_sort">Order</th>
		 	 		 			<th width="10%" class="to_hide_phone span2">Created</th>         
		 	 		 			<th width="10%" class="ms no_sort ">Actions</th>       
		 	 		 		</tr>                        
		 	 		 	</thead>                        
		 	 		 	<tbody> 
                        	<?php if (!empty($category)): ?>
		 	 		 	<form role="form" method="post" action="<?php echo current_url(); ?>">
                        		<?php  $i = 1; foreach ($category as $row):  ?> 
		 	 		 		 <input type="hidden" name="category_id[]" value="<?php if(!empty($row->id)) echo $row->id; ?>"/>
		 	 		 		<tr> 
		 	 		 			<td><?php echo $i . "."; ?></td>
		 	 		 			<td class=""><a href="<?php echo base_url() . 'backend/faqs/category_edit/' . $row->id; ?>" class="btn btn-small"  rel="tooltip" data-placement="left" data-original-title=" Edit "><?php if (!empty($row->faq_category)) echo word_limiter($row->faq_category, 7); ?></a></td>
		 	 		 			<td class="to_hide_phone">
                                    <?php  if($row->status=='0'){ ?><a href="<?php echo base_url().'backend/faqs/category_change_status/1/'.$row->id; ?>" class="btn btn-xs yellow">Unpublish</a><?php }elseif($row->status=='1'){ ?><a href="<?php echo base_url().'backend/faqs/category_change_status/0/'.$row->id; ?>" class="btn btn-xs green">Publish</a> <?php } ?>
                                </td>

                                 </td>
                                <td class="to_hide_phone">
                                <div class="form-group">
                                <input type="text" class="form-control" name="order_<?php echo $i; ?>" value="<?php if(!empty($row->order)) echo $row->order; ?>">
                                </div>
                                </td> 
		 	 		 			
		 	 		 			 <td class="to_hide_phone"><?php echo date('d-m-Y', strtotime($row->created)); ?></td>                                       
		 	 		 			 <td class="ms">                                            
		 	 		 			 	<div class="btn-group">  
		 	 		 			 		<a href="<?php echo base_url() . 'backend/faqs/category_edit/' . $row->id; ?>"  class="btn btn-success btn-xs" rel="tooltip" data-placement="left" data-original-title=" Edit "><i class="icon-pencil"></i></a>
		 	 		 			 		<a href="<?php echo base_url() . 'backend/faqs/category_delete/' . $row->id; ?>" class="btn btn-danger btn-xs" rel="tooltip" rel="tooltip" data-placement="bottom" data-original-title="Remove" onclick="return confirm('Are you sure want to delete?');" ><i class="icon-trash "></i></a>      
		 	 		 			 	</div>                                        
		 	 		 			</td>                                    
		 	 		 		</tr>                                
		 	 		 	<?php $i++; endforeach; ?>   
		 	 		 	  <tr><td></td><td></td><td></td><td></td><td></td><td colspan="7">
                              <div class="form-actions fluid">
                              <div class="col-md-9">
                              <button class="btn btn-primary" type="submit">Submit</button> 
                              </div>        
                              </div>
                              </td></tr>
                        </form>                         
		 	 		 <?php else: ?>
		 	 		 	<tr>
		 	 		 	 <th colspan="6"> <center>No Faqs Category found.</center></th>
		 	 		 	</tr>
		 	 		 <?php endif; ?>
		 	 		 </tbody>
		 	 		 </table>
		 	 		
		 	 		</div> 
		 	 	</div> 
		 	</div>        <!-- END SAMPLE FORM PORTLET-->        <!-- END SAMPLE FORM PORTLET-->   
		</div>
	</div>