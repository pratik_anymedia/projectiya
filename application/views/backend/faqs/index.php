        <div class="row">
         <div class="col-mod-12">
              
                <h3 class="page-header"> Faqs <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
            </div>
          </div>

          <!-- Users widget -->
          


        <div class="row">
            <div class="col-md-12">
            <div class="panel">
            <div class="panel-heading text-primary">
              <h3 class="panel-title"><i class="fa fa-rocket"></i> List <a href="<?php echo base_url() ?>backend/faqs/add" class="btn btn-xs btn-success">Add New Faq <i class="icon-plus"></i> </a>  </h3>
        	</div> 

        <div class="panel-body">
         
 <?php  echo msg_alert_backend();  ?>
      <br>
             
                  <div class="table-responsive">
                    	 	 		 	<table class="table table-bordered table-hover">                        
	 	 		 		<thead>                            
	 	 		 			<tr>                                
		 	 		 			<th width="5%" class="jv no_sort">#</th>
		 	 		 			<th width="20%" class="no_sort">Question</th>
		 	 		 			<th width="20%" class="no_sort">Answer</th>
		 	 		 			<!-- <th width="25%" class="no_sort">Category</th> -->
		 	 		 			<th width="10%" class="to_hide_phone ue no_sort">Status</th>
		 	 		 			<th width="10%" class="to_hide_phone span2">Created</th>         
		 	 		 			<th width="10%" class="ms no_sort ">Actions</th>       
		 	 		 		</tr>                        
		 	 		 	</thead>                        
		 	 		 	<tbody>  
		 	 		 		<?php if (!empty($faqs)): $i = $offset; 
		 	 		 		foreach ($faqs as $row): $i++; ?> 
		 	 		 		<tr> 
		 	 		 			<td><?php echo $i . "."; ?></td>
		 	 		 			<td class=""><a href="<?php echo base_url() . 'backend/faqs/edit/' . $row->id . '/' . $offset ?>" class="btn btn-small"  rel="tooltip" data-placement="left" data-original-title=" Edit "><?php if (!empty($row->question)) echo word_limiter($row->question, 7); ?></a></td>
		 	 		 			<td class=""><?php if (!empty($row->answer)) echo word_limiter($row->answer, 7); ?></a></td>
								<!--	<td class="to_hide_phone"><?php //$faq = get_data('faq_category',array('id'=>$row->category)); echo $faq->faq_category; ?> -->		 	 		 				
		 	 		 			 </td>
		 	 		 			<td class="to_hide_phone">
		 	 		 				<?php if ($row->status) { ?>
		 	 		 					<a href="<?php echo base_url() . 'backend/faqs/changestatus/' . $row->id .'/'.$row->status.'/'. $offset ?>"><span class="label label-success label-mini" >Publish </span></a>
		 	 		 				 <?php } else { ?> <a href="<?php echo base_url() . 'backend/faqs/changestatus/' . $row->id . '/'.$row->status.'/'. $offset ?>"><span class="label label-warning label-mini"> Unpublish </span></a>
		 	 		 				 <?php } ?> 
		 	 		 			 </td>                                        

		 	 		 			 <td class="to_hide_phone"><?php echo date('d-m-Y', strtotime($row->created)); ?></td>                                       
		 	 		 			 <td class="ms">                                            
		 	 		 			 	<div class="btn-group">  
		 	 		 			 		<a href="<?php echo base_url() . 'backend/faqs/edit/' . $row->id . '/' . $offset ?>"  class="btn btn-success btn-xs" rel="tooltip" data-placement="left" data-original-title=" Edit "><i class="fa fa-pencil"></i></a>
		 	 		 			 		<a href="<?php echo base_url() . 'backend/faqs/delete/' . $row->id . '/' . $offset ?>" class="btn btn-danger btn-xs" rel="tooltip" rel="tooltip" data-placement="bottom" data-original-title="Remove" onclick="return confirm('Are you sure want to delete?');" ><i class="fa fa-trash-o "></i></a>      
		 	 		 			 	</div>                                        
		 	 		 			</td>                                    
		 	 		 		</tr>                                
		 	 		 	<?php endforeach; ?>                            
		 	 		 <?php else: ?>
		 	 		 	<tr>
		 	 		 	 <th colspan="6"> <center>No FAQs found.</center></th>
		 	 		 	</tr>
		 	 		 <?php endif; ?>
		 	 		 </tbody>
		 	 		 </table>
		 	 		 
                    <div class="row-fluid  control-group mt15">             
                        <div class="span12">
                          <?php if(!empty($pagination))  echo $pagination;?>              
                        </div>
                      </div>
                  </div>
</div>
</div>
</div>
</div>  <!-- / Users widget-->

