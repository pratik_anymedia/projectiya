      <div class="row">
        <div class="col-mod-12">
          <h3 class="page-header"> faqs <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
        </div>
      </div>
<!-- Users widget -->
      <div class="row">
          <div class="col-md-12">
            <div class="panel">
              <div class="panel-heading text-primary">
                <h3 class="panel-title"><i class="fa fa-comment"></i> Add faqs </h3>
              </div>
              <div class="panel-body">
                <div class="panel panel-default">
                  <div class="panel-heading">Add Post</div>
                  <div class="panel-body">
                  <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal')); ?>   
           <div class="form-body">  
		<!-- <div class="form-group">                
			<label class="col-md-2 control-label">Category</label>                
			<div class="col-md-5"> 
				<select name="category" class="form-control">
					<?php //if(!empty($category)): ?>
						<?php //foreach ($category as $row): ?>
				   			 <option value="<?php // if(!empty($row->id)) echo $row->id; ?>"<?php //if(!empty($_POST['category'])&& $_POST['category']==$row->id) echo 'selected="selected"'; ?>><?php  //if(!empty($row->faq_category)) echo $row->faq_category;  ?></option>
						<?php //endforeach; ?>
				<?php //endif; ?>
				</select><?php // echo form_error('category'); ?>                   
				
			</div>            
		</div> -->          
		<div class="form-group">                
			<label class="col-md-2 control-label"> Question</label>                
			<div class="col-md-10">                    
				<input type="text" placeholder="Question" class="form-control" name="question" value="<?php echo set_value('question'); ?>"><?php echo form_error('question'); ?>
			</div>            
		</div>          
		<div class="form-group">      
	        <label class="col-md-2 control-label">Answer</label> 
	            <div class="col-md-10">            
	                <div class="input-group">       
	         	        <textarea class="tinymce_editor form-control" cols="100" rows="12" name="answer"><?php echo set_value('answer'); ?></textarea>
	                    <?php echo form_error('answer'); ?>      
	                </div>     
	            </div>    
	    </div>          
		
		<div class="form-group">                
			<label class="col-sm-2 col-sm-2 control-label">Status</label>                
			<div class="col-md-10">                    
				<select name="status" class="form-control"> 
				    <option value="1">Publish</option> 
				     <option value="0">Unpublish</option>
				</select>                
			</div>            
		</div>        
	</div>        
	    <div class="form-actions text-center">      
	        <button type="submit" class="btn blue">Submit</button>     
	        <a href="<?php echo base_url() . 'backend/faqs/'; ?>" ><button class="btn btn-danger" type="button">Cancel</button></a>   
	    </div>   
	        </form>
                    <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>  <!-- / Users widget-->