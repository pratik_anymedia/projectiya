

                <div class="row">
                 <div class="col-mod-12">
                <!--  <ul class="breadcrumb">
                   <li><a href="index.html">Dashboard</a></li>
                   <li><a href="#">Tables</a></li>
                   <li class="active">Basic Tables</li>
                 </ul>
                  -->
                <!--  <div class="form-group hiddn-minibar pull-right">
                  <input type="text" class="form-control form-cascade-control nav-input-search" size="20" placeholder="Search through site" />

                  <span class="input-icon fui-search"></span>
                </div> -->

                <h3 class="page-header"> Courses <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
<!-- 
                <blockquote class="page-information hidden">
                 <p>
                  More styles of basic tables are available to represent static information.
                </p>
              </blockquote> -->
            </div>
          </div>

          <!-- Users widget -->
          <div class="row">
           <div class="col-md-12">
            <div class="panel">
             <div class="panel-heading text-primary">
              <h3 class="panel-title"><i class="fa fa-rocket"></i> Add Course
              
          </h3>
        </div>
        <div class="panel-body">
          

          <div class="panel panel-default">
            <div class="panel-heading">Add Course</div>
            <div class="panel-body">
                
      <?php echo form_open_multipart(current_url(),array('class'=>'form-horizontal')); ?> 

      <div class="form-body">

            <div class="form-group">

              <div class="col-md-12">   

                        <label class="control-label"><strong>Course Name</strong></label>                                    

                <input type="text" placeholder="Course Name" class="form-control" name="course" value="<?php echo set_value('course');?>">

                <?php echo form_error('course'); ?>

                    </div> 

                  </div>

          



                  <div class="form-group">

              <div class="col-md-12"> 

                        <label class="control-label"><strong>Course Description</strong></label>                                     

                <textarea class="form-control" name="description" ><?php echo set_value('description');?></textarea>

                <?php echo form_error('description'); ?>

                    </div> 

                  </div> 



         

                  <div class="form-group">

                    <div class="col-md-12"> 

                        <label class="control-label"><strong>Status</strong></label>                                     

                <select class="form-control" name="status" id="remote" style="width:100%">

                  <option value="1">Active</option>

                          <option value="0">Deactive</option>

                </select>                                 

                <?php echo form_error('status'); ?>

                    </div>

                  </div>

          </div> 

          <div class="form-actions">       

            <button type="submit" class="btn blue">Submit</button> 

            <a href="<?php echo base_url('backend/categories/'); ?>" ><button class="btn btn-danger" type="button">Cancel</button></a>

          </div>  

        <?php echo form_close(); ?>
            </div>
            </div>
        </div>
</div>
</div>
</div>  <!-- / Users widget-->

