      <div class="row">
        <div class="col-mod-12">
          <h3 class="page-header">Message Reply</h3>
        </div>
      </div>
<!-- Users widget -->
      <div class="row">
          <div class="col-md-12">
            <div class="panel">
              <div class="panel-heading text-primary">
                <h3 class="panel-title"><i class="fa fa-eye"></i>View Reply</h3>
              </div>
              <div class="panel-body">
                <div class="panel panel-default">
                  <div class="panel-heading">View Reply</div>
                  <div class="panel-body">
                   <div class='form-horizontal'>
                    <?php echo $this->session->flashdata('msg_error');?>
                 <div class="form-body">
                    <div class="form-group">
                       <label class="col-md-3 control-label">Frist Name</label>
                       <div class="col-md-4">
                          <div><?php if(!empty($message->firstname)) echo $message->firstname;?></div>
                       </div>
                    </div>  

                    <div class="form-group">
                       <label class="col-md-3 control-label">Last Name</label>
                       <div class="col-md-4">
                          <div><?php if(!empty($message->lastname)) echo $message->lastname; ?></div>
                       </div>
                    </div>

                    
                    <div class="form-group">
                       <label class="col-md-3 control-label">Email Address</label>
                       <div class="col-md-4">
                          <div><?php if(!empty($message->email)) echo $message->email; ?></div>
                       </div>
                    </div>
                  
                   
                    <div class="form-group">
                       <label class="col-md-3 control-label">Question</label>
                       <div class="col-md-4">
                          <div><?php if(!empty($message->message)) echo $message->message; ?></div>                          
                       </div>
                    </div> 

                    <div class="form-group">
                       <label class="col-md-3 control-label">Message</label>
                       <div class="col-md-4">
                          <div><?php if(!empty($message->reply)) echo $message->reply; ?></div>             
                       </div>
                    </div> 
              
                 </div>
                 <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                       <a class="btn btn-primary" href="<?php echo base_url()?>backend/messages/">Back To Message</a>                                                    
                    </div>
                 </div>
                 </div>
              <!-- END FORM--> 
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>  <!-- / Users widget-->

