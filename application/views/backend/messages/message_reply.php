      <div class="row">
        <div class="col-mod-12">
          <h3 class="page-header">Message Reply</h3>
        </div>
      </div>
<!-- Users widget -->
      <div class="row">
          <div class="col-md-12">
            <div class="panel">
              <div class="panel-heading text-primary">
                <h3 class="panel-title"><i class="fa fa-comment"></i> Send Reply</h3>
              </div>
              <div class="panel-body">
                <div class="panel panel-default">
                  <div class="panel-heading">Send Reply</div>
                  <div class="panel-body">
                  <?php echo form_open(current_url(),array('class' =>'form-horizontal','method'=>'post','role'=>'form')); ?>
                  <?php echo $this->session->flashdata('msg_error');?>
                  <div class="form-body">
                    <div class="form-group">
                       <label class="col-md-3 control-label">Frist_name</label>
                       <div class="col-md-9">
                          <input type="text" placeholder="Name" class="form-control" name="first_name" value="<?php if(!empty($message->firstname)) echo $message->firstname; ?>"><?php echo form_error('first_name'); ?>
                       </div>
                    </div>  

                    <div class="form-group">
                       <label class="col-md-3 control-label">Last Name</label>
                       <div class="col-md-9">
                          <input type="text" placeholder="Name" class="form-control" name="last_name" value="<?php if(!empty($message->lastname)) echo $message->lastname; ?>"><?php echo form_error('last_name'); ?>
                       </div>
                    </div>

                    <div class="form-group">
                       <label class="col-md-3 control-label">Email Address</label>
                       <div class="col-md-9">
                          <div class="input-group"> 
                            <span class="input-group-addon"><i class="icon-envelope"></i></span>
                            <input type="email" placeholder="Email Address" class="form-control" name="email" value="<?php if(!empty($message->email)) echo $message->email; ?>"/>
                          </div>
                          <?php echo form_error('email'); ?>
                       </div>
                    </div>
                  
                    <div class="form-group">
                       <label class="col-md-3 control-label">Message</label>
                       <div class="col-md-9">
                          <input type="text" placeholder="State" class="form-control" name="subject" value="<?php if(!empty($message->message)) echo $message->message; ?>">
                          <?php echo form_error('subject'); ?>
                       </div>
                    </div> 

                    <div class="form-group">
                       <label class="col-md-3 control-label">Reply</label>
                       <div class="col-md-9">
                          <textarea placeholder="State" cols="100" rows="5" class="form-control" name="message"><?php echo set_value('message');?></textarea>
                          <?php echo form_error('message'); ?>
                       </div>
                    </div> 
              
                 </div>
                 <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                       <button class="btn blue" type="submit">Send</button>                                                    
                    </div>
                 </div>
              </form>
              <!-- END FORM--> 
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>  <!-- / Users widget-->

