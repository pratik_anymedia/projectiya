        <div class="row">
          <div class="col-mod-12">
            <h3 class="page-header">Contact Us Messages</h3>
          </div>
        </div>

          <!-- Users widget -->
    <div class="row">
     <div class="col-md-12">
      <div class="panel">
       <div class="panel-heading text-primary">
        <h3 class="panel-title"><i class="fa fa-rocket"></i> List  </h3>
        </div> 
        <?php
          if(!empty($_SERVER['QUERY_STRING']))
              $QUERY_STRING = "0?".$_SERVER['QUERY_STRING'];
          else
              $QUERY_STRING ='';
        ?>
        <div class="panel-body">
         
            <?php  echo msg_alert_backend();  ?>
            <br>
             
                  <div class="table-responsive">
                    <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
              <thead>
                <tr>
                  <th width="5%">#</th>                  
                  <th width="20%" class="no_sort">Name</th>
                  <th width="20%" class="no_sort">Email</th>
                  <th width="15%" class="no_sort">Message</th>                 
                  <th width="15%" class="no_sort">Status</th>                                                                
                  <th width="10%" class="to_hide_phone span2">Created On</th>
                  <th width="10%" class="ms no_sort">Actions</th>                 
                </tr>
              </thead>
              <tbody>
                <?php 
                if(!empty($messages)):
                $i=$offset; foreach($messages as $row): $i++;?>
                <tr>
                  <td><?php echo $i."." ;?></td>
                 
                  <td><?php  echo $row->firstname.' '.$row->lastname; ?></td>
                  <td><a style="text-decoration:none;" href="mailto:<?php echo $row->email; ?>"><?php  echo $row->email; ?></a></td>
                  <td><?php  echo $row->message; ?></td>
                 
                  <td class="to_hide_phone">
                    <?php if($row->status=='0'){ ?>
                        <button class="btn btn-xs btn-danger">Pending</button>
                        <a class="btn btn-xs btn-warning" href="<?php echo base_url().'backend/messages/message_reply/'.$row->id?>" > 
                          Send Reply
                        </a> 
                    <?php } if($row->status=='1'){?>
                        <button class="btn btn-xs btn-success">Answered</button>                    
                    <?php }?>
                  </td>
                  <td ><?php echo date('Y-m-d',strtotime($row->created)); ?></td>
                  <td class="ms">
                    <div class="btn-group"> 
                    <?php if($row->status=='1'){ ?>                                     
                      <a href="<?php echo base_url().'backend/messages/view_reply/'.$row->id?>" class="btn btn-xs btn-info"  rel="tooltip" data-placement="left" data-original-title=" Edit ">
                        <i class="fa fa-eye"></i>  Reply
                      </a> 
                    <?php } ?>
                      <a href="<?php echo base_url().'backend/messages/message_delete/'.$row->id?>" class="btn btn-xs btn-danger" rel="tooltip" data-placement="bottom" data-original-title="Remove" onclick="if(confirm('Are you sure you want to delete?')){return true;} else {return false;}">                  
                      <i class="fa fa-trash-o"></i></a> 
                    </div>
                  </td>
                </tr> 
                <?php endforeach; ?>
              <?php else: ?>
                <tr>
                  <th colspan="10"> <center>No Messages Found.</center></th>
                 
                </tr>

              <?php endif; ?>
              </tbody>
            </table>
                    <div class="row-fluid  control-group mt15">             
                        <div class="span12">
                          <?php if(!empty($pagination))  echo $pagination;?>              
                        </div>
                      </div>
                  </div>
</div>
</div>
</div>
</div>  <!-- / Users widget-->

