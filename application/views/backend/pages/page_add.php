 <div class="row">
        <div class="col-mod-12">
          <h3 class="page-header"> <?php echo $this->lang->line("PAGES"); ?>  </h3>
        </div>
      </div>
<!-- Users widget -->
      <div class="row">
          <div class="col-md-12">
            <div class="panel">
              <div class="panel-heading text-primary">
                <h3 class="panel-title"><i class="fa fa-comment"></i> <?php echo $this->lang->line("ADD_PAGE"); ?> </h3>
              </div>
              <div class="panel-body">
                <div class="panel panel-default">
                  <div class="panel-heading"><?php echo $this->lang->line("ADD_PAGE"); ?></div>
                  <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo current_url()?>">
                <?php echo $this->session->flashdata('msg_error');?>
                   <div class="form-body">
                        <div class="form-group">
                           <label class="col-md-2 control-label"> <?php echo $this->lang->line("TITLE"); ?></label>
                           <div class="col-md-9">
                              <input type="text" placeholder="Title" class="form-control " name="post_title" value=""><?php echo form_error('post_title'); ?>
                           </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-2 control-label"> <?php echo $this->lang->line("CONTENT"); ?></label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <textarea class="tinymce_edittor form-control" cols="100" rows="12" name="post_content"></textarea><?php echo form_error('post_content'); ?>
                                </div>
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo $this->lang->line("STATUS"); ?></label>
                            <div class="col-md-10">
                                <select name="post_status" data-placeholder="Select Status..." class="form-control select2me select2-offscreen" id="default-select">
                                    <option value="publish"><?php echo $this->lang->line("PUBLISH"); ?></option>
                                    <option value="unpublish"><?php echo $this->lang->line("UNPUBLISH"); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo $this->lang->line("RESOURCEPAGE"); ?></label>
                            <div class="col-md-2">
                               <label class="radio inline"> Resource <input type="radio"  name="pagetype" value="resource"></label>
                               <label class="radio inline">Page <input type="radio" class="checkbox " name="pagetype" value="page"></label>
                            </div>
                        </div>
                       
                    </div>
                     <div class="form-actions fluid">
                        <div class="col-md-offset-2 col-md-9">
                         <button class="btn btn-info blue" type="submit"><?php echo $this->lang->line("ADD"); ?></button>
                       <!--   <a href="<?php //echo base_url()?>superadmin/pages">
                         <button class="btn default" type="button">Cancel</button> </a>        -->                      
                      </div>
                   </div>
                </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>  <!-- / Users widget-->