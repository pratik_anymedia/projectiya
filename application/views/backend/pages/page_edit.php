      <div class="row">
        <div class="col-mod-12">
          <h3 class="page-header"> Pages  </h3>
        </div>
      </div>
<!-- Users widget -->
      <div class="row">
          <div class="col-md-12">
            <div class="panel">
              <div class="panel-heading text-primary">
                <h3 class="panel-title"><i class="fa fa-comment"></i> Edit Page </h3>
              </div>
              <div class="panel-body">
                <div class="panel panel-default">
                  <div class="panel-heading">Edit Page</div>
                  <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo current_url()?>">
                <?php echo $this->session->flashdata('msg_error');?>
                   <div class="form-body">
                      <div class="form-group">
                         <label class="col-md-2 control-label"> Title</label>
                         <div class="col-md-9">
                            <input type="text" placeholder="Title" class="form-control " name="post_title" value="<?php if(!empty($page->post_title)) echo $page->post_title; else echo set_value('post_title');?>"><?php echo form_error('post_title'); ?>
                         </div>
                      </div>
                    <div class="form-group">
                         <label class="col-md-2 control-label"> Content</label>
                         <div class="col-md-9">
                            <div class="input-group">
                                <textarea class="tinymce_edittor form-control" cols="100" rows="12" name="post_content"><?php if(!empty($page->post_content)) echo ($page->post_content); else echo set_value('post_content');?></textarea><?php echo form_error('post_content'); ?>
                               </div>
                         </div>
                      </div>
                    
                    <div class="form-group">
                        <label class="col-md-2 control-label">status</label>
                        <div class="col-md-10">
                            <select name="post_status" data-placeholder="Select Status..." class="form-control select2me select2-offscreen" id="default-select">
                                 <option value="publish" <?php if(strtolower($page->post_status)=='publish') echo 'selected="selected"'; ?>>Publish</option>
                                 <option value="unpublish" <?php if(strtolower($page->post_status)=='unpublish') echo 'selected="selected"'; ?>>Unpublish</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo $this->lang->line("RESOURCEPAGE"); ?></label>
                            <div class="col-md-2">
                               <label class="radio inline"> Resource <input type="radio" <?php if(strtolower($page->page_type)=='resource') echo 'checked="checked"'; ?> name="pagetype" value="resource"></label>
                               <label class="radio inline">Page <input type="radio" <?php if(strtolower($page->page_type)=='page') echo 'checked="checked"'; ?> class="checkbox " name="pagetype" value="page"></label>
                            </div>
                        </div>   
                       
                    </div>
                     <div class="form-actions fluid">
                      <div class="col-md-offset-2 col-md-9">
                         <button class="btn btn-info blue" type="submit">Update</button>
                       <!--   <a href="<?php //echo base_url()?>superadmin/pages">
                         <button class="btn default" type="button">Cancel</button> </a>        -->                      
                      </div>
                   </div>
                </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>  <!-- / Users widget-->

