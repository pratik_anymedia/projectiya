<div class="row">
  <div class="col-mod-12">
    <h3 class="page-header">  Pages <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
  </div>
</div>

<!-- Users widget -->
<div class="row">
<div class="col-md-12">
<div class="panel">
<div class="panel-heading text-primary">
  <h3 class="panel-title"><i class="fa fa-rocket"></i> List  <a href="<?php echo base_url('backend/pages/add') ?>" class="btn btn-info">Add New Page<i class="icon-plus"></i> </a> </h3>
 
</div> 

<?php
    if(!empty($_SERVER['QUERY_STRING']))
        $QUERY_STRING = "0?".$_SERVER['QUERY_STRING'];
    else
        $QUERY_STRING ='';
?>
        <div class="panel-body">
         
 <?php  echo msg_alert_backend();  ?>
      <br>
             
        <div class="table-responsive">
            <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
                <thead>
                  <tr>
                    <th width="5%" class="jv no_sort">#</th>
                    <th width="60%" class="no_sort">Page Name</th>                  
                    <th width="10%" class="to_hide_phone ue no_sort">Status</th>
                    <th width="10%" class="to_hide_phone span2">Created On</th>
                    <th width="10%" class="ms no_sort ">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  if(!empty($pages)):
                  $i=$offset; foreach($pages as $row): 
                   $i++;?>
                    <tr>
                        <td><?php echo $i.".";?></td>
                        <td class=""><a href="<?php echo base_url().'backend/pages/page_edit/'.$row->id.'/'.$offset?>" class="btn btn-small"  rel="tooltip" data-placement="left" data-original-title=" Edit "><?php if(!empty($row->post_title)) echo ucfirst(word_limiter($row->post_title,7)); ?></a></td>                 
                        <td class="to_hide_phone"><?php if(strtolower($row->post_status)=='publish')  echo  ucfirst($row->post_status); else echo ucfirst($row->post_status) ; ?></td>
                        <td class="to_hide_phone"><?php echo date('Y-m-d',strtotime($row->post_created)); ?></td>
                        <td class="ms">
                          <div class="btn-group"> 
                            <a href="<?php echo base_url().'backend/pages/page_edit/'.$row->id.'/'.$offset?>"class="btn btn-primary btn-xs default"rel="tooltip" data-placement="left" data-original-title=" Edit ">
                             Edit <i class="icon-edit"></i> 
                            </a> 
                             <a href="<?php echo base_url().'backend/pages/page_delete/'.$row->id.'/'.$offset?>"  class="btn btn-danger btn-xs black" rel="tooltip" data-placement="bottom" data-original-title="Remove" onclick="if(confirm('Are you sure want to delete?')){return true;} else {return false;}" >  Delete                      
                              <i class="icon-trash "></i></a>  
                              
                          </div>
                        </td>
                  </tr> 
                  <?php  endforeach; ?>
                    <?php else: ?>
                  <tr>
                        <th colspan="6"> <center>No Pages Found.</center></th>

                  </tr>

                 <?php endif; ?>
                </tbody>
            </table>
            <div class="row-fluid  control-group mt15">               
                <div class="span12">
                  <?php if(!empty($pagination))  echo $pagination;?>              
                </div>
            </div>
</div>
</div>
</div>
</div>
</div>  <!-- / Users widget-->
