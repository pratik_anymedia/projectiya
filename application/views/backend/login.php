<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>ProjectWala | Aadmin login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Loading Bootstrap -->
  <link href="<?php echo BACKEND_THEME_URL ?>css/bootstrap.css" rel="stylesheet">

  <!-- Loading Stylesheets -->    
  <link href="<?php echo BACKEND_THEME_URL ?>css/style.css" rel="stylesheet">
  <link href="<?php echo BACKEND_THEME_URL ?>css/login.css" rel="stylesheet">
  
  <!-- Loading Custom Stylesheets -->    
  <link href="<?php echo BACKEND_THEME_URL ?>css/custom.css" rel="stylesheet">

  <link rel="shortcut icon" href="<?php echo BACKEND_THEME_URL ?>images/favicon.ico">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
      <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <![endif]-->
     <style type="text/css">
    /*  .form-group {
          margin-bottom: 6px;
        }*/
        .error{
          color:red;
        }

     </style>
    </head>
    <body >
     <!--  <div class="list-group side-menu "> -->
     <!--    <a class="list-group-item" href="#lock-screen">Lock Screen</a> -->
       <!--  <a class="list-group-item" href="#login">Login</a> -->
     <!--    <a class="list-group-item" href="#register">Register</a>
        <a class="list-group-item" href="#forgot-password">Forgot Password?</a> -->
     <!--  </div> -->


<!--       <section id="lock-screen">
        <div class="row ">
         <div class="login-holder col-md-6 col-md-offset-3 text-center">
           <h2 class="page-header text-center text-primary"> Welcome to Cascade </h2>
           <form role="form" action="index.html" method="post">
            <img src="<?php //echo BACKEND_THEME_URL ?>images/profiles/eleven.png" alt="" class="user-avatar" />
            <h5>Logging in as <strong class="text-success">vijay kumar</strong></h5>
            <div class="form-group">
              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <div class="form-footer text-info">
              
             Not You? , <a class="" href="#login">Click here to Login as different User</a>
             
             <button type="submit" class="btn btn-info pull-right btn-submit">Login</button>
           </div>

         </form>
       </div>
     </div>
   </section> -->
   <section id="login">
    <div class="row animated fadeILeftBig">
     <div class="login-holder col-md-6 col-md-offset-3">
      <h2 class="page-header text-center text-primary"> Welcome to ProjectWala </h2>
      <?php  echo msg_alert_backend();  ?>
      <?php echo form_open(current_url()); ?>
        <div class="form-group">
          <input type="email" class="form-control" name="email" id="exampleInputEmail1" value="<?php echo  set_value('email');    ?>"  placeholder="Enter email">
        </div>
        <?php echo form_error('email'); ?>
        <div class="form-group">
          <input type="password" class="form-control" name="password"   id="exampleInputPassword1" placeholder="Password">
        </div>
        <?php echo form_error('password'); ?>
        <div class="form-footer">
          <!-- <label>
            <input type="checkbox" class="hidden" id="input-checkbox" value="0" > <i class="fa fa-check-square-o input-checkbox fa-square-o"></i> Remember me?
          </label>
          <label>
            <input type="checkbox" class="hidden" id="input-checkbox" value="0" >  <i class="fa fa-check-square-o input-checkbox fa-square-o"></i> Forgot Password?
          </label> -->
          <button type="submit" class="btn btn-info pull-right btn-submit">Login</button>
        </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</section>
<!-- <section id="register">
  <div class="row animated fadeILeftBig">
   <div class="login-holder col-md-6 col-md-offset-3">
     <h2 class="page-header text-center text-primary"> Welcome to Cascade </h2>
     <form role="form" action="index.html" method="post">
      <div class="form-group">
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Full Name">
      </div>
      <div class="form-group">
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
      </div>
      <div class="form-group">
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="City">
      </div>
      <div class="form-group">
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
      </div>
      <div class="form-footer">
        <label>
          <input type="checkbox" class="hidden" id="input-checkbox" value="0" >  <i class="fa fa-check-square-o input-checkbox fa-square-o"></i> I agree to the Terms &amp; Conditions
        </label>
        <button type="submit" class="btn btn-info pull-right btn-submit">Register</button>
      </div>
    </form>
  </div>
</div>
</section>
<section id="forgot-password">
  <div class="row animated fadeILeftBig">
   <div class="login-holder col-md-6 col-md-offset-3">
     <h2 class="page-header text-center text-primary"> Welcome to Cascade </h2>
     <form role="form" action="index.html" method="post">
      <div class="form-group">
        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Username / Email">
      </div>
      <div class="form-footer">
        
        <button type="submit" class="btn btn-info pull-right btn-submit">Send Instructions</button>
      </div>
    </form>
  </div>
</div>
</section> -->


<!-- Load JS here for Faster site load =============================-->
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery-ui-1.10.3.custom.min.js"></script>





</body>
</html>