<div class="row">
    <div class="col-mod-12">
        <h3 class="page-header"> Slider Images <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
    </div>
</div>
<!-- Users widget -->
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading text-primary">
                <h3 class="panel-title"><i class="fa fa-image"></i> Edit Slider Image</h3>
            </div>
            <div class="panel-body">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit</div>
                    <div class="panel-body">
                         <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal')); ?> 

                            <div class="form-body">     
                                <div class="form-group">      
                                    <div class="col-md-12">     
                                        <label class="control-label">Slider Image</label>   
                                        <input type="file" name="slider_image">
                                        <?php echo form_error('slider_image'); ?>
                                        <?php if(!empty($slider_image->path)){ ?>
                                       <img src='<?php echo base_url().$slider_image->path;?>' width="200" height="200" >
                                        <?php } ?>
                                        <small>(file_type: jpeg,jpg,png,gif)</small> 
                                    </div>    
                                </div>        
                                <div class="form-group">      
                                    <div class="col-md-12">           
                                        <label class="control-label">Status</label>

                                        <select class="form-control" name="status" id="remote" style="width:100%">

                                            <option value="1" <?php echo (isset($slider_image->status) && $slider_image->status == 1) ? "selected" : ''; ?>>Active</option>
                                            <option value="0" <?php echo (isset($slider_image->status) && $slider_image->status == 0) ? "selected" : ''; ?>>Deactive</option>
                                        </select>     

                                        <?php echo form_error('status'); ?>  
                                    </div>        

                                </div>    

                                <div class="form-group">     

                                    <div class="col-md-12">     

                                        <label class="control-label">Description</label>   

                                        <textarea name="slider_image_desc"  class="tinymce_edittor form-control" cols="100" rows="12" name="body"><?php if (!empty($slider_image->description)) echo $slider_image->description; ?></textarea><?php echo form_error('body'); ?>  

                                    </div>



                                </div>   



                            </div>  

                            <div class="form-actions">                

                                <button type="submit" class="btn blue">Submit</button>   

                                <a href="<?php echo base_url('backend/slider_images'); ?>" >

                                    <button class="btn btn-danger" type="button">Cancel</button>

                                </a>                </div>     

                         <?php echo form_close(); ?>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  <!-- / Users widget-->



