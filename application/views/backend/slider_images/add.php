

<div class="row">
    <div class="col-mod-12">
        <h3 class="page-header"> Slider Image </h3>          
    </div>
</div>

<!-- Users widget -->
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading text-primary">
                <h3 class="panel-title"><i class="fa fa-image"></i>  Add slider Image</h3>
            </div>
            <div class="panel-body">
                
                    
                        
                        <?php //echo validation_errors(); ?>   
                        <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal')); ?> 
                        <div class="form-body">
                            <div class="form-body">

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label"><strong>Slider Image</strong></label>
                                        <input type="file" name="slider_image">
                                        <?php echo form_error('slider_image'); ?>
                                        <?php //if(!empty($page_set->file_path)){ ?>
                                        <img src="<?php //echo base_url(str_replace('./','',$page_set->file_thumb_path));    ?>" alt="">	
                                        <?php //} ?>
                                        <small>(file_type: jpeg,jpg,png,gif)</small> 
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <div class="col-md-12"> 
                                        <label class="control-label"><strong>Status</strong></label>                                     
                                        <select class="form-control" name="status" id="remote" style="width:100%">
                                            <option value="1">Active</option>
                                            <option value="0">Deactive</option>
                                        </select>                                 
                                        <?php //echo form_error('status'); ?>
                                    </div>
                                </div> 
                                 <div class="form-group">
                                    <div class="col-md-12"> 
                                        <label class="control-label"><strong>Description</strong></label>                                               <textarea class="tinymce_edittor form-control" name="slider_image_desc"></textarea>
                                                                        
                                        <?php //echo form_error('status'); ?>
                                    </div>
                                </div> 
                                

                            </div> 

                            <div class="form-actions">       
                                <button type="submit" class="btn blue">Submit</button> 
                                <a href="<?php echo base_url('backend/projects/'); ?>" ><button class="btn btn-danger" type="button">Cancel</button></a>
                            </div>  

                            <?php echo form_close(); ?>
                        </div>
                    
              
            </div>
        </div>
    </div>  <!-- / Users widget-->

