        <div class="row">
         <div class="col-mod-12">
                <h3 class="page-header">  Slider Images  </h3>
            </div>
          </div>

          <!-- Users widget -->
          <div class="row">
           <div class="col-md-12">
            <div class="panel">
             <div class="panel-heading text-primary">
              <h3 class="panel-title"><i class="fa fa-rocket"></i> List <a href="<?php echo base_url('backend/slider_images/add') ?>" class="btn btn-success">Add New </a> 
               
          </h3>
        </div> 
        <div class="panel-body">
         
 <?php  echo msg_alert_backend();  ?>
      <br>
             
                  <div class="table-responsive">
                    <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
                      <thead>
                      <tr>

                <th width="5%">#</th>

                <th width="45%">Slider Image</th>

                <th width="40%">Description</th>

                <th width="10%">Actions</th>

              </tr>

                      </thead>
                      <tbody>

              <?php

              if (!empty($slider_images)):

                $i = 0; foreach ($slider_images as $row) { $i++;

                  ?>

                  <tr>

                    <td><?php echo $row->id . "."; ?></td>

                    

                    <td ><img src='<?php echo base_url().$row->path;?>' width="200" height="200" ></td>

                    <td><?php echo $row->description; ?></td>

                    <td>

                    <a href="<?php echo base_url('backend/slider_images/edit/' . $row->id ) ?>" class="btn btn-success btn-xs" rel="tooltip" data-placement="left" data-original-title=" Edit ">

                        <i class="fa fa-pencil"></i>

                      </a>

                      <a href="<?php echo base_url('backend/slider_images/delete/' . $row->id) ?>" class="btn btn-danger btn-xs" rel="tooltip" rel="tooltip" data-placement="bottom" data-original-title="Remove" onclick="return confirm('Are you sure want to delete?');" >

                        <i class="fa fa-trash-o "></i></a>

                      </td>

                    </tr>

                    <?php } ?><?php else: ?>

                    <tr>

                      <th colspan="7">

                        <center>No Slider Images Found.</center>

                      </th>

                    </tr>

                  <?php endif; ?>

                </tbody>
                    </table>
                    <div class="row-fluid  control-group mt15">             
                        <div class="span12">
                          <?php if(!empty($pagination))  echo $pagination;?>              
                        </div>
                      </div>
                  </div>
</div>
</div>
</div>
</div>  <!-- / Users widget-->

