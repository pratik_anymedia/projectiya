<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                <div class="panel panel-default">
                    <div class="panel-heading">Common Setting</div>
                    <div class="panel-body">       

                        <?php echo form_open(current_url(), array('class' => 'form-horizontal')); ?> 
                        <div class="form-body">
                            <div class="form-body">
                              
                                <div class="form-group">
                                    <div class="col-md-12">   
                                        <label class="control-label"><strong>Show Project count</strong></label>                                    
                                        <br>
                                        <label class="checkbox-inline">
<!--                                            <input class="toggle-one"  value="1"  <?php if (!empty($show_project_count) && $show_project_count == 1) echo 'checked="checked" value="1"'; else echo 'value="0"' ?> name="show_project_count" type="checkbox">-->
                                            <?php //set_value('can_join')?set_value('can_join'):(isset($grpDetails->can_join) ? $grpDetails->can_join : '') ?>
                                            <input class="toggle-one"  value="1"  <?php if ($show_project_count == 1){ ?> checked <?php } ?> name="show_project_count" type="checkbox">
                                        </label>
                                            <?php //echo form_error('available_item');  ?>
                                    </div> 
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">   
                                        <label class="control-label"><strong><?php echo $show_user_count; ?>Show Active user count</strong></label>                                    
                                        <br>
                                        <label class="checkbox-inline">
                                            <input class="toggle-one" value="1" <?php if ($show_user_count == 1){ ?> checked <?php } ?>  name="show_active_user_count"  type="checkbox">
                                        </label>
                                            <?php ///echo form_error('featured_item');  ?>
                                    </div> 
                                </div>
                            </div> 
                            <div class="form-actions">       
                                <button type="submit" class="btn blue">Submit</button> 
                            </div>  
                                <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  <!-- / Users widget-->
</div>