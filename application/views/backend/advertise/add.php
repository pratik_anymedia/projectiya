<div class="row">
    <div class="col-mod-12">
        <h3 class="page-header"> Advertisements</h3>
    </div>
</div>
<!-- Users widget -->
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading text-primary">
                <h3 class="panel-title"><i class="fa fa-comment"></i> Add Advertise </h3>
            </div>
            <div class="panel-body">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Advertise </div>
                    <div class="panel-body">
                        <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal')); ?>   
                        <div class="form-body">  

                            <div class="form-group">                
                                <label class="col-md-2 control-label">Title</label>                
                                <div class="col-md-9">                    
                                    <input type="text" placeholder="Title" class="form-control" name="title" value="<?php echo set_value('title'); ?>"><?php echo form_error('title'); ?>
                                </div>            
                            </div>          
                            <div class="form-group">      
                                <label class="col-md-2 control-label">Description</label> 
                                <div class="col-md-9">            
                                    <div class="input-group">       
                                        <textarea class="form-control" cols="100" rows="5" name="description"><?php echo set_value('description'); ?></textarea>
                                        <?php echo form_error('description'); ?>      
                                    </div>     
                                </div>    
                            </div>  


                            <div class="form-group">      
                                <label class="col-md-2 control-label">Link</label> 
                                <div class="col-md-9">            

                                    <a class="input-group demo-input-group">
                                        <span class="input-group-addon"> WIth http://</span>
                                        <input class="form-control" type="text" name="url"  value="<?php echo set_value('url'); ?>" placeholder="Link">
                                    </a>  
                                    <?php echo form_error('url'); ?>       
                                </div>     

                            </div>   
                            <div class="form-group">      
                                <label class="col-md-2 control-label">Advertise Image</label> 
                                <div class="col-md-4">            
                                    <div class="input-group">       
                                        <input type="file" name="advertise_image" >
                                        <?php echo form_error('advertise_image'); ?>      
                                    </div>  

                                </div>   
                                <label class="col-sm-2 control-label"><strong>Pop up Images</strong></label>                                    
                                <div class="col-md-4">    
                                    <label class="checkbox-inline">
                                        <input class="toggle-one"  value="1" name="popup_image" type="checkbox">
                                    </label>
                                </div>   
                            </div>    


                            <div class="form-group">                
                                <label class="col-sm-2 col-sm-2 control-label">Status</label>                
                                <div class="col-md-9">                    
                                    <select name="status" class="form-control"> 
                                        <option value="1">Active</option> 
                                        <option value="0">InActive</option>
                                    </select>                
                                </div>            
                            </div>        
                        </div>        
                        <div class="form-actions text-center">      
                            <button type="submit" class="btn blue">Save</button>     
                            <a href="<?php echo base_url() . 'backend/advertisements/'; ?>" ><button class="btn btn-danger" type="button">Cancel</button></a>   
                        </div>   
                        </form>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  <!-- / Users widget-->