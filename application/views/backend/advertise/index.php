        <div class="row">
         <div class="col-mod-12">
              
                <h3 class="page-header"> Advertisements  </h3>
            </div>
          </div>

          <!-- Users widget -->
          


        <div class="row">
            <div class="col-md-12">
            <div class="panel">
            <div class="panel-heading text-primary">
              <h3 class="panel-title"><i class="fa fa-rocket"></i> List <a href="<?php echo base_url() ?>backend/advertisements/add" class="btn btn-xs btn-success">Add Advertise <i class="icon-plus"></i> </a>  </h3>
        	</div> 

        <div class="panel-body">
         
 <?php  echo msg_alert_backend();  ?>
      <br>
             
                  <div class="table-responsive">
                    	 	 		 	<table class="table table-bordered table-hover">                        
	 	 		 		<thead>                            
	 	 		 			<tr>                                
		 	 		 			<th width="5%" class="jv no_sort">#</th>
		 	 		 			<th width="20%" class="no_sort">Title</th>
		 	 		 			<th width="20%" class="no_sort">Description</th>
		 	 		 			<!-- <th width="25%" class="no_sort">Category</th> -->
		 	 		 			<th width="10%" class="to_hide_phone ue no_sort">Status</th>
		 	 		 			<th width="10%" class="to_hide_phone span2">Created</th>         
		 	 		 			<th width="10%" class="ms no_sort ">Actions</th>       
		 	 		 		</tr>                        
		 	 		 	</thead>                        
		 	 		 	<tbody>  
		 	 		 		<?php if (!empty($advertises)): $i = $offset; 
		 	 		 		foreach ($advertises as $row): $i++; ?> 
		 	 		 		<tr> 
		 	 		 			<td><?php echo $i . "."; ?></td>
		 	 		 			<td class=""><?php if (!empty($row->title)) echo word_limiter($row->title, 10); ?></td>
		 	 		 			<td class=""><?php if (!empty($row->description)) echo word_limiter($row->description, 20); ?></a></td>
								<!--	<td class="to_hide_phone"><?php //$faq = get_data('faq_category',array('id'=>$row->category)); echo $faq->faq_category; ?> -->		 	 		 				
		 	 		 			 </td>
		 	 		 			<td class="to_hide_phone">
		 	 		 				<?php if ($row->status) { ?>
		 	 		 					<a href="<?php echo base_url() . 'backend/advertisements/changestatus/' . $row->id .'/'.$row->status.'/'. $offset ?>"><span class="label label-success label-mini" >Publish </span></a>
		 	 		 				 <?php } else { ?> <a href="<?php echo base_url() . 'backend/advertisements/changestatus/' . $row->id . '/'.$row->status.'/'. $offset ?>"><span class="label label-warning label-mini"> Unpublish </span></a>
		 	 		 				 <?php } ?> 
		 	 		 			 </td>                                        

		 	 		 			 <td class="to_hide_phone"><?php echo date('d-m-Y', strtotime($row->created)); ?></td>                                       
		 	 		 			 <td class="ms">                                            
		 	 		 			 	<div class="btn-group">  
		 	 		 			 		<a href="<?php echo base_url() . 'backend/advertisements/edit/' . $row->id . '/' . $offset ?>"  class="btn btn-success btn-xs" rel="tooltip" data-placement="left" data-original-title=" Edit "><i class="fa fa-pencil"></i></a>
		 	 		 			 		<a href="<?php echo base_url() . 'backend/advertisements/delete/' . $row->id . '/' . $offset ?>" class="btn btn-danger btn-xs" rel="tooltip" rel="tooltip" data-placement="bottom" data-original-title="Remove" onclick="return confirm('Are you sure want to delete?');" ><i class="fa fa-trash-o "></i></a>      
		 	 		 			 	</div>                                        
		 	 		 			</td>                                    
		 	 		 		</tr>                                
		 	 		 	<?php endforeach; ?>                            
		 	 		 <?php else: ?>
		 	 		 	<tr>
		 	 		 	 <th colspan="6"> <center>No Advertisements found.</center></th>
		 	 		 	</tr>
		 	 		 <?php endif; ?>
		 	 		 </tbody>
		 	 		 </table>
		 	 		 
                    <div class="row-fluid  control-group mt15">             
                        <div class="span12">
                          <?php if(!empty($pagination))  echo $pagination;?>              
                        </div>
                      </div>
                  </div>
</div>
</div>
</div>
</div>  <!-- / Users widget-->

