        <div class="row">
         <div class="col-mod-12">
                <!--  <ul class="breadcrumb">
                   <li><a href="index.html">Dashboard</a></li>
                   <li><a href="#">Tables</a></li>
                   <li class="active">Basic Tables</li>
                 </ul>
                  -->
                <!--  <div class="form-group hiddn-minibar pull-right">
                  <input type="text" class="form-control form-cascade-control nav-input-search" size="20" placeholder="Search through site" />

                  <span class="input-icon fui-search"></span>
                </div> -->

                <h3 class="page-header">  Project Technologies <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
<!-- 
                <blockquote class="page-information hidden">
                 <p>
                  More styles of basic tables are available to represent static information.
                </p>
              </blockquote> -->
            </div>
          </div>

          <!-- Users widget -->
          <div class="row">
           <div class="col-md-12">
            <div class="panel">
             <div class="panel-heading text-primary">
              <h3 class="panel-title"><i class="fa fa-rocket"></i> List
               
          </h3>
        </div> 
        <?php
          if(!empty($_SERVER['QUERY_STRING']))
              $QUERY_STRING = "0?".$_SERVER['QUERY_STRING'];
          else
              $QUERY_STRING ='';
        ?>
        <div class="panel-body">
         
 <?php  echo msg_alert_backend();  ?>
      <br>
             
                  <div class="table-responsive">
                    <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
                      <thead>
                        <tr>
                          <th width="5%" class="jv no_sort">#</th>
                          <th width="40%" class="no_sort">Blog Title</th>
                             
                          <th width="15%" class="no_sort">Comments</th>            
                          <th width="8%" class="no_sort">Pending</th>            
                          <th width="10%" class="text-center">Status </th>
                         <th width="10%" class="to_hide_phone span2">Created</th>
                          <th width="13%" class="ms no_sort ">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                        if(!empty($blogs)):
                        $i=$offset; foreach($blogs as $row): $i++;?>
                        <tr>
                          <td><?php echo $i.".";?></td>
                          <td class=""><a href="<?php echo base_url().'backend/blog/blog_edit/'.$row->id.'/'.$offset?>" class="btn btn-small"  rel="tooltip" data-placement="left" data-original-title=" Edit ">
                          <?php if(!empty($row->blog_title)) echo word_limiter($row->blog_title, 7); ?></a></td>

                            
                          <td><a href="<?php echo base_url('backend/blog/blog_comments/'.$row->id)?>"> <span class="label label-small label-success" ><?php if(!empty($row->comment_count)&&$row->comment_count>=1) echo $row->comment_count; else echo "0"  ?></span>Comments</a></td>                 
                          <td> <span class="label label-danger" ><?php $count= get_pending_comments($row->id); if(!empty($count)){ echo $count;  } else{ echo '0';} ?></span></td>
                          <td class="to_hide_phone"><?php if(!empty($row->blog_status)) echo  ucfirst('publish'); else echo ucfirst('unpublish') ; ?></td>
                          <td class="to_hide_phone"><?php echo date('Y-m-d',strtotime($row->created)); ?></td>
                          <td class="ms">
                            <div class="btn-group"> 
                              <a href="<?php echo base_url().'backend/blog/edit/'.$row->id.'/'.$offset?>"class="btn btn-default btn-xs"  rel="tooltip" data-placement="left" data-original-title=" Edit ">
                                Edit<i class="icon-edit"></i> 
                              </a> 
                              <a href="<?php echo base_url().'backend/blog/delete/'.$row->id.'/'.$offset?>" class="btn btn-danger btn-xs black" rel="tooltip" data-placement="bottom" data-original-title="Remove" onclick="if(confirm('Are you sure want to delete?')){return true;} else {return false;}" >  Delete                      
                                <i class="icon-trash "></i></a> 
                            </div>
                          </td>
                        </tr> 
                        <?php endforeach; ?>
                      <?php else: ?>
                        <tr>
                          <th colspan="6"> <center>No blog Found.</center></th>
                        </tr>
                      <?php endif; ?>
                      </tbody>
                    </table>
                    <div class="row-fluid  control-group mt15">             
                        <div class="span12">
                          <?php if(!empty($pagination))  echo $pagination;?>              
                        </div>
                      </div>
                  </div>
</div>
</div>
</div>
</div>  <!-- / Users widget-->

