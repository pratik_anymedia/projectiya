      <div class="row">
        <div class="col-mod-12">
          <h3 class="page-header"> Blogs  </h3>
        </div>
      </div>
<!-- Users widget -->
      <div class="row">
          <div class="col-md-12">
            <div class="panel">
              <div class="panel-heading text-primary">
                <h3 class="panel-title"><i class="fa fa-comment"></i> Edit Post </h3>
              </div>
              <div class="panel-body">
                <div class="panel panel-default">
                  <div class="panel-heading">Edit Post</div>
                  <div class="panel-body">
                    <?php echo form_open_multipart(current_url(),array('class'=>'form-horizontal')); ?> 
                        <div class="form-body">      
                          <div class="form-group">       
                            <label class="col-md-2 control-label">Blog Title</label>     
                            <div class="col-md-10">          
                              <input type="text" placeholder="Blog Title" class="form-control" name="blog_title" value="<?php  if (!empty($blog->blog_title)) echo $blog->blog_title;   else echo set_value('blog_title');   ?>">
                              <?php echo form_error('blog_title'); ?> 
                            </div> 
                          </div> 
                          <div class="form-group">            
                            <label class="col-md-2 control-label">Page Content</label>     
                            <div class="col-md-10">             
                              <div class="input-group">         
                                <textarea class="tinymce_edittor form-control" rows="5" cols="140" name="blog_content"><?php  if (!empty($blog->blog_content)) echo ($blog->blog_content);  else echo set_value('blog_content'); ?> </textarea>
                                <?php echo form_error('blog_content'); ?>  
                              </div>      
                            </div>    
                          </div>   
                          <div class="form-group">
                            <label class="col-md-2 control-label">Features Image</label>       
                            <div class="col-md-4">          
                              <input type="file" name="blog_features_image" >  
                              <?php echo form_error('blog_features_image'); ?>    
                              <img src="<?php echo base_url(str_replace('./', '', $blog->thumb_image)); ?>">   
                            </div>
                          </div>
                          <div class="form-group"> 
                            <label class="col-md-2 control-label">Blog Category</label>
                            <div class="col-md-10">       
                              <select  id="blog_category" name="blog_category" class="form-control">         
                                <option value="">Select Category</option>          
                                <?php if($categories): foreach ($categories as $value){ ?>      
                                <option value="<?php echo $value->slug;?>" <?php if (!empty($blog->blog_category) && $blog->blog_category==$value->slug) echo 'selected="selected"';?> ><?php echo $value->technology; ?></option>  
                                <?php } endif; ?>   
                              </select>     
                              <?php echo form_error('blog_category'); ?>   
                            </div>       
                          </div>      
                          <div class="form-group"> 
                            <label class="col-sm-2 col-sm-2 control-label">Status</label>    
                            <div class="col-md-10">     
                              <select name="blog_status" class="form-control">     
                                <option value="1"  <?php if (strtolower($blog->blog_status) == 1) echo 'selected="selected"'; ?>>Publish</option> 
                                <option value="0"  <?php if (strtolower($blog->blog_status) == 0) echo 'selected="selected"'; ?>>Unpublish</option> 
                              </select> 
                            </div>     
                          </div>       
                        </div>  
                        <div class="form-actions">   
                        <button type="submit" class="btn blue">Submit</button>           
                        <a href="<?php echo base_url() . 'backend/blog/'; ?>" >
                        <button class="btn btn-danger" type="button">Cancel</button></a>  
                        </div>
                    <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>  <!-- / Users widget-->

