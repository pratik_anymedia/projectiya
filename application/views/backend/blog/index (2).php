<script>
  function search_check(){
    if($('#search_query').val()==''){
      alert('Please Enter Blog Name');
      return false;
    }

  }
</script>
<div class="page-content-wrapper">
		<div class="page-content">
	
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Blogs <!-- <small>form controls and more</small> -->
					</h3>

					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					   <?php  echo msg_alert_backend(); ?>
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-file-text-o"></i> Blogs
								<a  class="label label-mini label-success"  href="<?php echo base_url('backend/blog/blog_add');  ?>">Add Blog</a>
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								
							</div>
						</div>
						<div class="portlet-body form">
			
			      			<div class="table-responsive">
						        <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
						          <thead>
						            <tr>
						              <th width="5%" class="jv no_sort">#</th>
						              <th width="40%" class="no_sort">Blog Title</th>
						                 
						              <th width="15%" class="no_sort">Comments</th>            
						             <th width="10%" class="text-center">Status </th>
						              <th width="10%" class="to_hide_phone span2">Created</th>
						              <th width="13%" class="ms no_sort ">Actions</th>
						            </tr>
						          </thead>
						          <tbody>
						            <?php 
						            if(!empty($blogs)):
						            $i=$offset; foreach($blogs as $row): $i++;?>
						            <tr>
						              <td><?php echo $i.".";?></td>
						              <td class=""><a href="<?php echo base_url().'backend/blog/blog_edit/'.$row->id.'/'.$offset?>" class="btn btn-small"  rel="tooltip" data-placement="left" data-original-title=" Edit ">
						              <?php if(!empty($row->blog_title)) echo word_limiter($row->blog_title, 7); ?></a></td>

						              	
						              <td><a href="<?php echo base_url('backend/blog/blog_comments/'.$row->id)?>"> <span class="label label-small label-success" ><?php if(!empty($row->blog_comment_count)&&$row->blog_comment_count>=1) echo $row->blog_comment_count; else echo "0"  ?></span>   Comments</a></td>                 
						              <td class="to_hide_phone"><?php if(strtolower($row->blog_status)=='publish')  echo  ucfirst($row->blog_status); else echo ucfirst($row->blog_status) ; ?></td>
						              <td class="to_hide_phone"><?php echo date('Y-m-d',strtotime($row->blog_created)); ?></td>
						              <td class="ms">
						                <div class="btn-group"> 
						                  <a href="<?php echo base_url().'backend/blog/blog_edit/'.$row->id.'/'.$offset?>"class="btn default btn-xs default"  rel="tooltip" data-placement="left" data-original-title=" Edit ">
						                    Edit<i class="icon-edit"></i> 
						                  </a> 
						                  <a href="<?php echo base_url().'backend/blog/blog_delete/'.$row->id.'/'.$offset?>" class="btn red btn-xs black" rel="tooltip" data-placement="bottom" data-original-title="Remove" onclick="if(confirm('Are you sure want to delete?')){return true;} else {return false;}" >  Delete                      
						                    <i class="icon-trash "></i></a> 
						                </div>
						              </td>
						            </tr> 
						            <?php endforeach; ?>
						          <?php else: ?>
						            <tr>
						              <th colspan="6"> <center>No blog Found.</center></th>
						            </tr>
						          <?php endif; ?>
						          </tbody>
						        </table>
						        <div class="row-fluid  control-group mt15">             
						            <div class="span12">
						              <?php if(!empty($pagination))  echo $pagination;?>              
						            </div>
						          </div>
						      </div>

						</div>
						</div>
					</div>
					
				</div>
				
	</div>