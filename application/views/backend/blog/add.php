      <div class="row">
        <div class="col-mod-12">
          <h3 class="page-header"> Blogs <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
        </div>
      </div>
<!-- Users widget -->
      <div class="row">
          <div class="col-md-12">
            <div class="panel">
              <div class="panel-heading text-primary">
                <h3 class="panel-title"><i class="fa fa-comment"></i> Add Post </h3>
              </div>
              <div class="panel-body">
                <div class="panel panel-default">
                  <div class="panel-heading">Add Post</div>
                  <div class="panel-body">
                    <?php echo form_open_multipart(current_url(),array('class'=>'form-horizontal')); ?> 
                                   <div class="form-body">      
                            <div class="form-group">         
                              <label class="col-md-2 control-label"> Title</label>         
                              <div class="col-md-10">           
                                <input type="text" placeholder="Title" class="form-control" name="blog_title" value="<?php echo set_value('blog_title'); ?>"><?php echo form_error('blog_title'); ?>  
                              </div>      
                            </div> 
                            <div class="form-group">         
                            <label class="col-md-2 control-label">Content</label>        
                            <div class="col-md-10">        
                              <div class="input-group">            
                              <textarea class="tinymce_edittor form-control" cols="140" rows="5" name="blog_content"><?php echo set_value('blog_content'); ?></textarea>  
                              <?php echo form_error('blog_content'); ?>
                              </div>            
                            </div>       
                            </div>    
                            <div class="form-group">     
                              <label class="col-md-2 control-label">Features Image</label>     
                              <div class="col-md-4">            
                              <input type="file" name="blog_features_image" >     
                              <?php echo form_error('blog_features_image'); ?>    
                              </div>
                            </div> 
                            <div class="form-group">    
                              <label class="col-md-2 control-label">Blog Category</label>        
                              <div class="col-md-10">             
                                <select  id="blog_category" name="blog_category" class="form-control">
                                <option value="">Select Category</option>         
                                <?php if($categories): foreach ($categories as $value){ ?>   
                                  <option value="<?php echo $value->slug;?>"><?php echo $value->technology; ?></option>
                                <?php } endif; ?>    
                                </select>          
                                 <?php echo form_error('blog_category'); ?>    
                              </div>    
                            </div>    
                            <div class="form-group">    
                              <label class="col-sm-2 col-sm-2 control-label">Status</label>  
                                <div class="col-md-10">    
                                  <select name="blog_status" class="form-control"> 
                                    <option value="1">Publish</option>
                                    <option value="0">Unpublish</option>  
                                  </select>            
                                </div>        
                            </div>      
                        </div>  
                        <div class="form-actions">   
                        <button type="submit" class="btn blue">Submit</button>           
                        <a href="<?php echo base_url() . 'backend/blog/'; ?>" >
                        <button class="btn btn-danger" type="button">Cancel</button></a>  
                        </div>
                    <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>  <!-- / Users widget-->

