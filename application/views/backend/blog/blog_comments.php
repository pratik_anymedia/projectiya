
      <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Blogs <!-- <small>form controls and more</small> -->
          </h3>

          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
      </div>
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
        <div class="col-md-12 ">
          <!-- BEGIN SAMPLE FORM PORTLET-->
             <?php  echo msg_alert_backend(); ?>
          <div class="portlet box blue">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-file-text-o"></i> Blog Comments
                <a  class="label label-mini label-success"  href="<?php echo base_url('backend/blog/');  ?>">Back To Blog</a>
              </div>
              <div class="tools">
                <a href="" class="collapse">
                </a>
                
              </div>
            </div>
            <div class="portlet-body form">

                     <div class="table-responsive">
        <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
          <thead>
            <tr>
              <th width="5%" class="jv no_sort">#</th>
              <th width="10%" class="no_sort">Author</th>  
              <th width="10%" class="no_sort">Email</th>      
              <th width="50%" class="no_sort">Comments</th>            
              <th width="10%" class="no_sort">Status</th>
              <th width="10%" class="to_hide_phone span2">Comment date</th>
              <th width="10%" class="ms no_sort ">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            if(!empty($comments)):
            $i=0; foreach($comments as $row): $i++;?>
            <tr>
              <td><?php echo $i.".";?></td>
              <td><?php echo $row->comment_author?></td>
              <td><?php echo $row->comment_author_email?></td>
              <td><?php echo $row->comment_content?></td> 
              <td>
                  <?php if($row->comment_approved=='0') { ?>
                        <a  class="btn btn-warning" href="<?php echo base_url().'backend/blog/status/'.$row->id.'/'.$row->comment_approved.'/'.$blog->id;?>" > Pending </a> 
                  <?php }if($row->comment_approved=='1'){?>
                        <a class="btn btn-success" href="<?php echo base_url().'backend/blog/status/'.$row->id.'/'.$row->comment_approved.'/'.$blog->id;?>" > Approved </a>                      
                  <?php } ?>
              </td>                
              
              <td class="to_hide_phone"><?php echo date('Y-m-d',strtotime($row->comment_date)); ?></td>
              <td class="ms">
                <div class="btn-group"> 
                   
                  <a href="<?php echo base_url().'backend/blog/comment_delete/'.$row->id.'/'.$blog->id?>" class="btn red btn-xs black" rel="tooltip" data-placement="bottom" data-original-title="Remove" onclick="if(confirm('Are you sure want to delete?')){return true;} else {return false;}" >  Delete                      
                    <i class="icon-trash "></i></a> 
                </div>
              </td>
            </tr> 
            <?php endforeach; ?>
          <?php else: ?>
            <tr>
              <th colspan="6"> <center>No blog Comments Found.</center></th>
            </tr>
          <?php endif; ?>
          </tbody>
        </table>
      
      </div>

           
            </div>
            </div>
          </div>
