<style type="text/css">
  .blog-content {
    background-color: #ffffff;
    min-height:240px;
    padding-bottom: 50px;
}
</style>
    <div class="clearfix"></div>
    <div class="page-wrapper">
        <div class="container">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Resources</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">
                            <a href="<?php echo base_url(); ?>resources">Resource List</a>
                        </li>
                    </ul>
                </div>    
            </header>
        </div>
    </div>
    <div class="blog-content" style="margin-top: 19px;">
      <div class="container">
        <div class="blog-discription">  
            <div class="col-md-12">
                <div id="mydiv" class="latest-list-warp">
                    <?php
                    if (!empty($resource)){
                        $i=0; 
                        foreach ($resource as $value):
                            $this->load->view('templates/frontend/widgets/resourceBox' , array('value' => $value));
                        $i++; 
                        endforeach;
                    }else{
                    ?>
                    <div class="container">
                        <div class="row-fluid  control-group mt15">             
                            <div class="span12">
                                <h2  class="text-center">NO RESOURCE FOUND </h2>
                            </div>
                        </div>
                    </div>    
                    
                    <?php  }  ?> 
                    <div class="container">             
                        <div class="row">             
                            <div class="span6">
                                <div id="example_info" class="dataTables_info">
                                    Showing <?php echo $offset; ?> to <?php echo $offset + $i; ?> of <?php echo $total_rows; ?> entries
                                </div>
                            </div>
                            <div class="span6">
                                <div class="pull-right">
                                    <?php if (!empty($pagination)){ echo $pagination; } ?>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div> 
        </div>
    </div>
</div>

