
<style type="text/css">
    .rating-warp {
      padding-top: 10px;
      padding-left: 70px;
    }
    .modal-header{
      background-color: #323543;
      color:#fff;
    }
    .modal-footer{
      background-color: #323543;
      color:#fff;
    }
   .error{
      color:red;
   }
 </style>
  <div class="clearfix"></div>
  <div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left">Project Description</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li><a href="#">Home</a></li>
                    <li class="breadcrumbs-label"><a href="#">Description</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>
<div class="container">
    <br>
    <div class="row">
    
    <div class="col-md-12">
     
       <div class="panel panel-default">
  <div class="panel-heading"> <h3>Checkout Process of Project</h3></div>
  <div class="panel-body">
   

       <table class="table table-bordered">
        <!--  <caption>table title and/or explanatory text</caption> -->
         <thead>
           <tr>
             <th width="30%">Title</th>
             <th width="20%">Technologies</th>
             <th width="20%">Courses</th>
             <th width="10%">Price</th>
           </tr>
         </thead>
         <tbody>
           <tr>
              <td><?php  if(!empty($project_info->title)) echo $project_info->title;    ?></td>
             <td> <?php if(!empty($project_info->technology)){ $Array=explode('#,#',trim($project_info->technology,'#'));  } ?>
             <?php  if(!empty($technology)):  ?>
                  <?php foreach ($technology as $value){ ?>
                  <?php if(!empty($Array)): for($i=0; $i < count($Array); $i++){ if($Array[$i]==$value->id){ echo ucfirst($value->technology).',';} } endif;  ?> 
                  <?php } ?>
                <?php endif; ?> </td>
             <td>
              <?php if(!empty($project_info->course_type)){ $Array=explode('#,#',trim($project_info->course_type,'#'));  } ?>
                <?php  if(!empty($courses)):  ?>
                  <?php foreach ($courses as $value){ ?>
                   <?php if(!empty($Array)): for($i=0; $i < count($Array); $i++){ if($Array[$i]==$value->id){ echo ucfirst($value->course).',';} } endif;  ?> 
                  <?php } ?>
                <?php endif; ?> 
             </td>
             <td> <?php   if(!empty($project_info->price)) echo  '<i class="fa fa-rupee"></i> '.$project_info->price;  ?></td>
            
           </tr> <tr>
             <td colspan="2" >&nbsp;</td>
             <td  ><strong> Discount </strong></td>
             <td > <i class="fa fa-rupee"> 0.00</i></td>
           </tr>  
           <tr>
             <td colspan="2" >&nbsp;</td>
             <td  ><strong> Total </strong></td>
             <td >  <?php   if(!empty($project_info->price)) echo  '<i class="fa fa-rupee"></i> '.$project_info->price;  ?></i></td>
           </tr>
         </tbody>
       </table>
      </div>
</div>
</div>


<div class="col-md-12 blog-main">

<div class="row shippig-form-panel">

                  <div class="col-md-offset-1 col-sm-8 col-xs-12">

                  <h3>Buyer  Information </h3>
              <p>Your billing address must watch the address associated with the creadit card you use</p>
              <?php echo form_open(current_url()); ?>
                      

                      <div class="shippig-form-panel">

                          <div class="form-horizontal" >                     

                          



                            <div class="form-group">

                              <label class="control-label col-sm-4"> First Name <span>*</span></label>

                              <div class="col-sm-8">

                                <input type="text" class="form-control" id="first_name" name="first_name" value="<?php if(!empty($user_info->first_name)){ echo $user_info->first_name; }else{ echo set_value('first_name'); } ?>" placeholder="First Name">

                                <?php  echo form_error('first_name'); ?><span style="color:red;" id="fn_error"></span>

                              </div>

                            </div>

                            

                            <div class="form-group">

                              <label class="control-label col-sm-4"> Last Name <span>*</span></label>

                              <div class="col-sm-8">

                                <input type="text" class="form-control" id="last_name" name="last_name" value="<?php if(!empty($user_info->last_name)){ echo $user_info->last_name; }else{  echo set_value('last_name');   } ?>" placeholder="Last Name">

                                <?php  echo form_error('last_name'); ?><span style="color:red;" id="ln_error"></span>

                              </div>

                            </div>

                            



                            <div class="form-group">

                              <label class="control-label col-sm-4"> Email <span>*</span></label>

                              <div class="col-sm-8">

                                <input type="text" class="form-control" id="email" name="email" value="<?php if(!empty($user_info->email)){ echo $user_info->email; }else{ echo set_value('email');   } ?>" placeholder="Email">

                                <?php  echo form_error('email'); ?><span style="color:red;" id="em_error"></span>

                              </div>

                            </div>

                            

                            <div class="form-group">

                              <label class="control-label col-sm-4">Address Line 1 <span>*</span></label>

                              <div class="col-sm-8">

                                <input type="text" class="form-control" id="address" name="address" value="<?php if(!empty($user_info->address)){ echo $user_info->address; }else{ echo set_value('address');   } ?>" placeholder="Address Line 1">

                                <?php  echo form_error('address'); ?><span style="color:red;" id="ad_error"></span>

                              </div>

                            </div>



                            <div class="form-group">

                              <label class="control-label col-sm-4">Address Line 2 </label>

                              <div class="col-sm-8">

                                <input type="text" class="form-control" id="address1"  name="address1"  value="<?php if(!empty($user_info->address1)){ echo $user_info->address1; }else{ echo set_value('address1');   } ?>" placeholder="Address Line 2">

                                <?php  echo form_error('address1'); ?>

                              </div>

                            </div>


                            <div class="form-group">

                              <label class="control-label col-sm-4"> Country <span>*</span></label>

                              <div class="col-sm-8">

                                <input type="text" class="form-control" readonly="readonly"  name="country"  value="India">

                                <?php echo form_error('country'); ?><span style="color:red;" id="con_error"></span>

                              </div>

                            </div>

                            

                      <div class="form-group">



                        <label for="inputPassword3" id="label" class="col-sm-4 control-label">State</label>

                        <div id="state_text" class="col-sm-8">

                            <input type="text" name="state" id="state" class="form-control" value="<?php if(!empty($user_info->state)){ echo $user_info->state; }else{ echo set_value('state');   } ?>">
                            <?php echo form_error('state'); ?><span style="color:red;" id="st_error"></span>

                        </div>

                      </div>



                        <div class="form-group">

                          <label class="control-label col-sm-4">City <span>*</span></label>

                          <div class="col-sm-8">

                            <input type="text"  id="city" class="form-control" name="city"  placeholder="City"  value="<?php if(!empty($user_info->city)){ echo $user_info->city; }else{  echo set_value('city');   } ?>" >

                            <?php echo form_error('city'); ?><span style="color:red;" id="ct_error"></span>

                          </div>

                        </div>                           

                        

                        <div class="form-group">

                          <label class="control-label col-sm-4">Zip Code <span>*</span></label>

                          <div class="col-sm-8">

                            <input type="text" id="zip_code" class="form-control" name="zip_code" maxlength="6"  placeholder="Zip Code" value="<?php if(!empty($user_info->zip_code)){ echo $user_info->zip_code; }else{ echo set_value('zip_code');    } ?>" >

                            <?php echo form_error('zip_code'); ?><span style="color:red;" id="zc_error"></span>

                          </div>

                        </div>                           

                        

                        <div class="form-group">

                          <label class="control-label col-sm-4">Phone <span>*</span></label>

                          <div class="col-sm-8">

                            <input type="text" class="form-control"  maxlength="15"  id="phone" name="phone" placeholder="Phone" value="<?php if(!empty($user_info->phone)){ echo $user_info->phone; }else{   echo set_value('phone');     } ?>" >

                            <?php echo form_error('phone'); ?><span style="color:red;" id="ph_error"></span>

                          </div>

                        </div>     
                        <hr>             
                     <div class="form-group">
                        <div class="col-sm-offset-6 col-sm-6">
                          <button type="submit" class="btn  btn-primary">Processed To Pay</button>
                        </div>
                      </div>
                            
                        <div>
                          
                        </div>
          <?php echo form_close(); ?>
                </div>

              </div>

            </div>

     </div>  
</div>

    </div>
    </div>
</div>

    
    
    