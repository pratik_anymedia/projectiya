    <div class="col-md-8">
     
       <div class="heading"><?php echo  $head_title; ?></div>
      <div class="box"> <br>
        
        <div class="col-md-12">
        <div class="heading"><?php echo  $blog_detail->blog_title; ?></div>
            <div class="full_video"> <img class="img-responsive" style="width:720px;  max-height:380px;"   src="<?php echo base_url(str_replace('./','',$blog_detail->file_path));?>" >
              <div class="caption"> 
                <!-- <h3>Thumbnail label</h3>-->
                 <!--  <p><a  href="<?php //echo base_url() ?>blog/blogs/" class="btn btn-default theme-btn pull-right hidden-xs" >Go to Blogs</a></p> -->
                <!-- <p class="smallest_grey">Nonummy eget maecenas, mi donec et, etiam quam ultrices<br> -->
                  <span class="smallest_text"><?php echo date("M d, Y",strtotime($blog_detail->blog_created));?> | <?php echo get_comment_blog1($blog_detail->id);; ?> comments <!-- | 2504 views --></span> </p>
                  <br>
                  <div class="shareit" style="float:left; margin-right:10px;">
                      <a onclick="blog_fbshare(<?php echo $blog_detail->id; ?>);" title="share on facebook" ids="facebook_share" href="javascript:void(0);" ><img src="<?php echo base_url() ?>assets/front_end/images/fb_share.gif"></a>
                    </div>  
                  <div style="margin-top:3px;">
                    <a  href="https://twitter.com/share" class="twitter-share-button"  data-count="none"  data-url="<?php echo base_url() ?>blog/blog_detail/<?php echo $blog_detail->id; ?>" data-via="Pluggedin" data-lang="en">Tweet</a>
                  </div>
                  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                  <br>
                  <p><?php echo  $blog_detail->blog_content; ?></p>
       
                   
                                
                </div>
              
               <div class="padding-10"></div>
              
              <!--Comments View-->
    <div class="comments">
      <h4 class="heading">Comments</h4>
      <input type="hidden" id="offset" name="offset" value="3">
      <?php if($this->session->flashdata('msg_success')){ ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('msg_success'); ?>
        </div>
      <?php } ?>
      <div id="comment_div">
      <?php $comment = get_blog_comments($blog_detail->id); if(!empty($comment)) foreach ($comment as $row){ ?>
        <div class="media">
          <a href="#" class="pull-left">
             <img src="<?php echo base_url()?>assets/front_end/images/comments_thumb.png" alt="" class="media-object">
          </a>
          <div class="media-body">
            <h4 class="media-heading"><?php echo $row->comment_author; ?></h4>
            <span class="smallest_text"><?php echo date("M d, Y ",strtotime($row->comment_date));?>at <?php echo timespan(strtotime($row->comment_date));  ?>  </span> 
          </div>
        </div>
        <div class="well"><?php echo $row->comment_content; ?></div>

      <?php } else {  ?>
        <div>
            <span>NO COMMENTS FOUND</span>
        </div>
      <?php }  ?>
        
      </div>  
      <span id="read_more"></span>
      <?php  $total_comment = total_comment($blog_detail->id); if(!empty($total_comment)){ if($total_comment >3){ ?>
        <div class="col-md-12">
          <div class="pull-right">
            <a onclick="return more_comments(<?php echo $blog_detail->id; ?>);" class="btn btn-default btn-xs" href="javascript:void(0)">Read More</a>
          </div>
        </div>
        <br>
      <?php } } ?>
              
          <div class="post-comment">
                <h5 class="heading">Leave a Comment</h5>
                <?php echo form_open(current_url(),array('role'=>'form')); ?>

                <div class="form-group">
                        <input type="text" name="user_name"  class="form-control" value="<?php echo  set_value('user_name'); ?>" id="exampleInputEmail2" placeholder="Your Name">
                <span><?php echo form_error('user_name'); ?></span>
              </div>
                                                

              <div class="form-group">
                <input type="text"  name="user_email" class="form-control" value="<?php echo  set_value('user_email'); ?>" id="exampleInputEmail2" placeholder="Your Email">
                <span><?php echo form_error('user_email'); ?></span>
              </div>
                                                
                                               

              <div class="form-group">
                <textarea class="form-control" rows="8" name="comment" placeholder="Comment"><?php echo  set_value('comment'); ?></textarea>
  						  <span><?php echo form_error('comment');?></span>
  						</div>

						  <p><button class="btn btn-default theme-btn" type="submit">Post a Comment</button></p>
						<?php echo form_close(); ?>
            <!-- </form> -->
					</div>
          
          <!--Comments View Close-->
          </div>
        </div>
      </div>
    </div>
    
      
       
      <div class="clearfix padding-10"></div>
      <!--Music Tuition Videos Guitar Box-->
      

    <!--Music Tuition Videos Box close--> 
    </div>
    
    
    <!--Right Menu Box-->
    
    <div class="col-md-4">
          <div class="heading">Blogs</div>
      <div class="box">
       <div id="tabwidget" class="widget tab-container"> 
        <ul id="tabnav" class="clearfix"> 
          <li><h3><a  href="#tab1" data-toggle="tab" class="selected"> <i class="icon icon-heart"></i> Recent</a></h3></li>
          <li><h3><a href="#tab2" data-toggle="tab"> <i class="icon  icon-time"></i> Popular</a></h3></li>
          <li><h3><a href="#tab3" data-toggle="tab"> <i class="icon icon-comments"></i>  Comments</a></h3></li>
        </ul> 

    <div class="tab-content">
      
      <div id="tab1" class="tab-pane active" >
        <ul id="itemContainer" class="recent-tab">
          <?php $recent_blog = get_recent_blog(); if(!empty($recent_blog)){ foreach ($recent_blog as $row) { ?>
            <li>
              <a href="<?php echo base_url();?>blog/blog_detail/<?php echo $row->id;?>"><img width="90px" height="80px" src="<?php echo base_url(str_replace('./','',$row->thumb_image));?>" class="thumb" alt="shutterstock_134257640" /></a>
              <h4 class="post-title"><a href="<?php echo base_url();?>blog/blog_detail/<?php echo $row->id;?>"><?php echo $row->blog_title; ?></a></h4>
              <p><?php echo word_limiter($row->blog_content,15); ?></p>
              <div class="clearfix"></div>  
            </li>
          <?php } }  ?>   
        </ul>        
        <div class="holder clearfix"></div>
        <div class="clear"></div>

      <!-- End most viewed post -->     
      
      </div><!-- /#tab1 -->
      <div id="tab2" class="tab-pane"  >  
        <ul id="itemContainer2" class="recent-tab">
          <?php if(!empty($blog)){ foreach ($blog as $row){  ?>
          <li>
            <a href="<?php echo base_url() ?>blog/blog_detail/<?php echo $row->id;?>"><img width="90px" height="80px" src="<?php echo base_url(str_replace('./','',$row->thumb_image));?>" class="thumb" alt="shutterstock_134257640" /></a>
            <h4 class="post-title"><a style="text-decoration:none;" href="<?php echo base_url() ?>blog/blog_detail/<?php echo $row->id;?>"><?php echo $row->blog_title;?></a></h4>
            
            <p><?php echo word_limiter($row->blog_content,15);?></p>
            <div class="clearfix"></div>        
          </li>
          <?php } } ?>                             
 
        </ul>    
      </div><!-- /#tab2 --> 

      <div id="tab3" class="tab-pane"  >
        <ul>
          <?php $recent_blog_comments =get_recent_blog_comments(); if(!empty($recent_blog_comments)){ foreach ($recent_blog_comments as $row){ ?>
            <li><span class="author"><?php echo $row->comment_author; ?></span> on <a href="<?php echo base_url() ?>blog/blog_detail/<?php echo $row->comment_blog_id; ?>" title="View comment"><?php echo get_comment_blog($row->comment_blog_id); ?></a></li>
          <?php } } ?>
        </ul>
      </div><!-- /#tab2 --> 
  
      </div><!-- /#tab-content -->

      </div><!-- /#tab-widget --> 
      <!--Blog Box close--> 
    
  </div>
      <!--Right Menu Close-->
      </div>
      <!--Right Menu Box close--> 
    </div>
    <div class="clearfix padding-20"></div>
  <!--Container Middle close--> 
  </div>
  <script type="text/javascript">
    function more_comments(arguments){
      var offset =$('#offset').val();
      $.post('<?php echo  base_url() ?>blog/more_blog_comments',{'blog_id':arguments,'offset':offset}, function(data){
        if(data!=''){
          $('#comment_div').append(data);
          var off= parseInt(offset)+3;
          $('#offset').val(off);
        }
        else{
          $('#read_more').html('');
          $('#read_more').append('NO COMMENTS FOUND');
        }
      });
    }
  </script>