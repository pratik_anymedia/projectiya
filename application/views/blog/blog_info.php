<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left">Blogs</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li><a href="#">Post</a></li>
                    <li class="breadcrumbs-label"><a href="#">Description</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>
<div class="content container">
    <div class="page-wrapper">
        <div class="page-content">
            <div class="row page-row">
                <div class="news-wrapper col-md-8 col-sm-7">                         
                    <article class="news-item">
                        <p class="meta text-muted">Posted By: <a href="#">Admin</a> | <i class="fa fa-calendar"></i> Posted on: <?php echo date("M d, Y",strtotime($blog_detail->created));?> | <i class="fa fa-comments"></i> <span class="label label-xs label-info">2</span> Comments </p>
                        <p class="featured-image">
                            <img class="img-responsive" src="<?php echo base_url(str_replace('./','',$blog_detail->file_path));?>" alt="" />
                        </p>
                        <?php if(isset($blog_detail->blog_title)): ?><h1><?php echo  $blog_detail->blog_title; ?></h1><?php  endif;?>

                        <?php if(!empty($blog_detail->blog_content)){ ?>
                            <p>
                                <?php echo  html_entity_decode($blog_detail->blog_content); ?>
                            </p>
                        <?php } ?>
                    </article><!--//news-item-->
                </div>
                <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1 col-xs-12">                    
                    <section class="widget has-divider">
                       <h3 class="title" style="padding:13px;">Recent Blog</h3>
                       <?php $Recent_blog=  get_recent_blog(); ?>
                       <?php if(!empty($Recent_blog)){  foreach ($Recent_blog as $value) { ?>
                       <article class="news-item row"> 
                           <figure class="date-label-wrapper col-md-3 col-sm-4 col-xs-4">
                               <img src="<?php echo base_url(str_replace('./','',$value->thumb_image));?>" alt="" />
                           </figure>
                           <div class="details col-md-9 col-sm-9 col-xs-9">
                               <h4 class="title"><a href="<?php echo base_url()?>blog/blog_detail/<?php echo $value->blog_slug; ?>"><?php echo character_limiter(ucfirst($value->blog_title),30);  ?></a></h4>
                           </div>
                       </article><!--//news-item-->
                       <?php } } else{ ?>
                       <h4 class="title" style="padding:10px;">No Data Found</h4>
                       <?php  } ?>
                    </section><!--//widget-->
                </aside>
            </div>
        </div>               
    </div>
</div>

<div class="content container">
    <div class="page-wrapper">
        <div class="page-content">
           <div class="row page-row">
               <div class="news-wrapper col-md-8 col-sm-7 z-depth-1">
                    <div class="comment-form">
                        <h4 style="padding: 15px">Leave a comment</h4>
                        <?php if($this->session->flashdata('msg_success')){ ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('msg_success'); ?>
                        </div>
                        <?php } ?>
                        <span class="alert alert-danger" style='display:none;' id="login_error"></span>
                        <?php echo form_open(current_url(),array('role'=>'form','id'=>'comment_submission_form')); ?>
                        <?php  if(!get_user_info())
                                                            {?>
                        <div class="row">
                            <div class="col-md-4">
                              <input type="text" name="user_name"  class="form-control" value="<?php echo  set_value('user_name'); ?>" id="exampleInputEmail2" placeholder="Your Name">
                                <span><?php echo form_error('user_name'); ?></span>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-md-4">
                            <input type="text"  name="user_email" class="form-control" value="<?php echo  set_value('user_email'); ?>" id="exampleInputEmail2" placeholder="Your Email">
                              <span><?php echo form_error('user_email'); ?></span>
                          </div> 
                        </div>
                        <?php }?>
                        <div class="row">
                            <div class="col-md-6">
                                <textarea class="form-control" rows="8" name="comment" placeholder="Comment"><?php echo  set_value('comment'); ?></textarea>
                                <span><?php echo form_error('comment');?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-default theme-btn"  id="submit_comment" type="submit">Post Comment</button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="content container">
    <div class="page-wrapper">
        <div class="page-content">
            <?php $comment = get_blog_comments($blog_detail->id); if(!empty($comment)) foreach ($comment as $row){ $user_info=get_user_information($row->user_id);  ?>
            <div class="row page-row">
                <div class="news-wrapper col-md-8 col-sm-7 z-depth-1">
                    <img src="<?php echo FRONTEND_THEME_URL ?>images/user.png">
                    <div class="author-details">
                        <strong> <?php if(!empty($user_info->first_name)) echo ucfirst($user_info->first_name.' '.$user_info->last_name);  ?></strong> , Student
                        <ul class="list-inline pull-right">
                          <li><i class="fa fa-facebook-square"></i></li>
                          <li><i class="fa fa-twitter-square"></i></li>
                        </ul> 
                    </div>
                    <p class="author-description">
                        <?php  if(!empty($row->comment_content)) echo $row->comment_content; ?>
                    </p>
                </div>
            </div>
            <?php } else {  ?>
            <div class="page-content">
                <div class="row page-row">
                    <div class="news-wrapper col-md-8 col-sm-7 z-depth-1">
                        <div class='text-center'>
                            <p style="padding:10px;">NO COMMENTS FOUND</p>
                        </div> 
                    </div>
                </div>
        </div>
            <?php }  ?>
        </div>
    </div>
</div>    

        