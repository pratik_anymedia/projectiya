<style type="text/css">
  .blog-content {
    background-color: #ffffff;
    min-height:240px;
    padding-bottom: 50px;
}
</style>
<div class="clearfix"></div>
<div class="page-wrapper">
    <div class="container">
        <header class="page-heading clearfix">
            <h1 class="heading-title pull-left">Blogs</h1>
            <div class="breadcrumbs pull-right">
                <ul class="breadcrumbs-list">
                    <li class="breadcrumbs-label"><a href="<?php echo base_url(); ?>projects/project_list">Project List</a></li>
                </ul>
            </div>    
        </header>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if(!empty($blogs)){ foreach ($blogs as $row){ ?>
            <div class="z-depth-1">
                <!--  Horizontal News Box -->
                <div class="news horizontal"  style="background: #f5f5f5 none repeat scroll 0 0;">
                    <div class="col-md-3 no-padding">
                        <!-- News Image -->

                        <div class="news-image">
                            <a  href="<?php echo base_url()?>blog/blog_detail/<?php echo $row->blog_slug; ?>">
                                <img class="img-responsive"    src="<?php echo base_url(str_replace('./','',$row->thumb_image));?>" >
                            </a>
                        </div>

                    </div>
                    <div class="col-md-9 no-padding">
                        <!-- News Description -->
                        <div class="news-description">
                            <div class="news-time">
                                <!--<i class="fa fa-clock-o"></i>-->
                                <a  style="text-decoration:none;" href="<?php echo base_url()?>blog/blog_detail/<?php echo $row->blog_slug; ?>">
                                    <h2><?php echo ucfirst($row->blog_title);  ?></h2>
                                </a>
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-user"></i>&nbsp; <?php //echo (isset($user_id_name_arr[$row->user_id]))? $user_id_name_arr[$row->user_id]:''; ?> </a></li>
                                    <li><a href="#"><i class="fa fa-calendar"></i>&nbsp;<?php echo date("M d, Y",strtotime($row->created));?></a></li>
                                    <li><a href="#"><i class="fa fa-film"></i>&nbsp;Far Cry3 Ubisoft Corporation</a></li>
                                </ul>
                                <lead><?php if(!empty($row->blog_content)){ echo  character_limiter(strip_tags(html_entity_decode($row->blog_content)),130); } ?></lead>
                                <div class="learn"><a href="<?php echo base_url()?>blog/blog_detail/<?php echo $row->blog_slug; ?>">Learn More about this post now!&nbsp;<i class="fa fa-long-arrow-right"></i></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }  }else{ ?>
                <h2  class="text-center">NO RECORD FOUND </h2>
              <?php  }  ?>
            <div class="pagelink">
                <?php if(!empty($pagination))  echo $pagination;?>
            </div>
        </div>
    </div>
</div>
