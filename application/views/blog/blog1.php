
    <div class="col-md-8">
   <div class="heading"><?php echo  $head_title; ?></div>  
      
      <div class="box"> <br>
        
        <div class="col-md-12"> 
              <?php if(!empty($blogs)){ foreach ($blogs as $row){ ?>
              <a  style="text-decoration:none;" href="<?php echo base_url()?>blog/blog_detail/<?php echo $row->id; ?>"><div class="heading_one"><?php echo ucfirst($row->blog_title);  ?></div></a>
                <a  href="<?php echo base_url()?>blog/blog_detail/<?php echo $row->id; ?>"><img class="img-responsive" style="width:720px; height:380px !important;"   src="<?php echo base_url(str_replace('./','',$row->file_path));?>" ></a>
                <div class="caption"> 
                    <p></p>                  
                    <!-- <p class="smallest_grey">Nonummy eget maecenas, mi donec et, etiam quam ultrices<br> -->
                    <span class="smallest_text"><?php echo date("M d, Y",strtotime($row->blog_created));?> | <?php echo get_comment_blog1($row->id); ?> comments <!-- | 2504 views --></span> </p>
                    <p><?php echo word_limiter($row->blog_content,20); ?></p>
                    <p><a href="<?php echo base_url()?>blog/blog_detail/<?php echo $row->id; ?> " class="btn btn-default theme-btn" >Read more</a></p>
                </div>
              <div class="padding-10"></div>
              <?php }  }else{ ?>

                <div class="heading_one">NO RECORD FOUND </div>

              <?php  }  ?>
              
              <div class="text-center">
                  <?php if(!empty($pagination))  echo $pagination;?>
              </div>
               
              
              
            </div>
          </div>
      <div class="clearfix padding-10"></div>
       
      </div>
      
      
       
      <!--Music Tuition Videos Guitar Box-->
      

    <!--Music Tuition Videos Box close--> 
 
    <!--Right Menu Box-->
    
  <div class="col-md-4">
        <div class="heading">Blogs</div>
      <div class="box">
       <div id="tabwidget" class="widget tab-container"> 
        <ul id="tabnav" class="clearfix"> 
          <li><h3><a  href="#tab1" data-toggle="tab" class="selected"> <i class="icon icon-heart"></i> Recent </a></h3></li>
          <li><h3><a href="#tab2" data-toggle="tab"> <i class="icon  icon-time"></i> Popular</a></h3></li>
          <li><h3><a href="#tab3" data-toggle="tab"> <i class="icon icon-comments"></i> Comments</a></h3></li>
        </ul> 

      <div class="tab-content">
      
      <div id="tab1" class="tab-pane active" >
        <ul id="itemContainer" class="recent-tab">
           <?php $recent_blog = get_recent_blog(); if(!empty($recent_blog)){ foreach ($recent_blog as $row) { ?>
            <li>
              <a href="<?php echo base_url();?>blog/blog_detail/<?php echo $row->id;?>"><img width="90px" height="80px" src="<?php echo base_url(str_replace('./','',$row->thumb_image));?>" class="thumb" alt="shutterstock_134257640" /></a>
              <h4 class="post-title"><a href="<?php echo base_url();?>blog/blog_detail/<?php echo $row->id;?>"><?php echo $row->blog_title; ?></a></h4>
              <p><?php echo word_limiter($row->blog_content,15); ?></p>
              <div class="clearfix"></div>  
            </li>
          <?php } }  ?>
        </ul>        
        <div class="holder clearfix"></div>
        <div class="clear"></div>

      <!-- End most viewed post -->     
      
      </div><!-- /#tab1 -->
 
      <div id="tab2" class="tab-pane"  >  
        <ul id="itemContainer2" class="recent-tab">
          <?php if(!empty($blog)){ foreach ($blog as $row){  ?>
          <li>
            <a href="<?php echo base_url();?>blog/blog_detail/<?php echo $row->id;?>"><img width="90px" height="80px" src="<?php echo base_url(str_replace('./','',$row->thumb_image));?>" class="thumb" alt="shutterstock_134257640" /></a>
            <h4 class="post-title"><a style="text-decoration:none;" href="<?php echo base_url();?>blog/blog_detail/<?php echo $row->id;?>"><?php echo $row->blog_title;?></a></h4>
            <p><?php echo word_limiter($row->blog_content,15);?></p>
            <div class="clearfix"></div>        
          </li>
          <?php } } ?>                             
        </ul>    
      </div><!-- /#tab2 --> 

      <div id="tab3" class="tab-pane"  >
        <ul>
          <?php $recent_blog_comments =get_recent_blog_comments(); if(!empty($recent_blog_comments)){ foreach ($recent_blog_comments as $row){ ?>
            <li><span class="author"><?php echo $row->comment_author; ?></span> on <a href="<?php echo base_url() ?>blog/blog_detail/<?php echo $row->comment_blog_id; ?>" title="View comment"><?php echo get_comment_blog($row->comment_blog_id); ?></a></li>
          <?php } }else{ ?>
              <li>NO COMMENTS ON BLOG</li>
          <?php  }  ?>   
        </ul>
      </div><!-- /#tab2 --> 
  
      </div><!-- /#tab-content -->

      </div><!-- /#tab-widget --> 
      <!--Blog Box close--> 
    
  </div>
       <!--Right Menu Close-->
      </div>
    <!--Right Menu Box close--> 
   </div>
  <div class="clearfix padding-20"></div>
 <!--Container Middle close--> 
</div>