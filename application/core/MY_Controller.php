<?php

class MY_Controller extends CI_Controller
{
    public $SETTINGS;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('text');
        //get project count
        $data['total_project_count'] = $this->common_model->projects(0, 0);

        //get active user count
        $user_result = $this->common_model->get_result('users', array('status' => 1));
        $data['active_user_count'] = count($user_result);

        $common_setting = $this->common_model->get_result('common_setting', array('status' => 1));
        $data['pages'] = $this->common_model->get_result('posts', array('post_type' => 'page', 'post_status' => 'publish'));

        $show_project_count = 0;
        $show_user_count = 0;
        if (!empty($common_setting)) {
            foreach ($common_setting as $key => $value) {

                if ($value->attribute_type == 1 && $value->status == 1) {
                    $show_project_count = 1;
                }

                if ($value->attribute_type == 2 && $value->status == 1) {
                    $show_user_count = 1;
                }
            }
        }

        $data['show_project_count'] = $show_project_count;
        $data['show_user_count'] = $show_user_count;
        $data['technology'] = $this->common_model->get_result('technologies', array('status' => 1));

        $data['technologies'] = array();
        if (!empty($data['technology'])) {
            foreach ($data['technology'] as $key => $value) {
                $value = get_object_vars($value);
                $data['technologies'][$value['id']] = $value['technology'];
            }
        }
        $data['courses'] = $this->common_model->get_result('courses', array('status' => 1));
        $data['course_arr'] = array();
        if (!empty($data['courses'])) {
            foreach ($data['courses'] as $key => $value) {
                $value = get_object_vars($value);
                $data['course_arr'][$value['id']] = $value['course'];
            }
        }
        
        $this->load->model('settings_model');
        $data['SETTINGS'] = array();
        $settings = $this->settings_model->getSettings();
        foreach($settings as $setting){
            $data['SETTINGS'][$setting->key] = $setting->value;
        }
        $this->load->vars($data);
    }
}


