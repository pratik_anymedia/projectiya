<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
  

  class Order extends MY_Controller
  {
        public function __construct()
        {
          parent::__construct();
          $this->load->model('user_model');
          $this->load->model('common_model');
          require_once APPPATH."/third_party/payu.php";

        }

        public function index()
        {
            redirect('order/checkout');
        }

        public function checkout($pro_unique_id='')
        {
          if($_POST){
             
            if(!empty($_POST['mihpayid']) && $_POST['status']=='success' && !empty($_POST['mode'])  && $_POST['unmappedstatus'] =='captured' ){
                redirect('order/payment_success/'.$_POST['mihpayid']);
            }
            if(!empty($_POST['mihpayid']) && $_POST['status']=='failure' && !empty($_POST['mode'])  && $_POST['unmappedstatus'] =='failed' ){
                redirect('order/payment_failure/'.$_POST['mihpayid']);
            }
          }
          
          if(empty($pro_unique_id)) redirect('projects/');
          $data['project_info'] =$this->common_model->get_row('projects',array('unique_id'=>$pro_unique_id,'status'=>1));
          if(empty($data['project_info'])) redirect('projects/');
      
          if(user_logged_in() === TRUE){ 
             $data['user_info'] = $this->common_model->get_row('users',array('id'=>user_id(),'status'=>1,'user_role'=>1));
          }else{
             $data['user_info'] = '';
          }
          $this->form_validation->set_rules('first_name','First Name','trim|required');
          $this->form_validation->set_rules('last_name','Last Name','trim|required');
          $this->form_validation->set_rules('email','Email','trim|valid_email|required|valid_email');
          $this->form_validation->set_rules('address','Address','trim|required');
          $this->form_validation->set_rules('address1','Address','trim');
          $this->form_validation->set_rules('city','City','trim|required');
          $this->form_validation->set_rules('country','Country','trim|required');
          $this->form_validation->set_rules('zip_code', 'Zip Code','trim|required|callback_alpha_numeric_space');
          $this->form_validation->set_rules('phone', 'Phone','trim');
          $this->form_validation->set_error_delimiters('<div style="color:red;" class="error">','</div>');
          if($this->form_validation->run()==TRUE){
              $rand_id= substr(hash('sha256', mt_rand() . microtime()), 0, 20); 

              $order_data= array( 
                            'project_id'=>$data['project_info']->id,
                            'unique_id'     =>$data['project_info']->unique_id,
                            'project_title' =>$data['project_info']->title,
                            'total_amount'  =>$data['project_info']->price, 
                            'transaction_id'=>$rand_id   
                          );
              if(user_logged_in() === TRUE){ 
                $order_data['user_id']    = user_id();
                $order_data['user_name']  = $data['user_info']->first_name.' '.$data['user_info']->last_name;
                $order_data['user_email'] = $data['user_info']->email;
                $order_data['user_status']= 'register';
              } else {
                  $order_data['user_role'] =1;
                  $order_data['status']    = 0;
                  $order_data['first_name']= $this->input->post('first_name');
                  $order_data['last_name'] = $this->input->post('last_name');
                  $order_data['address'] = $this->input->post('address');
                  $order_data['city'] = $this->input->post('city');
                  $order_data['email'] = $this->input->post('email');
                  $order_data['password']= sha1($this->input->post('first_name'));
                  $order_data['phone']   = $this->input->post('phone');
                  $order_data['user_status']= 'guest';
              }    
              if($this->session->userdata('order_previous_info')){
                $this->session->unset_userdata('order_previous_info');
                $this->session->set_userdata('order_previous_info',$order_data);
              }else{
                $this->session->set_userdata('order_previous_info',$order_data);
              }

              pay_page( 
                  array( 
                         'key'    => 'gtKFFx',//'FPMy4T',
                         'txnid' =>$rand_id, 
                         'amount'=>$data['project_info']->price,
                         'firstname'=>$this->input->post('first_name'), 
                         'email' =>   $this->input->post('email'), 
                         'phone' => $this->input->post('phone'),
                         'productinfo' =>$data['project_info']->title, 
                         'surl'  => 'order/payment_success',
                         'furl'  => 'order/payment_failure' 
                      ),
                  'eCwWELxi'//'048ZesGl'
              );  
        }
        $data['courses']=$this->common_model->get_result('courses',array('status'=>1));
        $data['technology']=$this->common_model->get_result('technologies',array('status'=>1));
         
        $data['template']='order/checkout';
        redirect('order/payment_success/');
        $this->load->view('templates/frontend/layout', $data);
      }

      public function payment_success($transaction_id='')
      {

         if($this->session->userdata('order_previous_info')){
         
              $order_info =  $this->session->userdata('order_previous_info');
            
              $order_data= array(
                                  'buyer_id'     => $transaction_id,
                                  'token_id'     => random_string('alnum', 16),
                                  'order_id'     => random_string('alnum',8),
                                  //'project_id'   => $order_info['project_id'],
                                  'unique_id'    => $order_info['unique_id'],
                                  'project_title'=> $order_info['project_title'],
                                  'total_amount' => $order_info['total_amount'],
                                  'order_status' => 1,
                                  'created'      => date('Y-m-d h:i:s'),
                                ); 

              if($order_info['user_status']!='register'){

                  $user_data['user_role'] =1;
                  $user_data['status']    = 0;
                  $user_data['first_name']=$order_info['first_name'];
                  $user_data['last_name'] =$order_info['last_name'];
                  $user_data['address'] = $order_info['address'];
                  $user_data['city'] = $order_info['city'];
                  $user_data['email'] = $order_info['email'];
                  $user_data['password']= sha1($order_info['first_name']);
                  $user_data['phone']   = $order_info['phone'];
                  if($user_info_id = $this->user_model->insert('users', $user_data)){
                        

                        $user_info = $this->user_model->get_row('users',array('id'=>$user_info_id));
                        $secret_key=trim(md5($user_info->email));
                        $user_info->email;
                        $this->user_model->update('users',array('secret_key'=>$secret_key),array('id'=>$user_info->id));
                        $i=3;
                        $this->load->library('developer_email');
                        $email_template=$this->developer_email->get_email_template($i);
                        $param=array(
                              'template'  =>  array(
                              'temp'  =>  $email_template->template_body,
                              'var_name'  =>  array(
                                              'full_name'  => $user_info->first_name.' '.$user_info->last_name,
                                              'site_name'   => SITE_NAME,
                                              'Password'    => $order_info['first_name'],
                                              'site_url'    => base_url(),
                                              'email_address'  => $user_info->email,
                                              'activation_key'=>base_url('user/activation/'.$user_info->user_role.'/'.$secret_key)
                                            ),
                                ),
                                'email' =>  array(
                                'to'    =>   $user_info->email,
                                'from'  =>   NO_REPLY_EMAIL,
                                'from_name' => SITE_NAME,
                                'subject' =>   $email_template->template_subject,
                            )
                        );
                  $status=$this->developer_email->send_mail($param);
                

                  $order_data['user_id']=$user_info_id;
                  $order_data['user_name']=$order_info['first_name'].' '.$order_info['last_name'];
                  $order_data['user_email']=$order_info['email'];
              }

            }else{
              $order_data['user_id']    = user_id();
              $order_data['user_name']  = $order_info['user_name'];
              $order_data['user_email'] = $order_info['user_email'];
            
            }  
      
            if($order_info_id = $this->user_model->insert('orders',$order_data)){

                  $order_info = $this->user_model->get_row('orders',array('id'=>$order_info_id)); 
                  $user_info = $this->user_model->get_row('users',array('id'=>$order_info->user_id));
                  $i=2;
                  $this->load->library('developer_email');
                  $email_template=$this->developer_email->get_email_template($i);
                  $param=array(
                        'template'=>  array(
                        'temp'    =>  $email_template->template_body,
                        'var_name'=>  array(
                                        'full_name'  => $user_info->first_name.' '.$user_info->last_name,
                                        'site_name'   => SITE_NAME,
                                        'project_title'=>ucfirst($order_data['project_title']),
                                        'order_id'    => $order_info->order_id,
                                        'total_amount'=> $order_data['total_amount'],
                                        'site_url'    => base_url(),
                                        'download_url'=>base_url('user/pro_download/'.$order_info->token_id.'/'.$order_data['unique_id']),
                                      ),
                          ),
                          'email' =>  array(
                          'to'    =>   $user_info->email,
                          'from'  =>   NO_REPLY_EMAIL,
                          'from_name' => SITE_NAME,
                          'subject' =>   $email_template->template_subject,
                      )
                    );
                    $param1=array(
                        'template' =>array(
                        'temp'     =>$email_template->template_body_admin,
                        'var_name' =>array(
                                          'full_name'    => $user_info->first_name.' '.$user_info->last_name,
                                          'site_name'    => SITE_NAME,
                                          'project_title'=> ucfirst($order_data['project_title']),
                                          'order_id'     => $order_info_id,
                                          'total_amount' => $order_data['total_amount'],
                                          'site_url'     => base_url(),
                                          'email'        => $user_info->email,
                                          'user_phone'   => $user_info->phone,
                                          'project_price'=> $order_data['total_amount'],
                                      ),
                          ),
                          'email' =>  array(
                          'to'    =>   SUPERADMIN_EMAIL,
                          'from'  =>   NO_REPLY_EMAIL,
                          'from_name' => SITE_NAME,
                          'subject' =>   $email_template->template_subject_admin,
                      )
                   );
                  if($status=$this->developer_email->send_mail($param)){

                     $status=$this->developer_email->send_mail($param1); 
                    
                  }

            $this->session->set_flashdata('msg_success', 'Congrats!!! Your order has been placed successfully.');
          } 
          if($this->session->userdata('order_previous_info')){
                $this->session->unset_userdata('order_previous_info');
                $this->session->set_userdata('order_previous_info',$order_data);
              }

        }else{
          $this->session->set_flashdata('msg_error', 'Sorry !!! Order info submission error.');
        }
        $data['template']='order/order_success';
        $this->load->view('templates/frontend/layout', $data);
      }
      
      public function payment_failure($value='')
      {
        $this->session->set_flashdata('msg_error', 'Order has been failed submission error.');
        $data['template']='order/order_failed';
        $this->load->view('templates/frontend/layout', $data);
      }





  }   