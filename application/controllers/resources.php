<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Resources extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('message', 'english');
        $this->load->model(array('common_model','project_model'));
    }
    
    
    public function index($offset = 0) {
        
        $data['resource'] = $this->common_model->resource();
        //echo '<pre>';print_r($data['resource']);die;
        $per_page = 2;
        $data['offset'] = $offset;
        $data['resource'] = $this->common_model->resources($offset, $per_page);
        //echo '<pre>';print_r($data['resources']);die;
        $config = frontend_pagination();
        $config['base_url'] = base_url() . 'resources/index';
        $config['total_rows'] = $this->common_model->resources(0, 0);
        $data['total_rows'] = $this->common_model->resources(0, 0);
        //print_r($config['total_rows']);die;
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
       
        $data['pagination'] = $this->pagination->create_links();
        
        
                
        $data['template'] = 'resource/index';
        $this->load->view('templates/frontend/layout', $data);
    }
    
}