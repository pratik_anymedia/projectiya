<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller
{

    public function __construct()
    {
      parent::__construct();
      clear_cache();
      $this->load->model('user_model');
    }

    public function index()
    {
      redirect(base_url('user/login'));
    }

    public function login($id='',$type=''){
      if(user_logged_in()===TRUE) redirect('user/dashboard');
        $data['title_top']='Login';
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_error_delimiters('<div style="color:red;" class="error">','</div>');
        if($this->form_validation->run() == TRUE){
            //$this->load->model('user_model');
            $data  = array('email'=>$this->input->post('email'),'password'=>sha1($this->input->post('password')),'status'=>1);      
            if($this->user_model->login($data)){
               redirect('user/dashboard');
            }else{
               redirect('user/login');
            }
        }
        // $data['category']=$this->common_model->get_result('categories',array('status'=>1));
        $data['template']='user/login';
        $this->load->view('templates/frontend/layout', $data);
    }

    

    public function logout(){

        _check_user_login();  //check  login authentication

        $this->session->unset_userdata('user_info');

        $this->session->sess_destroy();

        redirect('user/login');

    }

      
    public function user_login(){  
        if(user_logged_in() === TRUE){ 
            echo 'already_login';
            return TRUE;
        }
        if($_POST){
          $email=$_POST['email'];
          $password= $_POST['password'];
          if(!empty($email) &&!empty($password)){
            $this->load->model('user_model');
            $data = array('email' => $this->input->post('email',TRUE), 'password' => sha1($this->input->post('password',TRUE)));
            if($this->user_model->for_login($data)){
               echo 'successfully';
               return TRUE; 
            }else{
               echo 'login_fail_cred';
               return FALSE;
            }
          }   
        }        
        echo 'login_fail';   
    }


    private function check_login(){

      if(trainer_logged_in()===FALSE)
        redirect(base_url('user/login'));
      }


    function email_check($str=''){

      if($this->user_model->get_row('users',array('email'=>$str))):

            $this->form_validation->set_message('email_check', 'The Email Address already exists.');

            return FALSE;

       else:
            return TRUE;
       endif;

    }



    public function checkout_login($value='')

    {

      if($_POST){  

            $this->form_validation->set_rules('password', 'Password', 'required');

            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

            $this->form_validation->set_error_delimiters('<div style="color:red;" class="error">', '</div>');

            if($this->form_validation->run() == TRUE){

                //$this->load->model('user_model');

                $data  = array('email'=>$this->input->post('email'),'password' => sha1($this->input->post('password')),'status'=>1);      

                if($this->user_model->login($data)){

                  $this->session->set_flashdata('login_success','Login successfully');

                  redirect('cart/checkout');

                }else{

                  $this->session->set_flashdata('login_error','Login Credentials are not vaild');

                  redirect('cart/proceed_checkout');

                }

            }else{

              redirect('cart/proceed_checkout');

            }

      } else {

        redirect('cart/');

      }   

    }  

    public function register()
    {
      if(user_logged_in() === TRUE) redirect('user/dashboard');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|callback_email_check');
        $this->form_validation->set_rules('userpassword', 'Password', 'required|matches[con_password]');
        $this->form_validation->set_rules('con_password', 'Confirm Password','required');
        $this->form_validation->set_rules('mobile_no', 'Mobile No.', 'required|numeric');
        $this->form_validation->set_error_delimiters('<div style="color:red;" class="error">', '</div>');
        if($this->form_validation->run() == TRUE){
            $user_data['user_role'] =1;
            $user_data['status']    = 0;
            $user_data['first_name']= $this->input->post('first_name');
            $user_data['last_name'] = $this->input->post('last_name');
            $user_data['email'] = $this->input->post('user_email');
            $user_data['password']= sha1($this->input->post('userpassword'));
            $user_data['phone']   = $this->input->post('mobile_no');
            $user_data['created'] = date('Y-m-d');
            if($user_info_id = $this->user_model->insert('users', $user_data)) {
                $user_info = $this->user_model->get_row('users',array('id'=>$user_info_id));
                $secret_key=trim(md5($user_info->email));
                $user_info->email;
                $this->user_model->update('users',array('secret_key'=>$secret_key),array('id'=>$user_info->id));
                $i=3;
          
              $this->load->library('developer_email');

              $email_template=$this->developer_email->get_email_template($i);

              $param=array(

                 'template'  =>  array(

                      'temp'  =>  $email_template->template_body,

                      'var_name'  =>  array(

                              'full_name'  => $user_info->first_name.' '.$user_info->last_name,

                              'site_name'   => SITE_NAME,

                              'password'=> $this->input->post('userpassword'),

                              'site_url'    => base_url(),

                              'email_address'  => $user_info->email,

                              'activation_key'=>base_url('user/activation/'.$user_info->user_role.'/'.$secret_key)

                            ),

                        ),

                  'email' =>  array(

                  'to'    =>   $user_info->email,

                  'from'  =>   NO_REPLY_EMAIL,

                  'from_name' => SITE_NAME,

                  'subject' =>   $email_template->template_subject,

                )

              );

              $status=$this->developer_email->send_mail($param);
              if(!empty($status)){

                  $this->session->set_flashdata('msg_success', 'Thank you for registering on Projectiya.com. To activate your account, please check your email.');

                  redirect('user/login');

              } else {

                  $this->session->set_flashdata('msg_error', 'Failed, Please try again.');

                  redirect('user/register');

              }

            }

        

      }

      // $data['country']=$this->common_model->get_result('countries',array('status'=>1),'',array('slug','asc'));

      // $data['category']=$this->common_model->get_result('categories',array('status'=>1));

      $data['template'] = 'user/registration';

      $this->load->view('templates/frontend/layout', $data);

    }


  public function activation($user_role='',$key=''){

    if($key=="" || $key==NULL){
      redirect('user/register');
    }
    if($user_role=="" || $user_role==NULL){
      redirect('user/register');
    }
    $type='';
    if($user=$this->user_model->get_row('users', array('user_role'=>$user_role,'status'=>0,'secret_key'=>trim($key)))){      
      if($user->user_role==1){  
        $user_data=array('status'=>1,'secret_key'=>'');

        $this->user_model->update('users',$user_data,array('id'=>$user->id));     

        $data  = array('email'=>$user->email,'password'=>$user->password);

        if($user->user_role==1) $type='user';    

        if($this->user_model->login($data)){

          if($type=='user')

            redirect('user/profile/');

          else

            redirect('user/profile/');

        }

      }

    }else{

      redirect('user/login');

    }

  }



 



  public function sign_up()

  {

    if($_POST){

          $email_status =$this->email_check($_POST['email']);

          if(!$email_status){

            $arrayName = array('msg_error' => 'Email Address Already exist','Status'=>0);

            echo $test = json_encode($arrayName);        

            return FALSE;

          }

            $user_data['user_role'] =1;

            $user_data['status'] = 1;

            $user_data['first_name']= $_POST['first_name'];

            $user_data['last_name'] = $_POST['last_name'];

            $user_data['email']   = $_POST['email']; 

            $user_data['password']= sha1(trim($_POST['password']));

            $user_data['created'] = date('Y-m-d');



            if($user_info_id = $this->user_model->insert('users', $user_data)){

              $user_info = $this->user_model->get_row('users',array('id'=>$user_info_id));

              //$secret_key=trim(md5($user_info->email));

              $user_info->email;

              //$this->user_model->update('users',array('secret_key'=>$secret_key),array('id'=>$user_info->id));

              $i='';

              if(!empty($user_info->user_role)&&$user_info->user_role==1){

                $i=7;

              }

              $this->load->library('developer_email');

              $email_template=$this->developer_email->get_email_template($i);

               $param=array(

                   'template' =>  array(

                   'temp'  =>  $email_template->template_body,

                   'var_name' =>  array(

                                'full_name'  => $user_info->first_name.' '.$user_info->last_name,

                                'site_name'   => SITE_NAME,

                                'site_url'    => base_url(),

                                'email'  => $user_info->email,

                                'pass'=>trim($_POST['password'])

                              ),

                          ),

                    'email' =>  array(

                    'to'    =>   $user_info->email,

                    'from'  =>   NO_REPLY_EMAIL,

                    'from_name' => SITE_NAME,

                    'subject' =>   $email_template->template_subject,

                  )

                );



              $status=$this->developer_email->send_mail($param);



              if(!empty($status)){

                $this->load->model('user_model');

                 $data  = array('email'=>$user_info->email,'password'=>sha1(trim($_POST['password'])));

                 if($this->user_model->for_login($data)){

                    $arrayName = array('msg_success'=>'Thank you for registering on dermacessity.Com. your account detail has been sent on your Email Address.','Status'=>2);

                    echo $test = json_encode($arrayName); 

                    return TRUE;

                    

                  }else{

                     $arrayName = array('msg_error'=>'Please Check  Login operation Failed Please try again ','Status'=>3);

                     echo $test = json_encode($arrayName); 

                     return FALSE;

                  }

                  $arrayName = array('msg_success'=>'Thank you for registering on dermacessity.Com. your account detail has been sent on your Email Address.','Status'=>2);

                  echo $test = json_encode($arrayName); 

                  return TRUE;

                 

              }

              

         }

    }

    $arrayName = array('msg_error'=>'sign up operation Failed Please try again','Status'=>1);

    //echo $test = json_encode($arrayName);  

    $data['template'] = 'user/login';

    $this->load->view('templates/frontend/layout', $data);

  }

    public function dashboard()
    {
        _check_user_login();  //check User login authentication
        $uId = $this->session->userdata('user_info');
        $data['totalProject'] = $this->user_model->getUserProject($uId['id']);
        $data['totalEarning'] = $this->user_model->getTotalEarning($uId['id']);
        $data['totalBuy'] = $this->user_model->getTotalBuys($uId['id']);
        $data['totalReview'] = $this->user_model->getTotalReviews($uId['id']);
        $data['totalComment'] = $this->user_model->getTotalComment($uId['id']);
        $data['template'] = 'user/dashboard';
        $this->load->view('templates/frontend/layout', $data);
    }
    
    function testimonial(){
        _check_user_login();
        $this->load->model(array('testimonial_model'));
        $userId = user_id();
        $data['userTestimonial'] = $this->testimonial_model->getByUserId($userId);
        
        $this->form_validation->set_rules('testimonial','Testimonial','trim|required|min_length[500]');
        $this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');
        if($this->form_validation->run() == TRUE){   
            $content = $this->input->post('testimonial');
            $this->testimonial_model->add(trim($content) , $userId);
            $this->session->set_flashdata('msg_success','Thank You!!! Your Testimonial Submitted Successfully!!!');
            redirect('user/testimonial');
            
        }    
        $data['template'] = 'user/testimonial';
        $this->load->view('templates/frontend/layout', $data);
    }
    
  public function profile(){
      _check_user_login();  //check login authentication 
      $data['customer'] = $this->user_model->get_row('users',array('id'=>user_id(),'user_role'=>1));
      $data['customer_academy_info'] = $this->user_model->get_row('user_academy_information',array('user_id'=>$data['customer']->id));

        if(!empty($_POST['general_info'])){
         
            $this->form_validation->set_rules('first_name','First Name','trim|required');
            $this->form_validation->set_rules('last_name','Last Name','trim|required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_check_email['.$data['customer']->email.']');
            $this->form_validation->set_rules('address','Address','trim|required');
            $this->form_validation->set_rules('city','City','trim|required');
            $this->form_validation->set_rules('state','state','trim|required');
            $this->form_validation->set_rules('mobile','Mobile','trim|required|numeric');
            $this->form_validation->set_rules('zip_code', 'Zip Code','trim|required|callback_alpha_numeric_space');
            if(!empty($_FILES['resume']['name'])){
              $this->form_validation->set_rules('resume','Resume File','trim|callback_resume_check[]');
            }
        }
        if(!empty($_POST['academic_info'])){
            $this->form_validation->set_rules('high_school_name',' High School Name','trim|required');
            $this->form_validation->set_rules('high_school_city','City','trim|required');
            $this->form_validation->set_rules('high_school_percentage','High School Percentage','trim|required|numeric');
            $this->form_validation->set_rules('high_secondary_school_name','School Name','trim|required');
            $this->form_validation->set_rules('high_secondary_school_city','City','trim|required');
            $this->form_validation->set_rules('high_secondary_school_percentage','Higher Secondary Percentage','trim|required|numeric');
            $this->form_validation->set_rules('graduation_institude_name','Institude Name','trim|required');
            $this->form_validation->set_rules('graduation_institude_city','Institude city','trim|required');
            $this->form_validation->set_rules('graduation_course_name','Course Name','trim|required');
            $this->form_validation->set_rules('graduation_branch_name','Branch Name','trim|required');
            $this->form_validation->set_rules('graduation_aggr_percentage','Graduation Percentage','trim|required|numeric');
            $this->form_validation->set_rules('post_graduation_institude_name','Institude Name','trim|required');
            $this->form_validation->set_rules('post_graduation_city','Institude city','trim|required');
            $this->form_validation->set_rules('post_graduation_course_name','Course Name','trim|required');
            $this->form_validation->set_rules('post_graduation_branch_name','Branch Name','trim|required');
            $this->form_validation->set_rules('post_graduation_aggr_percentage','Post Graduation Percentage','trim|required');
            $this->form_validation->set_rules('other_course_info','Other Course','trim');
            $this->form_validation->set_rules('project_done_info','Post Graduation Percentage','trim');
            $this->form_validation->set_rules('skills','Post Graduation Percentage','trim');
            $this->form_validation->set_rules('total_experience','Post Graduation Percentage','trim|numeric');
        }
        if(!empty($_POST['bank_detail'])){
            $this->form_validation->set_rules('bank_name','Bank Name','trim|required');
            $this->form_validation->set_rules('bank_acc_no','Bank Account No.', 'trim|required');
            $this->form_validation->set_rules('bank_ifsc_code','Bank IFSC Code','trim|required');
            $this->form_validation->set_rules('bank_branch','Bank Branch','trim|required');
            $this->form_validation->set_rules('bank_city','Bank City','trim|required');
        }
        $this->form_validation->set_error_delimiters('<div style="color:red;" class="alert-danger">', '</div>');
        if($this->form_validation->run() == TRUE){    
          

          if(!empty($_POST['general_info'])){
              $user_data = array( 
                                  'first_name'=> $this->input->post('first_name'),
                                  'last_name'=> $this->input->post('last_name'),
                                  'email'   => $this->input->post('email'),
                                  'address'=> $this->input->post('address'),
                                  'city'   => $this->input->post('city'),
                                  'state'   => $this->input->post('state'),    
                                  'address1'=> $this->input->post('address1'),
                                  'phone'   => $this->input->post('mobile'),     
                                  'zip_code'=> $this->input->post('zip_code'),
                                  'modified'=> date('Y-m-d h:i:s')
                                );
              if($this->session->userdata('resume')):
                   $resume_file = $this->session->userdata('resume');   
                   $user_data['resume_file'] = $resume_file['file_path']; 
                   if(!empty($data['customer']->resume_file)){
                      $new_file=$data['customer']->resume_file;
                      @unlink($new_file);
                   }
              endif;


              if($this->user_model->update('users',$user_data,array('id'=>user_id(),'user_role'=>1))){
                  if($this->session->userdata('resume')):
                    $this->session->unset_userdata('resume');   
                  endif;
                  $this->session->set_flashdata('msg_success','your general information has been updated successfully.');
                  redirect('user/profile');
              }else{
                  $this->session->set_flashdata('msg_error','Update failed, Please try again.');
                  redirect('user/profile');
              }

          }    
          if(!empty($_POST['academic_info'])){
                  $user_academic_data = array(
                            'high_school_name'=> $this->input->post('high_school_name'),
                            'high_school_city'=> $this->input->post('high_school_city'),
                            'high_school_percentage'=> $this->input->post('high_school_percentage'),
                            'high_secondary_school_name'=> $this->input->post('high_secondary_school_name'),
                            'high_secondary_school_city'=> $this->input->post('high_secondary_school_city'),
                            'high_secondary_school_percentage'=> $this->input->post('high_secondary_school_percentage'),
                            'graduation_institude_name'   => $this->input->post('graduation_institude_name'),
                            'graduation_institude_city'   => $this->input->post('graduation_institude_city'),
                            'graduation_course_name'    => $this->input->post('graduation_course_name'),
                            'graduation_branch_name'    => $this->input->post('graduation_branch_name'),
                            'graduation_aggr_percentage'    => $this->input->post('graduation_aggr_percentage'),
                            'post_graduation_institude_name'=> $this->input->post('post_graduation_institude_name'),
                            'post_graduation_city'=> $this->input->post('post_graduation_city'),
                            'post_graduation_course_name'=> $this->input->post('post_graduation_course_name'),
                            'post_graduation_branch_name'=> $this->input->post('post_graduation_branch_name'),
                            'post_graduation_aggr_percentage'   => $this->input->post('post_graduation_aggr_percentage'),
                            'other_course_info'=> $this->input->post('other_course_info'),
                            'project_done_info'  => $this->input->post('project_done_info'),                                 
                            'total_experience'  => $this->input->post('total_experience'),                                 
                            'skills'  => $this->input->post('project_done_info'),                                 
                            'modified'=> date('Y-m-d h:i:s')
                  );

                  if(!empty($data['costomer_academy_info'])){
                    if($this->user_model->update('user_academy_information',$user_academic_data,array('user_id'=>user_id()))){    
                        
                        $this->session->set_flashdata('msg_success','Your Academy information has been updated successfully.');
                        redirect('user/profile');
                    }
                  }else{
                    $user_academic_data['user_id']=$data['customer']->id;
                    if($this->user_model->insert('user_academy_information',$user_academic_data)){ 
                        $this->session->set_flashdata('msg_success','Your Academy information has been updated successfully.');
                        redirect('user/profile');
                    }
                  }                
                  $this->session->set_flashdata('msg_error','Update failed, Please try again.');
                  redirect('user/profile');
                  
              } 

            if(!empty($_POST['bank_detail'])){
                $user_bank_data = array(
                     'bank_name'=> $this->input->post('bank_name'),
                     'bank_branch'=> $this->input->post('bank_branch'),
                     'bank_city'=> $this->input->post('bank_city'),
                     'bank_ifsc_code'   => $this->input->post('bank_ifsc_code'),
                     'bank_account_no'   => $this->input->post('bank_acc_no')
                );

                if($this->user_model->update('users',$user_bank_data,array('id'=>user_id(),'user_role'=>1))){    
                    $this->session->set_flashdata('msg_success','Your bank information has been updated successfully.');
                    redirect('user/profile');
                }else{
                    $this->session->set_flashdata('msg_error','Update failed, Please try again.');
                    redirect('user/profile');
                }
            }
           
            
      }
      $data['technology']=$this->common_model->get_result('technologies',array('status'=>1));
      $data['template'] = "user/profile";
      $this->load->view('templates/frontend/layout', $data);
    }

    

     public function resume_check($str){           

        if(!empty($_FILES['resume']['name'])){
            if($this->session->userdata('resume')){               
                return TRUE;
            }else{
                $param['upload_path'] = './assets/uploads/resume/';
                $param['allowed_types'] = 'pdf|doc|docx';
                $param['max_size'] = '1000000000000';
                $param['encrypt_name'] =TRUE;
                $this->load->library('upload', $param);
                $this->upload->initialize($param);
                
                if(!$this->upload->do_upload('resume'))
                {
                      $this->form_validation->set_message('resume_check', $this->upload->display_errors());                
                      return FALSE;
                } else {  
                    $upload_file['UPLOAD_DATA']=$this->upload->data();
                    $this->session->set_userdata('resume',array('file_path'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name']));
                    return TRUE;    
                      
                }
            }
        }
    }



    public function userfile_check($str){           

        if(!empty($_FILES['userfile']['name'])){

            if($this->session->userdata('userfile')){               

                return TRUE;

            }else{

                if(!empty($_FILES['userfile']['tmp_name'])){
                  $img=getimagesize($_FILES['userfile']['tmp_name']);
                  $maximum = array('width' => '300', 'height' => '252');
                  $width= $img[0];
                  $height =$img[1];
                    if($width > $maximum['width'] || $height > $maximum['height']){
                        $this->form_validation->set_message('userfile_check', "Image dimensions are too small. maximum width x height is {$maximum['width']} x{$maximum['height']}.");
                        return FALSE;
                    }
                }
                $param=array(
                    'file_name' =>'userfile',
                    'upload_path'  => './assets/uploads/profile_photo/',
                    'allowed_types'=> 'gif|jpg|png|jpeg',
                    //'image_resize' => TRUE,
                    'source_image' => './assets/uploads/profile_photo/',
                    'new_image'    => './assets/uploads/profile_photo/thumb/',
                    'encrypt_name' => TRUE,
                );

                $this->load->library('upload');
                $this->upload->initialize($param);
                
                if (!$this->upload->do_upload('userfile'))
                {

                      $this->form_validation->set_message('userfile_check', $this->upload->display_errors());                
                      return FALSE;
                } else {

                      $upload_file = array('UPLOAD_DATA' => $this->upload->data());                    
                      $config['image_library'] = 'gd2';
                      $config['source_path'] = $upload_file['UPLOAD_DATA']['file_path'];
                      $config['file_name']   = $upload_file['UPLOAD_DATA']['file_name'];
                      $config['destination_path'] = $param['new_image'];
                      $config['width'] =  250;
                      $config['height'] = 250;
                      create_thumbnail($config);
                      if($upload_file['UPLOAD_DATA']){
                      $this->session->set_userdata('userfile',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));
                      return TRUE;    
                }     

                      

                }



            }

        }

    }



    public function check_email($new_email){   

        if($_POST){

           $user_info = $this->user_model->get_row('users', array('id'=>user_id()));  

           if($_POST['email']!=$user_info->email){

              $resp = $this->user_model->get_row('users', array('email'=>$_POST['email']));

              if($resp){

                  $this->form_validation->set_message('check_email', 'Email already exist.');

                  return FALSE;

              } else{

                  return TRUE;

              }

           }else{

              return TRUE;

           }

        }   

    }





    public function change_password()
    {
        _check_user_login();  //check login authentication
        $this->form_validation->set_rules('oldpassword', 'Old Password', 'required|callback_password_check');
        $this->form_validation->set_rules('newpassword', 'New Password', 'required|matches[confpassword]');
        $this->form_validation->set_rules('confpassword','Confirm Password', 'required');
        $this->form_validation->set_error_delimiters('<div style="color:red;" >', '</div>');  
        if($this->form_validation->run() == TRUE){
            $user_data  = array('password' => sha1($this->input->post('newpassword')));
            if($this->user_model->update('users',$user_data,array('id'=>user_id()))){       
              $this->session->set_flashdata('msg_success','Password updated successfully.');
              redirect('user/change_password');
            } else {
              $this->session->set_flashdata('msg_error','Update failed, Please try again.');
              redirect('user/change_password');
            }
        }
        $data['technology']=$this->common_model->get_result('technologies',array('status'=>1));
        $data['template'] = "user/change_password";
        $this->load->view('templates/frontend/layout', $data);
    }



    public function password_check($oldpassword){

      //$this->load->model('user_model');      

      if($this->user_model->password_check(array('password'=>sha1($oldpassword)),user_id())){

        return TRUE;

      }else{

        $this->form_validation->set_message('password_check', 'The %s does not match.');

        return FALSE;

      }       

    }

   public function forgot_password()
   {

      if(user_logged_in()){
        redirect('user/dashboard');
      }
      $data['title_top'] = 'Forgot password';

      $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

      $this->form_validation->set_error_delimiters('<div style="color:red;" class="error">', '</div>');

      if($this->form_validation->run() == TRUE) {

            if($user = $this->user_model->get_row('users', array('email'=>trim($_POST['email'])))){

              $new_password_key = trim(md5($user->email));
              $user->id;
              $user->email;
              $this->user_model->update('users', array('new_password_key' => $new_password_key,'new_password_requested' => date('Y-m-d H:i:s')), array('id' => $user->id));
              $this->load->library('developer_email');
              $email_template = $this->developer_email->get_email_template(8);
              $param = array(

                  'template' => array(

                      'temp' => $email_template->template_body,

                      'var_name' => array(

                          'site_name'=>SITE_NAME,

                          'full_name' => $user->first_name.' '.$user->last_name,

                          'reset_url' => base_url() . 'user/reset_password/' . $new_password_key

                      ),

                  ),

                  'email' => array(
                      'to' => $user->email,
                      'from' => NO_REPLY_EMAIL,
                      'from_name' => NO_REPLY_EMAIL_FROM_NAME,
                      'subject' => $email_template->template_subject,
                  )

              );

              $status = $this->developer_email->send_mail($param);
              $msg_success = 'Instructions have been sent on '.$user->email.'.Please check your email,Your reset password link send to your account.';
              $this->session->set_flashdata('msg_success', $msg_success);



            } else {

                $this->session->set_flashdata('msg_error', 'Your Email address not found in our Services.');
           
            }

            redirect('user/forgot_password');

        }
        $data['technology']=$this->common_model->get_result('technologies',array('status'=>1));
        $data['template'] = 'user/forget_password';
        $this->load->view('templates/frontend/layout', $data);
    }


    public function pro_download($token_id='',$unique_id='')
    {
        if(empty($token_id) || empty($unique_id)){ redirect('projects/');   }
        $project_check = $this->common_model->get_row('projects',array('unique_id'=>$unique_id,'status'=>1));
        if(empty($project_check)) redirect('projects/'); 
        $order_info = $this->common_model->get_row('orders',array('unique_id'=>$unique_id,'token_id'=>$token_id));
        if(empty($order_info)) redirect('projects/'); 


        if($order_info->download_count<3){
          
          $projects = $this->common_model->get_row('projects',array('unique_id'=>$order_info->unique_id,'status'=>1));
          if(!empty($projects) && !empty($projects->id)){
              $this->source_file_download_user($projects->unique_id,$projects->id);

              increase_download_count($order_info->unique_id,$order_info->download_count);
              $msg_success = 'Thanks For downloading';
              $this->session->set_flashdata('msg_success', $msg_success);
              redirect('projects/');
            
          }else{
              redirect('projects/');
          }

        }else{
            $data['template'] = 'projects/dowloadExpired';
            $this->load->view('templates/frontend/layout', $data);
        }
    }


    public function source_file_download_user($unique_id='',$pro_id='')
    {
        $this->load->helper('download');
        if(empty($pro_id)) redirect('projects/');
        $data['project'] = $this->common_model->get_row('projects',array('id'=>$pro_id,'unique_id'=>$unique_id));
        if(empty($data['project'])) redirect('projects/');





      $file_path = str_replace('./','',$data['project']->main_file);
      $file_array = explode('/',str_replace('./','',$data['project']->main_file));
        // $file = 'monkey.gif';
        if (file_exists($file_path)) {

//      echo  $name = $file_array[3];
// die();
             header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file_path).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file_path));
            readfile($file_path);
             return TRUE;
        }
       // $data = file_get_contents($file_path); // Read the file's contents
        // $name = $file_array[3];
        // if(force_download($name,$data)){
        //    return TRUE;
        // } else{
        //    return FALSE;
        // }
    }



     public function reset_password($activation_key = ''){



        $data['title_top'] = 'Reset password';

        $this->form_validation->set_rules('password', 'New Password', 'required|matches[confpassword]');

        $this->form_validation->set_rules('confpassword', 'Confirm Password', 'required');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == TRUE) {

            $user = $this->user_model->get_row('users', array('new_password_key' => trim($activation_key)));

            if ($user == FALSE) {

                $this->session->set_flashdata('msg_error', 'Your activation key expired.');

                redirect('user/reset_password');

            }



            $user_data = array(
              'password' => sha1(trim($this->input->post('password'))),
              'new_password_key' => ''
            );

            if ($this->user_model->update('users', $user_data, array('id' => $user->id))) {

                $this->session->set_flashdata('msg_success', 'Your Password has reset successfully now you can Login.');

                redirect('user/login');

            }

      }

      $data['technology']=$this->common_model->get_result('technologies',array('status'=>1));
      $data['courses'] = $this->common_model->get_result('courses',array('status'=>1));
      $data['template'] = 'user/reset_password';
      $this->load->view('templates/frontend/layout', $data);

    }

    public function upload_project($value='')
    {
      _check_user_login();  //check login authentication
      $userId = user_id();
      $user = $data['customer'] = $this->user_model->get_row('users',array('id'=>$userId));
      
      $this->load->model(array('project_model' , 'package_model'));
      $projectCount = $this->project_model->getUserProjectCount($userId);
      if($user->package_id == 0){
          $packageId  = 1;
    }else{
        $packageId = $user->package_id ;
    }
        
      
      $userPackage = $this->package_model->getById($packageId);
      
      if($userPackage->limit <= $projectCount){
        $this->session->set_flashdata('msg_error','You have exceeded the limit of project posting. Please upgrade your membership to post more projects!!!');
        redirect('user/projects/');
      }
      
      $project_files = $this->session->userdata('project_file');
      if(!empty($_POST)){
        if(empty($project_files)) {
          $this->session->set_flashdata('msg_error','Please upload source files first and properly.');
        }
      }
      $this->form_validation->set_rules('title','Title','trim|required');
      $this->form_validation->set_rules('description','Description', 'trim|required');
      $this->form_validation->set_rules('short_description','Short Description', 'trim|required');
      $this->form_validation->set_rules('price','Price', 'trim|required|numeric');
      $this->form_validation->set_rules('form_pages','Form Pages', 'trim|required|numeric');
      $this->form_validation->set_rules('no_of_tables','No Of Tables', 'trim|required|numeric');
      $this->form_validation->set_rules('backend_technology','Backend Technology', 'trim|required');
      $this->form_validation->set_rules('technology[]','technology', 'trim|required');
      $this->form_validation->set_rules('course[]','Course', 'trim|required');
      
      $this->form_validation->set_rules('featured_image','Freatured Image','callback_featured_file_add');
      $this->form_validation->set_rules('doc_file','Document File','callback_docfile_check_add');
      $this->form_validation->set_error_delimiters('<div style="color:red;" class="error">','</div>');
      

      if($this->form_validation->run() == TRUE &&  !empty($project_files)){
       

          $technology = "#".implode('#,#',$this->input->post('technology'))."#";
          $course     = "#".implode('#,#',$this->input->post('course'))."#";
        
          $project = array(
                            'user_id'=>user_id(),
                           'unique_id'=>random_string('alnum',8),
                            'title'         => $this->input->post('title'),
                            'slug'        => url_title($this->input->post('title'),'-',TRUE),
                            'technology'    => $technology,
                            'course_type'        => $course,
                            'short_description' => $this->input->post('short_description'),
                            'description'   =>htmlspecialchars($this->input->post('description')),
                            'price'         => $this->input->post('price'),
                            'video_url'=>$this->input->post('video_url'),
                            'backend_technology'=>$this->input->post('backend_technology'),
                            'form_pages'=>$this->input->post('form_pages'),
                            'no_of_tables'=>$this->input->post('no_of_tables'),
                            //'discount_item'=>$this->input->post('discount_item'),
                            'featured_project'=>$this->input->post('featured_item'),
                            'status'    =>0,
                            'created'=> date('Y-m-d h:i:s')
                        );


          if($this->session->userdata('project_file')):
                $project_file=$this->session->userdata('project_file');   
                $project['main_file'] = $project_file['file_path']; 
          endif;
          if($this->session->userdata('doc_file')):
                $doc_file=$this->session->userdata('doc_file');   
                $project['doc_file'] = $doc_file['image']; 
          endif;
          if($this->session->userdata('featured_image')):
                $featured_image=$this->session->userdata('featured_image');   
                $project['main_image']  = $featured_image['image']; 
                $project['thumb_image'] = $featured_image['thumb_image'];
          endif;


          if($project_id=$this->user_model->insert('projects',$project)){
            if($this->session->userdata('project_file')): 
                  $this->session->unset_userdata('project_file');
            endif;   
            if($this->session->userdata('doc_file')): 
                  $this->session->unset_userdata('doc_file');
            endif;
            if($this->session->userdata('featured_image')): 
                $this->session->unset_userdata('featured_image');
            endif;
            $this->session->set_flashdata('msg_success','Project added successfully.');
            redirect('user/projects/');

          }
      }

      
      $data['technology']=$this->common_model->get_result('technologies',array('status'=>1));
      $data['courses'] = $this->common_model->get_result('courses',array('status'=>1));
      $data['template'] = 'user/upload_project';
      $this->load->view('templates/frontend/layout', $data);
    }

    public function uploaded_project_edit($pro_id=''){
          _check_user_login(); //check login authentication
          if(empty($pro_id)) redirect('user/projects/');
          $data['project'] = $this->common_model->get_row('projects',array('id'=>$pro_id,'user_id'=>user_id()));
          if(empty($data['project'])) redirect('user/projects/');

          $this->form_validation->set_rules('title', 'Project Title', 'trim|required');
          $this->form_validation->set_rules('description','Description', 'trim|required');
          $this->form_validation->set_rules('short_description','Short Description', 'trim|required');
          $this->form_validation->set_rules('form_pages','Form Pages', 'trim|required|numeric');
          $this->form_validation->set_rules('no_of_tables','No Of Tables', 'trim|required|numeric');
          $this->form_validation->set_rules('backend_technology','Backend Technology', 'trim|required');

          $this->form_validation->set_rules('price','Price', 'trim|required|numeric');
          $this->form_validation->set_rules('technology[]','technology', 'trim|required');
          $this->form_validation->set_rules('course[]','Course', 'trim|required');
          if(!empty($_FILES['userfile']['name'])){
              $this->form_validation->set_rules('userfile','image','callback_userfile_check_add');
          }
          if(!empty($_FILES['featured_image']['name'])){
            $this->form_validation->set_rules('featured_image','Freatured Image','callback_featured_file_add');
          }
          if(!empty($_FILES['doc_file']['name'])){
            $this->form_validation->set_rules('doc_file','Document File','callback_docfile_check_add');
          }
          $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
          if($this->form_validation->run()==TRUE){  
            $technology = "#".implode('#,#',$this->input->post('technology'))."#";
            $course     = "#".implode('#,#',$this->input->post('course'))."#";
            $project = array(

                      'title' => $this->input->post('title'),         
                      'slug'  => url_title($this->input->post('title'),'-',TRUE),       
                      'technology' => $technology,
                      'course_type'     => $course,
                      'description' =>htmlspecialchars($this->input->post('description')),      
                      'short_description' => $this->input->post('short_description'),
                      'price' => $this->input->post('price'),           
                      'video_url'=>$this->input->post('video_url'),
                      'backend_technology'=>$this->input->post('backend_technology'),
                      'form_pages'=>$this->input->post('form_pages'),
                      'no_of_tables'=>$this->input->post('no_of_tables'),
                      // 'discount_item'=>$this->input->post('discount_item')
                      'modified'=> date('Y-m-d h:i:s')
                      );
            
                    if($this->session->userdata('userfile')):
                        $userfile=$this->session->userdata('userfile');   
                        $project['main_file'] = $userfile['image'];  
                        if(!empty($data['project'])){
                          $new_file=$data['project']->main_file;
                          @unlink($new_file);
                        }        
                    endif;  

                  if($this->session->userdata('doc_file')):
                      $doc_file=$this->session->userdata('doc_file');   
                      $project['doc_file'] = $doc_file['image'];  
                      if(!empty($data['project'])){
                        $new_file=$data['project']->doc_file;
                        @unlink($new_file);
                      }        
                  endif;    

                  if($this->session->userdata('featured_image')):
                      $featured_image=$this->session->userdata('featured_image');   
                      $project['main_image'] = $featured_image['image'];  
                      $project['thumb_image'] = $featured_image['thumb_image'];
                      if(!empty($data['project'])){
                        $new_file=$data['project']->main_image;
                        $thumb_file=$data['project']->thumb_image;
                        @unlink($new_file);
                        @unlink($thumb_file);
                      }        
                  endif;

            if($this->common_model->update('projects',$project,array('id' =>$data['project']->id,'user_id'=>user_id()))){
                if($this->session->userdata('userfile')): 
                        $this->session->unset_userdata('userfile');
                endif;
                if($this->session->userdata('doc_file')): 
                      $this->session->unset_userdata('doc_file');
                endif;
                if($this->session->userdata('featured_image')): 
                    $this->session->unset_userdata('featured_image');
                endif;
              $this->session->set_flashdata('msg_success','Project Upadted successfully.');
              redirect('user/projects');
            }

          }
          $data['technology'] = $this->common_model->get_result('technologies',array('status'=>1));
          $data['courses'] = $this->common_model->get_result('courses',array('status'=>1));
          $data['template']='user/uploaded_project_edit';
          $this->load->view('templates/frontend/layout',$data);  

    }



    public function userfile_check_add(){              
        if($this->session->userdata('userfile')){                   
            return TRUE;
        }else{
            $param=array(
                'file_name'    => 'userfile',
                'upload_path'  => './assets/uploads/projects/',
                'allowed_types'=> 'zip|rar|pdf|doc|docx|ppt|pptx|xml|ppt',
                'source_image' => './assets/uploads/projects/',
                'max_size'     => '10000000000000',
                'encrypt_name' => TRUE,
            ); 
            $this->load->library('upload',$param);    
      if(!$this->upload->do_upload('userfile')){      
        $this->form_validation->set_message('userfile_check_add', $this->upload->display_errors());              
          return FALSE;   
      }else{      
          $upload_file=$this->upload->data();     
        $this->session->set_userdata('userfile',array('image'=>$param['upload_path'].$upload_file['file_name']));            
                return TRUE;
      }                  
        }     
    }



    public function docfile_check_add(){              
        if($this->session->userdata('doc_file')){                   
            return TRUE;
        }else{
            $param1=array(
                'file_name'    => 'doc_file',
                'upload_path'  => './assets/uploads/projects/',
                'allowed_types'=> 'pdf|doc|docx|ppt',
                'source_image' => './assets/uploads/projects/',
                'max_size'     => '10000000000000',
                'encrypt_name' => TRUE,
            ); 
            $this->load->library('upload',$param1);   
            if(!$this->upload->do_upload('doc_file')){  
              
                $this->form_validation->set_message('docfile_check_add', $this->upload->display_errors());              
                return FALSE;   
            }else{    
                $upload_file=$this->upload->data();     
                $this->session->set_userdata('doc_file',array('image'=>$param1['upload_path'].$upload_file['file_name']));            
                return TRUE;
            }                  
        }     
    }

    public function featured_file_add($str){
        if($this->session->userdata('featured_image')){               
            return TRUE;
        }else{
            $param=array(
                      'file_name' =>'featured_image',
                      'upload_path'  => './assets/uploads/project_files/',
                      'allowed_types'=> 'gif|jpg|png|jpeg',
                      'image_resize' => TRUE,
                      'source_image' => './assets/uploads/project_files/',
                      'new_image'    => './assets/uploads/project_files/thumb/',
                      'resize_width' => 200,
                      'resize_height' => 200,
                        'encrypt_name' => TRUE,
                  );
            $upload_file=upload_file($param);
            if($upload_file['STATUS']){
                $this->session->set_userdata('featured_image',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));            
                return TRUE;        
            }else{          
                $this->form_validation->set_message('featured_file_add', $upload_file['FILE_ERROR']);              
                return FALSE;
            }
        }
    }

    public function download_doc_file($pro_id='')
    {
        _check_user_login(); //check login authentication
        $this->load->helper('download');
        if(empty($pro_id)) redirect('user/projects/');
        $data['project'] = $this->common_model->get_row('projects',array('id'=>$pro_id));
        if(empty($data['project'])) redirect('backend/projects/');
        $file_path = base_url(str_replace('./','',$data['project']->doc_file));
        $file_array = explode('/',str_replace('./','',$data['project']->doc_file));
          $data = file_get_contents($file_path); // Read the file's contents
        $name = $file_array[3];
        if(force_download($name, $data)){
          redirect('user/uploaded_project_edit/'.$pro_id);
        } 
    }

    public function download_source_file($pro_id='')
    {
        _check_user_login(); //check login authentication
        $this->load->helper('download');
        if(empty($pro_id)) redirect('user/projects/');
        $data['project'] = $this->common_model->get_row('projects',array('id'=>$pro_id));
        if(empty($data['project'])) redirect('user/projects/');
        $file_path = base_url(str_replace('./','',$data['project']->main_file));
        $file_array = explode('/',str_replace('./','',$data['project']->main_file));
          $data = file_get_contents($file_path); // Read the file's contents
        $name = $file_array[3];
        if(force_download($name, $data)){
          redirect('user/uploaded_project_edit/'.$pro_id);
        } 
    }


    public function delete($pro_id=''){
        _check_user_login(); //check login authentication
        if(empty($pro_id)) redirect('user/projects');
        $data['project'] = $this->user_model->get_row('projects',array('id'=>$pro_id,'user_id'=>user_id()));
        if(empty($data['project'])) redirect('user/projects/');
          if(!empty($data['project'])){
            $new_file=$data['project']->main_file;
            @unlink($new_file);
          }        
          if(!empty($data['project'])){
            $new_file=$data['project']->doc_file;
            @unlink($new_file);
          }                
          if(!empty($data['project'])){
            $new_file=$data['project']->main_image;
            $thumb_file=$data['project']->thumb_image;
            @unlink($new_file);
            @unlink($thumb_file);
          }        
        $this->user_model->delete('projects',array('id'=>$pro_id));
        $this->session->set_flashdata('msg_success','project has been deleted successfully.');
        redirect('user/projects');   
    }



    public function overview($value='')
    {
       $data['category']=$this->common_model->get_result('categories',array('status'=>1));
       $data['template'] = 'user/overview';
       $this->load->view('templates/frontend/layout', $data);
    }

    public function favorites($value='')
    {
       $data['products']=$this->common_model->get_result('favorite_product',array('user_id'=>user_id()));
       $data['category']=$this->common_model->get_result('categories',array('status'=>1));
       $data['template'] = 'user/favorites';
       $this->load->view('templates/frontend/layout', $data);
    }

    public function order_view($order_id='')
    {
        _check_user_login();  //check login authentication
        if(empty($order_id)) redirect('user/orders');
        $data['order'] = $this->common_model->get_row('orders',array('id'=>$order_id,'user_id'=>user_id()));
        if(empty($data['order'])) redirect('user/orders');
        $data['order_user_info'] = $this->common_model->get_row('order_info',array('order_id'=>$order_id));
        $data['orders_products'] = $this->common_model->get_result('orders_products',array('orders_id'=>$order_id));
        if(empty($data['orders_products']) && empty($data['order_user_info'])) redirect('user/orders');
        $data['category']=$this->common_model->get_result('categories',array('status'=>1));
        $data['template'] = 'user/order_view';
        $this->load->view('templates/frontend/layout', $data);
    }   



    public function orders($value='')
    {
      _check_user_login();  //check login authentication
      $data['orders']=$this->common_model->get_result('orders',array('user_id'=>user_id()));
      $data['category']=$this->common_model->get_result('categories',array('status'=>1));
      $data['template'] = 'user/orders';
      $this->load->view('templates/frontend/layout', $data);
    } 

    public function projects($sort_by='id',$sort_order='desc',$offset=0)
    {
        _check_user_login();  //check login authentication
        $per_page=5;
        $data['projects'] = $this->user_model->projects($offset,$per_page,$sort_by,$sort_order);
        $data['offset'] = $offset;
        $config=backend_pagination();
        $config['base_url'] = base_url().'user/projects/'. $sort_by.'/'.$sort_order.'/';
        $config['total_rows'] = $this->user_model->projects(0,0,$sort_by,$sort_order);
        $config['per_page'] = $per_page;
        $config['uri_segment']=6;
        if(!empty($_SERVER['QUERY_STRING'])){
            $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
        }
        $this->pagination->initialize($config);
        $data['pagination']=$this->pagination->create_links();
        $data['sort_by']=$sort_by;
        $data['sort_order']=$sort_order;
        $data['technology'] = $this->user_model->get_result('technologies',array('status'=>1));
        $data['template'] = 'user/projects';
        $this->load->view('templates/frontend/layout', $data);
    }

    public function project_reviews($value='')
    {
      _check_user_login();  //check login authentication
      $data['technology']=$this->common_model->get_result('technologies',array('status'=>1));
      $data['reviews']=$this->common_model->get_result('project_reviews',array('user_id'=>user_id()));
      $data['customer'] = $this->user_model->get_row('users',array('id'=>user_id(),'user_role'=>1));
      $data['template'] = 'user/project_reviews';
      $this->load->view('templates/frontend/layout', $data);
    }

    public function project_reviews_edit($review_id='')
    {
        _check_user_login();  //check login authentication
        if(empty($review_id)) redirect('user/project_reviews/');
        $data['reviews'] = $this->common_model->get_row('project_reviews',array('id'=>$review_id,'user_id'=>user_id()));
        if(empty($data['reviews'])) redirect('user/project_reviews/');
        $this->form_validation->set_rules('review_message','Review Message','trim|required');
        $this->form_validation->set_rules('user_email','Email','trim');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if($this->form_validation->run() == TRUE){
            $review_data = array(
                                'message'=>$this->input->post('review_message'),
                              ); 
            if($this->user_model->update('project_reviews', $review_data, array('id'=>$data['reviews']->id,'user_id'=>user_id()))) {
              $this->session->set_flashdata('msg_success', 'Your Review has been updated successfully.');
              redirect('user/project_reviews/');
            }
        }
        $data['technology']=$this->common_model->get_result('technologies',array('status'=>1));
        $data['template'] = 'user/project_review_edit';
        $this->load->view('templates/frontend/layout', $data);
    }

    public function project_review_delete($review_id='')
    {
        _check_user_login();  //check login authentication
        if(empty($review_id)) redirect('user/project_reviews/');
        $this->user_model->delete('project_reviews',array('id'=>$review_id));
        $this->session->set_flashdata('msg_success','Project Review has been deleted successfully.');
        redirect('user/project_reviews/');
    }

    public function project_file_upload()
    { 
         _check_user_login();  //check login authentication
        $ret = array();
        if(!empty($_FILES))
        {
          if($this->session->userdata('project_file')) {
            $this->session->unset_userdata('project_file');
          }
          if(!empty($_FILES['myfile']['name'])){
              $param=array(
                          'file_name' => 'myfile',
                          'upload_path' => './assets/uploads/projects/',
                          'allowed_types' => 'zip|rar',
                          'encrypt_name' => TRUE
                        );
              $filepath='./assets/uploads/products/';
              $upload_file = upload_file($param);
              if($upload_file['STATUS']){
                $ret[]= $_FILES["myfile"]["name"];
                $this->session->set_userdata('project_file',array('file_path'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'] ,'client_name'=>$upload_file['UPLOAD_DATA']['client_name'] ));
                echo json_encode($ret);
                return TRUE;   
              } else {
                  $ret[]=$upload_file['FILE_ERROR'];   
                  echo json_encode($ret);         
                  return FALSE;
              }

          } 

        }   
    }
    public function project_file_delete($value='')
    {
        _check_user_login();  //check login authentication
        if(!empty($_POST['op']) && $_POST['op']=='delete'){

            if($this->session->userdata('project_file')){
               $file_data = $this->session->userdata('project_file');
               $file_path= $file_data['file_path'];
               if(!empty($file_path)) unlink($file_path);
               $this->session->unset_userdata('project_file');
               echo 'true';
            }

        }
    }

    public function reupload_file($value='')
    {
        
        _check_user_login();  //check login authentication
        if($this->session->userdata('project_file')){
           $file_data = $this->session->userdata('project_file');
           $file_path= $file_data['file_path'];
           if(!empty($file_path)) unlink($file_path);
           $this->session->unset_userdata('project_file');
           $this->session->set_flashdata('msg_success','Now you can use fresh set up, your data has been cleared successfully.');
           redirect('user/upload_project/');
        }else{

          $this->session->set_flashdata('msg_error','there was problem but system has removed your data. Go Ahead..');
          redirect('user/upload_project/');

        }
      
    }
    
    public function earning()
    {
      _check_user_login();  //check login authentication
      $uId = $this->session->userdata('user_info');
      $data['totalEarning'] = $this->user_model->getTotalEarning($uId['id']);
      $data['allEarning'] = $this->user_model->getAllEarning($uId['id']);
      //print_r($data['allEarning']);
      $data['template'] = 'user/earning';
      $this->load->view('templates/frontend/layout', $data);
    }
    
    public function userOrder()
    {
      _check_user_login();  //check login authentication
      $uId = $this->session->userdata('user_info');
      $data['totalEarning'] = $this->user_model->getTotalEarning($uId['id']);
      $data['allEarning'] = $this->user_model->getAllEarning($uId['id']);
      //print_r($data['allEarning']);
      $data['template'] = 'user/order';
      $this->load->view('templates/frontend/layout', $data);
    }
    
    public function requestForPayment()
    {
      _check_user_login();  //check login authentication
      //print_r($_POST);
        $this->form_validation->set_rules('withdrawal_amount','Withdrawal Amount','required|numeric');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if($this->form_validation->run() == TRUE){
            $uId = $this->session->userdata('user_info');
            $userId = $uId['id'];
            $data = array(
                            'amount' => $this->input->post('withdrawal_amount'),
                            'user_id' => $userId,
                          ); 
            if($this->user_model->insert('request_payment', $data)) {
              $this->session->set_flashdata('msg_success', 'Your Request Has been sent successfully.');
              redirect('user/requestForPayment/');
            }
        }
      $uId = $this->session->userdata('user_info');
      $data['userDetails'] = $this->user_model->getUserDetailsById($uId['id']);
      $data['totalEarning'] = $this->user_model->getTotalEarning($uId['id']);
      $data['allEarning'] = $this->user_model->getAllEarning($uId['id']);
      //print_r($data['userDetails']);
      $data['template'] = 'user/requestforayment';
      $this->load->view('templates/frontend/layout', $data);
    }
    
    function projectRequestForm(){
        //$data['template'] = 'user/project_request';
        $this->load->view('user/project_request');
    }
    
    function projectRequest(){
        $this->load->helper(array('form', 'url'));
        
        $this->form_validation->set_rules('email','Email Address','required|valid_email');
        $this->form_validation->set_rules('mobile_no','Mobile Number','required|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('project_title','Project Title','required');
        $this->form_validation->set_rules('description','Description','required');
        
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if($this->form_validation->run() == TRUE){
        
        if(!empty($_FILES['document']['name'])){
            if($this->session->userdata('document')){               
                return TRUE;
            }else{
                $param['upload_path'] = './assets/uploads/requestproject/';
                $param['allowed_types'] = 'pdf|doc|docx';
                $param['max_size'] = '10000';
                $param['encrypt_name'] =TRUE;
                $this->load->library('upload', $param);
                $this->upload->initialize($param);

                if(!$this->upload->do_upload('document'))
                {
                      $this->form_validation->set_message('document_check', $this->upload->display_errors()); 
                      return FALSE;
                } else {  
                    $upload_data = $this->upload->data();
                    
                }
            }
        }
            
        
            
        
            $docName = ($upload_data)?$upload_data['file_name']:'';
        
            $uId = $this->session->userdata('user_info');
            $userId = $uId['id'];
            if($userId == NULL){
                $userId=0;
            }else{
                $userDetails = $this->user_model->get_row('users',array('id' => $userId));
            }
            $data = array(
                'email' => (isset($userDetails) && !empty($userDetails))?$userDetails->email:$this->input->post('email'),
                'mobile_no' => $this->input->post('mobile_no'),
                'title' => $this->input->post('project_title'),
                'description' => $this->input->post('description'),
                'filename' => $docName,
                'user_id' => $userId,
            ); 
            $this->user_model->insert('project_request', $data);
            $this->session->set_flashdata('msg_success', 'Request submitted successfully.');
            redirect('user/projects');
        }else{
            $this->session->set_flashdata('req_msg_error', validation_errors());
            redirect('user/projects');
        }
    redirect($_SERVER['HTTP_REFERER']);
}
    

}