<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blog extends MY_Controller {

    public function __construct() {
        parent::__construct();
        clear_cache();
    }

    public function index() {
        $this->posts();
    }

    public function posts($offset = 0) {
        $data['head_title'] = 'Blogs';
        $data['blog'] = $this->common_model->blog_favorite();
        $per_page = 10;
        $data['offset'] = $offset;
        $data['blogs'] = $this->common_model->blogs($offset, $per_page);
        $config = frontend_pagination();
        $config['base_url'] = base_url() . 'blog/posts/';
        $config['total_rows'] = $this->common_model->blogs(0, 0);
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['technology'] = $this->common_model->get_result('technologies', array('status' => 1));

        $data['user_id_name_arr'] = get_user_name_id();
        $data['template'] = 'blog/blog';
        $this->load->view('templates/frontend/layout', $data);
    }

    public function blog_detail($slug = '') {
        $data['head_title'] = 'Blogs';
        $data['blog'] = $this->common_model->blog_favorite();
        if (empty($slug))
            redirect('blog/posts');
        $data['blog_detail'] = $this->common_model->get_row('blogs', array('blog_slug' => $slug));
        if (empty($data['blog_detail']))
            redirect('blog/posts');
        //$this->form_validation->set_rules('user_name', 'User Name', 'required');
        //$this->form_validation->set_rules('user_email', 'User Email', 'required|valid_email');
        $this->form_validation->set_rules('comment', 'Comment', 'required');
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            if (user_logged_in() === TRUE) {
                
                $user_info = get_user_info();
                $array = array(
                    'comment_blog_id' => $data['blog_detail']->id,
                    'user_id' => user_id(),
                    'comment_author' => $user_info->first_name.' '.$user_info->last_name,
                    'comment_author_email' =>  $user_info->email,
                    'comment_content' => $this->input->post('comment'),
                    'comment_date' => date('Y-m-d h:i:s'),
                    'comment_author_ip' => $this->input->ip_address(),
                    'comment_author_url' => current_url(),
                    'comment_approved' => 1
                );

                //increase_comments($blog_id,1,$data['blog_detail']->comment_count);

                if ($this->common_model->insert('comments', $array)) {
                    $this->session->set_flashdata('msg_success', 'Thank you! Your comment has been submitted and is awaiting approval');
                    redirect('blog/blog_detail/' . $data['blog_detail']->blog_slug);
                } else {
                    $this->session->set_flashdata('msg_error', 'Update failed, Please try again.');
                    redirect('blog/blog_detail/' . $data['blog_detail']->blog_slug);
                }
            }
        }
        $data['template'] = 'blog/blog_info';
        $this->load->view('templates/frontend/layout', $data);
    }

    public function fbshare($id = NULL, $url = '') {
        $data['result'] = $this->common_model->get_row('blogs', array('id' => $id));
        if ($url == '')
            $data['url'] = base_url() . 'blog/blog_detail/' . $data['result']->id;
        else
            $data['url'] = $url;
        $this->load->view('blog_fbshare', $data);
    }

    public function more_blog_comments() {
        if ($_POST) {
            $blog_id = $this->input->post('blog_id');
            $offset = $this->input->post('offset');
            $comment = $this->common_model->get_more_comment($blog_id, $offset);
            if (!empty($comment)) {
                $resp = '';
                foreach ($comment as $row) {
                    $resp.='<div class="media">';
                    $resp.='<a href="#" class="pull-left">';
                    $resp.='<img src="' . base_url() . 'assets/front_end/images/comments_thumb.png" alt="" class="media-object">';
                    $resp.='</a>';
                    $resp.='<div class="media-body">';
                    $resp.='<h4 class="media-heading">' . $row->comment_author . '</h4>';
                    $resp.='<span class="smallest_text">' . date("M d, Y ", strtotime($row->comment_date)) . 'at' . timespan(strtotime($row->comment_date)) . '</span> ';
                    $resp.='</div>';
                    $resp.='</div>';
                    $resp.='<div class="well">' . $row->comment_content . '</div>';
                }

                echo $resp;
            } else {
                echo '';
            }
        }
    }

    public function check_user_login() {
        if ($_GET) {
            if (user_logged_in() === FALSE) {
                echo "FALSE";
                return FALSE;
            } else {
                echo "TRUE";
                return TRUE;
            }
        }
    }

}
