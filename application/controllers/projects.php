<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Projects extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('message', 'english');
        $this->load->model(array('common_model','project_model'));
    }

    public function index($sort_by='id',$sort_order='desc',$offset=0) {
        $data['courses'] = $this->common_model->get_result('courses', array('status' => 1));
        $data['technology'] = $this->common_model->get_result('technologies', array('status' => 1));
        //$data['projects'] = $this->common_model->projects();
        $per_page = 5;
        $data['projects'] = $this->project_model->projects($per_page,$offset,$sort_by,$sort_order);
        
        $config = backend_pagination();
        $config['base_url'] = base_url().'projects/index/'. $sort_by.'/'.$sort_order.'/';
            $config['per_page'] = $per_page;
        $config['uri_segment']=5;
        $config['total_rows'] = $this->project_model->projects(0, 0,$sort_by,$sort_order);
        if(!empty($_SERVER['QUERY_STRING'])){
            $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
        }

        $this->pagination->initialize($config);

        $data['pages'] = $this->common_model->get_result('posts', array('post_type' => 'page', 'post_status' => 'publish'));
        $data['pagination'] = $this->pagination->create_links();
        
        $data['template'] = 'projects/index';
        $this->load->view('templates/frontend/layout', $data);
    }
    
    public function technology($id,$sort_by='id',$sort_order='desc',$offset=0)
    {
        $data['courses'] = $this->common_model->get_result('courses', array('status' => 1));
        $data['technology'] = $this->common_model->get_result('technologies', array('status' => 1));
        
        $data['catId'] = $cat_id;
        $options['technology'] = $id;
        $per_page = 5;
        $data['projects'] = $this->common_model->projects($offset, $per_page);
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'projects/projects/'.$id;
        $config['uri_segment']=4;
        $config['total_rows'] = $this->common_model->projects(0, 0);

        $config['per_page'] = $per_page;

        $this->pagination->initialize($config);

        $data['pages'] = $this->common_model->get_result('posts', array('post_type' => 'page', 'post_status' => 'publish'));
        $data['pagination'] = $this->pagination->create_links();
        
        $data['template'] = 'projects/index';
        $this->load->view('templates/frontend/layout', $data);
    }
    
    function get_projects() {
        if ($_POST) {
            $resop = '';
            if (!empty($_POST['course'])) {
                $course_ids = $_POST['course'];
            } else {
                $course_ids = '';
            }
            if (!empty($_POST['tech'])) {
                $techno_ids = $_POST['tech'];
            } else {
                $techno_ids = '';
            }
            $data['projects'] = $this->common_model->get_filter_projects($techno_ids, $course_ids, 0, 5);
            if (!empty($data['projects'])) {
                foreach ($data['projects'] as $value) {
                    $resop.= $this->load->view('templates/frontend/widgets/projectBox' , array('value' => $value) , true);
//                    $resop.='<div class="list-block">';
//                    $resop.='<div class="col-md-1 post-date-warp">';
//                    $resop.='<div class="post-date text-center">';
//                    $resop.='<i class="fa fa-calendar"></i>';
//                    $resop.='<h4>' . date('d', strtotime($value->created)) . '</h4>';
//                    $resop.='<h5>' . date('M', strtotime($value->created)) . '</h5>';
//                    $resop.='</div>';
//                    $resop.='<hr>';
//                    $resop.='<div class="post-date text-center">';
//                    $resop.='<i class="fa fa-thumbs-o-up"></i>';
//                    $total_like = get_total_like_of_project($value->id);
//                    $resop.='<h5>' . $total_like . '</h5>';
//                    $resop.='</div>';
//                    $resop.='</div>';
//                    $resop.='<div class="col-md-3">';
//                    if (!empty($value->main_image)) {
//                        $resop.='<a href="' . base_url('projects/detail/' . $value->slug) . '">';
//                        $resop.='<img src="' . base_url(str_replace('./', '', $value->main_image)) . '" class="img-responsive">';
//                        $resop.='</a>';
//                    }
//                    $resop.='</div>';
//                    $resop.='<div class="col-md-8 list-block-right">';
//                    $resop.='<a href="' . base_url('projects/detail/' . $value->slug) . '"><h4 class="title">';
//                    if (!empty($value->title)) {
//                        $resop.= $value->title;
//
//                        $resop.='</h4></a>';
//                        $resop.='<div class="block-content">';
//                        if (!empty($value->short_description)) {
//                            $resop.= $value->short_description;
//                        }
//                        $resop.='</div>';
//                        $resop.='<div class="rate-warp">';
//                        $resop.='<div class="row">';
//                        $resop.='<div class="col-md-4 block">';
//                        if (!empty($value->id)) {
//                            $total_rating = product_rating_count($value->id);
//                        } else {
//                            $total_rating = 0;
//                        }
//                        $resop.='<div class="exemple">';
//                        $resop.='<div class="showrate" data-average="';
//                        if (!empty($total_rating)) {
//                            $resop.= $total_rating;
//                        } else {
//                            $resop.=0;
//                        }
//                        $resop.='data-id="1"></div>';
//                        $resop.='</div>';
//                        $resop.='</div>';
//                        $resop.='<div class="col-md-2 block">';
//                        $resop.='<a class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="10220 times downloaded"   href="javascript:;"> <i class="fa fa-download"></i> 10220</a>';
//                        $resop.='</div>';
//                        $resop.='<div class="col-md-3 block">';
//                        $resop.='<a class="btn btn-info btn-xs"  href="' . base_url('projects/detail/' . $value->slug) . '">Watch Video</a>';
//                        $resop.='</div>';
//                        $resop.='<div class="col-md-3 text-right">';
//                        $resop.='<a href="' . base_url('projects/download_doc_file/' . $value->unique_id) . '" class="btn btn-danger btn-xs"><i class="fa fa-download"></i> Download</a>';
//                        $resop.='</div>';
//                        $resop.='</div>';
//                        $resop.='</div>';
//                        $resop.='</div>';
//                        $resop.='<div class="clearfix"></div>';
//                        $resop.='</div>';
//                    }
                }
                echo $resop;
            } else {
                echo 'NoProjectFound';
                return False;
            }
        }
    }

    function get_latest_projects() {
        if ($_GET['latest'] == 'latest') {
            $resop = '';
            $data['projects'] = $this->common_model->projects( 0, 5);
            if (!empty($data['projects'])) {
                foreach ($data['projects'] as $value) {
                    $resop.='<div class="list-block">';
                    $resop.='<div class="col-md-1 post-date-warp">';
                    $resop.='<div class="post-date text-center">';
                    $resop.='<i class="fa fa-calendar"></i>';
                    $resop.='<h4>' . date('d', strtotime($value->created)) . '</h4>';
                    $resop.='<h5>' . date('M', strtotime($value->created)) . '</h5>';
                    $resop.='</div>';
                    $resop.='<hr>';
                    $resop.='<div class="post-date text-center">';
                    $resop.='<i class="fa fa-thumbs-o-up"></i>';
                    $total_like = get_total_like_of_project($value->id);
                    $resop.='<h5>' . $total_like . '</h5>';
                    $resop.='</div>';
                    $resop.='</div>';
                    $resop.='<div class="col-md-3">';
                    if (!empty($value->main_image)) {
                        $resop.='<a href="' . base_url('projects/detail/' . $value->slug) . '">';
                        $resop.='<img src="' . base_url(str_replace('./', '', $value->main_image)) . '" class="img-responsive">';
                        $resop.='</a>';
                    }
                    $resop.='</div>';
                    $resop.='<div class="col-md-8 list-block-right">';
                    $resop.='<a href="' . base_url('projects/detail/' . $value->slug) . '"><h4 class="title">';
                    if (!empty($value->title)) {
                        $resop.= $value->title;
                        $resop.='</h4></a>';
                        $resop.='<div class="block-content">';
                        if (!empty($value->short_description)) {
                            $resop.= $value->short_description;
                        }
                        $resop.='</div>';
                        $resop.='<div class="rate-warp">';
                        $resop.='<div class="row">';
                        $resop.='<div class="col-md-4 block">';
                        if (!empty($value->id)) {
                            $total_rating = product_rating_count($value->id);
                        } else {
                            $total_rating = 0;
                        }
                        $resop.='<div class="exemple">';
                        $resop.='<div class="showrate" data-average="';
                        if (!empty($total_rating)) {
                            $resop.= $total_rating;
                        } else {
                            $resop.=0;
                        }
                        $resop.='data-id="1"></div>';
                        $resop.='</div>';
                        $resop.='</div>';
                        $resop.='<div class="col-md-2 block">';
                        $resop.='<a class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="10220 times downloaded"   href="javascript:;"> <i class="fa fa-download"></i> 10220</a>';
                        $resop.='</div>';
                        $resop.='<div class="col-md-3 block">';
                        $resop.='<a class="btn btn-info btn-xs"  href="' . base_url('projects/detail/' . $value->slug) . '">Watch Video</a>';
                        $resop.='</div>';
                        $resop.='<div class="col-md-3 text-right">';
                        $resop.='<a href="' . base_url('projects/download_doc_file/' . $value->unique_id) . '" class="btn btn-danger btn-xs"><i class="fa fa-download"></i> Download</a>';
                        $resop.='</div>';
                        $resop.='</div>';
                        $resop.='</div>';
                        $resop.='</div>';
                        $resop.='<div class="clearfix"></div>';
                        $resop.='</div>';
                    }
                }
                echo $resop;
            } else {
                echo 'NoProjectFound';
                return False;
            }
        }
    }

    public function fbshare($id = NULL, $url = '') {
        $data['result'] = $this->common_model->get_row('projects', array('id' => $id));
        if ($url == '')
            $data['url'] = base_url() . 'projects/detail/' . trim($data['result']->slug);
        else
            $data['url'] = $url;
        $this->load->view('fbshare', $data);
    }

    public function detail($slug = '') {
          if (empty($slug))
        redirect('projects');
        $data['project_info'] = $this->common_model->get_row('projects', array('slug' => $slug, 'status' => 1));

        if (!empty($data['project_info']->technology)) {
            $tech_id_arr = explode(',', $data['project_info']->technology);
            $project_id = $data['project_info']->id;
            $query = "SELECT * FROM projects "
                    . "WHERE status = 1 "
                    . "AND id !=$project_id ";
            // ->where('id!=',$data['project_info']->id);
            $i = 0;
            foreach ($tech_id_arr as $key => $value) {
                $query.= ($i == 0)?' AND':' OR';
                $query.=" technology LIKE '" . $value . "'";
                $i++;        
            }

            $query.=" LIMIT 5";
            
            $result = $this->db->query($query)->result();
            $data['related_projects'] = $result;
        }

        if (empty($data['project_info']))
            redirect('projects');

        $data['technology'] = $this->common_model->get_result('technologies', array('status' => 1));
        $data['technologies'] = array();
        if (!empty($data['technology'])) {
            foreach ($data['technology'] as $key => $value) {
                $value = get_object_vars($value);
                $data['technologies'][$value['id']] = $value['technology'];
            }
        }
        $data['courses'] = $this->common_model->get_result('courses', array('status' => 1));
        $data['course_arr'] = array();
        if (!empty($data['courses'])) {
            foreach ($data['courses'] as $key => $value) {
                $value = get_object_vars($value);
                $data['course_arr'][$value['id']] = $value['course'];
            }
        }
        //project Details
        $data['projects'] = $this->common_model->get_result('projects', array('status' => 1), '', array('id', 'desc'), 4);
        //udpate view count
        $view_update_result = $this->common_model->update('projects', array('view_count' => $data['project_info']->view_count + 1), array('id' => $data['project_info']->id));

        $data['advertisements'] = $this->common_model->get_result('advertisements', array('status' => 1));

        //get username
        if (!empty($data['project_info']->user_id) && $data['project_info']->user_id != 0) {
            $user_info = $this->common_model->get_result('users', array('id' => $data['project_info']->user_id));
            if(!empty($user_info)){
                $data['project_info']->name = $user_info[0]->first_name . ' ' . $user_info[0]->last_name;
            }
        } else {
            $data['project_info']->name = '';
        }
        $data['template'] = 'projects/project_detail';
        $this->load->view('templates/frontend/layout', $data);
    }

    public function rating($value = '') {
        if ($_POST) {
            // return json_encode(array('test' => 'test'));
            if ($_POST['action'] == 'rating'):
                $pro_id = $_POST['idBox'];
                $user_id = user_id();
                $rate = $_POST['rate'];
                $user = $this->common_model->get_row('users', array('status' => 1, 'id' => $user_id, 'user_role' => 1));
                $rating = $this->common_model->get_row('project_rating', array('status' => 1, 'user_id' => $user_id, 'project_id' => $pro_id));
                if (!empty($rating)) {
                    echo 'alreadyRate';
                    return;
                } else {
                    if (!empty($user)) {
                        $arrayName = array(
                            'project_id' => $pro_id,
                            'user_id' => $user->id,
                            'status' => 1,
                            'rate' => $rate,
                            'created' => date('Y-m-d h:i:s')
                        );
                        $status = $this->common_model->insert('project_rating', $arrayName);
                        if ($status) {
                            echo 'TRUE';
                            return;
                        }
                    }
                }
            endif;
        }
        echo 'FALSE';
    }

    public function add_to_favorite() {
        if ($_POST) {
            // return json_encode(array('test' => 'test'));     
            $pro_id = $_POST['pro_id'];
            $user_id = user_id();
            $product = $this->common_model->get_row('projects', array('status' => 1, 'id' => $pro_id));
            $user = $this->common_model->get_row('users', array('status' => 1, 'id' => $user_id, 'user_role' => 1));
            $favorite = $this->common_model->get_row('favorite_project', array('status' => 1, 'user_id' => $user_id, 'project_id' => $pro_id));
            if (!empty($favorite)) {
                echo 'alreadyfavorite';
                return;
            } else {
                if (!empty($user) && !empty($product)) {
                    $arrayName = array(
                        'project_id' => $pro_id,
                        'user_id' => $user->id,
                        'status' => 1,
                        'favorite' => 1,
                        'created' => date('Y-m-d h:i:s')
                    );
                    $status = $this->common_model->insert('favorite_project', $arrayName);
                    if ($status) {
                        echo 'TRUE';
                        return;
                    }
                }
            }
        }
        echo 'FALSE';
    }

    public function download_doc_file($pro_id = '') {
        $this->load->helper('download');
        if (empty($pro_id))
            redirect('projects/');
        $data['project'] = $this->common_model->get_row('projects', array('unique_id' => $pro_id));
        if (empty($data['project']))
            redirect('projects/');

        $file_path = str_replace('./', '', $data['project']->doc_file);
        $file_array = explode('/', str_replace('./', '', $data['project']->doc_file));
        if (file_exists($file_path)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file_path) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file_path));
            readfile($file_path);
            return TRUE;
        }
    }

    public function add_review() {
        if ($_POST) {
            $first_name = $_POST['full_name'];
            $email = $_POST['email'];
            $message = $_POST['message'];
            $user_id = user_id();
            $project_id = $_POST['project_id'];
            $user = $this->common_model->get_row('users', array('status' => 1, 'id' => $user_id, 'user_role' => 1));
            $reveiw = $this->common_model->get_row('project_reviews', array('user_id' => $user->id, 'project_id' => $project_id));
            if (!empty($reveiw)) {
                echo 'alreadyRate';
                return;
            } else {
                if (!empty($user)) {
                    $arrayName = array(
                        'user_email' => $email,
                        'user_id' => $user->id,
                        'project_id' => $project_id,
                        'status' => 0,
                        'message' => $message,
                        'created' => date('Y-m-d h:i:s')
                    );
                    $status = $this->common_model->insert('project_reviews', $arrayName);
                    if ($status) {
                        echo 'TRUE';
                        return;
                    }
                }
            }
        }
        echo 'FALSE';
    }

    public function buynow($project_id = '') {
        if (empty($project_id))
            redirect('projects/');
        $data['project_info'] = $this->common_model->get_row('projects', array('unique_id' => $project_id, 'status' => 1));
        if (empty($data['project_info']))
            redirect('projects/');
        //$cart_project = $this->session->userdata('cart_projet_info');
        if (user_logged_in() === TRUE) {
            redirect('order/checkout/' . $project_id);
        }
        $data['technology'] = $this->common_model->get_result('technologies', array('status' => 1));
        $data['courses'] = $this->common_model->get_result('courses', array('status' => 1));
        $data['template'] = 'projects/buynow';
        $this->load->view('templates/frontend/layout', $data);
    }

    function project_list($offset = 0) {

        $per_page = 10;
        $data['projects'] = $this->common_model->projects($offset, $per_page);
        $data['technology'] = $this->common_model->get_result('technologies', array('status' => 1));
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'projects/project_list/';

        $config['total_rows'] = $this->common_model->projects(0, 0);

        $config['per_page'] = $per_page;

        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'projects/project_list';
        $this->load->view('templates/frontend/layout', $data);
    }

}
