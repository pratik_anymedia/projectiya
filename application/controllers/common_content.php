<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common_content extends MY_Controller {

    public function __construct() {
        parent::__construct();
        clear_cache();
    }

    public function index($id = '') {
        $data['page'] = array();
        if (!empty($id)) {
            $data['page'] = $this->common_model->get_row('posts', array('post_type' => 'page', 'post_status' => 'publish', 'id' => $id));
        }

        
         $data['technology']=$this->common_model->get_result('technologies',array('status'=>1));
         
         $data['pages'] = $this->common_model->get_result('posts', array('post_type' => 'page', 'post_status' => 'publish'));
         
        $data['template'] = 'common_pages';
        $this->load->view('templates/frontend/layout', $data);
    }

}
