<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Contact_us extends MY_Controller
{
    public function __construct()
    {
		parent::__construct();
		clear_cache();
      
    }

    public function index()
    {
    
        $data['technology']=$this->common_model->get_result('technologies',array('status'=>1));
        $data['courses']=$this->common_model->get_result('courses',array('status'=>1));
        $data['projects']=$this->common_model->get_result('projects',array('status'=>1),'',array('id','desc'),4);
        $data['content']=$this->common_model->get_row('posts',array('post_status'=>'publish','post_type'=>'page','post_slug'=>'contact-us'));
      	
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name',  'Last Name', 'required');
        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('message',    'Message', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if($this->form_validation->run() == TRUE){
            $user_data  = array(
                                    'firstname' =>  $this->input->post('first_name'),
                                    'lastname'  =>  $this->input->post('last_name'),
                                    'email'     =>  $this->input->post('user_email'),                       
                                    'created'   =>  date('Y-m-d'),
                                    'message'   =>  $this->input->post('message'),
                                    'user_ip'   =>  $this->input->ip_address(),
                                );
            $text_message = $this->input->post('question');
            $email        = $this->input->post('email');
            $first_name   = $this->input->post('first_name');
            $last_name    = $this->input->post('last_name');
            if($user_id=$this->common_model->insert('contact_us', $user_data)){
                // $this->load->library('developer_email');
                // $email_template=$this->developer_email->get_email_template($i);
            
                // $admin_email   = $this->common_model->get_row('options',array('option_name' => 'EMAIL'));
                
                // $param=array(
                //             'template'=> array(
                //             'temp'    => $email_template->template_body_admin,
                //             'var_name'=> array(
                //                             'user_full_name'=>  $first_name.' '.$last_name,
                //                             'text_message'  =>$text_message                                                 
                //                         ), 
                //             ),          
                //             'email'=> array(
                //                             'to'        =>   $admin_email->option_value,
                //                             'from'      =>   FROM_EMAIL,
                //                             'from_name' =>   SITE_URL,
                //                             'subject'   =>   $email_template->template_subject_admin,
                //                         )
                //             );  
                // $status=$this->developer_email->send_mail($param);
                $this->session->set_flashdata('msg_success','Your message has sent successfully.');
                redirect('contact_us');
            }else{
                $this->session->set_flashdata('msg_error','Failed, Please try again.');
                redirect('contact_us');
            }
        }

        $data['template']='contact_us/index';
  		$this->load->view('templates/frontend/layout', $data);
    
    }

}    