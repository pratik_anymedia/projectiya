<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class About_us extends MY_Controller
{
    public function __construct()
    {
		parent::__construct();
		clear_cache();
      
    }

    public function index()
    {
    
        $data['technology']=$this->common_model->get_result('technologies',array('status'=>1));
        $data['courses']=$this->common_model->get_result('courses',array('status'=>1));
        $data['projects']=$this->common_model->get_result('projects',array('status'=>1),'',array('id','desc'),4);
        $data['content']=$this->common_model->get_row('posts',array('post_status'=>'publish','post_type'=>'page','post_slug'=>'about-us'));
      	$data['template']='about_us/index';
  		$this->load->view('templates/frontend/layout', $data);
    
    }

}    