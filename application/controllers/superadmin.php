<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Superadmin extends MY_Controller {

    public function __construct() {

        parent::__construct();

        clear_cache();
        $this->load->library('form_validation');
        $this->load->model('superadmin_model');
    }

    public function index() {

        redirect('superadmin/login');
    }

    public function login() {

        if (superadmin_logged_in() === TRUE)
            redirect('backend/dashboard');

        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');



        if ($this->form_validation->run() == TRUE) {

            $this->load->model('user_model');

            $data = array('email' => $this->input->post('email', TRUE), 'password' => sha1($this->input->post('password', TRUE)));

            if ($this->user_model->login($data, 'superadmin')) {

                redirect('backend/dashboard');
            } else {

                $this->session->set_flashdata('msg_error', 'Incorrect email Address or password.');

                redirect('superadmin/login');
            }
        }

        $this->load->view('backend/login');
    }

    public function logout() {

        _check_superadmin_login(); //check  login authentication

        $this->session->sess_destroy();

        redirect('superadmin/login');
    }

    public function profile() {

        _check_superadmin_login(); //check login authentication

        $this->form_validation->set_rules('first_name', 'First Name', 'required');

        $this->form_validation->set_rules('last_name', 'last Name', 'required');

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');



        if ($this->form_validation->run() == TRUE) {

            $user_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
            );



            if ($this->superadmin_model->update('users', $user_data, array('id' => superadmin_id()))) {

                $this->session->set_flashdata('msg_success', 'Profile updated successfully.');

                redirect('superadmin/profile');
            } else {



                $this->session->set_flashdata('msg_error', 'Update failed, Please try again.');

                redirect('superadmin/profile');
            }
        } else {



            $data['user'] = $this->superadmin_model->get_row('users', array('id' => superadmin_id()));

            $data['template'] = 'backend/profile';

            $this->load->view('templates/backend/layout', $data);
        }
    }

    public function change_password() {

        _check_superadmin_login(); //check login authentication

        $this->form_validation->set_rules('oldpassword', 'Old Password', 'required|callback_password_check');

        $this->form_validation->set_rules('newpassword', 'New Password', 'required|matches[confpassword]');

        $this->form_validation->set_rules('confpassword', 'Confirm Password', 'required');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == TRUE) {

            $user_data = array('password' => sha1($this->input->post('newpassword')));

            if ($this->superadmin_model->update('users', $user_data, array('id' => superadmin_id()))) {

                $this->session->set_flashdata('msg_success', 'Password updated successfully.');

                redirect('superadmin/change_password');
            } else {
                $this->session->set_flashdata('msg_error', 'Update failed, Please try again.');
                redirect('superadmin/change_password');
            }
        }
        $data['template'] = 'backend/change_password';
        $this->load->view('templates/backend/layout', $data);
    }

    public function password_check($oldpassword) {

        $this->load->model('user_model');

        if ($this->user_model->password_check(array('password' => sha1($oldpassword)), superadmin_id())) {

            return TRUE;
        } else {

            $this->form_validation->set_message('password_check', 'The %s does not match.');

            return FALSE;
        }
    }

    public function option() {

        _check_superadmin_login(); //check login authentication
        // if($_POST){
        //  print_r($_POST);
        //  die();
        // }

        $data['option'] = $this->superadmin_model->get_result('options', array('status' => 1));

        foreach ($data['option'] as $value) {

            $this->form_validation->set_rules($value->option_name, $value->option_name, 'required');
        }

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == TRUE) {

            $name = $this->input->post('name');
            $i = 1;
            //print_r($name);
            foreach ($_POST as $key => $value) {
                $post_data = array('option_value' => htmlentities($value));
                $optionid = array('option_name' => $key);
                $this->superadmin_model->update('options', $post_data, $optionid);
                $i++;
            }

            $this->session->set_flashdata('msg_success', 'Data updated successfully.');

            redirect('superadmin/option');
        }



        $data['template'] = 'backend/option';

        $this->load->view('templates/backend/layout', $data);
    }

    function add_popup_image() {

        _check_superadmin_login();

        //$this->form_validation->set_rules('popup_image', 'Popup Image', 'required');
       
        if (!empty($_FILES['popup_image']['name']) && $_FILES['popup_image']['name'] != '') {

           $this->popup_image_add();
            $image_data = array();
            if ($this->session->userdata('popup_image')):
                $userfile = $this->session->userdata('popup_image');
                $image_data['path'] = $userfile['image'];
                $image_data['image_type'] = 1;
                $image_data['status'] = 1;
            endif;


            if ($this->session->userdata('popup_image')):
                $this->session->unset_userdata('popup_image');
            endif;

            $result = $this->superadmin_model->get_result('image_setting', array('status' => 1, 'image_type' => 1), array('path'));


            if (!empty($result)) {
                $image_file = $result[0]->path;
                @unlink($image_file);

                $this->superadmin_model->update('image_setting', array('path' => $image_data['path']), array('image_type' => 1));
            } else if ($image_id = $this->superadmin_model->insert('image_setting', $image_data)) {



                $this->session->set_flashdata('msg_success', 'Image added successfully.');
            }
            //superadmin/view_popup_image
            redirect('/superadmin/view_popup_image');
        }

        $data['template'] = 'add_popup_image';

        $this->load->view('templates/backend/layout', $data);
    }

    public function view_popup_image() {
       
        $result = $this->superadmin_model->get_result('image_setting', array('status' => 1, 'image_type' => 1), array('path'));

        if (!empty($result)) {
            $data['image_path'] = $result[0]->path;
        }

        $data['template'] = 'view_popup';

        $this->load->view('templates/backend/layout', $data);
    }

    function popup_image_add() {
        
    
        if ($this->session->userdata('popup_image')) {

            return TRUE;
        } else {
            $param = array(
                'file_name' => 'popup_image',
                'upload_path' => './assets/backend/images/popup_images/',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'image_resize' => TRUE,
                'source_image' => './assets/backend/images/popup_images/',
                'new_image' => './assets/backend/images/popup_images/thumb/',
                'resize_width' => 200,
                'resize_height' => 200,
                'encrypt_name' => TRUE,
            );

            $upload_file = upload_file($param);

            if ($upload_file['STATUS']) {
                $this->session->set_userdata('popup_image', array('image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'],
                        //'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']
                ));
                return TRUE;
            } else {
                $this->form_validation->set_message('popup_image_add', $upload_file['FILE_ERROR']);
                return FALSE;
            }
        }
    }

    function saveCommonSettings() {
        $this->load->model('settings_model');
            $update['show_project_count'] = $this->input->post('show_project_count') ? $this->input->post('show_project_count') : 0;
            $update['show_active_user_count'] = $this->input->post('show_active_user_count') ? $this->input->post('show_active_user_count') : 0;
            
            foreach ($update as $key => $val) {
                $this->settings_model->setSetting($key, $val);
            }

            $this->session->set_flashdata('msg_success','Updated Successfully!!!.');
            redirect('superadmin/settings');
        
        }

    public function settings() {
       $data['template'] ='backend/settings';
       $this->load->view('templates/backend/layout', $data);
    }    
    
    function saveBasicSettings() {
        $this->load->model('settings_model');
        $fieldsValidation[] = array('field' => 'admin_email', 'label' => 'Admin Email', 'rules' => 'trim|required|valid_email');
        $fieldsValidation[] = array('field' => 'noreply_email', 'label' => 'No-reply Email', 'rules' => 'trim|required|valid_email');
        if ($_FILES['logo']['name'] == '' && ( isset($this->SETTINGS['logo']) && $this->SETTINGS['logo'] == '')) {
            $this->session->set_flashdata('msg_success','Please Upload Your Site Logo.');
            redirect('superadmin/settings');
        }

        $this->form_validation->set_rules($fieldsValidation);
        if ($this->form_validation->run() === TRUE) {
            $update['admin_email'] = $this->input->post('admin_email') ? $this->input->post('admin_email') : NULL;
            $update['welcome_text'] = $this->input->post('welcome_text') ? $this->input->post('welcome_text') : NULL;
            $update['noreply_email'] = $this->input->post('noreply_email') ? $this->input->post('noreply_email') : NULL;
            $update['header_email'] = $this->input->post('header_email') ? $this->input->post('header_email') : NULL;
            $update['phone_number'] = $this->input->post('phone_number') ? $this->input->post('phone_number') : NULL;
            $update['cont_adrs'] = $this->input->post('cont_adrs') ? $this->input->post('cont_adrs') : NULL;
            $update['cont_fax'] = $this->input->post('cont_fax') ? $this->input->post('cont_fax') : NULL;
            $update['cont_web'] = $this->input->post('cont_web') ? $this->input->post('cont_web') : NULL;
            $update['copyright'] = $this->input->post('copyright') ? $this->input->post('copyright') : NULL;
            $config['upload_path'] = FCPATH . 'assets/uploads/logo';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (isset($_FILES['logo']) && $_FILES['logo']['error'] != 4) {
                if ($this->upload->do_upload('logo')) {
                    $uploadData = $this->upload->data();
                    $update['logo'] = $uploadData['file_name'];
                    
                }else {
                    $this->session->set_flashdata('msg_success',$this->upload->display_errors());
                    redirect('superadmin/settings');
                }
            }
            foreach ($update as $key => $val) {
                $this->settings_model->setSetting($key, $val);
            }
            $this->session->set_flashdata('msg_success','Updated Successfully!!!');
            redirect('superadmin/settings');
            
        } else {
            $this->session->set_flashdata('msg_success',validation_errors());
            redirect('superadmin/settings'); 
        }
    }
    
    function saveSocialSettings() {
        $this->load->model('settings_model');
        if ($this->input->post()) {
            $update['fb_url'] = $this->input->post('fb_url') ? $this->input->post('fb_url') : NULL;
            $update['twitter_url'] = $this->input->post('twitter_url') ? $this->input->post('twitter_url') : NULL;
            $update['youtube_url'] = $this->input->post('youtube_url') ? $this->input->post('youtube_url') : NULL;
            $update['vimeo_url'] = $this->input->post('vimeo_url') ? $this->input->post('vimeo_url') : NULL;
            $update['gplus_url'] = $this->input->post('gplus_url') ? $this->input->post('gplus_url') : NULL;
            $update['trumblr_url'] = $this->input->post('trumblr_url') ? $this->input->post('trumblr_url') : NULL;
            $update['dribbble_url'] = $this->input->post('dribbble_url') ? $this->input->post('dribbble_url') : NULL;
            $update['linkedin_url'] = $this->input->post('linkedin_url') ? $this->input->post('linkedin_url') : NULL;
            $update['pinterest_url'] = $this->input->post('pinterest_url') ? $this->input->post('pinterest_url') : NULL;
            foreach ($update as $key => $val) {
                $this->settings_model->setSetting($key, $val);
            }

            $this->session->set_flashdata('msg_success','Links Save Successfully!!!.');
            redirect('superadmin/settings');
        }
        
    }
    
    function saveContactSettings() {
        $this->load->model('settings_model');
        $fieldsValidation[] = array('field' => 'contact_email', 'label' => 'Contact Email', 'rules' => 'trim|required|valid_email');
        $fieldsValidation[] = array('field' => 'contact_subject', 'label' => 'Subject Line', 'rules' => 'trim|required');
        $this->form_validation->set_rules($fieldsValidation);
        if ($this->form_validation->run() === TRUE) {
            $update['contact_email'] = $this->input->post('contact_email') ? $this->input->post('contact_email') : NULL;
            $update['contact_subject'] = $this->input->post('contact_subject') ? $this->input->post('contact_subject') : NULL;
            
            foreach ($update as $key => $val) {
                $this->settings_model->setSetting($key, $val);
            }

            $this->session->set_flashdata('msg_success','Updated Successfully!!!.');
            redirect('superadmin/common_setting');
        } else {
            $this->session->set_flashdata('msg_success',validation_errors());
            redirect('superadmin/common_setting');
        }
        
    }
    
    
    
    
    function downloadProject($file_name){
//        echo $file_name;die;
        $this->load->helper('download');
        $data = file_get_contents(base_url()."assets/uploads/requestproject/".$file_name); // Read the file's contents
        //$name = 'myphoto.jpg';
        
        force_download($file_name, $data); 
    }
    

}
