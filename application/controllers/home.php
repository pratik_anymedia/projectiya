<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        clear_cache();
        $this->lang->load('message', 'english');
        $this->load->model('common_model');
        $this->load->model('testimonial_model');
    }

    public function index() {
        $data['advertisements'] = $this->common_model->get_result('advertisements', array('status' => 1,'role' => 0));
//        echo "<pre>"; print_R($data['advertisements']); die;
        $data['popupImage']=$this->common_model->get_result('advertisements',array('role'=>1));
        $data['projects'] = $this->common_model->get_result('projects', array('status' => 1), '', array('id', 'desc'), 4);
        //print_r($data['projects']);
        
        $data['slider_images'] = $this->common_model->get_result('image_setting', array('status' => 1, 'image_type' => 2), array('path', 'description'));
        $data['pages'] = $this->common_model->get_result('posts', array('post_type' => 'page', 'post_status' => 'publish'));

        //get project count
        $data['total_project_count'] = $this->common_model->projects(0, 0);

        //get active user count
        $user_result = $this->common_model->get_result('users', array('status' => 1));
        $data['active_user_count'] = count($user_result);

        $common_setting = $this->common_model->get_result('common_setting', array('status' => 1));
        $show_project_count = 0;
        $show_user_count = 0;
        if (!empty($common_setting)) {
            foreach ($common_setting as $key => $value) {

                if ($value->attribute_type == 1 && $value->status == 1) {
                    $show_project_count = 1;
                }

                if ($value->attribute_type == 2 && $value->status == 1) {
                    $show_user_count = 1;
                }
            }
        }

        $data['show_project_count'] = $show_project_count;
        $data['show_user_count'] = $show_user_count;
        $result = $this->common_model->get_result('advertisements', array('role' => 1), array('file_path'));
        if (!empty($result)) {
            $data['image_path'] = $result[0]->file_path;
            $data['popup'] = $this->load->view('popup_window', $data , true);
        }
        $data['testimonials'] = $this->testimonial_model->getApproved();
        
        $data['template'] = 'home/index';
        $this->load->view('templates/frontend/layout', $data);
    }

}
