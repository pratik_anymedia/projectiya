<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Testimonial extends CI_Controller {

    public function __construct() {
        parent::__construct();
        clear_cache();
        $this->load->model('testimonial_model');
    }

    public function index($sort_by = 'id', $sort_order = 'desc', $offset = 0) {
        _check_superadmin_login(); //check login authentication
        $per_page = 40;
        $limit = $per_page;
        $config = backend_pagination();
        $data['testimonials'] = $this->testimonial_model->getAll($offset, $limit, $sort_by, $sort_order);

        //echo '<pre>'; print_r($data['orders']);
        $config['base_url'] = base_url() . 'backend/testimonial/index/' . $sort_by . '/' . $sort_order . '/';
        $config['total_rows'] = $this->testimonial_model->getAll(0, 0, $sort_by, $sort_order);
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['uri_segment'] = 6;
        if (!empty($_SERVER['QUERY_STRING'])) {
            $config['suffix'] = "?" . $_SERVER['QUERY_STRING'];
        }
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $offset++;
        $data['offset'] = $offset;
        $data['template'] = 'backend/testimonial/index';
        $this->load->view('templates/backend/layout', $data);
    }

    public function delete($id ='')  {

        _check_superadmin_login(); //check login authentication

        if(empty($id)) redirect('backend/testimonial');

        //$this->order_model->delete('order_info',array('order_id'=>$order_id));

        //$this->order_model->delete('orders_products',array('orders_id'=>$order_id));

        $this->testimonial_model->delete('testimonial',array('id'=>$id));

        $this->session->set_flashdata('msg_success','Testimonial has been deleted successfully.');

        redirect('backend/testimonial/');    

      }
      
      public function approve($id ='')  {

        _check_superadmin_login(); //check login authentication

        if(empty($id)) redirect('backend/testimonial');

        //$this->order_model->delete('order_info',array('order_id'=>$order_id));

        //$this->order_model->delete('orders_products',array('orders_id'=>$order_id));

        $this->testimonial_model->update('testimonial',array('is_approved' => 1) , array('id'=>$id));

        $this->session->set_flashdata('msg_success','Testimonial has been Approved successfully.');

        redirect('backend/testimonial/');    

      }
      
      public function unapprove($id ='')  {

        _check_superadmin_login(); //check login authentication

        if(empty($id)) redirect('backend/testimonial');

        //$this->order_model->delete('order_info',array('order_id'=>$order_id));

        //$this->order_model->delete('orders_products',array('orders_id'=>$order_id));

        $this->testimonial_model->update('testimonial',array('is_approved' => 0) , array('id'=>$id));

        $this->session->set_flashdata('msg_success','Testimonial has been unapproved successfully.');

        redirect('backend/testimonial/');    

      }
    
      public function multi_delete()
  {
    _check_superadmin_login(); //check Professor login authentication
    if($_POST){
      $this->form_validation->set_rules('checkall[]','order', 'trim|required');
      $this->form_validation->set_error_delimiters('<div style="color:red;" class="error">','</div>');
      if($this->form_validation->run() == TRUE){
        $order_ids= $this->input->post('checkall');
        $FLAG=TRUE;
        // print_r($student_id_array);
        // die();
        //$status = $_POST['all_status'];
        $count_order = count($order_ids);
//        print_r($count_order);die;
        for($i=0; $i<$count_order ; $i++){     
          //$this->order_model->delete('order_info', array('order_id'=>$order_ids[$i]));
          //$this->order_model->delete('orders_products', array('orders_id'=>$order_ids[$i]));

          $order_status = $this->testimonial_model->delete('testimonial',array('id'=>$order_ids[$i]));

          if(!$order_status){

            $FLAG=FALSE;

          }

        }     

        if($FLAG){

          $this->session->set_flashdata('msg_success',"Testimonials have been Deleted successfully.");

          redirect('backend/testimonial');

        }

      }else{

        $this->session->set_flashdata('msg_error',"Testimonials delete Operation Failed.");

        redirect('backend/testimonial');

      }

    }else{

      $this->session->set_flashdata('msg_error',"Testimonials delete Operation Failed...");

      redirect('backend/testimonial');

    } 

  }
      
}
