<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Country extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        clear_cache();
        $this->load->model('country_model');
    }

    // public function index($sort_by='id',$sort_order='desc',$offset = 0)
    // {
        
    //     _check_superadmin_login(); //check login authentication
    //     $per_page = 100;

    //     $data['country'] = $this->country_model->countries($offset,$per_page,$sort_by,$sort_order);

    //     $data['offset'] = $offset;

    //     $config=backend_pagination();

    //     $config['base_url'] = base_url().'backend/services/index/'. $sort_by.'/'.$sort_order.'/';

    //     $config['total_rows'] = $this->country_model->countries(0,0,$sort_by,$sort_order);

    //     $config['per_page'] = $per_page;

    //     $config['uri_segment'] =3;

    //     if(!empty($_SERVER['QUERY_STRING'])){

    //         $config['suffix'] = "?".$_SERVER['QUERY_STRING'];

    //     }

    //     $this->pagination->initialize($config);

    //     $data['pagination']=$this->pagination->create_links();

    //     $data['sort_by']=$sort_by;

    //     $data['sort_order']=$sort_order;

    //     $data['template'] = "backend/countries/index";

    //     $this->load->view('templates/backend/layout', $data);

    // }



    // public function add()

    // {

    //     _check_superadmin_login(); //check login authentication



    //     $this->form_validation->set_rules('country_name','Country Name','trim|required|callback_check_country_name'); 

    //     $this->form_validation->set_rules('status', 'Status', 'trim|required');

    //     $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

    //     if($this->form_validation->run() == TRUE){



    //         $con_data = array(

    //                             'country_name'=>  $this->input->post('country_name'),

    //                             'slug'        =>  url_title($this->input->post('country_name'), '-', TRUE),

    //                             'status'      =>  $this->input->post('status'),

    //                             'for_state_province' => $this->input->post('for_state_province'),

    //                             'created'     =>  date('Y-m-d h:i:s')

    //                         );

    //         if(!empty($_POST['for_state_province'])){

    //            $con_data['label_state_province'] = $this->input->post('label_state_province');

    //            $state = $this->input->post('state');   

    //         }else{

    //            $con_data['label_state_province'] ='';

    //         }



           



            

    //         if($con_id=$this->country_model->insert('countries',$con_data)){

    //             if(!empty($state)){

    //                 for($i=0; $i<count($state); $i++) { 

    //                     $array= array(

    //                            'con_id'=>$con_id,

    //                            'state_name'=>$state[$i],

    //                            'slug'=>url_title($state[$i],'-',TRUE),

    //                            'status'=>1,

    //                            'created'=>date('Y-m-d h:i:s')

    //                         ); 

    //                     $this->country_model->insert('states',$array);

    //                 } 

    //             }

    //             $this->session->set_flashdata('msg_success','New Country has been created successfully.');

    //             redirect('backend/country');

    //         }else{

    //             $this->session->set_flashdata('msg_error','Failed, Please try again.');

    //             redirect('backend/country');

    //         }

    //     }



    //     $data['template'] = "backend/countries/country_add";

    //     $this->load->view('templates/backend/layout', $data);

    // }



    // public function edit($con_id='',$offset=''){

    //     _check_superadmin_login(); //check login authentication



    //     if(empty($con_id)) redirect('backend/services');

    //     $data['country'] = $this->country_model->get_row('countries',array('id'=>$con_id));

    //     if(empty($data['country']->id)) redirect('backend/country/');

    //     if(!empty($data['country']->for_state_province)){

    //         $data['states']= $this->country_model->get_result('states',array('con_id'=>$con_id));

    //     }else{

    //         $data['states']='';

    //     }

    //     $this->form_validation->set_rules('country_name','Country Name','trim|required|callback_check_country_name_edit['.$con_id.']'); 

    //     $this->form_validation->set_rules('status', 'Status', 'trim|required');

    //     $this->form_validation->set_error_delimiters('<div class="error">','</div>');

    //     if($this->form_validation->run() == TRUE)  {

    //         $con_data = array(

    //                                 'country_name'  =>  $this->input->post('country_name'),

    //                                 'slug' =>   url_title($this->input->post('country_name'), '-', TRUE),

    //                                 'status'        =>  $this->input->post('status'),

    //                                 'for_state_province' => $this->input->post('for_state_province'),

    //                                 'modified'    =>  date('Y-m-d h:i:s')

    //                             );

    //         if(!empty($_POST['for_state_province'])){

    //            $con_data['label_state_province'] = $this->input->post('label_state_province');

    //            $state = $this->input->post('state');   

    //         }else{

    //            $con_data['label_state_province'] ='';

    //            $this->country_model->delete('states',array('con_id'=>$con_id));

    //         }



           

    //         if($this->country_model->update('countries',$con_data,array('id'=>$data['country']->id))){

    //             if(!empty($state)){

    //                 $this->country_model->delete('states',array('con_id'=>$con_id));

    //                 for($i=0; $i<count($state); $i++) { 

    //                     $array= array(

    //                            'con_id'=>$con_id,

    //                            'state_name'=>$state[$i],

    //                            'slug'=>url_title($state[$i],'-',TRUE),

    //                            'status'=>1,

    //                            'created'=>date('Y-m-d h:i:s')

    //                         ); 

    //                     $this->country_model->insert('states',$array);

    //                 } 
    //             }
    //             $this->session->set_flashdata('msg_success','Country has been updated successfully.');
    //             redirect('backend/country/index/'.$offset);
    //         }
    //     }
    //     $data['template'] = "backend/countries/country_edit";
    //     $this->load->view('templates/backend/layout', $data);

    // }





   

    // public function delete($con_id = '')

    // {

    //     _check_superadmin_login(); //check login authentication

    //     if(empty($con_id)) redirect('backend/services');

    //     if($this->country_model->delete('countries',array('id'=>$con_id))){

    //         $this->country_model->delete('states',array('con_id'=>$con_id));

    //         $this->session->set_flashdata('msg_success', 'Country has been deleted successfully.');

    //         redirect('backend/country');

    //     }else{

    //         $this->session->set_flashdata('msg_error', 'Delete failed, Please try again.');

    //         redirect('backend/country');

    //     }

    // }



    // public function check_country_name($name){

    //     $cat_name  = url_title($name, '-', TRUE);     

    //     if($data=$this->country_model->get_row('countries',array('slug'=>$cat_name))){

    //         $this->form_validation->set_message('check_country_name', 'This Country is already exist.');

    //         return FALSE;

    //     }else{

    //         //$this->form_validation->set_message('password_check', 'The %s does not match.');

    //         return TRUE;

    //     }           

    // }



    // public function check_country_name_edit($name,$id){

       

    //     $cat_name  = url_title($this->input->post('country_name'), '-', TRUE);



    //     if($cat=$this->country_model->get_row('countries',array('slug'=>$cat_name))){

    //         $data=$this->country_model->get_row('countries',array('id'=>$id));

    //         if($data->slug==trim($cat->slug)){

    //             return TRUE;

    //         }else{

    //             $this->form_validation->set_message('check_country_name_edit', 'This Country is already exist.');

    //             return FALSE;

    //         }

    //     }else{

    //         //$this->form_validation->set_message('password_check', 'The %s does not match.');

    //         return TRUE;

    //     }           

    // }









    // public function states($con_id='',$sort_by='id',$sort_order='desc',$offset=0)

    // {

    //     _check_superadmin_login(); //check login authentication

    //     $per_page = 10;

    //     if(empty($con_id)||!is_numeric($con_id)) redirect('backend/country');

    //     $data['country'] = $this->country_model->get_row('countries',array('id'=>$con_id));

    //     if(empty($data['country']->id)) redirect('backend/country/');

    //     $data['states'] = $this->country_model->states($offset,$per_page,$con_id,$sort_by,$sort_order);

    //     $data['offset'] = $offset;

    //     $config=backend_pagination();

    //     $config['base_url'] = base_url().'backend/country/states/'.$con_id.'/'. $sort_by.'/'.$sort_order.'/';

    //     $config['total_rows'] = $this->country_model->states(0,0,$con_id,$sort_by,$sort_order);

    //     $config['per_page'] = $per_page;

    //     $config['uri_segment'] =3;

    //     if(!empty($_SERVER['QUERY_STRING'])){

    //         $config['suffix'] = "?".$_SERVER['QUERY_STRING'];

    //     }      

    //     $this->pagination->initialize($config);

    //     $data['pagination']=$this->pagination->create_links();

    //     $data['sort_by']=$sort_by;

    //     $data['sort_order']=$sort_order;

    //     $data['country_id'] =  $con_id;

    //     $data['template'] = "backend/countries/states";

    //     $this->load->view('templates/backend/layout', $data);

    // }





    // public function state_add($con_id='')

    // {

    //     _check_superadmin_login(); //check login authentication

    //     if(empty($con_id)) redirect('backend/country/');

    //     $data['country'] = $this->country_model->get_row('countries',array('id'=>$con_id));

    //     if(empty($data['country']->id)) redirect('backend/country/');

    //     $this->form_validation->set_rules('state_name','State Name','trim|required|callback_check_state_name');

    //     $this->form_validation->set_rules('status', 'Status', 'trim|required');

    //     $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

    //     if($this->form_validation->run() == TRUE){



    //         $service_data  = array(

    //                                 'con_id'=> $data['country']->id,

    //                                 'state_name'  =>  $this->input->post('state_name'),

    //                                 'slug' => url_title($this->input->post('state_name'),'-', TRUE),

    //                                 'status'        =>  $this->input->post('status'),

    //                                 'created'    =>  date('Y-m-d h:i:s')

    //                             );



    //         if($this->country_model->insert('states',$service_data)){

             

    //             $this->session->set_flashdata('msg_success','New State has been created successfully.');

    //             redirect('backend/country/states/'.$data['country']->id);

    //         } else {

    //             $this->session->set_flashdata('msg_error','Failed, Please try again.');

    //             redirect('backend/country/states/'.$data['country']->id);

    //         }

    //     }



    //     $data['template'] = "backend/countries/state_add";

    //     $this->load->view('templates/backend/layout', $data);

    // }



    // public function state_edit($con_id='',$state_id='',$offset=''){

    //     _check_superadmin_login(); //check login authentication

    //     if(empty($con_id)) redirect('backend/country');

    //         $data['country'] = $this->country_model->get_row('countries',array('id'=>$con_id));

    //     if(empty($data['country']->id)) redirect('backend/country/');

    //     if(empty($state_id)) redirect('backend/country/states/'.$data['country']->id);

    //     $data['state'] = $this->country_model->get_row('states',array('id'=>$state_id,'con_id'=>$con_id));

    //     if(empty($data['state']->id)) redirect('backend/country/states/'.$data['state']->id);



    //     $this->form_validation->set_rules('state_name','State Name','trim|required|callback_check_state_name_edit['.$state_id.']');

    //     $this->form_validation->set_rules('status', 'Status', 'trim|required');

    //     $this->form_validation->set_error_delimiters('<div class="error">','</div>');

    //     if($this->form_validation->run() == TRUE)  {

    //         $service_data = array(

    //                                 'con_id'=> $data['country']->id,

    //                                 'state_name'=>  $this->input->post('state_name'),

    //                                 'slug'=> url_title($this->input->post('state_name'),'-', TRUE),

    //                                 'status'=>  $this->input->post('status'),

    //                                 'modified'=>  date('Y-m-d h:i:s')

    //                             );



    //         if($this->country_model->update('states',$service_data,array('id'=>$state_id))){

    //             $this->session->set_flashdata('msg_success','State has been updated successfully.');

    //             redirect('backend/country/states/'.$data['country']->id.'/'.$offset);

    //         }

    //     }

    //     $data['template'] = "backend/countries/state_edit";

    //     $this->load->view('templates/backend/layout', $data);

    // }



    // public function state_delete($con_id='',$state_id='',$offset='')

    // {

    //     _check_superadmin_login(); //check login authentication

    //     if(empty($con_id)) redirect('backend/country');      

    //     if(empty($state_id)) redirect('backend/country/states/'.$con_id);

    //     if ($this->country_model->delete('states',array('id'=>$state_id))){

    //         $this->session->set_flashdata('msg_success', 'State has been deleted successfully.');

    //        redirect('backend/country/states/'.$con_id.'/'.$offset);

    //     } else {

    //         $this->session->set_flashdata('msg_error', 'Delete failed, Please try again.');

    //         rredirect('backend/country/states/'.$con_id.'/'.$offset);

    //     }

    // }



    // public function check_state_name($name){

    //     $sat_name  = url_title($name, '-', TRUE);     

    //     if($data=$this->country_model->get_row('states',array('slug'=>$sat_name))){

    //         $this->form_validation->set_message('check_state_name', 'This State is already exist.');

    //         return FALSE;

    //     }else{

    //         //$this->form_validation->set_message('password_check', 'The %s does not match.');

    //         return TRUE;

    //     }           

    // }



    // public function check_state_name_edit($name,$id){       

    //     $sat_name  = url_title($this->input->post('state_name'), '-', TRUE);     

    //     if($data=$this->country_model->get_row('states',array('id'=>$id))){

    //         if($data->slug==trim($sat_name)){

    //             return TRUE;

    //         }else{

    //             $this->form_validation->set_message('check_state_name_edit', 'This State is already exist.');

    //             return FALSE;

    //         }

    //     }else{

    //         //$this->form_validation->set_message('password_check', 'The %s does not match.');

    //         return TRUE;

    //     }           

    // }







    // public function cities($con_id='',$state_id='',$sort_by='id',$sort_order='desc',$offset=0)

    // {

    //     _check_superadmin_login(); //check login authentication

    //     $per_page = 10;

        

    //     if(empty($con_id)||!is_numeric($con_id)) redirect('backend/country');

    //     if($state_id!=0){

    //         if(empty($state_id)||!is_numeric($state_id)) redirect('backend/country');

    //     }

    //     $data['country'] = $this->country_model->get_row('countries',array('id'=>$con_id));

    //     if(empty($data['country']->id)) redirect('backend/country/');

    //     if($state_id!=0){

    //         $data['state'] = $this->country_model->get_row('states',array('id'=>$state_id));

    //         if(empty($data['state']->id)) redirect('backend/country/');

    //     }else{

    //         $data['state']='';

    //     }

    //     $data['cities'] = $this->country_model->cities($offset,$per_page,$con_id,$state_id,$sort_by,$sort_order);

    //     $data['offset'] = $offset;

    //     $config=backend_pagination();

    //     $config['base_url'] = base_url().'backend/country/cities/'.$con_id.'/'.$state_id.'/'.$sort_by.'/'.$sort_order.'/';

    //     $config['total_rows'] = $this->country_model->cities(0,0,$con_id,$state_id,$sort_by,$sort_order);

    //     $config['per_page'] = $per_page;

    //     $config['uri_segment'] =3;

    //     if(!empty($_SERVER['QUERY_STRING'])){

    //         $config['suffix'] = "?".$_SERVER['QUERY_STRING'];

    //     }

    //     $this->pagination->initialize($config);

    //     $data['pagination']=$this->pagination->create_links();

    //     $data['sort_by']=$sort_by;

    //     $data['sort_order']=$sort_order;

    //     $data['state_id'] =  $state_id;       

    //     $data['template'] = "backend/countries/cities";

    //     $this->load->view('templates/backend/layout', $data);

    // }





    // public function city_add($id='',$type='')

    // {

    //     _check_superadmin_login(); //check login authentication

    //     if(empty($id)) redirect('backend/country/');

    //     if($type=='sta'){

    //         $data['state'] = $this->country_model->get_row('states',array('id'=>$id));

    //         if(empty($data['state']->id)) redirect('backend/country/');

    //     }else if($type=='con'){

    //         $data['country'] = $this->country_model->get_row('countries',array('id'=>$id));

    //         if(empty($data['country']->id)) redirect('backend/country/');

    //     }else{  redirect('backend/country/'); }

    //     $this->form_validation->set_rules('city_name','City Name','trim|required|callback_check_city_name');

    //     $this->form_validation->set_rules('status', 'Status', 'trim|required');

    //     $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

    //     if($this->form_validation->run() == TRUE){



    //         if(!empty($data['state'])){

    //             $service_data = array(

    //                                     'con_id'=>$data['state']->con_id,

    //                                     'state_id'=>$data['state']->id,

    //                                     'city_name'=>  $this->input->post('city_name'),

    //                                     'slug' => url_title($this->input->post('city_name'),'-', TRUE),

    //                                     'status' =>  $this->input->post('status'),

    //                                     'created'=>  date('Y-m-d h:i:s')

    //                                 );

    //         }else{



    //             $service_data = array(

    //                                     'con_id'=>$data['country']->id,

    //                                     'state_id'=>0,

    //                                     'city_name'=>  $this->input->post('city_name'),

    //                                     'slug' => url_title($this->input->post('city_name'),'-', TRUE),

    //                                     'status' =>  $this->input->post('status'),

    //                                     'created'=>  date('Y-m-d h:i:s')

    //                                 );



    //         }



    //         if($this->country_model->insert('cities',$service_data)){

    //             $this->session->set_flashdata('msg_success','New City has been created successfully.');

    //             if(!empty($data['state'])){

    //                 redirect('backend/country/cities/'.$data['state']->con_id.'/'.$data['state']->id);

    //             }else if(!empty($data['country'])){

    //                 redirect('backend/country/cities/'.$data['country']->id.'/0');

    //             }

    //         } else {

    //             $this->session->set_flashdata('msg_error','Failed, Please try again.');

    //             if(!empty($data['state'])){

    //                 redirect('backend/country/cities/'.$data['state']->con_id.'/'.$data['state']->id);

    //             }else if(!empty($data['country'])){

    //                 redirect('backend/country/cities/'.$data['country']->id.'/0');

    //             }

    //         }

    //     }



    //     $data['template'] = "backend/countries/city_add";

    //     $this->load->view('templates/backend/layout', $data);

    // }



    // public function city_edit($id='',$city_id='',$type='',$offset=''){

    //     _check_superadmin_login(); //check login authentication

    //     $id;

    //     $city_id;



    //     if($type!='con'){

    //         if($type!='sta'){

    //             redirect('backend/country/');

    //         }

    //     }



    //     if(empty($id)||!is_numeric($id)) redirect('backend/country');

    //     if($type=='sta'){

    //         $data['state'] = $this->country_model->get_row('states',array('id'=>$id));

    //         if(empty($data['state']->id)) redirect('backend/country/');

    //     }else if($type=='con'){

    //         $data['country'] = $this->country_model->get_row('countries',array('id'=>$id));

    //         if(empty($data['country']->id)) redirect('backend/country/');

    //     }else{  redirect('backend/country/'); }

        

    //     if(empty($city_id)){

    //        if(!empty($data['state'])){

    //          redirect('backend/country/states/'.$data['state']->id);

    //        }else if(!empty($data['country'])){

    //          redirect('backend/country/');

    //        }

    //     }

    //     $data['cities'] = $this->country_model->get_row('cities',array('id'=>$city_id));

    //     if(empty($data['cities']->id)) redirect('backend/country/cities/'.$data['cities']->id);



    //     $this->form_validation->set_rules('city_name','City Name','trim|required|callback_check_city_name_edit['.$city_id.']');

    //     $this->form_validation->set_rules('status', 'Status', 'trim|required');

    //     $this->form_validation->set_error_delimiters('<div class="error">','</div>');

    //     if($this->form_validation->run() == TRUE){

            



    //         if(!empty($data['state'])){

    //             $service_data=array(

    //                                     'con_id'=>$data['state']->con_id,

    //                                     'state_id'=>$data['state']->id,

    //                                     'city_name'=>  $this->input->post('city_name'),

    //                                     'slug' => url_title($this->input->post('city_name'),'-', TRUE),

    //                                     'status' =>  $this->input->post('status'),

    //                                     'modified'=>  date('Y-m-d h:i:s')

    //                                 );

    //         }else{



    //             $service_data=array(

    //                                     'con_id'=>$data['country']->id,

    //                                     'state_id'=>0,

    //                                     'city_name'=>  $this->input->post('city_name'),

    //                                     'slug' => url_title($this->input->post('city_name'),'-', TRUE),

    //                                     'status' =>  $this->input->post('status'),

    //                                     'modified'=>  date('Y-m-d h:i:s')

    //                                 );



    //         }

    //         // $service_data = array(

    //         //                         'con_id'=>$data['state']->con_id,

    //         //                         'state_id'=>$data['state']->id,

    //         //                         'city_name'=>$this->input->post('city_name'),

    //         //                         'slug'=> url_title($this->input->post('city_name'),'-', TRUE),

    //         //                         'status'=>  $this->input->post('status'),

    //         //                         'modified'=>  date('Y-m-d h:i:s')

    //         //                     );



    //         if($this->country_model->update('cities',$service_data,array('id'=>$city_id))){

    //            $this->session->set_flashdata('msg_success','New city has been updated successfully.');

    //             if(!empty($data['state'])){

    //                 redirect('backend/country/cities/'.$data['state']->con_id.'/'.$data['state']->id);

    //             }else if(!empty($data['country'])){

    //                 redirect('backend/country/cities/'.$data['country']->id.'/0');

    //             }

    //         } else {

    //             $this->session->set_flashdata('msg_error','Failed, Please try again.');

    //            if(!empty($data['state'])){

    //                 redirect('backend/country/cities/'.$data['state']->con_id.'/'.$data['state']->id);

    //             }else if(!empty($data['country'])){

    //                 redirect('backend/country/cities/'.$data['country']->id.'/0');

    //             }

    //         }

    //     }

    //     $data['template'] = "backend/countries/city_edit";

    //     $this->load->view('templates/backend/layout', $data);

    // }





    // public function city_delete($state_id='',$city_id='',$offset='')

    // {

    //     _check_superadmin_login(); //check login authentication

    //     if(empty($state_id)) redirect('backend/country');      

    //     if(empty($city_id)) redirect('backend/country/states/'.$state_id);

    //     if($this->country_model->delete('cities',array('id'=>$city_id))){

    //         $this->session->set_flashdata('msg_success', 'City has been deleted successfully.');

    //         redirect('backend/country/cities/'.$state_id.'/'.$offset);

    //     }else{

    //         $this->session->set_flashdata('msg_error', 'Delete failed, Please try again.');

    //         redirect('backend/country/cities/'.$state_id.'/'.$offset);

    //     }

    // }





    // public function check_city_name($name){

    //     $city_name  = url_title($name, '-', TRUE);     

    //     if($data=$this->country_model->get_row('cities',array('slug'=>$city_name))){

    //         $this->form_validation->set_message('check_city_name', 'This City is already exist.');

    //         return FALSE;

    //     }else{

    //         //$this->form_validation->set_message('password_check', 'The %s does not match.');

    //         return TRUE;

    //     }           

    // }



    // public function check_city_name_edit($name,$id){ 

        

    //     $city_name  = url_title($this->input->post('city_name'),'-',TRUE);

    //     if($data=$this->country_model->get_row('cities',array('id'=>$id))){



    //         $data1=$this->country_model->get_row('cities',array('slug'=>$city_name));

    //         if(!empty($data1)){

    //             if($data->slug==trim($data1->slug)){

    //                 return TRUE;

    //             }else{   

    //                 $this->form_validation->set_message('check_city_name_edit', 'This City is already exist.');

    //                 return FALSE;

    //             } 

    //         }else{

               

    //            return TRUE; 

    //         }



    //     }else{

    //         //$this->form_validation->set_message('password_check', 'The %s does not match.');

    //         return TRUE;

    //     }           

    // }



}