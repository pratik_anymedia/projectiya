<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faqs extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('faq_model');
    }
    public function index($offset = 0)
    {
         _check_superadmin_login(); //check login authentication
        $per_page = 10;
        $data['faqs'] = $this->faq_model->faqs($offset, $per_page);
        $data['offset'] = $offset;
        $config = backend_pagination();
        $config['base_url'] = base_url().'faqs/index/';
        $config['total_rows'] = $this->faq_model->faqs(0, 0);
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'backend/faqs/index';
        $this->load->view('templates/backend/layout', $data);
    }

       //Faqs category

    public function category()
    {

        if(!empty($_POST['category_id'])){
            $i=1;
            foreach ($_POST['category_id'] as $row) {

               $data = array('order' => $this->input->post('order_'.$i));
               $this->superadmin_model->update('faq_category', $data, array('id'=>$row));
              $i++;  
            }
            $this->session->set_flashdata('msg_success', 'Category Order Updated Successfully.');
            redirect('backend/faqs/category');
        }
      

        $data['category'] = $this->faq_model->get_result('faq_category','','',array('order','asc'));
        $data['template'] = "backend/faqs/category";
        $this->load->view('templates/backend/layout', $data);
    }

    public function category_add()
    {
        _check_superadmin_login(); //check login authentication

        $this->form_validation->set_rules('faq_category', 'Category', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $faq_data['faq_category'] = $this->input->Post('faq_category');
            $faq_data['faq_category_slug'] = url_title($this->input->Post('faq_category'), '-', TRUE);
            $faq_data['created'] = date('Y-m-d h:i:s');

            if ($this->faq_model->insert('faq_category', $faq_data)) {
                 $this->session->set_flashdata('msg_success', 'New Faqs Category add successfully.');
                    redirect('backend/faqs/category');
            }else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/faqs/category');
            }
         }
        $data['template'] = 'backend/faqs/category_add';
        $this->load->view('templates/backend/layout', $data);
    }


     public function category_edit($id='')
    {
        _check_superadmin_login(); //check login authentication

        if(empty($id))  redirect('backend/faqs/category');

        $this->form_validation->set_rules('faq_category', 'Category', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $faq_data['faq_category'] = $this->input->Post('faq_category');
            $faq_data['faq_category_slug'] = url_title($this->input->Post('faq_category'), '-', TRUE);
            $faq_data['updated'] = date('Y-m-d h:i:s');

            if ($this->faq_model->update('faq_category', $faq_data, array('id' => $id))) {
                $this->session->set_flashdata('msg_success', 'Faqs Category update successfully.');
                redirect('backend/faqs/category/');
            }else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/faqs/category/');
            }
         }
        //$data['category'] = $this->faq_model->get_row('faq_category', array('id' => $id));
        
        $data['template'] = 'backend/faqs/category_edit';
        $this->load->view('templates/backend/layout', $data);
    }


    public function category_delete($id = '')
    {
        _check_superadmin_login(); //check login authentication

       if(empty($id))  redirect('backend/faqs/category');

        if ($this->faq_model->delete('faq_category', array('id' => $id))) {
            // $this->faq_model->delete('Faqss', array('category_id' => $id));
            // $this->faq_model->delete('faq_comments', array('faq_category_id' => $id));

            $this->session->set_flashdata('msg_success', 'Faqs category deleted successfully.');
            redirect('backend/faqs/category');
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/faqs/category');
        }
    }

     public function category_change_status($status="",$id="") {
        $data=array('status'=>$status);

        if($this->faq_model->update('faq_category',$data,array('id'=>$id))){
        $this->session->set_flashdata('msg_success','Status Updated successfully.');
        redirect('backend/faqs/category');
        }else{
        $this->session->set_flashdata('msg_error','Status Updated successfully.');
        redirect('backend/faqs/category');
        }
    }



    public function add()
    {
         _check_superadmin_login(); //check login authentication
        //$this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_rules('question', 'Question', 'required');
        $this->form_validation->set_rules('answer', 'Answer', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $category_data = array(
                'category' =>$this->input->post('category'),
                'question' => $this->input->post('question'),
                'answer' => $this->input->post('answer'),
                'status' => $this->input->post('status'),
                // 'tag_id'=>$this->input->post('tag_id'),
                'created' => date('Y-m-d h:i:s')
                );

            if($this->faq_model->insert('faqs', $category_data)) {
                $this->session->set_flashdata('msg_success', 'Faq added successfully.');
                redirect('backend/faqs/index');
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/faqs/add');
            }
        }        
        //$data['category'] = $this->faq_model->get_result('faq_category');
        $data['template'] = 'backend/faqs/add';
        $this->load->view('templates/backend/layout', $data);
    }

    public function edit($faq_id = '')
    {
         _check_superadmin_login(); //check login authentication
        if (empty($faq_id)) redirect(base_url().'backend/faqs/index');
       // $this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_rules('question', 'Question', 'required');
        $this->form_validation->set_rules('answer', 'Answer', 'required');
        // $this->form_validation->set_rules('tag_id','Tag', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">','</div>');
        if ($this->form_validation->run() == TRUE) {
            $category_data = array(
                'category' =>$this->input->post('category'),
                'question' => $this->input->post('question'),
                'answer' => $this->input->post('answer'),
                'status' => $this->input->post('status'),
                // 'tag_id'=>$this->input->post('tag_id')
            );
            if ($this->faq_model->update('faqs', $category_data, array('id' => $faq_id))) {
                $this->session->set_flashdata('msg_success', 'Faq updated successfully.');
                redirect('backend/faqs/index');
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/faqs/index');
            }
        }
        //$data['category'] = $this->faq_model->get_result('faq_category');
        $data['faq'] = $this->faq_model->get_row('faqs', array('id' => $faq_id));
        $data['template'] = 'backend/faqs/edit';
        $this->load->view('templates/backend/layout', $data);
    }

    public function delete($faq_id = '')
    {
         _check_superadmin_login(); //check login authentication
        if(empty($faq_id)) redirect(base_url() . 'backend/faqs/index');
        if($this->faq_model->delete('faqs', array('id'=>$faq_id))) {
            $this->session->set_flashdata('msg_success', 'Faq deleted successfully.');
            redirect('backend/faqs/index');
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/faqs/index');
        }
    }

    public function changestatus($id="",$status="",$offset="")
    {
        
        if(!empty($id)){
            $this->faq_model->changestatus($id,$status,$offset,"faqs");
            
        }else{
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/faqs/index');
        }
    }
}