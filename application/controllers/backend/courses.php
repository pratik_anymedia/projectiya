<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	class Courses extends CI_Controller
	{
		public function __construct()
	    {
	        parent::__construct();
	        clear_cache();
	        $this->load->model('course_model');
	    }
	    public function index($offset=0){
			_check_superadmin_login(); //check login authentication	
			$per_page=25;
			$data['courses'] = $this->course_model->courses($offset,$per_page);
			$data['offset'] = $offset;
	 		$config=backend_pagination();
			$config['base_url'] = base_url().'backend/courses/index/';
			$config['total_rows'] = $this->course_model->courses(0,0);
			$config['per_page'] = $per_page;
			$config['uri_segment']=4;
			// if(!empty($_SERVER['QUERY_STRING'])){
		 		//   $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
		  	// }
			$this->pagination->initialize($config);
			$data['pagination']=$this->pagination->create_links();
			// $data['sort_by']=$sort_by;
	        // $data['sort_order']=$sort_order;
	 		$data['template']='backend/course/index';
			$this->load->view('templates/backend/layout',$data);
		}

		public function add(){
			_check_superadmin_login(); //check login authentication
			$this->form_validation->set_rules('course', 'Course Name', 'trim|required');
			$this->form_validation->set_rules('description', 'Course Description', 'trim|required');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if($this->form_validation->run() == TRUE){
				$tech_data = array(
									'course'	  => $this->input->post('course'),
									'description' => $this->input->post('description'),
									'slug'		  => url_title($this->input->post('course'),'-',TRUE),
									'status'	  => $this->input->post('status'),
									'created_date'=> date('Y-m-d h:i:s')
								);

				if($this->course_model->insert('courses',$tech_data)){
					if($this->session->userdata('userfile')):
	                    $this->session->unset_userdata('userfile');
	                endif;
					$this->session->set_flashdata('msg_success','Course has been created successfully.');
					redirect('backend/courses/');

				}
			}
			$data['template']='backend/course/add';
			$this->load->view('templates/backend/layout', $data);
		}

		public function edit($cour_id='')	{
        	_check_superadmin_login(); //check login authentication
			if(empty($cour_id)) redirect(base_url().'backend/technologies');
		   	$data['courses'] = $this->course_model->get_row('courses',array('id'=>$cour_id));
			$this->form_validation->set_rules('course', 'Course Name', 'trim|required');
			$this->form_validation->set_rules('description', 'Course Description', 'trim|required');
			$this->form_validation->set_error_delimiters('<div class="error">','</div>');
			if($this->form_validation->run() == TRUE){
			    $course_data=array(
								'course'	  => $this->input->post('course'),
								'description' => $this->input->post('description'),
								'slug'		  => url_title($this->input->post('course'), '-', TRUE),
								'status'	  => $this->input->post('status'),
								'created_date'=> date('Y-m-d h:i:s')
							);			
				if($this->course_model->update('courses',$course_data,array('id'=>$cour_id))){
					$this->session->set_flashdata('msg_success','Course has been Updated successfully.');

					redirect('backend/courses/');

				}

			}
			$data['template']='backend/course/edit';
			$this->load->view('templates/backend/layout',$data);	
		}







	// public function delete_coupons()
	// {

	// 	_check_superadmin_login(); //check Professor login authentication

	// 	if($_POST){

	// 		$this->form_validation->set_rules('checkall[]','Coupon', 'trim|required');

	// 		$this->form_validation->set_error_delimiters('<div style="color:red;" class="error">','</div>');

	// 		if($this->form_validation->run() == TRUE){

	// 			$coup_id_array= $this->input->post('checkall');

	// 			$FLAG=TRUE;

	// 			// print_r($student_id_array);

	// 			// die();

	// 			//$status = $_POST['all_status'];

	// 			$coup_cust = count($coup_id_array);

	// 			for($i=0; $i<$coup_cust ; $i++){ 

	// 				$cust_status = $this->course_model->delete('coupons',array('id'=>$coup_id_array[$i]));	

	// 				//$this->course_model->delete('user_shipping_info',array('customer_id'=>$cust_id_array[$i]));

	// 				if(!$cust_status){

	// 					$FLAG=FALSE;

	// 				}

	// 			}			

	// 			if($FLAG){

	// 				$this->session->set_flashdata('msg_success',"Coupon have been Deleted successfully.");

	// 				redirect('backend/coupons');

	// 			}

	// 		}else{

	// 			$this->session->set_flashdata('msg_error',"Coupon delete Operation Failed.");

	// 			redirect('backend/coupons');

	// 		}

	// 	}else{

	// 		$this->session->set_flashdata('msg_error',"Coupon delete Operation Failed...");

	// 		redirect('backend/coupons');

	// 	} 

	// }


	public function delete($cour_id =''){

		_check_superadmin_login(); //check login authentication

		if(empty($cour_id)) redirect('backend/courses');

		$this->course_model->delete('courses',array('id'=>$cour_id));

		$this->session->set_flashdata('msg_success','Course has been Deleted successfully.');

		redirect('backend/courses');		

	}	





	}    