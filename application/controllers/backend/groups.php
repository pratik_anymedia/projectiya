<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        clear_cache();
        $this->load->model('customer_model');
    }

    public function index($offset=0,$flag=0){
		_check_superadmin_login(); //check login authentication
		$per_page=5;
		$data['groups'] = $this->customer_model->groups($offset,$per_page);
		$data['offset'] = $offset;
		$data['flag'] = $flag;
 		$config=backend_pagination();
		$config['base_url'] = base_url().'backend/groups/index/';
		$config['total_rows'] = $this->customer_model->groups(0,0);
		$config['per_page'] = $per_page;
		$config['uri_segment']=4;
		// if(!empty($_SERVER['QUERY_STRING'])){
	 	//   $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
	  	// }
		$this->pagination->initialize($config);
		$data['pagination']=$this->pagination->create_links();
		// $data['sort_by']=$sort_by;
  //       $data['sort_order']=$sort_order;
 		$data['template']='backend/group/index';
		$this->load->view('templates/backend/layout', $data);
	}

	public function add(){
		_check_superadmin_login(); //check login authentication

		$this->form_validation->set_rules('group_name','Group Name','trim|required');
		$this->form_validation->set_rules('group_description','Group Description','trim');
		$this->form_validation->set_rules('shipping_discount','Shipping Discount','numeric');
		$this->form_validation->set_rules('free_tax','Free Tax','numeric');
		$this->form_validation->set_rules('other_discount','Other ','numeric');

		$this->form_validation->set_error_delimiters('<div class="error">','</div>');
		if($this->form_validation->run() == TRUE){

			$cust_data = array(
								'group_name'=>$this->input->post('group_name'),
								'group_description'=>$this->input->post('group_description'),
								'tax_free'=>$this->input->post('free_tax'),
								'shipping_discount'=>$this->input->post('shipping_discount'),
								'other_discount'=>$this->input->post('other_discount'),
								'status' => $this->input->post('status'),
								'created'=> date('Y-m-d h:i:s')
							  );

			if($group_id=$this->customer_model->insert('groups',$cust_data)){
				// $count= count($this->input->post('mutliple_customers'));
				// $customer=$this->input->post('mutliple_customers');
				// for($i=0;$i<$count;$i++){
					// 	$cut_id=$customer[$i];
					// 	$user = $this->customer_model->update('users',array('group_id'=>$group_id),array('id'=>$cut_id));
				// }
				$this->session->set_flashdata('msg_success','New Group has been created successfully.');
				redirect('backend/groups/');
			}
		}
		$data['customers']=$this->customer_model->get_result('users',array('status'=>1,'user_role'=>1,'group_id'=>''));
		$data['template']='backend/group/add';
		$this->load->view('templates/backend/layout', $data);
	}


	public function add_customers($group_id='')
	{
		_check_superadmin_login(); //check login authentication
		if(empty($group_id)) redirect('backend/groups/');
		$data['group'] = $this->customer_model->get_row('groups',array('id'=>$group_id));
		if(empty($data['group'])) redirect('backend/groups/');		
		$data['customers']=$this->customer_model->add_customers('users',array('status'=>1,'user_role'=>1,'group_id'=>''));
		$data['template']='backend/group/add_customers';
		$this->load->view('templates/backend/layout', $data);
	}

	public function send_email_groups()
    {
		if($_POST){
			$flag='';
			$rope='';
			$ids= $_POST['group_id'];
			$gro_name= $_POST['group_name'];
			$subject = $_POST['subject']; 
			$message = $_POST['message']; 
			$groups  = $this->customer_model->get_result('groups',array('id'=>$ids));
			if(empty($groups)){
				echo 'problem';
				return false;
			}
		    $customers = $this->customer_model->get_result('users',array('user_role'=>1,'status'=>1,'group_id'=>$ids)); 
			if(empty($customers)){
				echo 'problem';
				return false;
			}		
			foreach($customers as $value){
				$arr = $value->id;	
				$customer = $this->customer_model->get_row('users',array('id'=>$arr,'status'=>1));
				$status = $this->sent($customer->email,$customer->first_name,$customer->last_name,$subject,$message); 
			}
			if($flag){
				echo 'problem';
				return false;
			}
			echo 'success';
			return TRUE;
		}   
    }

    public function sent($email='',$first_name='',$last_name='',$subject='',$message='')
	{
		if(!empty($email)){
		    $email;
            $i=4;
            $this->load->library('developer_email');
            $email_template=$this->developer_email->get_email_template($i);
            $param=array(
                 'template' =>  array(
                 'temp'  =>  $email_template->template_body,
                 'var_name' =>  array(
                              'full_name'=> $first_name.' '.$last_name,
                              'site_name'=> SITE_NAME,
                              'site_url' => base_url(),
                              'email'    => $email,
                              'message'  => $message
                            ),
                        ),
                  'email' =>  array(
                  'to'    =>   $email,
                  'from'  =>   NO_REPLY_EMAIL,
                  'from_name' => SITE_NAME,
                  'subject' => $subject,
                )
            );
            $status=$this->developer_email->send_mail($param);
		}		
	}	



	function remove_from_group($value='')
	{
		if($_POST){
			$cust_id= $_POST['cust_id'];
			$group_id = $_POST['group_id']; 
			$group = $this->customer_model->get_row('groups',array('id'=>$group_id,'status'=>1));
			if(empty($group)){
				echo 'problem';
				return FALSE;
			}
			$customer = $this->customer_model->get_row('users',array('id'=>$cust_id));
			if(empty($group)){
				echo 'problem';
				return FALSE;
			}
			$array = array('group_id' =>0);
			$status = $this->customer_model->update('users',$array,array('id'=>$cust_id));
			echo 'success';
			return TRUE;

		}
	}

	
	
	public function edit($group_id){

		_check_superadmin_login(); //check login authentication
		if(empty($group_id)) redirect(base_url().'backend/groups/');
		$this->form_validation->set_rules('group_name','Group Name','trim|required');
		$this->form_validation->set_rules('group_description','Group Description','trim');
		$this->form_validation->set_rules('shipping_discount','Shipping Discount','numeric');
		$this->form_validation->set_rules('free_tax','Free Tax','numeric');
		$this->form_validation->set_rules('other_discount','Other ','numeric');
		$this->form_validation->set_error_delimiters('<div class="error">','</div>');
		if($this->form_validation->run() == TRUE){
			
			$cust_data = array(
								'group_name'=>$this->input->post('group_name'),
								'group_description'=>$this->input->post('group_description'),
								'tax_free'=>$this->input->post('free_tax'),
								'shipping_discount'=>$this->input->post('shipping_discount'),
								'other_discount'=>$this->input->post('other_discount'),
								'status' => $this->input->post('status'),
								'created'=> date('Y-m-d h:i:s')
							  );
			// $cust_data = array(
			// 					'group_name'=> $this->input->post('group_name'),
			// 					'tax_free'=>$free_tax,
			// 					'free_shipping'=> $free_ship,
			// 					'ship_date'=>$ship_date,
			// 					'status' => $this->input->post('status'),
			// 					'created'=> date('Y-m-d h:i:s')
			// 				  );
		
			if($this->customer_model->update('groups',$cust_data,array('id'=>$group_id))){
				$this->session->set_flashdata('msg_success','New Group has been Updated successfully.');
				redirect('backend/groups/');
			}
		}
		$data['group'] = $this->customer_model->get_row('groups',array('id'=>$group_id));
		$data['template']='backend/group/edit';
		$this->load->view('templates/backend/layout', $data);
	}

	public function customer_group_delete()
	{
		if($_POST){
			$flag='';
			$rope='';
			$ids= $_POST['ids'];
			$group_id = $_POST['group_id']; 
			$group = $this->customer_model->get_row('groups',array('id'=>$group_id,'status'=>1));
			if(empty($group)){
				echo 'group_no';
				return FALSE;
			}
			for($i=0; $i <count($ids);$i++){ 
				$arr = explode('_',$ids[$i]);	
				$customer = $this->customer_model->get_row('users',array('id'=>$arr[2],'status'=>1));
				if($customer->group_id!=$group_id){
					$flag=1;
				}else{
					$arrayName = array('group_id'=>0);
					$this->customer_model->update('users',$arrayName,array('id'=>$arr[2]));
				}
			}
			if($flag){
				echo 'Allready';
				return FALSE; 
			}
			echo 'success';
			return TRUE;
		}	
	}

    function customer_group()
	{
		if($_POST){
			$flag='';
			$rope='';
			$ids= $_POST['ids'];
			$group_id = $_POST['group_id']; 
			$group = $this->customer_model->get_row('groups',array('id'=>$group_id,'status'=>1));
			if(empty($group)){
				echo 'group_no';
				return FALSE;
			}
			for($i=0; $i <count($ids);$i++){ 
				$arr = explode('_',$ids[$i]);	
				$customer = $this->customer_model->get_row('users',array('id'=>$arr[2],'status'=>1));
				if(!empty($customer->group_id)){
					$flag=1;
				}else{
					$arrayName = array('group_id'=>$group_id);
					$this->customer_model->update('users',$arrayName,array('id'=>$arr[2]));
				}
			}
			if($flag){
				echo 'Allready';
				return FALSE; 
			}
			echo 'success';
			return TRUE;
		}	
	}

	public function delete($group_id ='')	{
		_check_superadmin_login(); //check login authentication
		if(empty($group_id)) redirect('backend/groups');
		$this->customer_model->delete('groups',array('id'=>$group_id));
		// $this->customer_model->delete('user_shipping_info',array('customer_id'=>$cust_id));
		$this->session->set_flashdata('msg_success','Group has been deleted successfully.');
		redirect('backend/groups');		
	}


}