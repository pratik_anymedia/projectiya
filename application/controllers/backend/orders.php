<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller
{

  public function __construct()
  {
      parent::__construct();
      clear_cache();
      $this->load->model('order_model');
  }

  public function index($sort_by='id',$sort_order='desc',$offset=0)
  {   
      _check_superadmin_login(); //check login authentication
      $per_page=40;
      $limit=$per_page;    
      $config=backend_pagination();  
      $data['orders'] = $this->order_model->orders($offset,$limit,$sort_by,$sort_order);  
      
      //echo '<pre>'; print_r($data['orders']);
      $config['base_url']    = base_url().'backend/orders/index/'. $sort_by.'/'.$sort_order.'/';
      $config['total_rows']  = $this->order_model->orders(0,0,$sort_by,$sort_order);
      $config['per_page']    = $limit;
      $config['num_links']   = 5;   
      $config['uri_segment'] = 6;
      if(!empty($_SERVER['QUERY_STRING'])){
        $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
      }
      $this->pagination->initialize($config);     
      $data['pagination'] = $this->pagination->create_links();
      $offset++;
      $data['offset'] = $offset;
      $data['template']='backend/orders/index';
      $this->load->view('templates/backend/layout', $data);
  }


  public function add($type='',$id='')
  {
      _check_superadmin_login(); //check login authentication
      $data['type']=$type;
      if(!empty($type)){        
        if($type=='cust_info' && !empty($id)){
            $data['customer_info'] = $this->order_model->search_cust_info($id);
            if(!empty($data['customer_info'])){
              $data['cust_info_id'] = $data['customer_info']->id;    
            }else{
              $data['customer_info']='';
              $data['cust_info_id']='';
            } 
        }else{
          $data['customer_info']='';
          $data['cust_info_id']='';
        }
        if($type=='customer'){
            $data['customers'] = $this->order_model->search_customers(); 
        }
        if($type=='product'){
            $data['products'] = $this->order_model->search_customers();     
        }
      }
      $data['template']='backend/orders/add';
      $this->load->view('templates/backend/layout', $data);
  }



  public function order_info($order_id=''){   

    $this->check_login();

    if($order_id=='')

    redirect('superadmin/orders');    

    $user_data=$this->order_model->order_user_info($order_id);

    $data['order_user_info']=$user_data;

    $this->form_validation->set_rules('order_status','Order Status','required');

      if($this->form_validation->run() == TRUE){  

        $status = $this->input->post('order_status');

        $resp=$this->order_model->update("orders", array('order_status' => $status), array('id' => $order_id));

          if($resp){

              $msg = "Status of the order with order id <strong>#".$user_data->order_id."</strong> has been updated to <strong>".fetch_order_status($status)."</strong>";

              $this->load->library('smtp_lib/smtp_email');

              $subject = 'Notification For Change In Order Status'; // Subject for email

              $from = array('no-reply@shirtscore.com' =>'shirtscore.com');  // From email in array form

              $to = array($user_data->email => $user_data->recipient_name);

              $html = $this->template_order_info_status($user_data->recipient_name,$subject,$msg);

              $is_fail = $this->smtp_email->sendEmail($from, $to, $subject, $html);

              if($is_fail){

                $this->session->set_flashdata('error_msg','Email notification failed.');

                $this->session->set_flashdata('success_msg','Order status updated successfully.');

              }else{

                $this->session->set_flashdata('success_msg','Order status updated successfully.');

              }

          }

        redirect('superadmin/order_info/'.$order_id);

      }

      $data['order_info'] = $this->order_model->order_info($order_id);    

      $data['order_id'] = $order_id;

      $data['template'] = 'superadmin/order_info';

      $this->load->view('templates/superadmin_template', $data);    

  }



  public function delete_orders($order_id ='')  {

    _check_superadmin_login(); //check login authentication

    if(empty($order_id)) redirect('backend/orders');

    //$this->order_model->delete('order_info',array('order_id'=>$order_id));

    //$this->order_model->delete('orders_products',array('orders_id'=>$order_id));

    $this->order_model->delete('orders',array('id'=>$order_id));

    $this->session->set_flashdata('msg_success','Order has been deleted successfully.');

    redirect('backend/orders/');    

  }





  public function multi_delete_orders()
  {
    _check_superadmin_login(); //check Professor login authentication
    if($_POST){
      $this->form_validation->set_rules('checkall[]','order', 'trim|required');
      $this->form_validation->set_error_delimiters('<div style="color:red;" class="error">','</div>');
      if($this->form_validation->run() == TRUE){
        $order_ids= $this->input->post('checkall');
        $FLAG=TRUE;
        // print_r($student_id_array);
        // die();
        //$status = $_POST['all_status'];
        $count_order = count($order_ids);
//        print_r($count_order);die;
        for($i=0; $i<$count_order ; $i++){     
          //$this->order_model->delete('order_info', array('order_id'=>$order_ids[$i]));
          //$this->order_model->delete('orders_products', array('orders_id'=>$order_ids[$i]));

          $order_status = $this->order_model->delete('orders',array('id'=>$order_ids[$i]));

          if(!$order_status){

            $FLAG=FALSE;

          }

        }     

        if($FLAG){

          $this->session->set_flashdata('msg_success',"Order have been Deleted successfully.");

          redirect('backend/orders');

        }

      }else{

        $this->session->set_flashdata('msg_error',"Order delete Operation Failed.");

        redirect('backend/orders');

      }

    }else{

      $this->session->set_flashdata('msg_error',"Order delete Operation Failed...");

      redirect('backend/orders');

    } 

  }

  public function change_status()

  {  

    _check_superadmin_login(); //check login authentication

    if($_POST){

        $id = $_POST['order_id'];

        $status = $_POST['status'];

        $data = array('order_status'=>$status);

        if($this->order_model->update('orders',$data,array('id'=>$id)))

        {

          echo 'successfully';

          return TRUE;

        }else{

          echo 'failed';

          return FALSE;

        }

    }  

  }



  public function change_multiple_status()

  {

    _check_superadmin_login(); //check login authentication

    if($_POST){

      $FLAG=TRUE;

      $ids = $_POST['ids'];

      $status = $_POST['status'];

      $data = array('order_status'=>$status);

      for($i=0; $i <count($ids) ; $i++) { 

         $status= $this->order_model->update('orders',$data,array('id'=>$ids[$i]));

        if(empty($status)){

             $FLAG=FALSE;

        } 

      }

      if($FLAG){

        echo 'successfully';

        return TRUE;

      }else{

        echo 'failed';

        return FALSE;

      }

    }  

  }


  public function order_view($order_id='')
  {
      //print_r($order_id);
    _check_superadmin_login(); //check login authentication
    if(empty($order_id)) redirect('backend/orders');
    $data['order'] = $this->order_model->get_row('orders',array('id'=>$order_id));
    
    if(empty($data['order'])) redirect('backend/orders');
   
    $data['order_user_info'] = $this->order_model->get_row('users',array('id'=> $data['order']->user_id));

    $data['ordered_project'] = $this->order_model->get_result('projects',array('id'=>$data['order']->project_id));

    if(empty($data['ordered_project']) && empty($data['order_user_info'])) redirect('backend/orders');

    $this->form_validation->set_rules('ship_first_name','First Name','trim|required');

    $this->form_validation->set_rules('ship_last_name','Last Name','trim|required');

    $this->form_validation->set_rules('ship_email','Email','trim|valid_email|required');

    $this->form_validation->set_rules('ship_street_address_1','First Street Address','trim|required');

    $this->form_validation->set_rules('ship_street_address_2','Second Street Address','trim');

    $this->form_validation->set_rules('ship_city','City','trim|required');

    $this->form_validation->set_rules('ship_country','Country','trim|required');

    $this->form_validation->set_rules('ship_zip_code', 'Zip Code','trim|required|callback_alpha_numeric_space');

    $this->form_validation->set_rules('ship_state','State','trim');

    $this->form_validation->set_rules('ship_phone','Phone','trim|numeric');

    $this->form_validation->set_rules('memo','Memo','trim');

    $this->form_validation->set_rules('note','Note','trim');

    $this->form_validation->set_rules('comment','Comment','trim');

    $this->form_validation->set_error_delimiters('<div class="error">','</div>');

    if($this->form_validation->run()==TRUE){

        $order_data = array(
                              'memo'=>$this->input->post('memo'),
                              'note'=>$this->input->post('note'),
                              'comment'=>$this->input->post('comment'),
                              'order_status'=>$this->input->post('order_status')
                            ); 

        $this->order_model->update('orders',$order_data,array('id'=>$order_id));
        $cust_data_ship = array(
                                  'ship_first_name'=> $this->input->post('ship_first_name'),
                                  'ship_last_name'=> $this->input->post('ship_last_name'),
                                  'ship_email'   => $this->input->post('ship_email'),
                                  'ship_phone'   => $this->input->post('ship_phone'),
                                  'ship_zip_code'=> $this->input->post('ship_zip_code'),
                                  // 'ship_fax' => $this->input->post('ship_fax'),
                                  'ship_street_address_1'=> $this->input->post('ship_street_address_1'),
                                  'ship_street_address_2'=> $this->input->post('ship_street_address_2'),
                                  'ship_city'   => $this->input->post('ship_city'),
                                  'ship_country'=> $this->input->post('ship_country'),
                                  'ship_state'  => $this->input->post('ship_state'),
                                );

        $this->order_model->update('orders',$cust_data_ship,array('id'=>$order_id));
        $this->session->set_flashdata('msg_success','Order data has been Updated successfully.');
        redirect('backend/orders/order_view/'.$order_id);
    }

    //$data['country']=$this->order_model->get_result('countries',array('status'=>1),'',array('slug','asc'));
    $data['template']='backend/orders/view_';
    $this->load->view('templates/backend/layout', $data);

  }



  public function alpha_numeric_space($str)
  {
    if(!preg_match("/^([-a-z0-9_ ])+$/i",$str)){
      $this->form_validation->set_message('alpha_numeric_space', 'Please provide alpha numeric value in zip code.');
      return FALSE;
    } else {
      return TRUE;
    }
  }


    public function export($type='csv',$arrayName=''){

      _check_superadmin_login(); //check login authentication

      // if(empty($class_id)) redirect('professor/classes');

      $ids='';

      if($_POST){

        $cust_id_array= $this->input->post('checkall');

        if(!empty($cust_id_array)){

          $ids= $cust_id_array;

        }

      }

      $orders = $this->order_model->get_orders_info($ids);     

      if($orders==FALSE){

        $this->session->set_flashdata('msg_info',"No Orders Available.");

        redirect('backend/orders/');

      }

      header("Content-type: text/csv");

      header("Content-Disposition: attachment; filename=orders_list_".date('Y-m-d').".csv");

      header("Pragma: no-cache");

      header("Expires: 0");

      $file = fopen('php://output', 'w');

              fputcsv($file, array(

                '#',

                'Order Id',

                'buyer id' ,

                'Order Status',

                'first name',

                'last name',

                'email',

                'phone',

                'fax',

                'street address 1',

                'street address 2' ,

                'city',

                'state',

                'country',

                'zip code' ,   

                'ship first name',

                'ship last name' ,

                'ship email',

                'ship street address 1', 

                'ship street address 2', 

                'ship phone', 

                'ship city', 

                'ship state', 

                'ship country' ,

                'ship zip code',

                'created',

                'Gross Amount' ,

                'total amount',

                'redeem amount' ,

                'payment method',

                'shipping method' ,

                'shipping amount', 

                'transaction id', 

                'is_gift' ,

                'payment_status',

                'Items(Prodcut name,Quantity,unit price,SKU,Total Price)'

                // 'biling_adrs_same_2_adrs',

                // 'receive_email',

                // 'order_status',

                // 'Items'quantity pro_name unit_price product_type pro_sku total_price

            ));

         $str='';  $p=0;foreach ($orders as $row) { $p++;

            if($row->order_status==1){ $status='Pending';  }else if($row->order_status==2){ $status='Approved'; }else if($row->order_status==3){  $status='Shipped'; }else if($row->order_status==4){  $status='Completed'; }else if($row->order_status==5){  $status='On Hold'; }  

            $order_products = $this->order_model->get_result('orders_products',array('orders_id'=>$row->id));

            



            foreach ($order_products as $value){

                $str.=$value->pro_name." , ".$value->quantity." , ".$value->unit_price." , ".$value->pro_sku." , ".$value->total_price."\r\n";

            }

          



        $rows=array(

          $p,

                      trim($row->order_id),

                      trim($row->buyer_id),

                      trim($status),

                      trim($row->first_name),

                      trim($row->last_name),

                      trim($row->email),

                      trim($row->phone),

                      trim($row->fax),

                      trim($row->street_address_1),

                      trim($row->street_address_2),

                      trim($row->city),

                      trim($row->state),

                      trim($row->country),

                      trim($row->zip_code),

                      trim($row->ship_first_name),

                      trim($row->ship_last_name),

                      trim($row->ship_email),

                      trim($row->ship_street_address_1),

                      trim($row->ship_street_address_2),

                      trim($row->ship_phone),

                      trim($row->ship_city),

                      trim($row->ship_state),

                      trim($row->ship_country),

                      trim($row->ship_zip_code),

                      date('d-m-Y',strtotime($row->created)),

                      trim($row->gross_amount),

                      trim($row->total_amount),

                      trim($row->redeem_amount),

                      trim($row->payment_method),

                      trim($row->shipping_method),

                      trim($row->shipping_amount),

                      trim($row->transaction_id),

                      trim($row->is_gift),

                      trim($row->payment_status),

                      trim($str),

      );

      $str='';

        fputcsv($file, $rows);

        }

        exit();

  }







  public function stamp_export($type='csv',$arrayName='')

  {

      _check_superadmin_login(); //check login authentication

      // if(empty($class_id)) redirect('professor/classes');

      $ids='';

      if($_POST){

        $cust_id_array= $this->input->post('checkall');

        if(!empty($cust_id_array)){

          $ids= $cust_id_array;

        }

      }

      $orders = $this->order_model->get_orders_info($ids);     

      if($orders==FALSE){

        $this->session->set_flashdata('msg_info',"No Orders Available.");

        redirect('backend/orders/');

      }

      header("Content-type: text/csv");

      header("Content-Disposition: attachment; filename=orders_list_".date('Y-m-d').".csv");

      header("Pragma: no-cache");

      header("Expires: 0");

      $file = fopen('php://output', 'w');

              fputcsv($file, array(

                '#',

                'Order Id',

                'Order Status',

                'ship first name',

                'ship last name' ,

                'ship email',

                'ship street address 1', 

                'ship street address 2', 

                'ship phone', 

                'ship city', 

                'ship state', 

                'ship country' ,

                'ship zip code',

                'created',

                'shipping method' ,

               // 'Items(Prodcut name,Quantity,unit price,SKU,Total Price)'

                // 'biling_adrs_same_2_adrs',

                // 'receive_email',

                // 'order_status',

                // 'Items'quantity pro_name unit_price product_type pro_sku total_price

            ));

         $str='';  $p=0;foreach ($orders as $row) { $p++;

            if($row->order_status==1){ $status='Pending';  }else if($row->order_status==2){ $status='Approved'; }else if($row->order_status==3){  $status='Shipped'; }else if($row->order_status==4){  $status='Completed'; }else if($row->order_status==5){  $status='On Hold'; }  

            // $order_products = $this->order_model->get_result('orders_products',array('orders_id'=>$row->id));

            



            // foreach ($order_products as $value){

            //     $str.=$value->pro_name." , ".$value->quantity." , ".$value->unit_price." , ".$value->pro_sku." , ".$value->total_price."\r\n";

            // }

            



        $rows=array(

          $p,

                      trim($row->order_id),

                      trim($status),

                      trim($row->ship_first_name),

                      trim($row->ship_last_name),

                      trim($row->ship_email),

                      trim($row->ship_street_address_1),

                      trim($row->ship_street_address_2),

                      trim($row->ship_phone),

                      trim($row->ship_city),

                      trim($row->ship_state),

                      trim($row->ship_country),

                      trim($row->ship_zip_code),

                      date('d-m-Y',strtotime($row->created)),

                      trim($row->shipping_method),

                     

      );

      $str='';

        fputcsv($file, $rows);

        }

        exit();









  }



  public function add_new_customer()

  {

    _check_superadmin_login(); //check login authentication

    if($_POST){

      $array = array(

                     'first_name'=>$_POST['first_name'],

                     'last_name'=>$_POST['first_name'],

                     'email'=>$_POST['email'],

                     'company_name'=>$_POST['company_name'],

                    );  

      $this->session->set_userdata('add_new_customer',$array);

      return 'create_session';

    }

  }





} 