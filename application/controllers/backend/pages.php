<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct() {
        parent::__construct();
        clear_cache();
         $this->lang->load('message', 'english');
        $this->load->model('course_model');
    }

    private function _check_login() {
        if (superadmin_logged_in() === FALSE)
            redirect('superadmin/login');
    }

    public function index($offset = 0) {
        $this->_check_login(); //check login authentication		
        $per_page = 20;
        $data['offset'] = $offset;
        $data['pages'] = $this->course_model->pages($offset, $per_page, 'page');
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'superadmin/pages/';
        $config['total_rows'] = $this->course_model->pages(0, 0, 'page');
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'backend/pages/pages';
        $this->load->view('templates/backend/layout', $data);
    }

    public function page_edit($page_id = '', $offset = '') {
        $this->_check_login(); //check login authentication
        if (empty($page_id))
            redirect('superadmin/pages');
        $this->form_validation->set_rules('post_title', 'Page Title', 'required');
        //$this->form_validation->set_rules('post_content','Page Content','required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $page_data = array(
                'post_title' => $this->input->post('post_title'),
                'post_slug' => url_title($this->input->post('post_title'), '-', TRUE),
                'post_content' => $this->input->post('post_content'),
                'post_status' => $this->input->post('post_status'),
                'page_type' => $this->input->post('pagetype'),
                'post_type' => 'page',
                'post_updated' => date('Y-m-d h:i:s')
            );
            if ($this->course_model->update('posts', $page_data, array('id' => $page_id))) {
                $this->session->set_flashdata('msg_success', 'Page updated successfully.');
                redirect('backend/pages/');
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/pages/');
            }
        }
        $data['page'] = $this->course_model->get_row('posts', array('id' => $page_id, 'post_type' => 'page'));
        $data['template'] = 'backend/pages/page_edit';
        $this->load->view('templates/backend/layout', $data);
    }

    public function add() {
        $this->_check_login(); //check login authentication
        $this->form_validation->set_rules('post_title', 'Page Title', 'required');
        //$this->form_validation->set_rules('post_content','Page Content','required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $page_data = array(
                'post_title' => $this->input->post('post_title'),
                'post_slug' => url_title($this->input->post('post_title'), '-', TRUE),
                'post_content' => $this->input->post('post_content'),
                'post_status' => $this->input->post('post_status'),
                'page_type' => $this->input->post('pagetype'),
                'post_type' => 'page',
                'post_updated' => date('Y-m-d h:i:s')
            );
            if ($this->course_model->insert('posts', $page_data)) {
                $this->session->set_flashdata('msg_success', $this->lang->line('ADD_PAGE_SUCCESS'));
                redirect('backend/pages/');
            } else {
                $this->session->set_flashdata('msg_error', $this->lang->line("ADD_PAGE_FAILED"));
                redirect('backend/pages/');
            }
        }
        
        $data['template'] = 'backend/pages/page_add';
        $this->load->view('templates/backend/layout', $data);
    }

    public function page_delete($page_id ='',$offset=''){
       $this->_check_login(); //check login authentication
       if(empty($page_id)) redirect('superadmin/pages');
       if($this->course_model->delete('posts',array('id'=>$page_id,'post_type'=>'page'))){								
               $this->session->set_flashdata('msg_success','Page deleted successfully.');
               redirect('backend/pages/');				
       }else{
               $this->session->set_flashdata('msg_error','Failed, Please try again.');
               redirect('backend/pages/');
       }		
    }
     
    // public function posts($offset=0){
    // 	    $this->_check_login(); //check login authentication	
    // 	    $per_page=20;
    // 	    $data['offset']=$offset;
    // 	    $data['pages'] = $this->course_model->pages($offset,$per_page,'post');
    // 		$config=backend_pagination();
    // 	    $config['base_url'] = base_url().'superadmin/posts/';
    // 	    $config['total_rows'] = $this->course_model->pages(0,0,'post');
    // 	    $config['per_page'] = $per_page;
    // 	    $config['uri_segment'] =3;		
    // 	    $this->pagination->initialize($config);
    // 	    $data['pagination']=$this->pagination->create_links();
    // 	    $data['template']='superadmin/posts';
    // 		$this->load->view('templates/backend/layout', $data);
    //  }
    // public function post_add(){
    // 	$this->_check_login(); //check login authentication	
    // 	$this->form_validation->set_rules('post_title','Page Title','required');
    // 	$this->form_validation->set_rules('post_content','Page Content','required');
    // 	$this->form_validation->set_error_delimiters('<div class="error">', '</div>');			
    // 	if($this->form_validation->run() == TRUE){			
    // 		$page_data = array( 
    // 							'post_title'	=>	$this->input->post('post_title'),
    // 							'post_slug'		=> 	url_title($this->input->post('post_title'), '-', TRUE),		
    // 							'post_category' 	=>	$this->input->post('post_category'),				
    // 							'post_content'	=>	htmlentities($this->input->post('post_content')),
    // 							'post_status'	=> 	$this->input->post('post_status'),
    // 							'post_type'		=>	'post',
    // 							'post_created'	=>	date('Y-m-d h:i:s')
    // 						  );		
    // 		if($this->course_model->insert('posts',$page_data)){
    // 			$this->session->set_flashdata('msg_success','Post added successfully.');
    // 			redirect('superadmin/posts');				
    // 		}else{
    // 			$this->session->set_flashdata('msg_error','Failed, Please try again.');
    // 			redirect('superadmin/posts');
    // 		}
    // 	}
    // 	$data['post']=$this->course_model->get_result('posts',array('post_type'=>'post'));
    // 	$data['template'] ='superadmin/post_add';
    // 	$this->load->view('templates/backend/layout', $data);
    // }
    // public function post_edit($page_id='',$offset=''){
    // 	$this->_check_login(); //check login authentication
    // 	if(empty($page_id)) redirect('superadmin/posts');
    // 	$this->form_validation->set_rules('post_title','Page Title','required');
    // 	//$this->form_validation->set_rules('post_content','Page Content','required');	
    // 	$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
    // 	if ($this->form_validation->run() == TRUE){			
    // 		$page_data = array( 
    // 							'post_title'	=>	$this->input->post('post_title'),
    // 							'post_slug'		=> 	url_title($this->input->post('post_title'), '-', TRUE),	
    // 							'post_content'	=>	htmlentities($this->input->post('post_content')),			
    // 							'post_category' =>	$this->input->post('post_category'),					
    // 							'post_status'	=> 	$this->input->post('post_status'),	
    // 							'post_type'	    =>	'post',			
    // 							'post_updated'	=>	date('Y-m-d h:i:s')
    // 						  );		
    // 		if($this->course_model->update('posts',$page_data,array('id'=>$page_id))){								
    // 			$this->session->set_flashdata('msg_success','Post updated successfully.');
    // 			redirect('superadmin/posts/'.$offset);				
    // 		}else{
    // 			$this->session->set_flashdata('msg_error','Failed, Please try again.');
    // 			redirect('superadmin/posts/'.$offset);
    // 		}
    // 	}
    // 	$data['page'] = $this->course_model->get_row('posts',array('id'=>$page_id,'post_type'=>'post'));
    // 	$data['template'] ='superadmin/post_edit';
    // 	$this->load->view('templates/backend/layout', $data); 
    // }
    // public function post_delete($page_id ='',$offset=''){
    // 	$this->_check_login(); //check login authentication
    // 	if(empty($page_id)) redirect('superadmin/pages');
    // 	if($this->course_model->delete('posts',array('id'=>$page_id,'post_type'=>'post'))){								
    // 			$this->session->set_flashdata('msg_success','Post deleted successfully.');
    // 			redirect('superadmin/posts/'.$offset);				
    // 	}else{
    // 		$this->session->set_flashdata('msg_error','Failed, Please try again.');
    // 		redirect('superadmin/posts/'.$offset);
    // 	}		
    // }
}
