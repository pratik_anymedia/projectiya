<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Zones extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        clear_cache();
        $this->load->model('zone_model');
    }

    public function index($offset=0){
		_check_superadmin_login(); //check login authentication
		$per_page=40;
		$data['zones'] = $this->zone_model->zones($offset,$per_page);
		$data['offset'] = $offset;
 		$config=backend_pagination();
		$config['base_url'] = base_url().'backend/zones/index/';
		$config['total_rows'] = $this->zone_model->zones(0,0);
		$config['per_page'] = $per_page;
		$config['uri_segment']=4;
		// if(!empty($_SERVER['QUERY_STRING'])){
		//    $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
		// }
		$this->pagination->initialize($config);
		$data['pagination']=$this->pagination->create_links();
		// $data['sort_by']=$sort_by;
  		// $data['sort_order']=$sort_order;
 		$data['template']='backend/zones/index';
		$this->load->view('templates/backend/layout', $data);
	}

	public function add($value=''){
		_check_superadmin_login(); //check login authentication
		if($_POST){
			if(!empty($_POST['country']) || !empty($_POST['state'])){
				// $country = implode(',',$this->input->post('country'));
				// $state = implode(',',$this->input->post('state'));
				$arrayName=array(
								   	'zone_id'=>random_string('numeric',8),
								   	'zone_name'=>$this->input->post('zone_name'),	
					               	'countries'=>$this->input->post('con_div'),
					               	'states'=>$this->input->post('state_div'),
					               	'discount_type'=>'order-subtotal',
					               	'zip_codes'=>$this->input->post('zipcode_div'),
					               	'status'=>$this->input->post('status'),
					               	'created'=>date('Y-m-d h:i:s')
					            );

				if($this->zone_model->insert('zones',$arrayName)){
					$this->session->set_flashdata('msg_success','Zone has been added successfully.');
					redirect('backend/zones');
				}else{
					$this->session->set_flashdata('msg_error','Action has been failed.');
					redirect('backend/zones');
				}  
			}else{
				$this->session->set_flashdata('msg_error','At least one field is Required');
			}
		}
		$data['country']=$this->zone_model->get_result('countries',array('status'=>1));
		$data['template']='backend/zones/add';
		$this->load->view('templates/backend/layout', $data);
	}	

	// public function configuration($zone_id=''){
	// 	_check_superadmin_login(); //check login authentication
	// 	if(empty($zone_id)) redirect('backend/zones');
	// 	$data['zone'] = $this->zone_model->get_row('zones',array('id'=>$zone_id));
	// 	if(empty($data['zone'])) redirect('backend/zones');
	// 	$this->form_validation->set_rules('discount_type','Discount Type','trim|required');
	// 	if($this->form_validation->run() == TRUE){
	// 		$arrayName = array('discount_type'=>$this->input->post('discount_type'));
	// 		$status=$this->zone_model->update('zones',$arrayName,array('id'=>$zone_id));
	// 		if($status){
	// 			$this->session->set_flashdata('msg_success','Configuration discount type has been updated successfully.');
	// 			redirect(current_url());
	// 		}
	// 	}
	// 	$data['template']='backend/zones/configuration';
	// 	$this->load->view('templates/backend/layout', $data);		
	// }
	public function configure_price($zone_id='')
	{
		_check_superadmin_login(); //check login authentication
		$flag=''; 
		if(empty($zone_id)) redirect('backend/zones');
		$discount_type='order-subtotal';
		$data['zone'] = $this->zone_model->get_row('zones',array('id'=>$zone_id));
		if(empty($data['zone'])) redirect('backend/zones');
		// if(empty($data['zone']->discount_type)){ 
		// 	$this->session->set_flashdata('msg_error','Please Select type First');
		// 	redirect('backend/zones/configuration/'.$data['zone']->id);
		// }
		// $discount = trim($discount_type);
		// if($discount=='order-subtotal' || $discount=='percentage-of-order-subtotal' || $discount=='order-weight' || $discount=='by-product' || $discount=='sub-total-combination' || $discount=='real-time-shipping-carriers' || $discount=='pickup'){
		// 	$flag=TRUE; 
		// }else{
		// 	$flag=FALSE;
		// }
		//if(!$flag) redirect('backend/zones/configuration/'.$zone_id);
		$data['variation']=$this->zone_model->get_result('price_configurtion',array('zone_id'=>$data['zone']->id,'discount_type'=>$discount_type));
		$data['methods'] = $this->zone_model->get_row('zone_shipping_method',array('zone_id'=>$zone_id,'discount_type'=>$discount_type)); 
		if($_POST){
		    if(!empty($data['variation'])){
			foreach($data['variation'] as  $value) {

				
				 $arrayName = array(
				            'zone_id' =>$zone_id, 
				            'discount_type'=>'order-subtotal', 
				            'low_price' =>$_POST['low_price_'. $value->id],
				            'high_price' =>$_POST['high_price_'. $value->id], 
				            'first_method_price' =>$_POST['first_price_'. $value->id], 
				            'second_method_price' =>$_POST['second_price_'. $value->id], 
				            'third_method_price' =>$_POST['third_price_'. $value->id], 
				            'fourth_method_price' =>$_POST['fourth_price_'. $value->id], 
				            'fifth_method_price' =>$_POST['fifth_price_'. $value->id], 
				            'sixth_method_price' =>$_POST['sixth_price_'. $value->id], 
				            'seven_method_price' =>$_POST['seven_price_'. $value->id]
				        );

				$this->zone_model->update('price_configurtion',$arrayName,array('id'=>$value->id));
			}
			
			}




			if(!empty($_POST['method_first']) || !empty($_POST['method_second']) || !empty($_POST['method_third']) || !empty($_POST['method_fourth']) || !empty($_POST['method_fifth']) || !empty($_POST['method_sixth'])){

				$arrayName = array(
								'zone_id'=>$data['zone']->id,
								'discount_type'=>'order-subtotal',
								'method_first'=>$this->input->post('method_first'),
								'method_second'=>$this->input->post('method_second'),
								'method_third'=>$this->input->post('method_third'),
								'method_fourth'=>$this->input->post('method_fourth'),
								'method_fifth'=>$this->input->post('method_fifth'),
								'method_sixth'=>$this->input->post('method_sixth'),
								'method_seven'=>$this->input->post('method_seven'),
							);
			if(empty($data['methods'])){
				if($this->zone_model->insert('zone_shipping_method',$arrayName)){
					$this->session->set_flashdata('msg_success','methood has been added successfully.');
					redirect('backend/zones/configure_price/'.$zone_id.'/'.$discount_type);	
				}
			}else{
				if($this->zone_model->update('zone_shipping_method',$arrayName,array('id'=>$data['methods']->id))){
					$this->session->set_flashdata('msg_success','methood has been updated successfully.');
					redirect('backend/zones/configure_price/'.$zone_id.'/'.$discount_type);	
				}
			}


			}else{
				$this->session->set_flashdata('msg_error','At least one field is Required in Methods');
			}	
		}
		$data['template']='backend/zones/configure_price';
		$this->load->view('templates/backend/layout', $data);		
	}

	public function variation_add()
	{
		if($_POST){
			$arr = explode('_', $this->input->post('discount_zone'));
			$discount_type  = 'order-subtotal';
			$zone_id =  $arr[1];

			$low_price  =  $this->input->post('low_price');
			$high_price =  $this->input->post('high_price');
			$first_price=  $this->input->post('first_price');
			$second_price=   $this->input->post('second_price');
			$third_price=  $this->input->post('third_price');
			$fourth_price=   $this->input->post('fourth_price');
			$fifth_price  =  $this->input->post('fifth_price');
			$sixth_price  =  $this->input->post('sixth_price');
			$seven_price  =  $this->input->post('seven_price');
			

			//$this->zone_model->get_price_variation($low_price,$high_price,$discount_type,$zone_id);	
			$arrayName = array(
				            'zone_id' =>$zone_id, 
				            'discount_type'=>'order-subtotal', 
				            'low_price' =>$low_price,
				            'high_price' =>$high_price, 
				            'first_method_price' =>$first_price, 
				            'second_method_price' =>$second_price, 
				            'third_method_price' =>$third_price, 
				            'fourth_method_price' =>$fourth_price, 
				            'fifth_method_price' => $fifth_price, 
				            'sixth_method_price' =>$sixth_price, 
				            'seven_method_price' => $seven_price, 
				            'created_date'=>date('Y-m-d h:i:s')
				        );

			if($this->zone_model->insert('price_configurtion',$arrayName)){
				echo 'success';
				return TRUE;			
			}else{
				echo 'failed';
				return FALSE;
			}
		}
	}

	public function variation_edit($zone_id='',$id='')
	{
		_check_superadmin_login(); //check login authentication
		if(empty($zone_id)) redirect('backend/zones/');
		$data['zone'] = $this->zone_model->get_row('zones',array('id'=>$zone_id));
		if(empty($data['zone'])) redirect('backend/zones');
		if(empty($id)) redirect('backend/zones/configure_price/'.$zone_id);
		$data['config'] = $this->zone_model->get_row('price_configurtion',array('id'=>$id));
		if(empty($data['config'])) redirect('backend/zones');

		$this->form_validation->set_rules('low_price','Low Price','trim|numeric');
		$this->form_validation->set_rules('high_price','Low Price','trim|numeric');
		$this->form_validation->set_rules('first_price','First Price','trim|numeric');
		$this->form_validation->set_rules('second_price','Second Price','trim|numeric');
		$this->form_validation->set_rules('third_price','Third Price','trim|numeric');
		$this->form_validation->set_rules('fourth_price','Fourth Price','trim|numeric');
		$this->form_validation->set_rules('fifth_price','Fifth Price','trim|numeric');
		$this->form_validation->set_rules('sixth_price','Sixth Price','trim|numeric');
		$this->form_validation->set_rules('seven_price','Seven Price','trim|numeric');

		if($this->form_validation->run() == TRUE){

			$arrayName = array(
				            'zone_id' =>$data['zone']->id, 
				            'discount_type'=>'order-subtotal', 
				            'low_price' =>$this->input->post('low_price'),
				            'high_price' =>$this->input->post('high_price'), 
				            'first_method_price' =>$this->input->post('first_price'), 
				            'second_method_price' =>$this->input->post('second_price'), 
				            'third_method_price' =>$this->input->post('third_price'), 
				            'fourth_method_price' =>$this->input->post('fourth_price'), 
				            'fifth_method_price' => $this->input->post('fifth_price'), 
				            'sixth_method_price' =>$this->input->post('sixth_price'), 
				            'seven_method_price' =>  $this->input->post('seven_price'), 
				            'created_date'=>date('Y-m-d h:i:s')
				        );

			
			if($this->zone_model->update('price_configurtion',$arrayName,array('id'=>$id))){
				$this->session->set_flashdata('msg_success','Zone has been updated successfully.');
				redirect('backend/zones');
			}else{
				$this->session->set_flashdata('msg_error','Action has been failed.');
				redirect('backend/zones');
			}  
		}	
		$data['template']='backend/zones/variation_edit';
		$this->load->view('templates/backend/layout', $data);	
	}

	public function variation_delete($zone_id='',$id='')
	{
		_check_superadmin_login(); //check login authentication
		if(empty($zone_id)) redirect('backend/zones/');
		$data['zone'] = $this->zone_model->get_row('zones',array('id'=>$zone_id));
		if(empty($data['zone'])) redirect('backend/zones');
		if(empty($id)) redirect('backend/zones/configure_price/'.$zone_id);     
		$this->zone_model->delete('price_configurtion',array('id'=>$id));
		$this->session->set_flashdata('msg_success','Zone Price Configuration has been deleted successfully.');
		redirect('backend/zones/configure_price/'.$zone_id); 		
	}
	public function edit($id=''){
		_check_superadmin_login(); //check login authentication
		if(empty($id)) redirect('backend/zones');
		$data['zone'] = $this->zone_model->get_row('zones',array('id'=>$id));
		
		if(empty($data['zone'])) redirect('backend/zones');

		if($_POST){
			if(!empty($_POST['country']) || !empty($_POST['state'])){
			
				// $country = implode(',',$this->input->post('country'));
				// $state   = implode(',',$this->input->post('state'));
				$arrayName = array(
					               	'countries'=>$this->input->post('con_div'),
					               	'zone_name'=>$this->input->post('zone_name'),	
					               	'states'=>$this->input->post('state_div'),
					               	'discount_type'=>'order-subtotal',
					               	'zip_codes'=>$this->input->post('zipcode_div'),
					               	'status'=>$this->input->post('status'),
					               	'modified'=>date('Y-m-d h:i:s')
					            );

				if($this->zone_model->update('zones',$arrayName,array('id'=>$id))){
					$this->session->set_flashdata('msg_success','Zone has been updated successfully.');
					redirect('backend/zones');
				}else{
					$this->session->set_flashdata('msg_error','Action has been failed.');
					redirect('backend/zones');
				}  
			    //for($i=0;$i<count($_POST['country']);$i++){ 

			    // }

			}else{
				$this->session->set_flashdata('msg_error','At least one field is Required');
			}
		}
		$data['country']=$this->zone_model->get_result('countries',array('status'=>1));
		$data['template']='backend/zones/edit';
		$this->load->view('templates/backend/layout', $data);

	}

	public function get_state()
	{
	    if($_POST){
	    	$resp='';
	        $slug = $this->input->post('slug');
	       
	        $coun_info = $this->zone_model->get_row('countries',array('slug'=>$slug));
	        if(!empty($coun_info)){
	       		$states = $this->zone_model->get_result('states',array('con_id'=>$coun_info->id));
	       		if(!empty($states)){
       				foreach($states as $value){
       					$resp.='<div class="col-md-6"><input type="checkbox" id="'.$value->slug.'" class="state" name="state[]" value="'.$value->slug.'">'.$value->state_name.'</div>';		
       				}
       				echo($resp);
       				return TRUE;
	       		} else {
	       			echo 'NoFound';
					return FALSE;	
	       		} 	
	        } else {
	        	echo 'NoFound';
	        	return FALSE;
	        }
	    }
	}

	function get_con_info($value='')
	{
		if($_POST){
			$resp='';
	        $slug = $this->input->post('country');
	        $zone_id = $this->input->post('zone_id');
	        $property = $this->zone_model->country_check($slug,$zone_id);
	        if(!empty($property)){
	        	echo 'failed';
	        	return FALSE;
	        }else{
	        	echo 'success';
	        	return FALSE;
	        }
		}
	}

    function get_sta_info($value='')
	{
		if($_POST){
			$resp='';
	        $slug = $this->input->post('state');
	        $zone_id = $this->input->post('zone_id');
	        $property = $this->zone_model->state_check($slug,$zone_id);
	        if(!empty($property)){
	        	echo 'failed';
	        	return FALSE;
	        }else{	
	        	echo 'success';
	        	return FALSE;
	        }
		}
	}
	function get_zip_info($value='')
	{
		if($_POST){
			$resp='';
	        $slug = $this->input->post('zip_code');
	        $zone_id = $this->input->post('zone_id');
	        $property = $this->zone_model->zip_code_check($slug,$zone_id);
	        if(!empty($property)){
	        	echo 'failed';
	        	return FALSE;
	        }else{
	        	echo 'success';
	        	return FALSE;
	        }
		}
	}

	public function delete($id=''){
		_check_superadmin_login(); //check login authentication
		if(empty($id)) redirect('backend/zones');     
		$this->zone_model->delete('zones',array('id'=>$id));
		$this->session->set_flashdata('msg_success','Zone has been deleted successfully.');
		redirect('backend/zones/');		
	}
} 