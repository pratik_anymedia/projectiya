<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller
{

	public function __construct(){
        parent::__construct();
        clear_cache();
        $this->load->model('users_model');
    }

	public function index($sort_by='id',$sort_order='desc',$offset=0){
            _check_superadmin_login(); //check login authentication
            $per_page=40;
            $data['users'] = $this->users_model->users($offset,$per_page,$sort_by,$sort_order);
            $data['offset'] = $offset;
            $config=backend_pagination();
            $config['base_url'] = base_url().'backend/users/index/'. $sort_by.'/'.$sort_order.'/';
            $config['total_rows'] = $this->users_model->users(0,0,$sort_by,$sort_order);
            $config['per_page'] = $per_page;
            $config['uri_segment']=6;
            if(!empty($_SERVER['QUERY_STRING'])){
            $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
        }
            $this->pagination->initialize($config);
            $data['pagination']=$this->pagination->create_links();
            $data['sort_by']=$sort_by;
            $data['sort_order']=$sort_order;
            $data['template']='backend/users/index';
            $this->load->view('templates/backend/layout', $data);
	}

	public function add(){

		_check_superadmin_login(); //check login authentication

		$this->form_validation->set_rules('first_name','First Name','trim|required');
		$this->form_validation->set_rules('last_name','Last Name','trim|required');
		$this->form_validation->set_rules('email','Email','trim|valid_email|required|is_unique[users.email]');
		$this->form_validation->set_rules('address','Address','trim|required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|matches[con_password]');
        $this->form_validation->set_rules('con_password', 'Confirm Password','required');
		$this->form_validation->set_rules('city','City','trim|required');

		$this->form_validation->set_rules('phone', 'Phone','trim|required|numeric');
	
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		if($this->form_validation->run() == TRUE){

			$cust_data = array(

                                                'user_role'=>1,

                                                'first_name'=> $this->input->post('first_name'),

                                                'last_name'=> $this->input->post('last_name'),

                                                'email'   => $this->input->post('email'),

                                                'phone'   => $this->input->post('phone'),

                                                'password'=>sha1($this->input->post('password')),

                                                'address'=> $this->input->post('address'),

                                                'city'	 => $this->input->post('city'),

                                                'country'=> 'india',

                                                'status' => $this->input->post('status'),

                                                'created'=> date('Y-m-d h:i:s')

                                          );


			if($user_id=$this->users_model->insert('users',$cust_data)){




            // $user_info = $this->users_model->get_row('users',array('id'=>$user_id));

	     //          //$secret_key=trim(md5($user_info->email));

	     //          $user_info->email;

	     //          //$this->user_model->update('users',array('secret_key'=>$secret_key),array('id'=>$user_info->id));

	     //           $i='';

	     //          if(!empty($user_info->user_role)&&$user_info->user_role==1){

	     //            $i=7;

	     //          }

	     //          $this->load->library('developer_email');

	     //          $email_template=$this->developer_email->get_email_template($i);

	     //          $param=array(

	     //             'template' =>  array(

	     //             'temp'  =>  $email_template->template_body,

	     //             'var_name' =>  array(

	     //                          'full_name'  => $user_info->first_name.' '.$user_info->last_name,

	     //                          'site_name'   => SITE_NAME,

	     //                          'site_url'    => base_url(),

	     //                          'email'  => $user_info->email,

	     //                          'pass'=>$this->input->post('password')

	     //                        ),

	     //                    ),

	     //              'email' =>  array(

	     //              'to'    =>   $user_info->email,

	     //              'from'  =>   NO_REPLY_EMAIL,

	     //              'from_name' => SITE_NAME,

	     //              'subject' =>   $email_template->template_subject,

	     //            )

	     //          );



	     //          $status=$this->developer_email->send_mail($param);



				// if($this->session->userdata('userfile')):

    //                 $this->session->unset_userdata('userfile');

    //             endif;



				$this->session->set_flashdata('msg_success','New user has been created successfully.');

				redirect('backend/users/');

			}

		}

		$data['template']='backend/users/add';
		$this->load->view('templates/backend/layout', $data);

	}




    public function download()

    {

    	$this->load->helper('download');

    	$data = file_get_contents(base_url()."assets/uploads/customers.csv"); // Read the file's contents

		$name = 'customers.csv';

		if(force_download($name, $data)){

			redirect('backend/customers/');

		} 

    }



	public function userfile_check_add($str){           

        if($this->session->userdata('userfile')):

            $this->session->unset_userdata('userfile');

        endif;  

        if($this->session->userdata('userfile')){               

            return TRUE;

        }else{

            $param=array(

                'file_name' =>'userfile',

                'upload_path'  => './assets/uploads/users/',

                'allowed_types'=> 'gif|jpg|png|jpeg',

                'image_resize' => TRUE,

                'source_image' => './assets/uploads/users/',

                'new_image'    => './assets/uploads/users/thumb/',

                'resize_width' => 200,

                'resize_height' => 200,

                'encrypt_name' => TRUE,

            );
        

            // if(!empty($_FILES['event_banner_image']['tmp_name'])){

            //     $img=getimagesize($_FILES['event_banner_image']['tmp_name']);

            //     $minimum = array('width' => '1200', 'height' => '350');

            //     $width=$img[0];

            //     $height=$img[1];

            //     if($width < $minimum['width'] || $height < $minimum['height']){

            //         $this->form_validation->set_message('event_banner_image_check', "Image dimensions are too small. Minimum width x height is {$minimum['width']} x{$minimum['height']}.");

            //         return FALSE;

            //     }

            // }     

    
                $upload_file=upload_file($param);

                if($upload_file['STATUS']){

                    $this->session->set_userdata('userfile',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));            

                    return TRUE;        

                }else{          

                    $this->form_validation->set_message('userfile_check_add', $upload_file['FILE_ERROR']);              

                    return FALSE;

                }      

        }

    }



    public function send_email_customers()

    {

		if($_POST){

			$flag='';

			$rope='';

			$ids= $_POST['ids'];

			$subject = $_POST['subject']; 

			$message = $_POST['message']; 

			for($i=0; $i <count($ids);$i++){ 

				$arr = explode('_',$ids[$i]);	

				$customer = $this->users_model->get_row('users',array('id'=>$arr[2],'status'=>1));

				$status = $this->sent($customer->email,$customer->first_name,$customer->last_name,$subject,$message); 

			}

			if($flag){

				echo 'problem';

				return false;

			}

			echo 'success';

			return TRUE;

		}   

    }



    public function sent($email='',$first_name='',$last_name='',$subject='',$message='')
	{

		if(!empty($email)){

		    $email;

            $i=4;

            $this->load->library('developer_email');

            $email_template=$this->developer_email->get_email_template($i);

            $param=array(

                 'template' =>  array(

                 'temp'  =>  $email_template->template_body,

                 'var_name' =>  array(

                              'full_name'=> $first_name.' '.$last_name,

                              'site_name'=> SITE_NAME,

                              'site_url' => base_url(),

                              'email'    => $email,

                              'message'  => $message

                            ),

                        ),

                  'email' =>  array(

                  'to'    =>   $email,

                  'from'  =>   NO_REPLY_EMAIL,

                  'from_name' => SITE_NAME,

                  'subject' => $subject,

                )

            );

            $status=$this->developer_email->send_mail($param);

		}		

	}	





	public function edit($cust_id='')	{

		_check_superadmin_login(); //check login authentication

		if(empty($cust_id)) redirect(base_url().'backend/users');

		$data['customer'] = $this->users_model->get_row('users',array('id'=>$cust_id,'user_role'=>1));

		if(empty($data['customer'])) redirect('backend/users/');


		$this->form_validation->set_rules('first_name','First Name','trim|required');

		$this->form_validation->set_rules('last_name','Last Name','trim|required');

		$this->form_validation->set_rules('email','Email','trim|valid_email|required|callback_check_email['.$data['customer']->id.']');

		$this->form_validation->set_rules('address','Address','trim|required');

		$this->form_validation->set_rules('city','City','trim|required');
		
		$this->form_validation->set_rules('phone','Phone','trim|required|numeric');	

		$this->form_validation->set_error_delimiters('<div class="error">','</div>');

		if($this->form_validation->run() == TRUE){

			$cust_data = array(

								'user_role'=>1,

								'company_name'=> $this->input->post('company_name'),

								'first_name'=> $this->input->post('first_name'),

								'last_name'=> $this->input->post('last_name'),

								'email'   => $this->input->post('email'),

								'phone'   => $this->input->post('phone'),

								'fax'    => $this->input->post('fax'),

								'about_me'=>$this->input->post('memo'),

								'redeem_points'=>$this->input->post('redeem_points'),

								'zip_code'=> $this->input->post('zip_code'),

								'address'=> $this->input->post('address'),

								'address1'=> $this->input->post('address1'),

								'city'	 => $this->input->post('city'),

								'country'=> $this->input->post('country'),

								'state'	 => $this->input->post('state'),

								'group_id'	 => $this->input->post('group'),

								'status' => $this->input->post('status'),

								'created'=> date('Y-m-d h:i:s')

							  );

			if($this->users_model->update('users',$cust_data,array('id'=>$cust_id))){

				$this->session->set_flashdata('msg_success','Customer has been Updated successfully.');

				redirect('backend/users/');

			}

		}

		$data['template']='backend/users/edit';

		$this->load->view('templates/backend/layout',$data);	

	}



	 public function check_email($email,$id){   

        if($_POST){ 	

           $user_info = $this->users_model->get_row('users', array('id'=>$id));  

           if($_POST['email']!=$user_info->email){

              $resp = $this->users_model->get_row('users', array('email'=>$_POST['email']));

              if($resp){

                  $this->form_validation->set_message('check_email', 'Email already exist.');

                  return FALSE;

              } else{

                  return TRUE;

              }

           }else{

              return TRUE;

           }

        }   

    }



	public function alpha_numeric_space($str)

	{

		if(!preg_match("/^([-a-z0-9_ ])+$/i",$str)){

			$this->form_validation->set_message('alpha_numeric_space', 'Please provide alpha numeric value in zip code.');

            return FALSE;

		} else {

			return TRUE;

		}

	}



	public function delete_customers()

	{
//            print_r($_POST);die;

		_check_superadmin_login(); //check Professor login authentication

		if($_POST){

			$this->form_validation->set_rules('checkall[]','Customer', 'trim|required');

			$this->form_validation->set_error_delimiters('<div style="color:red;" class="error">','</div>');

			if($this->form_validation->run() == TRUE){

				$cust_id_array= $this->input->post('checkall');

				$FLAG=TRUE;

				 //print_r($cust_id_array);

				 //die();

				$status = $_POST['all_status'];

                                $count_cust = count($cust_id_array);

				 for($i=0; $i<$count_cust ; $i++){ 

				 	$cust_status = $this->users_model->delete('users',array('id'=>$cust_id_array[$i],'user_role'=>1));	

//				 	$this->users_model->delete('user_shipping_info',array('customer_id'=>$cust_id_array[$i]));

				 	if(!$cust_status){

				 		$FLAG=FALSE;

				 	}

				 }			

				if($FLAG){

					$this->session->set_flashdata('msg_success',"Customer have been Deleted successfully.");

					redirect('backend/users');

				}

			}else{

				$this->session->set_flashdata('msg_error',"Customer delete Operation Failed.");

				redirect('backend/users');

			}

		}else{

			$this->session->set_flashdata('msg_error',"Customer delete Operation Failed...");

			redirect('backend/users');

		} 

	}

	public function import($type='csv'){

		_check_superadmin_login(); //check login authentication

		// if(empty($class_id)) redirect('professor/classes'); 

       	$data['error']='';

       	if($_POST){

	       	if(empty($_POST['default_password'])){

	       		$this->session->set_flashdata('msg_error','Password field is required');

	       		redirect('backend/customers/import');

       		}

       	}

       	if($_FILES){

      	if(!empty($_FILES['import_csv']['name'])){

            //Сheck that we have a file

            if((!empty($_FILES["import_csv"])) && ($_FILES['import_csv']['error'] == 0)) {

	            //Check if the file is JPEG image and it's size is less than 350Kb

	            $filename = basename($_FILES['import_csv']['name']);

	            $ext = substr($filename, strrpos($filename, '.') + 1);

	            if(($ext == "csv") &&  ($_FILES["import_csv"]["size"] < 350000)) {

                  //Determine the path to which we want to save this file

                  $newname = './assets/uploads/customer_csv/'.$filename;

                  //Check if the file with the same name is already exists on the server

                  if (!file_exists($newname)) {

                    //Attempt to move the uploaded file to it's new place

	                    if((move_uploaded_file($_FILES['import_csv']['tmp_name'],$newname))) {

	                        //echo $data['error'] = "It's done! The file has been saved as: ".$newname; die;

	                        if($this->read_csv_xls_xlsx(array('file'=> $filename,'path'=>'./assets/uploads/customer_csv/','password'=>$this->input->post('default_password')))){                          

	                          $filename='./assets/uploads/customer_csv/'.$filename;

	                          unlink($filename);

	                       	 	//$this->session->set_flashdata('msg_success',"Customers imported successfully.");

	                          redirect('backend/customers/');

	                        }

	                    } else {



	                        $this->session->set_flashdata('msg_error','Error: A problem occurred during file upload!');

					  		redirect('backend/customers/');

	                    }

                   } else {

	                    $this->session->set_flashdata('msg_error_csv','Error: A problem occurred during file upload! File is already exists');

						//redirect('backend/customers/add/');

                   }

              } else {

                 

                $this->session->set_flashdata('msg_error','Error: Only .jpg images under 350Kb are accepted for upload');

				//redirect('backend/customers/add/');

              }

            } else {

                $this->session->set_flashdata('msg_error','Error: No file Selected');

				//redirect('backend/customers/add/');

            }

      

	     } else {



	        	$this->session->set_flashdata('msg_error_csv','Error: No file Selected');

				//redirect('backend/customers/add/');

	        }

       }



        //redirect('backend/customers/add/');

        $data['template']='backend/customers/import';

		$this->load->view('templates/backend/layout',$data);	

    }



    private function read_csv_xls_xlsx($param=array()){

		if(!isset($param['file']) && empty($param['file'])){

			$this->session->set_flashdata('msg_error','File Name can\'t be blank, Please try again.');

			return FALSE;

		}

		if(!isset($param['path']) && empty($param['path'])){

			$this->session->set_flashdata('msg_error','File Path can\'t be blank, Please try again.');

			return FALSE;

		}

		if(!isset($param['password']) && empty($param['password'])){

			$this->session->set_flashdata('msg_error','Defalut Password is not set properly.');

			return FALSE;

		}



		$filename = $param['path'].$param['file'];

		//$class_id = $param['class'];



		if(file_exists($filename)){

			require(APPPATH.'libraries/spreadsheet-reader/php-excel-reader/excel_reader2.php');

		   	require(APPPATH.'libraries/spreadsheet-reader/SpreadsheetReader.php');



		    $Reader = new SpreadsheetReader($filename);

		    $l=0; $u=0;$i=0; $R=0;

		    $customer_exist_data=array();

		    $password = $param['password'];

			foreach($Reader as $row):

				$customer = $this->users_model->get_row('users',array('email'=>trim($row[4])));



				if(!empty($customer)){

					$arrayName = array('first_name'=>$row[2],'last_name'=>$row[3],'email'=>$row[4]);

					$customer_exist_data[$R]=$arrayName;

					$R++;

				} else { 	

				   	if((!empty($row[1])) && $l>0):

					    $item_slug= trim($row[0]);



		                      $customer_data['user_role']    = 1; 

		                    if(isset($row[1]))

		                   	  $customer_data['company_name']= trim($row[1]);  

		                	if(isset($row[2]))

		                      $customer_data['first_name']= trim($row[2]);  

		                	if(isset($row[3]))

		                      $customer_data['last_name'] = trim($row[3]);

		                	if(isset($row[4]))

		                      $customer_data['email']     = trim($row[4]);

		                	if(isset($row[5]))

		                      $customer_data['status']    = $row[5];

		                	if(isset($row[6]))

		                      $customer_data['phone']     = trim($row[6]);

		                    if(isset($row[7]))

		                      $customer_data['zip_code']  = trim($row[7]);

		                    if(isset($row[8]))

		                      $customer_data['address']   = trim($row[8]);

		                    if(isset($row[9]))

		                      $customer_data['city']      = trim($row[9]);

		                    if(isset($row[10]))

		                      $customer_data['country']   = url_title(trim($row[10]), '-', TRUE);

		                    if(isset($row[11]))

		                      $customer_data['state']     = url_title(trim($row[11]), '-', TRUE);

		                   

		                	if(isset($row[22]))

		                      $customer_data['about_me']  = trim($row[22]);

		                    if(isset($row[23]))

		                      $customer_data['fax']       = trim($row[23]);



						    $customer_data['created']   = date('Y-m-d h:i:s');

						    $customer_data['password']  = sha1(trim($password));



							

							$cust_id = $this->users_model->insert('users',$customer_data);

					        

					        $ship_customer_data['customer_id']=$cust_id;



					        // if(isset($row[12]))

				           // $ship_customer_data['discount']     = trim($row[12]);

				           //  if(isset($row[13]))

				           // $ship_customer_data['tax_free']     = trim($row[13]);

				           //  if(isset($row[14]))

				           //   $ship_customer_data['free_shipping']= trim($row[14]); 

							if(isset($row[12]))

		                   	$ship_customer_data['company_name'] = trim($row[12]);  

		                	if(isset($row[13]))

		                    $ship_customer_data['first_name']   = trim($row[13]);  

		                	if(isset($row[14]))

		                    $ship_customer_data['last_name']    = trim($row[14]);

		                	if(isset($row[15]))

		                    $ship_customer_data['email']        = trim($row[15]);

		                	if(isset($row[16]))

		                    $ship_customer_data['phone']       = trim($row[16]);

		                	if(isset($row[17]))

		                    $ship_customer_data['zip_code']       = trim($row[17]);

		                    if(isset($row[18]))

		                    $ship_customer_data['address']       = trim($row[18]);

		                    if(isset($row[19]))

		                    $ship_customer_data['city']       = trim($row[19]);

		                    if(isset($row[20]))

		                    $ship_customer_data['country']       = url_title(trim($row[20]), '-', TRUE);

		                    if(isset($row[21]))

		                    $ship_customer_data['state']       =  url_title(trim($row[21]), '-', TRUE);

		                 

		                	  $this->users_model->insert('user_shipping_info',$ship_customer_data);



		                	  $user_info = $this->users_model->get_row('users',array('id'=>$cust_id));

				              //$secret_key=trim(md5($user_info->email));

				              $user_info->email;

				              //$this->users_model->update('users',array('secret_key'=>$secret_key),array('id'=>$user_info->id));

				               $j='';

				              if(!empty($user_info->user_role)&&$user_info->user_role==1){

				                $j=7;

				              }

				              $this->load->library('developer_email');

				              $email_template=$this->developer_email->get_email_template($j);

				              $param=array(

					                 'template' =>  array(

					                 'temp'  =>  $email_template->template_body,

					                 'var_name' =>  array(

					                              'full_name'  => $user_info->first_name.' '.$user_info->last_name,

					                              'site_name'   => SITE_NAME,

					                              'site_url'    => base_url('user/login'),

					                              'email'  => $user_info->email,

					                              'pass'=>$password

					                            ),

					                        ),

					                  'email' =>  array(

					                  'to'    =>   $user_info->email,

					                  'from'  =>   NO_REPLY_EMAIL,

					                  'from_name' => SITE_NAME,

					                  'subject' =>   $email_template->template_subject,

					                )

				              	);

				              $status=$this->developer_email->send_mail($param);

					        $i++;

		               // }

				    endif;

			}

			    $l++;

			endforeach;

		if(!$this->session->userdata('existing_customers')){

			$this->session->set_userdata('existing_customers',$customer_exist_data);

		}	

		$this->session->set_flashdata('msg_success',"Customers imported successfully.");

        return TRUE;



    }else{

	    $this->session->set_flashdata('msg_error','Students does not exist, Please try again.');

	    return FALSE;

    }

			

	}	









	public function export($type='csv'){

			_check_superadmin_login(); //check login authentication

			// if(empty($class_id)) redirect('professor/classes');

			$customers=$this->users_model->get_customers_info('users',array('user_role'=>2));			

			if($customers==FALSE){

				$this->session->set_flashdata('msg_info',"No Customers Available.");

				redirect('backend/customers/');

			}

			header("Content-type: text/csv");

			header("Content-Disposition: attachment; filename=Customers_list_".date('Y-m-d').".csv");

			header("Pragma: no-cache");

			header("Expires: 0");

			$file = fopen('php://output', 'w');

			        fputcsv($file, array(

			            '#',

						'company_name',

						'first_name',

						'last_name',

						'email',

						'phone',

						'fax',

						'zip_code',

						'address',

						'city',

						'country',

						'state',

						'discount',

						'tax_free',						

						'free_shipping',						

						'status',

						'created',

						'ship_company_name',

						'ship_first_name',

						'ship_last_name',

						'ship_email',

						'ship_phone',

						'ship_zip_code',

						'ship_address',

						'ship_city',

						'ship_country',

						'ship_state',

			      ));

		      $p=0;foreach ($customers as $row) { $p++;

		    $rows=array(

			    $p,

			    trim($row->company_name),

				trim($row->first_name),

				trim($row->last_name),

				trim($row->email),

				trim($row->phone),

				trim($row->fax),

				trim($row->zip_code),

				trim($row->address),

				trim($row->city),

				trim($row->country),

				trim($row->state),

				trim($row->ship_discount),

				trim($row->ship_tax_free),

				trim($row->ship_free_shipping),

				trim($row->status),

	            date('d-m-Y',strtotime($row->created)),

				trim($row->ship_company_name),

				trim($row->ship_first_name),

				trim($row->ship_last_name),

				trim($row->ship_email),

				trim($row->ship_phone),

	            trim($row->ship_zip_code),

	            trim($row->ship_address),

	            trim($row->ship_city),

	            trim($row->ship_country),

	            trim($row->ship_state),

			);

	      fputcsv($file, $rows);

	      }

	      exit();

	}


	public function delete($cust_id ='')	{
		_check_superadmin_login(); //check login authentication
		if(empty($cust_id)) redirect('backend/customers');
		$this->users_model->delete('users',array('id'=>$cust_id,'user_role'=>1));
		// $this->users_model->delete('user_shipping_info',array('customer_id'=>$cust_id));
		// $this->users_model->delete('product_rating',array('user_id'=>$cust_id));
		// $this->users_model->delete('product_reviews',array('user_id'=>$cust_id));
		$this->session->set_flashdata('msg_success','Users has been deleted successfully.');
		redirect('backend/users');		
	}

	public function user_projects($user_id='')
	{ 
		_check_superadmin_login(); //check login authentication
		if(empty($user_id)) redirect('backend/customers');
		$data['user'] = $this->users_model->get_row('users',array('id'=>$user_id,'user_role'=>1));
		$data['projects'] = $this->users_model->get_result('projects',array('user_id'=>$data['user']->id));
		$data['template']='backend/users/user_projects';
		$this->load->view('templates/backend/layout', $data);
	}
		
}	