<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	class Messages extends CI_Controller
	{
		public function __construct()
	    {
	        parent::__construct();
	        clear_cache();
	        $this->load->model('superadmin_model');
	    }

	    private function _check_login(){
			if(superadmin_logged_in()===FALSE)  redirect('superadmin/login');
	   	}

		public function index($offset=0){
			$this->_check_login(); //check login authentication
			$per_page=10;
			$data['offset']=$offset;
			$data['messages'] = $this->superadmin_model->messages($offset,$per_page);
	 		$config=backend_pagination();
			$config['base_url'] = base_url().'backend/messages/index/';
			$config['total_rows'] = $this->superadmin_model->messages(0,0);
			$config['per_page'] = $per_page;
			$config['uri_segment'] =3;		
			$this->pagination->initialize($config);
			$data['pagination']=$this->pagination->create_links();

	 		$data['template']='backend/messages/index';
			$this->load->view('templates/backend/layout', $data);
		}

		public function message_reply($id="")
		{
		  	$this->_check_login(); //check login authentication
		  	if(empty($id)) redirect('superadmin/messages');
		  	$data['message']=$this->superadmin_model->get_row('contact_us',array('id' => $id));

		  	$this->form_validation->set_rules('first_name','First Name','required');
		  	$this->form_validation->set_rules('last_name','Last Name','required');
		  	$this->form_validation->set_rules('email','Email','required|valid_email');
			$this->form_validation->set_rules('subject','Subject','required');
			$this->form_validation->set_rules('message','Message','required');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if ($this->form_validation->run() == TRUE){	
	
				$menu_data = array(
									'status' =>	1,		
									'reply'=>	$this->input->post('message'),
									'created'=>	date('Y-m-d h:i:s')
								   );
				$question     = $this->input->post('subject');
				$text_message = $this->input->post('message');
				$email        = $this->input->post('email');
				$first_name   = $this->input->post('first_name');
				$last_name    = $this->input->post('last_name');
				$form_email   = FROM_EMAIL; 
				$url          = SITE_URL;  
				if($this->superadmin_model->update('contact_us',$menu_data,array('id'=>$id))){
					// $this->load->library('chapter247_email');
					// $email_template=$this->chapter247_email->get_email_template(4);
					// $param=array(
					// 				'template' 	=>	array(
					// 					'temp'	=> 	$email_template->template_body,
					// 					'var_name' 	=> array(
					// 									'user_full_name'	=>	$first_name.' '.$last_name,
					// 									'question'=>$question,
					// 									'text_message'=>$text_message													
					// 						), 
					// 					),			
					// 					'email'	=> 	array(
					// 						'to' 		=>	 $email,
					// 						'from' 		=>	$form_email ,
					// 						'from_name' =>	$url,
					// 						'subject'	=>	$email_template->template_subject,
					// 					)
					// 			);	
					// $status=$this->chapter247_email->send_mail($param);							
					$this->session->set_flashdata('msg_success','Message Has Sent successfully.');
					redirect('backend/messages/');				
				}else{
					$this->session->set_flashdata('msg_error','Failed, Please try again.');
					redirect('backend/messages/');
				}
			}

		  	$data['template']='backend/messages/message_reply';
			$this->load->view('templates/backend/layout', $data);
		}

		public function view_reply($id="")
		{
		  	$this->_check_login(); //check login authentication
		  	if(empty($id)) redirect('superadmin/messages');
		  	$data['message']=$this->superadmin_model->get_row('contact_us',array('id' => $id));	  
			
		  	$data['template']='backend/messages/view_reply';
			$this->load->view('templates/backend/layout', $data);
		}

		public function message_delete($message_id=''){
			$this->_check_login(); //check login authentication		
			if(empty($message_id)) redirect(base_url().'superadmin/messages');	
			if($this->superadmin_model->delete('contact_us',array('id'=>$message_id))){								
				$this->session->set_flashdata('msg_success','Message deleted successfully.');
				redirect('backend/messages');				
			}else{
				$this->session->set_flashdata('msg_error','Message Delete Failed, Please try again.');
				redirect('backend/messages');
			}	
		}

}		