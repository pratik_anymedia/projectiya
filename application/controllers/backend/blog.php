<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('blog_model');
    }

    public function index()
    {
        $this->posts();
    }
    private function _check_login(){
		if(superadmin_logged_in()===FALSE)
                    redirect('superadmin/login');
	}


    public function posts($offset = 0)
    {
        $this->_check_login(); //check login authentication
        $per_page = 10;
        $data['offset'] = $offset;
        $data['blogs'] = $this->blog_model->blog($offset, $per_page);
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'backend/blog/posts/';
        $config['total_rows'] = $this->blog_model->blog(0, 0);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = "backend/blog/index";
        $this->load->view('templates/backend/layout', $data);
    }

    public function add()
    {
        $this->_check_login(); //check login authentication
        $this->form_validation->set_rules('blog_title', 'Title', 'required');
        $this->form_validation->set_rules('blog_content', 'Content', 'required');
        $this->form_validation->set_rules('blog_category','Blog Category','');
        //if(!empty($_FILES['blog_features_image']['name'])):
        $this->form_validation->set_rules('blog_features_image', '', 'callback_blog_features_image_check');
        //endif;

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {

            if ($this->session->userdata('blog_features_image')):
                $blog_features_image = $this->session->userdata('blog_features_image');
                $blog_data['file_path'] = $blog_features_image['image'];
                $blog_data['thumb_image'] = $blog_features_image['thumb_image'];
            endif;
            $blog_data['blog_title'] = $this->input->Post('blog_title');
            $blog_data['blog_slug'] = url_title($this->input->Post('blog_title'), '-', TRUE);
            $blog_data['blog_content']   = htmlspecialchars($this->input->Post('blog_content'));
            $blog_data['blog_category'] = $this->input->Post('blog_category');
            $blog_data['blog_status'] = $this->input->Post('blog_status');
            $blog_data['created'] = date('Y-m-d h:i:s');
            //$blog_data['tags']    =   $this->input->Post('tag');
            if ($this->blog_model->insert('blogs', $blog_data)) {
                $this->session->set_flashdata('msg_success', 'Blog post Created successfully.');
                if ($this->session->userdata('blog_features_image')):
                    $this->session->unset_userdata('blog_features_image');
                endif;
                redirect('backend/blog/');
            }else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/blog/');
            }
        }
        $data['categories'] = $this->blog_model->get_result('technologies');
        $data['template'] = 'backend/blog/add';
        $this->load->view('templates/backend/layout', $data);
    }

    public function edit($page_id = '', $offset = '')
    {
        $this->_check_login(); //check login authentication
        if (empty($page_id)) redirect('backend/blog/' . $offset);
        $data['blog'] = $this->blog_model->get_row('blogs', array('id' => $page_id));
        $this->form_validation->set_rules('blog_title', 'Title', 'required');
        $this->form_validation->set_rules('blog_content', 'Content', 'required');
        $this->form_validation->set_rules('blog_categories','Blog Tag','');
        if (!empty($_FILES['blog_features_image']['name'])):
            $this->form_validation->set_rules('blog_features_image', '', 'callback_blog_features_image_check');
        endif;
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {

            if ($this->session->userdata('blog_features_image')):
                $blog_features_image = $this->session->userdata('blog_features_image');
                $blog_data['file_path'] = $blog_features_image['image'];
                $blog_data['thumb_image'] = $blog_features_image['thumb_image'];
            endif;

            $blog_data['blog_title'] = $this->input->Post('blog_title');
            $blog_data['blog_slug'] = url_title($this->input->Post('blog_title'), '-', TRUE);
            $blog_data['blog_content']    = htmlspecialchars($this->input->Post('blog_content'));
            $blog_data['blog_category'] = $this->input->Post('blog_category');
            $blog_data['blog_status'] = $this->input->Post('blog_status');
            $blog_data['updated'] = date('Y-m-d h:i:s');
            //$blog_data['tags']    =   $this->input->Post('tag');
            if ($this->blog_model->update('blogs', $blog_data, array('id' => $page_id))) {


                if ($this->session->userdata('blog_features_image')):
                    $this->session->unset_userdata('blog_features_image');
                    @unlink($data['blog']->image);
                    @unlink($data['blog']->thumb_image);
                endif;

                $this->session->set_flashdata('msg_success', 'Blog post updated successfully.');
                redirect('backend/blog/posts/' . $offset);
            }else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/blog/posts/' . $offset);
            }
        }

        $data['categories'] = $this->blog_model->get_result('technologies');

        $data['template'] = 'backend/blog/edit';
        $this->load->view('templates/backend/layout', $data);
    }

    public function delete($page_id = '', $offset = '')
    {
        $this->_check_login(); //check login authentication

        if (empty($page_id)) redirect('backend/blog/' . $offset);

        $data['blog'] = $this->blog_model->get_row('blogs', array('id' => $page_id));

        @unlink($data['blog']->image);
        @unlink($data['blog']->thumb_image);

        if ($this->blog_model->delete('blogs', array('id' => $page_id))) {

            $this->blog_model->delete('comments', array('comment_blog_id' => $page_id));

            $this->session->set_flashdata('msg_success', 'Blog post deleted successfully.');
            redirect('backend/blog/posts/' . $offset);
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/blog/posts/' . $offset);
        }
    }

    public function blog_features_image_check($str)
    {
        if ($this->session->userdata('blog_features_image')) {
            return TRUE;
        } else {
            $param = array(
                'file_name' => 'blog_features_image',
                'upload_path' => './assets/uploads/blog/',
                'allowed_types' => 'gif|jpg|png|jpeg',
                // 'image_resize' => TRUE,
                'source_image' => './assets/uploads/blog/',
                'new_image' => './assets/uploads/blog/thumb/',
                // 'resize_width' => 320,
                // 'resize_height' => 300,
                'encrypt_name' => TRUE);

            $upload_file = upload_file($param);
            if ($upload_file['STATUS']) {
                $filepath='./assets/uploads/blog/';
                $configs = array();
                //$configs[] = array('source_image' => $upload_file['UPLOAD_DATA']['file_name'], 'new_image' => 'medium/'.$upload_file['UPLOAD_DATA']['file_name'], 'width' => 325, 'height' => 325,'maintain_ratio' =>TRUE);
                $configs[] = array('source_image' => $upload_file['UPLOAD_DATA']['file_name'], 'new_image' => 'thumb/'.$upload_file['UPLOAD_DATA']['file_name'], 'width' => 325, 'height' => 225,'maintain_ratio' => TRUE);
                $this->load->library('image_lib');
                foreach ($configs as $config) {
                    $this->image_lib->thumb($config,$filepath);
                }      
                $this->session->set_userdata('blog_features_image', array('image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'], 'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']));
                return TRUE;
            } else {
                $this->form_validation->set_message('blog_features_image_check', $upload_file['FILE_ERROR']);
                return FALSE;
            }
        }
    }

    public function blog_comments($id)
    {
        _check_superadmin_login(); //check login authentication
        $data['blog'] = $this->blog_model->get_row('blogs',array('id'=>$id));
        $data['comments'] = $this->blog_model->get_result('comments',array('comment_blog_id'=>$id));
        $data['template'] ='backend/blog/blog_comments';
        $this->load->view('templates/backend/layout', $data);
    }
    public function comment_delete($comment_id='',$id='')
    {
        _check_superadmin_login(); //check login authentication
        if(empty($id)) redirect('backend/blog/');
        $data['blogs']=$this->blog_model->get_row('blogs',array('id'=>$id));
        decrease_comments($id,$data['blogs']->comment_count);      
        $this->blog_model->delete('comments',array('id'=>$comment_id));
        $this->session->set_flashdata('msg_success','Blog Comment deleted successfully.');
        redirect('backend/blog/blog_comments/'.$id);
    }


    public function status($id="",$status="",$blog_id="")
    {
        _check_superadmin_login(); //check login authentication
        if(empty($id)) redirect('backend/blog/');
        //$data['blogs']=$this->blog_model->get_row('blogs',array('id'=>$id));
        if($status==0){
            $cat_status=1;
        }
        if($status==1){
            $cat_status=0;
        }       
        $data = array('comment_approved'=>$cat_status);
        if($this->blog_model->update('comments', $data ,array('id'=>$id)))
        {
            if($cat_status==1){
                $data['blogs']=$this->blog_model->get_row('blogs',array('id'=>$blog_id));
                increase_comments($blog_id,$data['blogs']->comment_count);
            }else{
                $data['blogs']=$this->blog_model->get_row('blogs',array('id'=>$blog_id));
                decrease_comments($blog_id,$data['blogs']->comment_count);
            }
           $this->session->set_flashdata('msg_success','Comment status has been updated successfully.');
           redirect('backend/blog/blog_comments/'.$blog_id);   
        }

    }

}