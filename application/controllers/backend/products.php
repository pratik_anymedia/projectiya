<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');
class Products extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        clear_cache();
        $this->load->model('product_model');
    }

	public function index($sort_by='id',$sort_order='desc',$offset=0){
		_check_superadmin_login(); //check login authentication
		$per_page=40;
		$data['products'] = $this->product_model->products($per_page,$offset,$sort_by,$sort_order);
		
                $data['offset'] = $offset;
 		$config=backend_pagination();
		$config['base_url'] = base_url().'backend/products/index/'. $sort_by.'/'.$sort_order.'/';
		$config['total_rows'] = $this->product_model->products(0,0,$sort_by,$sort_order);
		$config['per_page'] = $per_page;
		$config['uri_segment']=6;
		if(!empty($_SERVER['QUERY_STRING'])){
        	$config['suffix'] = "?".$_SERVER['QUERY_STRING'];
        }
		$this->pagination->initialize($config);
		$data['pagination']=$this->pagination->create_links();
		$data['sort_by']=$sort_by;
        $data['sort_order']=$sort_order;
        $data['category'] = $this->product_model->get_result('categories',array('status'=>1));
 		$data['template']='backend/product/index';
		$this->load->view('templates/backend/layout', $data);
	}
	public function add(){
		_check_superadmin_login(); //check login authentication	
		if($this->session->userdata('add_new_customer')){
			//$data['customer_session']
		}
		// if($_POST){
		// 	print_r($_POST);
		// 	die();
		// }
		$this->form_validation->set_rules('name', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('description','Description', 'trim|required');
		$this->form_validation->set_rules('short_description','Short Description', 'trim|required');
		$this->form_validation->set_rules('science_description','Science Description','trim|');
		$this->form_validation->set_rules('clinical_description','Clinical Description','trim|');
		$this->form_validation->set_rules('use_description','Use Description','trim|');
		if($_POST){
			if($this->input->post('price_type')==1){
				$this->form_validation->set_rules('price','Price', 'trim|required|numeric');
			}
		}
		$this->form_validation->set_rules('sku','SKU', 'trim|numeric|is_unique[products.SKU]');
		$this->form_validation->set_rules('weight','Weight', 'trim|numeric');
		$this->form_validation->set_rules('category[]','Category', 'trim|required');
		$this->form_validation->set_rules('slide_label','Slide Label', 'trim');
		$this->form_validation->set_rules('related_description','Related Description', 'trim|');
		$this->form_validation->set_rules('use_description_right','Use Descriptions Right', 'trim|');
		$this->form_validation->set_rules('science_image','Science image','callback_science_file_add');
		$this->form_validation->set_rules('clinical_image','Clinical image','callback_clinical_file_add');
		$this->form_validation->set_rules('clinical_image','Clinical image','callback_clinical_file_add');

		//if($_POST){
			// if(!empty($_POST['image_use_only'])){	
				// if(!empty($_FILES['image_first']['name'])){
				// 	$this->form_validation->set_rules('image_first','Image First','callback_image_first_add');
				// }
				// if(!empty($_FILES['image_second']['name'])){
				// 	$this->form_validation->set_rules('image_second','Image Second','callback_image_second_add');
				// }
				// if(!empty($_FILES['image_third']['name'])){
				// 	$this->form_validation->set_rules('image_third','Image Third','callback_image_third_add');
				// }
				// if(!empty($_FILES['image_four']['name'])){
				// 	$this->form_validation->set_rules('image_four','Image Four','callback_image_four_add');
				// }
			//}
		//}	
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$images=$this->session->userdata('product_image');

		if($this->form_validation->run() == TRUE  && !empty($images)){		
			
			$category = "#".implode('#,#',$this->input->post('category'))."#";
			if(!empty($_POST['rela_products'])){
				$rela_products = "#".implode('#,#',$this->input->post('rela_products'))."#";
			}else{
				$rela_products='';
			}
			// if(empty($images)){
			// 	$this->session->set_flashdata('image_error_msg','Please upload Product image');
			// 	redirect('backend/products/add');
			// }
			$product = array(
								'name'=> $this->input->post('name'),
								'slug'=> url_title($this->input->post('name'),'-',TRUE),
								'category' => $category,
								'description' => $this->input->post('description'),
								'price_type' => $this->input->post('price_type'),
								'SKU'=>$this->input->post('sku'),
								'related_products'=>$rela_products,
								'meta_title'=>$this->input->post('meta_title'),
								'meta_description'=>$this->input->post('meta_description'),
								'short_description'=>$this->input->post('short_description'),
								'meta_keywords'=>$this->input->post('meta_keywords'),
								'avilable_item'=>$this->input->post('available_item'),
								'discount_item'=>$this->input->post('discount_item'),
								'meta_title'=>$this->input->post('meta_title'),
								'meta_keywords'=>$this->input->post('meta_keywords'),
								'meta_description'=>$this->input->post('meta_description'),
								'featured_product'=>$this->input->post('featured_item'),
								'status'	  => $this->input->post('status'),
								'created'=> date('Y-m-d h:i:s')
							 );
			
			if($this->input->post('price_type')==1){
				$product['price'] =  $this->input->post('price');
			}
			if($product_id=$this->product_model->insert('products',$product)){

				if($this->input->post('price_type')==2){
					$prices= $this->input->post('price_for');
					$price_label= $this->input->post('price_label');
					for($i=0; $i<count($prices); $i++){ 
						$arrayName = array('product_id'=>$product_id,  'price_for'=>$prices[$i],'price_label'=>$price_label[$i]);
						$this->product_model->insert('multiple_price',$arrayName);											
					}
				}

	             $product_related=array(
		             						'product_id'=>$product_id,
		             						'use_description'=>htmlspecialchars($this->input->Post('use_description')),
		             						'clinical_description'=>htmlspecialchars($this->input->Post('clinical_description')),
		             						'science_description'=>htmlspecialchars($this->input->Post('science_description')),
		             						'use_description_right'=>htmlspecialchars($this->input->Post('use_description_right')),
		             						'related_description'=>htmlspecialchars($this->input->Post('related_description')),
		             					   	'slide_order' => $this->input->Post('slide_order'),
		             					   	'direction_image' => htmlspecialchars($this->input->Post('direction_image')),
		             					   	'first_section_order' => $this->input->Post('first_section_order'),
		             					   	'second_section_order' => $this->input->Post('second_section_order'),
		             					   	'use_section_order'=>$this->input->Post('use_section_order'),
		             					   	'first_section_sprint' => $this->input->Post('first_section_sprint'),
		             					   	'second_section_sprint' => $this->input->Post('second_section_sprint'),
		             					   	'use_section_sprint' => $this->input->Post('use_section_sprint'),
		             					   	'content_sprint' => $this->input->Post('content_sprint'),
		             					   	'slide_sprint' => $this->input->Post('slide_sprint'),
		             					   	'slide_use_only' => $this->input->Post('image_use_only'),
		             					   	'content_order' => $this->input->Post('content_order'),
	             					   );
	            if($_POST){
	            	if(!empty($_POST['content_use_only'])){
	            		$product_related['content_use_only']=$this->input->post('content_use_only');
	            	}
	            }
	            if($_POST){
	            	if(!empty($_POST['slide_use_only'])){
	            		$product_related['slide_use_only']=$this->input->post('slide_use_only');
	            	}
	            }
				if($this->session->userdata('science_image')):
	               $science_image=$this->session->userdata('science_image');   
	 		       $product_related['science_path'] = $science_image['image'];  
	  		       $product_related['science_thumb_path'] = $science_image['thumb_image'];   
	  		    endif;
	  		    if($this->session->userdata('clinical_image')):
	                $clinical_image = $this->session->userdata('clinical_image');
	                $product_related['clinical_path'] = $clinical_image['image'];
	                $product_related['clinical_thumb_path'] = $clinical_image['thumb_image'];
	            endif;
	            // if($this->session->userdata('image_first')):
	            //     $first_image = $this->session->userdata('image_first');
	            //     $product_related['image_first_path'] = $first_image['image'];
	            //     $product_related['image_first_thumb_path'] = $first_image['thumb_image'];
	            // endif;
	            // if($this->session->userdata('image_second')):
	            //     $second_image = $this->session->userdata('image_second');
	            //     $product_related['image_second_path'] = $second_image['image'];
	            //     $product_related['image_second_thumb_path'] = $second_image['thumb_image'];
	            // endif;
	            // if($this->session->userdata('image_third')):
	            //     $third_image = $this->session->userdata('image_third');
	            //     $product_related['image_third_path'] = $third_image['image'];
	            //     $product_related['image_third_thumb_path'] = $third_image['thumb_image'];
	            // endif;
	            // if($this->session->userdata('image_four')):
	            //     $four_image = $this->session->userdata('image_four');
	            //     $product_related['image_four_path'] = $four_image['image'];
	            //     $product_related['image_four_thumb_path'] = $four_image['thumb_image'];
	            // endif;

	            $this->product_model->insert('products_related_info',$product_related);

				$product_shpping = array(
											'product_id'=>$product_id,
											'weight'=>$this->input->post('weight'),
											//'freight_surcharge'=>$this->input->post('freight_surcharge'),
											//'shipping_surcharge'=>$this->input->post('shipping_surcharge'),
											'free_shipping'=>$this->input->post('free_shipping'),
											'exempt_shipping_charge'=>$this->input->post('exempt_shipping')
									    );

				$this->product_model->insert('product_shpping_info',$product_shpping);

				$arrayName = array();
				if($this->session->userdata('product_image')){
					$images = $this->session->userdata('product_image');
		   		 	$count  = count($images);
		   		 	for($i=0;$i<$count; $i++){
		   		 		$is_feature=0;
		   		 		$img=explode('/', $images[$i]['image']);
                        if(!empty($_POST['photo_'.$i])){
                            $image_name = $_POST['photo_'.$i]; 
                        }else{
                            $image_name = str_replace('.jpg','',$img[4]);
                        }
                        if(!empty($_POST['is_feature_'.$i])){
                            $is_feature = $_POST['is_feature_'.$i];
                        }

		   		 		$arrayName = array(
		   		 						'products_id'=>$product_id,
		   		 						'label'=>$image_name,
		   		 						'random_background'=>$is_feature,
			    						'path'=>$images[$i]['image'] ,
			    						'thumbnail_path'=>$images[$i]['thumb_image'],
			    						'new_thumb_image'=>$images[$i]['new_thumb_image']
	    				        	);
		   		 		$this->product_model->insert('products_photos',$arrayName);
		   		 	}
		   			$this->session->unset_userdata('product_image');
				}
				if($this->session->userdata('science_image')):	
                	$this->session->unset_userdata('science_image');
             	endif;
             	if($this->session->userdata('image_first')):	
                	$this->session->unset_userdata('image_first');
             	endif;
             	if($this->session->userdata('image_second')):	
                	$this->session->unset_userdata('image_second');
             	endif;
             	if($this->session->userdata('image_third')):	
                	$this->session->unset_userdata('image_third');
             	endif;
             	if($this->session->userdata('image_four')):	
                	$this->session->unset_userdata('image_four');
             	endif;
             	if($this->session->userdata('clinical_image')):	
                	$this->session->unset_userdata('clinical_image');
             	endif;
				$this->session->set_flashdata('msg_success','Product added successfully.');
				redirect('backend/products');
				
			}
		}
		// if($this->session->userdata('science_image')){
		// 	$science_img = $this->session->userdata('science_image');
		// 	$new_file=$science_img['image'];
		// 	$thumb_file=$science_img['thumb_image'];
		// 	@unlink($new_file);
		// 	@unlink($thumb_file);
		// 	$this->session->unset_userdata('science_image');
		// }
		// if($this->session->userdata('clinical_image')){
		// 	$clinical_img = $this->session->userdata('clinical_image');
		// 	$new_file=$science_img['image'];
		// 	$thumb_file=$science_img['thumb_image'];
		// 	@unlink($new_file);
		// 	@unlink($thumb_file);
		// 	$this->session->unset_userdata('clinical_image');
		// }
		
		if($this->session->userdata('product_image')){
			$image_url='assets/uploads/products/';
			$images = $this->session->userdata('product_image');
   		 	$count  = count($images);
   		 	for($i=0;$i<$count;$i++){
	   		 	$new_file=$images[$i]['image'];
				$thumb_file=$images[$i]['thumb_image'];
				$new_thumb_image=$images[$i]['new_thumb_image'];
				@unlink($new_file);
				@unlink($thumb_file);
				@unlink($new_thumb_image);
    		}
    		$this->session->unset_userdata('product_image');
    		$this->session->set_flashdata('msg_error','Please Reupload image files.');
   		}
		$data['category'] = $this->product_model->get_result('categories',array('status'=>1));
		$data['products'] = $this->product_model->get_result('products',array('status'=>1));
		$data['template']='backend/product/pro_add';
		$this->load->view('templates/backend/layout', $data);
	}
	public function edit($pro_id='')	{
		_check_superadmin_login(); //check login authentication
		// echo $pro_id;
		if(empty($pro_id)) redirect('backend/products/');
		$data['product'] = $this->product_model->get_row('products',array('id'=>$pro_id));
		// print_r($data['product']);
		// die();
		
		if(empty($data['product'])) redirect('backend/products/');
		$data['shipping_info']= $this->product_model->get_row('product_shpping_info',array('product_id'=>$data['product']->id));
		$data['related_info']= $this->product_model->get_row('products_related_info',array('product_id'=>$data['product']->id));
		$data['products_photos'] = $this->product_model->get_result('products_photos',array('products_id' =>$data['product']->id));

		$this->form_validation->set_rules('name', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('description','Description', 'trim|required');
		$this->form_validation->set_rules('short_description','Short Description', 'trim|required');
		$this->form_validation->set_rules('science_description','Science Description','');
		$this->form_validation->set_rules('clinical_description','Clinical Description','');
		$this->form_validation->set_rules('use_description','Use Description','');
		$this->form_validation->set_rules('price','Price', 'trim|numeric');
		$this->form_validation->set_rules('sku','SKU', 'trim|numeric|callback_check_sku['.$data['product']->id.']');
		$this->form_validation->set_rules('weight','Weight', 'trim|numeric');
		$this->form_validation->set_rules('category[]','Category', 'trim|required');
		$this->form_validation->set_rules('slide_label','Slide Label', 'trim');
		$this->form_validation->set_rules('use_description_right','Use Descriptions Right', '');
		$this->form_validation->set_rules('science_image','Science image','callback_science_check_edit');
		$this->form_validation->set_rules('clinical_image','Clinical image','callback_clinical_check_edit');
		//if($_POST){
			//if(!empty($_POST['image_use_only'])){	
				// if(!empty($_FILES['image_first']['name'])){
				// 	$this->form_validation->set_rules('image_first','Image First','callback_image_first_add');
				// }
				// if(!empty($_FILES['image_second']['name'])){
				// 	$this->form_validation->set_rules('image_second','Image Second','callback_image_second_add');
				// }
				// if(!empty($_FILES['image_third']['name'])){
				// 	$this->form_validation->set_rules('image_third','Image Third','callback_image_third_add');
				// }
				// if(!empty($_FILES['image_four']['name'])){
				// 	$this->form_validation->set_rules('image_four','Image Four','callback_image_four_add');
				// }
			//}
		//}	
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if($this->form_validation->run() == TRUE){		
			$category = "#".implode('#,#',$this->input->post('category'))."#";
			if(!empty($_POST['rela_products'])){
				$rela_products = "#".implode('#,#',$this->input->post('rela_products'))."#";
			}else{
				$rela_products='';
			}
			// $images=$this->session->userdata('product_image');
				// if(empty($images)){
				// 	$this->session->set_flashdata('image_error_msg','Please upload Product image');
				// 	redirect('backend/products/add');
			// }
			$product = array(
								'name'=> $this->input->post('name'),
								'slug'=> url_title($this->input->post('name'),'-',TRUE),
								'category' => $category,
								'description' => $this->input->post('description'),
								'price_type' => $this->input->post('price_type'),
								'SKU'=>$this->input->post('sku'),
								'related_products'=>$rela_products,
								'meta_title'=>$this->input->post('meta_title'),
								'meta_description'=>$this->input->post('meta_description'),
								'short_description'=>$this->input->post('short_description'),
								'meta_keywords'=>$this->input->post('meta_keywords'),
								'avilable_item'=>$this->input->post('available_item'),
								'discount_item'=>$this->input->post('discount_item'),
								'meta_title'=>$this->input->post('meta_title'),
								'meta_keywords'=>$this->input->post('meta_keywords'),
								'meta_description'=>$this->input->post('meta_description'),
								'featured_product'=>$this->input->post('featured_item'),
								'status'	  => $this->input->post('status'),
								'created'=> date('Y-m-d h:i:s')
						    );
			if($this->input->post('price_type')==1){
				$product['price'] =  $this->input->post('price');
				$this->product_model->delete('multiple_price',array('product_id'=>$data['product']->id));
			}

			if($this->product_model->update('products',$product,array('id' =>$data['product']->id))){

				if($this->input->post('price_type')==2){
					$this->product_model->delete('multiple_price',array('product_id'=>$data['product']->id));
					$prices= $this->input->post('price_for');
					$price_label= $this->input->post('price_label');
					for($i=0; $i<count($prices); $i++){ 
						$arrayName = array('product_id'=>$data['product']->id,  'price_for'=>$prices[$i],'price_label'=>$price_label[$i]);
						$this->product_model->insert('multiple_price',$arrayName);											
					}
				}

	            $product_related = array(
		             						'product_id'=>$data['product']->id,
		             						'use_description'=>htmlspecialchars($this->input->Post('use_description')),
		             						'clinical_description'=>htmlspecialchars($this->input->Post('clinical_description')),
		             						'science_description'=>htmlspecialchars($this->input->Post('science_description')),
		             						'use_description_right'=>htmlspecialchars($this->input->Post('use_description_right')),
		             						'related_description'=>htmlspecialchars($this->input->Post('related_description')),
		             					   	'content_use_only' => $this->input->Post('content_use_only'),
		             					   	'slide_order' => $this->input->Post('slide_order'),
		             					  	'direction_image' => htmlspecialchars($this->input->Post('direction_image')),
											'first_section_order' => $this->input->Post('first_section_order'),
		             					   	'second_section_order' => $this->input->Post('second_section_order'),
		             					   	'first_section_sprint' => $this->input->Post('first_section_sprint'),
		             					   	'second_section_sprint' => $this->input->Post('second_section_sprint'),
		             					   	'use_section_sprint' => $this->input->Post('use_section_sprint'),
		             					   	'content_sprint' => $this->input->Post('content_sprint'),
		             					   	'slide_sprint' => $this->input->Post('slide_sprint'),
		             					   	'use_section_order'=>$this->input->Post('use_section_order'),
		             					   	'slide_use_only' => $this->input->Post('image_use_only'),
		             					   	'content_order' => $this->input->Post('content_order'),
	             					    );
      
      
	
				if($this->session->userdata('science_image')):
	                $science_image=$this->session->userdata('science_image');   
	 		        $product_related['science_path'] = $science_image['image'];  
	  		        $product_related['science_thumb_path'] = $science_image['thumb_image']; 
	  		        if(!empty($data['related_info']->science_path)){
	            		$new_file = $data['related_info']->science_path;
						$thumb_file = $data['related_info']->science_thumb_path;
						@unlink($new_file);
						@unlink($thumb_file);
	            	}  
	  		    endif;
	  		    if($this->session->userdata('clinical_image')):
	                $clinical_image = $this->session->userdata('clinical_image');
	                $product_related['clinical_path'] = $clinical_image['image'];
	                $product_related['clinical_thumb_path'] = $clinical_image['thumb_image'];
	                if(!empty($data['related_info']->clinical_path)){
	            		$new_file = $data['related_info']->clinical_path;
						$thumb_file = $data['related_info']->clinical_thumb_path;
						@unlink($new_file);
						@unlink($thumb_file);
	            	}  
	            endif;
	     //        if($this->session->userdata('image_first')):
	     //            $first_image=$this->session->userdata('image_first');   
	 		  //       $product_related['image_first_path'] = $first_image['image'];  
	  		 //        $product_related['image_first_thumb_path'] = $first_image['thumb_image'];
	  		 //        if(!empty($data['related_info']->image_first_path)){
	     //        		$new_file = $data['related_info']->image_first_path;
						// $thumb_file = $data['related_info']->image_first_thumb_path;
						// @unlink($new_file);
						// @unlink($thumb_file);
	     //        	}     
	  		 //    endif;
	  		 //    if($this->session->userdata('image_second')):
	     //            $second_image=$this->session->userdata('image_second');   
	 		  //       $product_related['image_second_path'] = $second_image['image'];  
	  		 //        $product_related['image_second_thumb_path'] = $second_image['thumb_image'];
	  		 //        if(!empty($data['related_info']->image_second_path)){
	     //        		$new_file = $data['related_info']->image_second_path;
						// $thumb_file = $data['related_info']->image_second_thumb_path;
						// @unlink($new_file);
						// @unlink($thumb_file);
	     //        	}     
	     //        endif;
	     //        if($this->session->userdata('image_third')):
	     //            $third_image=$this->session->userdata('image_third');   
	 		  //       $product_related['image_third_path'] = $third_image['image'];  
	  		 //        $product_related['image_third_thumb_path'] = $third_image['thumb_image'];
	  		 //        if(!empty($data['related_info']->image_third_path)){
	     //        		$new_file = $data['related_info']->image_third_path;
						// $thumb_file = $data['related_info']->image_third_thumb_path;
						// @unlink($new_file);
						// @unlink($thumb_file);
	     //        	}     
	     //        endif;
	     //        if($this->session->userdata('image_four')):
	     //            $four_image=$this->session->userdata('image_four');   
	 		  //       $product_related['image_four_path'] = $four_image['image'];  
	  		 //        $product_related['image_four_thumb_path'] = $four_image['thumb_image'];
	  		 //        if(!empty($data['related_info']->image_four_path)){
	     //        		$new_file = $data['related_info']->image_four_path;
						// $thumb_file = $data['related_info']->image_four_thumb_path;
						// @unlink($new_file);
						// @unlink($thumb_file);
	     //        	}     
	     //        endif;

	            // print_r($product_related);
	            // die();
	            $this->product_model->update('products_related_info',$product_related,array('product_id' =>$data['product']->id));

				$product_shpping = array(
											'product_id'=>$data['product']->id,
											'weight'=>$this->input->post('weight'),
											// 'freight_surcharge'=>$this->input->post('freight_surcharge'),
											// 'shipping_surcharge'=>$this->input->post('shipping_surcharge'),
											'free_shipping'=>$this->input->post('free_shipping'),
											'exempt_shipping_charge'=>$this->input->post('exempt_shipping')
									    );
				// print_r($product_shpping);
				// die();
				$this->product_model->update('product_shpping_info',$product_shpping,array('product_id' =>$data['product']->id));

				$arrayName = array();
				if($this->session->userdata('product_image')){
					$image_url1='assets/uploads/products/';
					// if(!empty($data['products_photos'])):
					// 	foreach ($data['products_photos'] as $value){
					// 		$new_file=$value->path;
					// 		$thumb_file=$value->thumbnail_path;
					// 		@unlink($new_file);
					// 		@unlink($thumb_file);
					// 	}
					// endif;
					// $this->product_model->delete('products_photos',array('products_id'=>$data['product']->id));
					$images = $this->session->userdata('product_image');
		   		 	$count  = count($images);
		   		 	for($i=0;$i < $count; $i++){
						$is_feature=0;
		   		 		$img=explode('/', $images[$i]['image']);
                        if(!empty($_POST['photo_'.$i])){
                            $image_name = $_POST['photo_'.$i]; 
                        }else{
                            $image_name = str_replace('.jpg','',$img[4]);
                        }
                        if(!empty($_POST['is_feature_'.$i])){
                            $is_feature = $_POST['is_feature_'.$i];
                        }
		   		 		$arrayName = array(
			   		 						'products_id'=>$data['product']->id,
			   		 						'label'=>$image_name,
			   		 						'random_background'=>$is_feature,
				    						'path'=>$images[$i]['image'] ,
				    						'thumbnail_path'=>$images[$i]['thumb_image'],
				    						'new_thumb_image'=>$images[$i]['new_thumb_image']
				    				      );
		   		 		$this->product_model->insert('products_photos',$arrayName);
		   		 	}
		   			$this->session->unset_userdata('product_image');
				}

				if(!empty($data['products_photos'])){ 
                    $i=1; 
                    foreach($data['products_photos'] as $row){
                        if(!empty($_POST['label_'.$row->id.'_'.$i])){
        	
                      		if(!empty($_POST['is_feature_'.$row->id.'_'.$i])){
                            	$array = array('label' =>  $_POST['label_'.$row->id.'_'.$i], 'random_background'=> $_POST['is_feature_'.$row->id.'_'.$i] );
                          	}else{
                            	$array = array('label' =>  $_POST['label_'.$row->id.'_'.$i], 'random_background'=>0 );
                          	}

                         $this->product_model->update('products_photos', $array, array('id' => $row->id));    
                        }

                    $i++;
                    }                                                       
                }    
				
				if($this->session->userdata('science_image')):	
                	$this->session->unset_userdata('science_image');
             	endif;
             	if($this->session->userdata('clinical_image')):	
                	$this->session->unset_userdata('clinical_image');
             	endif;
             	if($this->session->userdata('image_first')):	
                	$this->session->unset_userdata('image_first');
             	endif;
             	if($this->session->userdata('image_second')):	
                	$this->session->unset_userdata('image_second');
             	endif;
             	if($this->session->userdata('image_third')):	
                	$this->session->unset_userdata('image_third');
             	endif;
             	if($this->session->userdata('image_four')):	
                	$this->session->unset_userdata('image_four');
             	endif;
				$this->session->set_flashdata('msg_success','Product has updated successfully.');
				redirect('backend/products');	
			}
		}
		// if($this->session->userdata('science_image')){
		// 	$science_img = $this->session->userdata('science_image');
		// 	$new_file=$science_img['image'];
		// 	$thumb_file=$science_img['thumb_image'];
		// 	@unlink($new_file);
		// 	@unlink($thumb_file);
		// 	$this->session->unset_userdata('science_image');
		// }
		// if($this->session->userdata('clinical_image')){
		// 	$clinical_img = $this->session->userdata('clinical_image');
		// 	$new_file=$science_img['image'];
		// 	$thumb_file=$science_img['thumb_image'];
		// 	@unlink($new_file);
		// 	@unlink($thumb_file);
		// 	$this->session->unset_userdata('clinical_image');
		// }
		
		if($this->session->userdata('product_image')){
			$image_url='assets/uploads/products/';
			$images = $this->session->userdata('product_image');
   		 	$count  = count($images);
   		 	for($i=0;$i<$count;$i++){
	   		 	$new_file=$images[$i]['image'];
				$thumb_file=$images[$i]['thumb_image'];
				@unlink($new_file);
				@unlink($thumb_file);
    		}
    		$this->session->unset_userdata('product_image');
    		$this->session->set_flashdata('msg_error','Please Reupload image files.');
   		}	
		$data['category'] = $this->product_model->get_result('categories',array('status'=>1));
		$data['products'] = $this->product_model->get_result('products',array('status'=>1));
		
		$data['template']='backend/product/pro_edit';
		$this->load->view('templates/backend/layout',$data);	
	}

	public function product_clone($product_id=''){
		_check_superadmin_login(); //check login authentication
		if(empty($product_id)) redirect('backend/products/');
		$product = $this->product_model->get_row('products',array('id'=>$product_id));
		$product_related = $this->product_model->get_row('products_related_info',array('product_id'=>$product_id));
		$shipping_related = $this->product_model->get_row('product_shpping_info',array('product_id'=>$product_id));
		if(empty($product)) redirect('backend/products/');
		if($product->price_type==2){
			$prices= $this->product_model->get_result('multiple_price',array('product_id'=>$product_id));
		}
		$product_info = (array)	 $product;
		$product_relt_info = (array) $product_related;
		$ship_info = (array) $shipping_related;

		$product_info['id']='';
		$product_info['SKU']='';
		$product_relt_info['science_path']='';
        $product_relt_info['science_thumb_path']='';
        $product_relt_info['clinical_path']='';
        $product_relt_info['clinical_thumb_path']='';
        $product_relt_info['image_first_path']='';
        $product_relt_info['image_first_thumb_path']='';
        $product_relt_info['image_second_path']='';
        $product_relt_info['image_second_thumb_path']='';
        $product_relt_info['image_third_path']='';
        $product_relt_info['image_third_thumb_path']='';
        $product_relt_info['image_four_path']='';
        $product_relt_info['image_four_thumb_path']='';
		if($pro_id=$this->product_model->insert('products',$product_info)){
        	$product_relt_info['product_id']=$pro_id;
        	$product_relt_info['id']='';
        	$ship_info['id']='';
        	$ship_info['product_id']=$pro_id;

		 	$status = $this->product_model->insert('products_related_info',$product_relt_info);
		 	$status1 = $this->product_model->insert('product_shpping_info',$ship_info);
		 	if(!empty($prices)){
			 	foreach ($prices as $value) {
			 		$array=  array('product_id'=>$pro_id,'price_for'=>$value->price_for,'price_label'=>$value->price_label);
			 		$status1 = $this->product_model->insert('multiple_price',$array);
			 	}
		 	}
		 	$this->session->set_flashdata('msg_success','Product clone has been added successfully.');
			redirect('backend/products');
		}else{
			$this->session->set_flashdata('msg_error','Action has been failed.');
			redirect('backend/products');
		}
	}

	public function delete_products()
	{
		_check_superadmin_login(); //check Professor login authentication
		if($_POST){
			$this->form_validation->set_rules('checkall[]','Product', 'trim|required');
			$this->form_validation->set_error_delimiters('<div style="color:red;" class="error">','</div>');
			if($this->form_validation->run() == TRUE){
				$cust_id_array= $this->input->post('checkall');
				$FLAG=TRUE;
				// print_r($student_id_array);
				// die();
				// $status = $_POST['all_status'];
				$count_cust = count($cust_id_array);
				for($i=0; $i<$count_cust ; $i++){ 
					$pro_status = $this->product_model->delete('products',array('id'=>$cust_id_array[$i]));	
					$this->product_model->delete('products_photos',array('products_id'=>$cust_id_array[$i]));
					$this->product_model->delete('products_related_info',array('product_id'=>$cust_id_array[$i]));
					if(!$pro_status){
						$FLAG=FALSE;
					}
				}			
				if($FLAG){
					$this->session->set_flashdata('msg_success',"Product have been Deleted successfully.");
					redirect('backend/products');
				}
			}else{
				$this->session->set_flashdata('msg_error',"Product delete Operation Failed.");
				redirect('backend/products');
			}
		}else{
			$this->session->set_flashdata('msg_error',"Product delete Operation Failed...");
			redirect('backend/products');
		} 
	}

	public function reviews($product_id='')
	{   _check_superadmin_login(); //check login authentication
		if(empty($product_id)) redirect('backend/products/');
		$data['product_info']= $this->product_model->get_row('products',array('id'=>$product_id));
		if(empty($data['product_info'])) redirect('backend/products/');
		$data['reviews'] = $this->product_model->get_result('product_reviews',array('product_id'=>$product_id));

		$data['template']='backend/product/reviews';
		$this->load->view('templates/backend/layout',$data);	
	}

	public function review_delete($id ='',$product_id=''){
		_check_superadmin_login(); //check login authentication
		if(empty($product_id)) redirect('backend/products/');
		if(empty($id)) redirect('backend/products/reviews/'.$product_id);
		$data['review_info'] = $this->product_model->get_row('product_reviews',array('id'=>$id));
		$this->product_model->delete('product_reviews',array('id'=>$id));
		$this->product_model->delete('product_rating',array('user_id'=>$data['review_info']->user_id,'product_id'=>$data['review_info']->product_id));
		$this->session->set_flashdata('msg_success','Review has been Deleted successfully.');
		redirect('backend/products/reviews/'.$product_id);
	}	

	public function review_view($id ='',$product_id=''){
		_check_superadmin_login(); //check login authentication
		if(empty($product_id)) redirect('backend/products/');
		$data['product_info']= $this->product_model->get_row('products',array('id'=>$product_id));
		if(empty($data['product_info'])) redirect('backend/products/');
		if(empty($id)) redirect('backend/products/reviews/'.$product_id);
		$data['review_info'] = $this->product_model->get_row('product_reviews',array('id'=>$id));
		
		$data['template']='backend/product/review_view';
		$this->load->view('templates/backend/layout',$data);
	}	


	public function change_review_status($id='',$product_id='',$status='')
	{

		if(empty($product_id)) redirect('backend/products/');
		if(empty($id)) redirect('backend/products/reviews/'.$product_id);

        if($status==0){
            $cat_status=1;
        }
        if($status==1){
            $cat_status=0;
        }       
        $data = array('status'=>$cat_status);
        if($this->product_model->update('product_reviews', $data ,array('id'=>$id)))
        {
           $this->session->set_flashdata('msg_success','Product Review status has been updated successfully.');
           redirect('backend/products/reviews/'.$product_id);  
        }else{
           redirect('backend/products/reviews/'.$product_id);
        }
	}



	public function get_available_item($value=''){
		_check_superadmin_login(); //check login authentication
		if($_POST){
			$pro = explode('_',$_POST['proid']);
			$pro_id= $pro[1];
		    $val = $_POST['val'];
		    if($val==0)
		    	$array = array('avilable_item'=>1);
		    else
		    	$array = array('avilable_item'=>0);
		    $status = $this->product_model->update('products',$array,array('id'=>$pro_id));
		    if($status){
		    	echo 'success';
		    	return TRUE;
		    }else{
		    	echo 'failed';
		    	return FALSE;
		    }
		}
	}
	public function get_featured_item($value=''){
		_check_superadmin_login(); //check login authentication
		if($_POST){
			$pro = explode('_',$_POST['proid']);
			$pro_id= $pro[1];
		    $val = $_POST['val'];
		    if($val==0)
		    	$array = array('featured_product'=>1);
		    else
		    	$array = array('featured_product'=>0);
		    $status = $this->product_model->update('products',$array,array('id'=>$pro_id));
		    if($status){
		    	echo 'success';
		    	return TRUE;
		    }else{
		    	echo 'failed';
		    	return FALSE;
		    }
		}
	}
	public function get_discount_item($value=''){
		_check_superadmin_login(); //check login authentication
		if($_POST){
			$pro = explode('_',$_POST['proid']);
			$pro_id= $pro[1];
		    $val = $_POST['val'];
		    if($val==0)
		    	$array = array('discount_item'=>1);
		    else
		    	$array = array('discount_item'=>0);
		    $status = $this->product_model->update('products',$array,array('id'=>$pro_id));
		    if($status){
		    	echo 'success';
		    	return TRUE;
		    }else{
		    	echo 'failed';
		    	return FALSE;
		    }
		}
	}

	public function status($id="",$status="",$offset="")
    {
        _check_superadmin_login(); //check login authentication
        if(empty($id)) redirect('backend/products/');
        if($status==0){
            $cat_status=1;
        }
        if($status==1){
            $cat_status=0;
        }       
        $data = array('status'=>$cat_status);
        if($this->product_model->update('products', $data ,array('id'=>$id)))
        {
           $this->session->set_flashdata('msg_success','Product status has been updated successfully.');
           redirect('backend/products/index/'.$offset);   
        }

    }

	public function check_sku($sku='',$id=''){   
		_check_superadmin_login(); //check login authentication
        if($_POST){ 	
           $pro_info = $this->product_model->get_row('products',array('id'=>$id));  
           if($_POST['sku']!=$pro_info->SKU){
              $resp = $this->product_model->get_row('products', array('SKU'=>$_POST['sku']));
              if($resp){
                  $this->form_validation->set_message('check_sku', 'SKU no. is already exist.');
                  return FALSE;
              } else{
                  return TRUE;
              }
           }else{
              return TRUE;
           }
        }   
    }

	public function science_file_add($str){           
      if(!empty($_FILES['science_image']['name'])){
        if($this->session->userdata('science_image')){               
            return TRUE;
        }else{


            $param=array(
	                    'file_name' =>'science_image',
	                    'upload_path'  => './assets/uploads/science_images/',
	                    'allowed_types'=> 'gif|jpg|png|jpeg',
	                    'image_resize' => TRUE,
	                    'source_image' => './assets/uploads/science_images/',
	                    'new_image'    => './assets/uploads/science_images/thumb/',
	                    'resize_width' => 200,
	                    'resize_height' => 200,
	                    'encrypt_name' => TRUE,
               		);

             
            // if(!empty($_FILES['event_banner_image']['tmp_name'])){
            //     $img=getimagesize($_FILES['event_banner_image']['tmp_name']);
            //     $minimum = array('width' => '1200', 'height' => '350');
            //     $width=$img[0];
            //     $height=$img[1];
            //     if($width < $minimum['width'] || $height < $minimum['height']){
            //         $this->form_validation->set_message('event_banner_image_check', "Image dimensions are too small. Minimum width x height is {$minimum['width']} x{$minimum['height']}.");
            //         return FALSE;
            //     }
            // }     
        
                $upload_file=upload_file($param);
				//thumbnail create

                if($upload_file['STATUS']){
                    $this->session->set_userdata('science_image',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));            
                    return TRUE;        
                }else{          
                    $this->form_validation->set_message('science_file_add', $upload_file['FILE_ERROR']);              
                    return FALSE;
                }
          
        }
        }
       
    }


    public function image_first_add($str){           
       if(!empty($_FILES['image_first']['name'])){
	        if($this->session->userdata('image_first')){               
	            return TRUE;
	        }else{
                $param=array(
		                    'file_name' =>'image_first',
		                    'upload_path'  => './assets/uploads/product_images/',
		                    'allowed_types'=> 'gif|jpg|png|jpeg',
		                    'image_resize' => TRUE,
		                    'source_image' => './assets/uploads/product_images/',
		                    'new_image'    => './assets/uploads/product_images/thumb/',
		                    'resize_width' => 200,
		                    'resize_height' => 200,
		                    'encrypt_name' => TRUE,
               		    );
                $upload_file=upload_file($param);
                if($upload_file['STATUS']){
                    $this->session->set_userdata('image_first',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));            
                    return TRUE;        
                }else{          
                    $this->form_validation->set_message('image_first_add', $upload_file['FILE_ERROR']);              
                    return FALSE;
                }
        	}
        }       
    }

    public function image_second_add($str){           
       if(!empty($_FILES['image_second']['name'])){
	        if($this->session->userdata('image_second')){               
	            return TRUE;
	        }else{
                $param=array(
		                    'file_name' =>'image_second',
		                    'upload_path'  => './assets/uploads/product_images/',
		                    'allowed_types'=> 'gif|jpg|png|jpeg',
		                    'image_resize' => TRUE,
		                    'source_image' => './assets/uploads/product_images/',
		                    'new_image'    => './assets/uploads/product_images/thumb/',
		                    'resize_width' => 200,
		                    'resize_height' => 200,
		                    'encrypt_name' => TRUE,
               		    );
                $upload_file=upload_file($param);
                if($upload_file['STATUS']){
                    $this->session->set_userdata('image_second',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));            
                    return TRUE;        
                }else{          
                    $this->form_validation->set_message('image_second_add', $upload_file['FILE_ERROR']);              
                    return FALSE;
                }
        	}
        }       
    }


    public function image_third_add($str){           
       if(!empty($_FILES['image_third']['name'])){
	        if($this->session->userdata('image_third')){               
	            return TRUE;
	        }else{
                $param=array(
		                    'file_name' =>'image_third',
		                    'upload_path'  => './assets/uploads/product_images/',
		                    'allowed_types'=> 'gif|jpg|png|jpeg',
		                    'image_resize' => TRUE,
		                    'source_image' => './assets/uploads/product_images/',
		                    'new_image'    => './assets/uploads/product_images/thumb/',
		                    'resize_width' => 200,
		                    'resize_height' => 200,
		                    'encrypt_name' => TRUE,
               		    );
                $upload_file=upload_file($param);
                if($upload_file['STATUS']){
                    $this->session->set_userdata('image_third',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));            
                    return TRUE;        
                }else{          
                    $this->form_validation->set_message('image_third_add', $upload_file['FILE_ERROR']);              
                    return FALSE;
                }
        	}
        }       
    }

    public function image_four_add($str){           
       if(!empty($_FILES['image_four']['name'])){
	        if($this->session->userdata('image_four')){               
	            return TRUE;
	        }else{
                $param=array(
		                    'file_name' =>'image_four',
		                    'upload_path'  => './assets/uploads/product_images/',
		                    'allowed_types'=> 'gif|jpg|png|jpeg',
		                    'image_resize' => TRUE,
		                    'source_image' => './assets/uploads/product_images/',
		                    'new_image'    => './assets/uploads/product_images/thumb/',
		                    'resize_width' => 200,
		                    'resize_height' => 200,
		                    'encrypt_name' => TRUE,
               		    );
                $upload_file=upload_file($param);
                if($upload_file['STATUS']){
                    $this->session->set_userdata('image_four',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));            
                    return TRUE;        
                }else{          
                    $this->form_validation->set_message('image_four_add', $upload_file['FILE_ERROR']);              
                    return FALSE;
                }
        	}
        }       
    }

	public function clinical_file_add($str){           
        if(!empty($_FILES['clinical_image']['name'])){ 
		        if($this->session->userdata('clinical_image')){               
		            return TRUE;
		        }else{


		                $param=array(
						                'file_name' =>'clinical_image',
						                'upload_path'  => './assets/uploads/clinical_images/',
						                'allowed_types'=> 'gif|jpg|png|jpeg',
						                'image_resize' => TRUE,
						                'source_image' => './assets/uploads/clinical_images/',
						                'new_image'    => './assets/uploads/clinical_images/thumb/',
						                'resize_width' => 200,
						                'resize_height' => 200,
						                'encrypt_name' => TRUE,
						            );

		             
			            // if(!empty($_FILES['event_banner_image']['tmp_name'])){
			            //     $img=getimagesize($_FILES['event_banner_image']['tmp_name']);
			            //     $minimum = array('width' => '1200', 'height' => '350');
			            //     $width=$img[0];
			            //     $height=$img[1];
			            //     if($width < $minimum['width'] || $height < $minimum['height']){
			            //         $this->form_validation->set_message('event_banner_image_check', "Image dimensions are too small. Minimum width x height is {$minimum['width']} x{$minimum['height']}.");
			            //         return FALSE;
			            //     }
			            // }     
		        
		 
		                $upload_file=upload_file($param);
						//thumbnail create

		                if($upload_file['STATUS']){
		                    $this->session->set_userdata('clinical_image',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));            
		                    return TRUE;        
		                }else{          
		                    $this->form_validation->set_message('clinical_file_add', $upload_file['FILE_ERROR']);              
		                    return FALSE;
		                }
		          
		        }
    	}
       
    }



    public function science_check_edit($str){           
        if(!empty($_FILES['science_image']['name'])){
            if($this->session->userdata('science_image')){               
                return TRUE;
            }else{

                $param=array(
                    'file_name' =>'science_image',
                    'upload_path'  => './assets/uploads/science_images/',
                   'allowed_types'=> 'gif|jpg|png|jpeg',
                   'image_resize' => TRUE,
                    'source_image' => './assets/uploads/science_images/',
                    'new_image'    => './assets/uploads/science_images/thumb/',
                    'resize_width' => 200,
                    'resize_height' => 200,
                    'encrypt_name' => TRUE,
                );

             
	            // if(!empty($_FILES['event_banner_image']['tmp_name'])){
	            //     $img=getimagesize($_FILES['event_banner_image']['tmp_name']);
	            //     $minimum = array('width' => '1200', 'height' => '350');
	            //     $width=$img[0];
	            //     $height=$img[1];
	            //     if($width < $minimum['width'] || $height < $minimum['height']){
	            //         $this->form_validation->set_message('event_banner_image_check', "Image dimensions are too small. Minimum width x height is {$minimum['width']} x{$minimum['height']}.");
	            //         return FALSE;
	            //     }
	            // }     
        
 
                $upload_file=upload_file($param);
				//thumbnail create

                if($upload_file['STATUS']){
                    $this->session->set_userdata('science_image',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));            
                    return TRUE;        
                }else{          
                    $this->form_validation->set_message('science_check_edit', $upload_file['FILE_ERROR']);              
                    return FALSE;
                }
            }
        }
    } 

    public function clinical_check_edit($str){           
        if(!empty($_FILES['clinical_image']['name'])){
            if($this->session->userdata('clinical_image')){               
                return TRUE;
            }else{

                $param=array(
                    'file_name' =>'clinical_image',
                    'upload_path'  => './assets/uploads/clinical_images/',
                   'allowed_types'=> 'gif|jpg|png|jpeg',
                   'image_resize' => TRUE,
                    'source_image' => './assets/uploads/clinical_images/',
                    'new_image'    => './assets/uploads/clinical_images/thumb/',
                    'resize_width' => 200,
                    'resize_height' => 200,
                    'encrypt_name' => TRUE,
                );

             
	            // if(!empty($_FILES['event_banner_image']['tmp_name'])){
	            //     $img=getimagesize($_FILES['event_banner_image']['tmp_name']);
	            //     $minimum = array('width' => '1200', 'height' => '350');
	            //     $width=$img[0];
	            //     $height=$img[1];
	            //     if($width < $minimum['width'] || $height < $minimum['height']){
	            //         $this->form_validation->set_message('event_banner_image_check', "Image dimensions are too small. Minimum width x height is {$minimum['width']} x{$minimum['height']}.");
	            //         return FALSE;
	            //     }
	            // }     
        
 
                $upload_file=upload_file($param);
				//thumbnail create

                if($upload_file['STATUS']){
                    $this->session->set_userdata('clinical_image',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));            
                    return TRUE;        
                }else{          
                    $this->form_validation->set_message('clinical_check_edit', $upload_file['FILE_ERROR']);              
                    return FALSE;
                }
            }
        }
    }

	public function delete($pro_id=''){
		_check_superadmin_login(); //check login authentication
		if(empty($pro_id)) redirect('backend/products');
		if($product_related=$this->product_model->get_row('products_related_info',array('product_id'=>$pro_id))){
			$products_photos = $this->product_model->get_result('products_photos',array('products_id'=>$pro_id));
			if(!empty($product_related->clinical_path)){
        		$new_file_clinical=$product_related->clinical_path;
				$thumb_file_clinical=$product_related->clinical_thumb_path;
				@unlink($new_file_clinical);
				@unlink($thumb_file_clinical);
        	}

        	if(!empty($product_related->science_path)){
        		$new_file=$product_related->science_path;
				$thumb_file=$product_related->science_thumb_path;
				@unlink($new_file);
				@unlink($thumb_file);
	        }

        	if(!empty($products_photos)):
				foreach ($products_photos as $value){
					$new_file=$value->path;
					$thumb_file=$value->thumbnail_path;
					$new_thumb_image=$value->new_thumb_image;
					@unlink($new_file);
					@unlink($thumb_file);
					@unlink($new_thumb_image);
				}
			endif;

		}	
		$this->product_model->delete('products_related_info',array('product_id'=>$pro_id));
		$this->product_model->delete('product_shpping_info',array('product_id'=>$pro_id));
   		$this->product_model->delete('products_photos',array('products_id'=>$pro_id));     
   		$this->product_model->delete('multiple_price',array('product_id'=>$pro_id));     
		$this->product_model->delete('products',array('id'=>$pro_id));
		$this->session->set_flashdata('msg_success','Products has been deleted successfully.');
		redirect('backend/products');		
	}


	
	function upload()
    {
		if(!empty($_FILES))
		{
			$arrayName=array();
		    //$tempFile = $_FILES['file']['tmp_name'];
		    //$targetPath = $_SERVER['DOCUMENT_ROOT'].'/christophestogo/assets/uploads/products/';
		   	//$targetFile = $targetPath . $_FILES['file']['name'];
		    if(!empty($_FILES['file']['name'])){

		    $param=array(
			                'file_name' => 'file',
			                'upload_path' => './assets/uploads/products/',
			                'allowed_types' => 'gif|jpg|png|jpeg',
			                // 'image_resize' => TRUE,
			                'source_image' => './assets/uploads/products/',
			                'new_image' => './assets/uploads/products/thumb/',
			                'new_thumb_image'=>'./assets/uploads/products/medium/',
			                // 'resize_width' => 350,
			                // 'resize_height' => 300,
			                'encrypt_name' => TRUE
	               		);
		    	$filepath='./assets/uploads/products/';
	            $upload_file = upload_file($param);
	            if($upload_file['STATUS']) {

	            	$configs = array();
	                $configs[] = array('source_image' => $upload_file['UPLOAD_DATA']['file_name'], 'new_image' => 'medium/'.$upload_file['UPLOAD_DATA']['file_name'], 'width' => 325, 'height' => 325,'maintain_ratio' =>TRUE);
	                $configs[] = array('source_image' => $upload_file['UPLOAD_DATA']['file_name'], 'new_image' => 'thumb/'.$upload_file['UPLOAD_DATA']['file_name'], 'width' => 100, 'height' => 100,'maintain_ratio' => FALSE);
	                $this->load->library('image_lib');
	                foreach ($configs as $config) {
	                    $this->image_lib->thumb($config,$filepath);
	                }
	              	$uplaod_file= array(
						               	'image' => $param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],
						               	'thumb_image' => $param['new_image'].$upload_file['UPLOAD_DATA']['file_name'],
						               	'new_thumb_image'=>$param['new_thumb_image'].$upload_file['UPLOAD_DATA']['file_name']
						               );


	            if($this->session->userdata('product_image')){
		   		    $images = $this->session->userdata('product_image');
		   		 	$count  = count($images);
		   		 	for($i=0;$i < $count; $i++){
		   		 		$j = $count-1;
		   		 		$arrayName[$i] = array(
					    						'image'=>$images[$i]['image'],
					    						'thumb_image' =>$images[$i]['thumb_image'],
		    					    		  	'new_thumb_image'=>$images[$i]['new_thumb_image']
		    					    		  );
		   		 		if($i==$j){
		   		 			$h =$i+1;
		   		 			$arrayName[$h] = $uplaod_file;
		   		 		}
		   		 	}
		   		 	$this->session->set_userdata('product_image',$arrayName);
		   		}
		   		else
		   		{
		   			$arrayName[0] = $uplaod_file;
		   			$this->session->set_userdata('product_image',$arrayName);
			   	}


	               	die('{"jsonrpc" : "2.0", "result" : "OK", "id" : "1"}');
	            } else {

	                die('{"jsonrpc" : "2.0", "result" : "'.$upload_file['FILE_ERROR'].'", "id" : "1"}');

	            }

		    }

		}
    }

     public function all_file()
    {
    	$resop='';
    	if($_POST){
    		$images = $this->session->userdata('product_image');
    		echo $count  = count($images);
    		for($i=0; $i < $count ; $i++){
                $resop.= '<tr>';
				$resop.='<td>';
				$resop.='<a href="'.base_url($images[$i]['thumb_image']).'" class="fancybox-button" data-rel="fancybox-button"><img class="img-responsive" src="'.base_url($images[$i]['thumb_image']).'" alt=""></a>' ;
				$resop.='</td>';
				$resop.='<td>';
				$img=explode('/', $images[$i]['image']);
				$image_name = str_replace('.jpg','',$img[4]);
				$resop.='<div> <input class="form-control" type="text" ';


				  $resop.=  ' name="photo_'.$i.'" value="'.$image_name.'" >';
				$resop.='</div>';
				$resop.='</td>';
				$resop.='<td> <div style="margin-left:20px;"  class="radio-list">';
                $resop.='<label class="checkbox-inline">';
                $resop.='<input type="checkbox" name="is_feature_'.$i.'"'; 
                
                if($i==0){$resop.='checked="checked"';}
                $resop.='class="is_feature" id="is_feature_'.$i.'" value="1" > YES </label>';
                // $resop.='<input type="c" name="is_feature" id="is_feature" value="1" > Featured </label>';
                // $resop.='<label class="radio-inline">';
               	// $resop.='<input type="radio" name="is_feature_'.$i.'" id="is_feature" value="0" checked> NO</label>';
                $resop.='</div>';
				$resop.='</td>';
				$resop.='<td>';
				$resop.='<a href="javascript:void(0)" onclick="remove_image('.$i.');"  class="btn default btn-sm">';
				$resop.='<i class="fa fa-times"></i> Remove </a>';
				$resop.='</td>';
				$resop.='</tr>';
    		}
    		echo $resop;
    	}
    }


    public function image_remove()
	{
		$resop='';
		if($_POST){
			$id = $_POST['image_id'];
			if($this->session->userdata('product_image')){
				$arrayName='';
	   		    $images = $this->session->userdata('product_image');
	   		 	$count  = count($images);
	   		 	$this->session->unset_userdata('product_image');
	   		 	for($i=0;$i < $count; $i++){

	   		 		if($id==$i){
	   		 			$image_url1='assets/uploads/products/';
	   		 			$new_file=$images[$i]['image'];
						$thumb_file=$images[$i]['thumb_image'];
						$new_thumb_image=$images[$i]['new_thumb_image'];
						@unlink($new_file);
						@unlink($thumb_file);
						@unlink($new_thumb_image);
						$id='flag';
	   		 		}else{
	   		 			if($id=='flag'){
	   		 				$j=$i-1;
			   		 		$arrayName[$j]=array(
				    						'image'=>$images[$i]['image'] ,
				    						'thumb_image' =>$images[$i]['thumb_image'],
				    						'new_thumb_image' =>$images[$i]['new_thumb_image']
			    					    );
		   		 		}
		   		 		else
		   		 		{
		   		 			$arrayName[$i]=array(
				    						'image'=>$images[$i]['image'] ,
				    						'thumb_image' =>$images[$i]['thumb_image'],
				    						'new_thumb_image' =>$images[$i]['new_thumb_image']
			    					    );
		   		 		}
	   		 		}
	   		 	}
	   		 	if(!empty($arrayName)){
	   		 		$this->session->set_userdata('product_image',$arrayName);
	   		 	}
		   	}

		   	$product_image = $this->session->userdata('product_image');

		   	if(!empty($product_image)){
			   	$images = $this->session->userdata('product_image');
	    		$count  = count($images);
	    		for($i=0; $i < $count ; $i++){
	                $resop.= '<tr>';
					$resop.='<td>';
					$resop.='<a href="'.base_url($images[$i]['thumb_image']).'" class="fancybox-button" data-rel="fancybox-button"><img class="img-responsive" src="'.base_url($images[$i]['thumb_image']).'" alt=""></a>' ;
					$resop.='</td>';
					$resop.='<td>';
					$img=explode('/', $images[$i]['image']);
					$image_name = str_replace('.jpg','',$img[4]);
					$resop.='<div> <input class="form-control" type="text" name="photo_'.$i.'" value="'.$image_name.'" >';
					$resop.='</div>';
					$resop.='</td>';
					$resop.='<td> <div style="margin-left:20px;"  class="radio-list">';
	                $resop.='<label class="radio-inline">';
	                 $resop.='<input type="checkbox" name="is_feature_'.$i.'"'; 
                
                if($i==0){$resop.='checked="checked"';}
                $resop.='class="is_feature" id="is_feature_'.$i.'" value="1" > YES </label>';
	                // $resop.='<input type="radio" name="is_feature_'.$i.'" id="is_feature" value="1" > YES </label>';
	                // $resop.='<label class="radio-inline">';
	               	// $resop.='<input type="radio" name="is_feature_'.$i.'" id="is_feature" value="0" checked> NO</label>';
	                $resop.='</div>';
					$resop.='</td>';				
					$resop.='<td>';
					$resop.='<a href="javascript:void(0)" onclick="remove_image('.$i.');"  class="btn default btn-sm">';
					$resop.='<i class="fa fa-times"></i> Remove </a>';
					$resop.='</td>';
					$resop.='</tr>';
	    		}
	    		echo $resop;
    		}
    		else{
    			echo $resop='<tr><td class="text-center" colspan="4">NO data Found</td></tr>';
    		}


		}
	}

	public function delete_image()
	{
		$resop='';
		if($_POST){
			$id = $_POST['image_id'];
			

			$product_image = $this->product_model->get_row('products_photos',array('id'=>$id));
			$this->product_model->delete('products_photos',array('id'=>$id));    
	 		$image_url1='assets/uploads/products/';
	 		$new_file=$product_image->path;
			$thumb_file=$product_image->thumbnail_path;
			$new_thumb_image=$product_image->new_thumb_image;
			if(!empty($new_file)) @unlink($new_file);
			if(!empty($new_thumb_image)) @unlink($new_thumb_image);
			
			$photos = $this->product_model->get_result('products_photos',array('products_id'=>$product_image->products_id));			
		   	//$product_image = $this->session->userdata('product_image');

		   	if(!empty($photos)){
			   	
	    	$i=0;	foreach($photos as $row){
	               	$resop.='<tr>';
			        $resop.='<td>';                           
			        $resop.='<a href="'.base_url($row->thumbnail_path).'" class="fancybox-button" data-rel="fancybox-button"><img class="img-responsive" src="'.base_url($row->thumbnail_path).'" alt=""></a>';
			      	$resop.='</td>';
			        $resop.='<td>';
			        $resop.='<div><input class="form-control" type="text" name="label_'.$row->id.'_'.$i.'" value="'.$row->label.'">';                  
			        $resop.='</div>';
			        $resop.='</td>'; 
			        $resop.='<td>';
			        $resop.='<div class="radio-list">';
			        $resop.='<label class="checkbox-inline">';
			        $resop.='<input type="checkbox" name="is_feature_'.$row->id.'_'.$i.'" class="is_feature"';  
			        if($row->random_background==1){ $resop.='checked="checked"';} 
			        $resop.='id="is_feature_'.$row->id.'_'.$i.'" value="1" > YES </label>';
			        $resop.='</div>';
			        $resop.='</td>';         
			        $resop.='<td>';
			        $resop.='<a href="javascript:void(0)" onclick="delete_image('.$row->id.');" class="btn default btn-sm">';
					$resop.='<i class="fa fa-times"></i> Remove </a>';
			        $resop.='</td>';
					$resop.='</tr>';
	    	$i++;	}
	    		echo $resop;
    		}
    		else{
    			echo $resop='<tr><td class="text-center" colspan="4">NO data Found</td></tr>';
    		}


		}
	}


	public function import($type='csv'){
		_check_superadmin_login(); //check login authentication
		// if(empty($class_id)) redirect('professor/classes'); 
       	// $data['error']='';
       	if($_FILES){

      	if(!empty($_FILES['import_csv']['name'])){
            //Сheck that we have a file
            if((!empty($_FILES["import_csv"])) && ($_FILES['import_csv']['error'] == 0)) {
	            //Check if the file is JPEG image and it's size is less than 350Kb
	            $filename = basename($_FILES['import_csv']['name']);
	            $ext = substr($filename, strrpos($filename, '.') + 1);
	            if(($ext == "csv") &&  ($_FILES["import_csv"]["size"] < 350000)) {
                  //Determine the path to which we want to save this file
                  $newname = './assets/uploads/product_csv/'.$filename;
                  //Check if the file with the same name is already exists on the server
                  if (!file_exists($newname)) {
                    //Attempt to move the uploaded file to it's new place
	                    if((move_uploaded_file($_FILES['import_csv']['tmp_name'],$newname))) {
	                        //echo $data['error'] = "It's done! The file has been saved as: ".$newname; die;
	                        if($this->read_csv_xls_xlsx(array('file'=> $filename,'path'=>'./assets/uploads/product_csv/'))){                          
	                          $filename='./assets/uploads/product_csv/'.$filename;
	                          unlink($filename);
	                       	 	//$this->session->set_flashdata('msg_success',"Customers imported successfully.");
	                          redirect('backend/products/');
	                        }
	                    } else {

	                        $this->session->set_flashdata('msg_error','Error: A problem occurred during file upload!');
					  		redirect('backend/products/');
	                    }
                   } else {
	                    $this->session->set_flashdata('msg_error_csv','Error: A problem occurred during file upload! File is already exists');
						//redirect('backend/products/import/');
                   }
              } else {
                 
                $this->session->set_flashdata('msg_error','Error: Only .jpg images under 350Kb are accepted for upload');
				//redirect('backend/products/import/');
              }
            } else {
                $this->session->set_flashdata('msg_error','Error: No file Selected');
				//redirect('backend/products/import/');
            }
      
	     } else {

	        	$this->session->set_flashdata('msg_error_csv','Error: No file Selected');
				//redirect('backend/products/import/');
	        }
       }

        //redirect('backend/customers/add/');
        $data['template']='backend/product/import';
		$this->load->view('templates/backend/layout',$data);	
    }

    private function read_csv_xls_xlsx($param=array()){
		if(!isset($param['file']) && empty($param['file'])){
			$this->session->set_flashdata('msg_error','File Name can\'t be blank, Please try again.');
			return FALSE;
		}

		if(!isset($param['path']) && empty($param['path'])){
			$this->session->set_flashdata('msg_error','File Path can\'t be blank, Please try again.');
			return FALSE;
		}
		// if(!isset($param['password']) && empty($param['password'])){
		// 	$this->session->set_flashdata('msg_error','Defalut Password is not set properly.');
		// 	return FALSE;
		// }

		$filename = $param['path'].$param['file'];
		//$class_id = $param['class'];

		if(file_exists($filename)){
			require(APPPATH.'libraries/spreadsheet-reader/php-excel-reader/excel_reader2.php');
		   	require(APPPATH.'libraries/spreadsheet-reader/SpreadsheetReader.php');

		    $Reader = new SpreadsheetReader($filename);
		    $l=0; $u=0;$i=0; $R=0;
		    $customer_exist_data=array();
		    //$password = $param['password'];
			foreach($Reader as $row):

				//$customer = $this->product_model->get_row('users',array('email'=>trim($row[4])));

				// if(!empty($customer)){
				// 	$arrayName = array('first_name'=>$row[2],'last_name'=>$row[3],'email'=>$row[4]);
				// 	$customer_exist_data[$R]=$arrayName;
				// 	$R++;
				// } else { 	
				   	if((!empty($row[1])) && $l>0):
					    $item_slug= trim($row[0]);

		                      //$pro_data[''] = 1; 
		                    if(isset($row[1]))
		                   	  $pro_data['name']= trim($row[1]);  
		                	if(isset($row[2]))
		                      $pro_data['description']= trim($row[2]);  
		                	if(isset($row[3]))
		                      $pro_data['short_description'] = trim($row[3]);
		                	if(isset($row[4]))
		                      $pro_data['SKU'] = trim($row[4]);
		                	if(isset($row[5]))
		                      $pro_data['status'] = $row[5];
		                	if(isset($row[6]))
		                      $pro_data['avilable_item'] = trim($row[6]);
		                    if(isset($row[7]))
		                      $pro_data['featured_product'] = trim($row[7]);
		                    if(isset($row[8]))
		                      $pro_data['discount_item'] = trim($row[8]);
		                    if(isset($row[9]))
		                      $pro_data['price_type'] = trim($row[9]);
		                    if(isset($row[10]))
		                      $pro_data['price'] = trim($row[10]);
		                    if(isset($row[11]))
		                      $pro_data['meta_title'] = url_title(trim($row[11]), '-', TRUE); 
		                    if(isset($row[12]))
		                      $pro_data['meta_keywords'] = url_title(trim($row[12]), '-', TRUE);        
		           			if(isset($row[13]))
		                      $pro_data['meta_description'] = url_title(trim($row[13]), '-', TRUE);
		                    if(isset($row[27])){
		                    	$array = array();
		                        $category = explode(',',$row[27]);
		           			    for($i=0; $i<count($category); $i++){ 
		           			   	   	$category = url_title(trim($category[$i]),'-', TRUE) ;
		           			   	   	$id = get_category_id($category);
		           			   	   	$array[$i]=$id;   
		           			   	}	            	
		           			   	$category = "#".implode('#,#',$array)."#";
		                    }   
		           
							$pro_data['created'] = date('Y-m-d h:i:s');
							$pro_data['slug']    = url_title(trim($row[1]),'-',TRUE);
							$pro_data['category']= $category;  
							
							$pro_id = $this->product_model->insert('products',$pro_data);	        
					        $ship_pro_data['product_id']=$pro_id;

						    if(isset($row[14]))
					           $ship_pro_data['content_use_only'] = trim($row[14]);
					        if(isset($row[15]))
					           $ship_pro_data['content_order'] = trim($row[15]);
					        if(isset($row[16]))
					           $ship_pro_data['slide_use_only']= trim($row[16]);
					        if(isset($row[17]))
					           $ship_pro_data['slide_order']= trim($row[17]); 
							if(isset($row[18]))
		                   	   $ship_pro_data['science_description'] = htmlspecialchars($row[18]);  
		                	if(isset($row[19]))
		                       $ship_pro_data['clinical_description']   = htmlspecialchars($row[19]);  
		                	if(isset($row[20]))
		                       $ship_pro_data['use_description'] = htmlspecialchars($row[20]);
		                	if(isset($row[21]))
		                       $ship_pro_data['use_description_right'] = htmlspecialchars($row[21]);
		                	if(isset($row[22]))
		                       $ship_pro_data['related_description'] = htmlspecialchars($row[22]);
		                	if(isset($row[23]))
		                       $ship_data['weight'] = trim($row[23]);
		                    if(isset($row[24]))
		                       $ship_data['free_shipping'] = trim($row[24]);
		                    if(isset($row[25]))
		                       $ship_data['exempt_shipping_charge'] = trim($row[25]);
		      
		                 
		                	$this->product_model->insert('products_related_info',$ship_pro_data);
		                	$ship_data['product_id']=$pro_id;
		                	$this->product_model->insert('product_shpping_info',$ship_data);
		                			
		                	if(trim($row[9])==2){
	                			if(!empty($row[26])){
	                				$test= explode(',',$row[26]);
	                				for($i=0; $i<count($test); $i++) { 
	                					$str = explode('-',$test[$i]);
	                					$price = $str[0];
	                					$label = $str[1];
	                					$array=array('product_id'=>$pro_id,'price_for'=>$price,'price_label'=>$label);
	                					$this->product_model->insert('multiple_price',$array);
	                				}
	                			}	
		                	}
				           
					        //$i++;
		               // }
				    endif;
			//}
			    $l++;
			endforeach;
		if(!$this->session->userdata('existing_customers')){
			$this->session->set_userdata('existing_customers',$customer_exist_data);
		}	
		$this->session->set_flashdata('msg_success',"Customers imported successfully.");
        return TRUE;

    }else{
	    $this->session->set_flashdata('msg_error','Students does not exist, Please try again.');
	    return FALSE;
    }
			
	}

	public function download()
    {
    	$this->load->helper('download');
    	$data = file_get_contents(base_url()."assets/uploads/products.csv"); // Read the file's contents
		$name = 'products.csv';
		if(force_download($name, $data)){
			redirect('backend/products/');
		} 
    }

	public function export($type='csv'){
			_check_superadmin_login(); //check login authentication
			// if(empty($class_id)) redirect('professor/classes');
			$products=$this->product_model->get_products_info();	
			// print_r($products);
			// die();
			if($products==FALSE){
				$this->session->set_flashdata('msg_info',"No Customers Available.");
				redirect('backend/products/');
			}
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=products_list_".date('Y-m-d').".csv");
			header("Pragma: no-cache");
			header("Expires: 0");
			$file = fopen('php://output', 'w');
			        fputcsv($file, array(
			            '#',
						'Product Name',
						'Short Description',
						'Description',
						'SKU',
						'Featured Product',
						'Available Item',
						'Discount Item',
						'Meta Title',
						'Meta Keywords',
						'Meta Description',
						'status',
						'Price Type',
						'Price',
						'Multiple Price',
						'weight',					
						'Related Description',
						'Content Use Only',
						'Content Order',
						'Science Description',
						'Clinical Description',
						'Use Description Left',
						'Use Description Right',
						'Slide Use Only',
						'Slide Order',
						'Free Shipping',
						'Exempt Shipping',
						'created',
			      ));

		      $p=0;foreach ($products as $row) { $p++;
		      
		      	$multiple_price=''; $fs=''; $esc='';$fp="";$ai=''; $di='';
		      	if($row->price_type==2){
		      	    $prices = $this->product_model->get_result('multiple_price',array('product_id'=>$row->id),array('price_label','price_for'));
		      		 if(!empty($prices)){
			      		 $test=array();
			      		$i=0;
			      		foreach($prices as $value){
			      			$test[$i]=$value->price_for.'-'.$value->price_label;
			      			$i++;
			      		}
			      		$multiple_price = implode(',',$test);
		      		}
			      	$type='multiple';
		      	} else {
		      		$type='single';
		      	}


		      	if($row->free_shipping==1){
				   $fs='yes';
				} else {
				   $fs='No';
				}
				if($row->exempt_shipping_charge==1){
				   $esc='yes';
				} else {
				   $esc='No';
				}
				if($row->featured_product==1){
				   $fp='yes';
				} else {
				   $fp='No';
				}

				if($row->avilable_item==1){
				   $ai='yes';
				} else {
				   $ai='No';
				}

				if($row->discount_item==1){
				    $di='yes';
				} else {
					$di='No';
				}
				

		    $rows=array(
			    $p,
			    trim($row->name),
				trim($row->short_description),
				trim($row->description),
				trim($row->SKU),
				trim($fp),
				trim($ai),
				trim($di),
				trim($row->meta_title),
				trim($row->meta_keywords),
				trim($row->meta_description),
				trim($row->status),
				trim($type),
				trim($row->price),
				$multiple_price,
				trim($row->weight),
				trim($row->related_description),
				trim($row->content_use_only),
				trim($row->content_order),
				trim($row->science_description),
				trim($row->clinical_description),
				trim($row->use_description),
				trim($row->use_description_right),
				trim($row->slide_use_only),
	            trim($row->slide_order),
	            $fs,
	            $esc,
	            date('d-m-Y',strtotime($row->created)),
			);
			
	      fputcsv($file, $rows);
	      }
	      exit();
	}

	public function product_page_setting()
	{
		_check_superadmin_login(); //check login authentication
		$data['page_set'] = $this->product_model->get_row('home_setting',array('id'=>1));
		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('subheading','Sub-Heading','trim');
		$this->form_validation->set_rules('description','Description', 'trim|required');
        $this->form_validation->set_rules('userfile','Side Image','callback_side_image');
        $this->form_validation->set_error_delimiters('<div class="error">','</div>');
        if($this->form_validation->run() == TRUE){	
        	$arrayHome = array(
	        					'title' => $this->input->post('title'), 
	        					'subheading' => $this->input->post('subheading'), 
	        					'description' => $this->input->post('description'),
	        					'modified'=>date('Y-m-d h:i:s')
        				      );
            if($this->session->userdata('userfile')):
                $userfile=$this->session->userdata('userfile');   
 		        $arrayHome['file_path'] = $userfile['image'];  
  		        $arrayHome['file_thumb_path'] = $userfile['thumb_image'];        
  		        if(!empty($data['page_set']->file_path))
					$new_file=$data['page_set']->file_path;
				if(!empty($data['page_set']->file_thumb_path))
					$new_thumb_image=$data['page_set']->file_thumb_path;
				@unlink($new_file);	
				@unlink($new_thumb_image);   
  		    endif;

    		if($this->product_model->update('home_setting',$arrayHome,array('id'=>1))){
    			if($this->session->userdata('userfile')){       
    					$this->session->unset_userdata('userfile');
    			}	
				$this->session->set_flashdata('msg_success','Product Page setting has been Updated successfully.');
				redirect('backend/products/product_page_setting/');
			}
        }        	
	 	$data['template']='backend/product/add';
		$this->load->view('templates/backend/layout',$data);
	}


	public function side_image($str){     
        if(!empty($_FILES['userfile']['name'])){
            if($this->session->userdata('userfile')){               
                return TRUE;
            }else{

                $param=array(
                    'file_name' =>'userfile',
                    'upload_path'  => './assets/uploads/products/',
                    'allowed_types'=> 'gif|jpg|png|jpeg',
                    'image_resize' => TRUE,
                    'source_image' => './assets/uploads/products/',
                    'new_image'    => './assets/uploads/products/thumb/',
                    'resize_width' => 300,
                    'resize_height' => 300,
                    'encrypt_name' => TRUE,
                );
 
                $upload_file=upload_file($param);
				//thumbnail create

                if($upload_file['STATUS']){
                    $this->session->set_userdata('userfile',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));            
                    return TRUE;        
                }else{          
                    $this->form_validation->set_message('side_image', $upload_file['FILE_ERROR']);              
                    return FALSE;
                }
            }
        }else{
        	$pro_set = $this->product_model->get_row('home_setting',array('id'=>1));
		 	if(empty($pro_set->file_path)){
		 		$this->form_validation->set_message('side_image','Please select file first..');              
	            return FALSE;
		 	}     
        }
    }
	

          



}	