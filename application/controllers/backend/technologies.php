<?php



if(!defined('BASEPATH')) exit('No direct script access allowed');

class Technologies extends CI_Controller

{

	public function __construct()
    {
        parent::__construct();
        clear_cache();
        $this->load->model('technology_model');
    }



	public function index($offset=0){
		_check_superadmin_login(); //check login authentication
		
		$per_page=25;

		$data['technologies'] = $this->technology_model->technologies($offset,$per_page);

		$data['offset'] = $offset;

 		$config=backend_pagination();

		$config['base_url'] = base_url().'backend/technologies/index';

		$config['total_rows'] = $this->technology_model->technologies(0,0);
		
		$config['per_page'] = $per_page;
		
		$this->pagination->initialize($config);
		
		$data['pagination']=$this->pagination->create_links();
 		
 		$data['template']='backend/technology/index';
		$this->load->view('templates/backend/layout', $data);
	}



	public function add(){

		_check_superadmin_login(); //check login authentication

		$this->form_validation->set_rules('technology', 'Technology Name', 'trim|required');

		$this->form_validation->set_rules('description', 'Category Description', 'trim|required');


		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		if($this->form_validation->run() == TRUE){

			$tech_data = array(

								'technology'	  => $this->input->post('technology'),

								'description' => $this->input->post('description'),

								'slug'		  => url_title($this->input->post('category_name'),'-',TRUE),

								'status'	  => $this->input->post('status'),

								'created_date'=> date('Y-m-d h:i:s')

							);

			if($this->technology_model->insert('technologies',$tech_data)){

				if($this->session->userdata('userfile')):

                    $this->session->unset_userdata('userfile');

                endif;

				$this->session->set_flashdata('msg_success','New Category has been created successfully.');

				redirect('backend/technologies/');

			}

		}

		$data['template']='backend/technology/add';

		$this->load->view('templates/backend/layout', $data);

	}



	// public function userfile_check_add($str){           

 //        if($this->session->userdata('userfile')):

 //            $this->session->unset_userdata('userfile');

 //        endif;  

 //        if($this->session->userdata('userfile')){               

 //            return TRUE;

 //        }else{

 //            $param=array(

 //                'file_name' =>'userfile',

 //                'upload_path'  => './assets/uploads/technologies/',

 //                'allowed_types'=> 'gif|jpg|png|jpeg',

 //                // 'image_resize' => TRUE,

 //                'source_image' => './assets/uploads/technologies/',

 //                'new_image'    => './assets/uploads/technologies/thumb/',

 //                // 'resize_width' => 200,

 //                // 'resize_height' => 200,

 //                'encrypt_name' => TRUE,

 //            );



 //            $filepath='./assets/uploads/technologies/';

 //            $upload_file=upload_file($param);		

 //            if($upload_file['STATUS']){

 //        		$configs = array();

 //                //$configs[] = array('source_image' => $upload_file['UPLOAD_DATA']['file_name'], 'new_image' => 'medium/'.$upload_file['UPLOAD_DATA']['file_name'], 'width' => 325, 'height' => 325,'maintain_ratio' =>TRUE);

 //                $configs[] = array('source_image' => $upload_file['UPLOAD_DATA']['file_name'], 'new_image' => 'thumb/'.$upload_file['UPLOAD_DATA']['file_name'], 'width' => 300, 'height' => 300,'maintain_ratio' => TRUE);

 //                $this->load->library('image_lib');

 //                foreach ($configs as $config) {

 //                    $this->image_lib->thumb($config,$filepath);

 //                }

 //                $this->session->set_userdata('userfile',array('image'=>$param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],'thumb_image'=>$param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));            

 //                return TRUE;        

 //            }else{          

 //                $this->form_validation->set_message('userfile_check_add', $upload_file['FILE_ERROR']);              

 //                return FALSE;

 //            } 

 //        }     

 //    }

	public function edit($tech_id='')	{



		_check_superadmin_login(); //check login authentication

		if(empty($tech_id)) redirect(base_url().'backend/technologies');

	   	$data['technology'] = $this->technology_model->get_row('technologies',array('id'=>$tech_id));

		$this->form_validation->set_rules('technology', 'Technology Name', 'trim|required');

		
		$this->form_validation->set_rules('description', 'Technology Description', 'trim|required');

		

		$this->form_validation->set_error_delimiters('<div class="error">','</div>');

		if($this->form_validation->run() == TRUE){

		    $tech_data=array(

							'technology'	  => $this->input->post('technology'),
	
							'description' => $this->input->post('description'),

							'slug'		  => url_title($this->input->post('technology'), '-', TRUE),

							'status'	  => $this->input->post('status'),

							'created_date'=> date('Y-m-d h:i:s')

						);

			
			if($this->technology_model->update('technologies',$tech_data,array('id'=>$tech_id))){

				

				$this->session->set_flashdata('msg_success','Category has been Updated successfully.');

				redirect('backend/technologies/');

			}

		}

		

		$data['template']='backend/technology/edit';

		$this->load->view('templates/backend/layout',$data);	

	}

	public function delete($tech_id ='')	{

		_check_superadmin_login(); //check login authentication

		if(empty($tech_id)) redirect('backend/technologies');

		$category=$this->technology_model->get_row('technologies',array('id'=>$tech_id));

		if(!empty($category)){

    		$new_file=$category->main_image;

			$thumb_file=$category->thumb_image;

			@unlink($new_file);

			@unlink($thumb_file);

        }    

		$this->technology_model->delete('technologies',array('id'=>$tech_id));

		$this->session->set_flashdata('msg_success','Category has been deleted successfully.');

		redirect('backend/technologies');		

	}





}	