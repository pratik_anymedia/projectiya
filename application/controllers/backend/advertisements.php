<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisements extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('advertise_model');
    }

    public function index($offset = 0) {
        _check_superadmin_login(); //check login authentication
        $per_page = 10;
        $data['advertises'] = $this->advertise_model->advertisements($offset, $per_page);
        $data['offset'] = $offset;
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'advertisements/index/';
        $config['total_rows'] = $this->advertise_model->advertisements(0, 0);
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'backend/advertise/index';
        $this->load->view('templates/backend/layout', $data);
    }

    public function add() {
        _check_superadmin_login(); //check login authentication
        //$this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('advertise_image', '', 'callback_advertise_image_check');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $ad_data = array(
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'url' => $this->input->post('url'),
                'slug' => url_title($this->input->Post('title'), '-', TRUE),
                'status' => $this->input->post('status'),
                'role' => $this->input->post('popup_image'),
                'created' => date('Y-m-d h:i:s')
            );
            if ($this->session->userdata('advertise_image')):
                $advertise_image = $this->session->userdata('advertise_image');
                $ad_data['file_path'] = $advertise_image['image'];
                $ad_data['thumb_path'] = $advertise_image['thumb_image'];
            endif;


            if ($this->advertise_model->insert('advertisements', $ad_data)) {
                if ($this->session->userdata('advertise_image')):
                    $this->session->unset_userdata('advertise_image');
                endif;
                $this->session->set_flashdata('msg_success', 'Advertisement added successfully.');
                redirect('backend/advertisements/');
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/advertisements/add');
            }
        }

        //$data['category'] = $this->advertise_model->get_result('faq_category');
        $data['template'] = 'backend/advertise/add';
        $this->load->view('templates/backend/layout', $data);
    }

    public function edit($ad_id = '') {
        _check_superadmin_login(); //check login authentication
        if (empty($ad_id))
            redirect(base_url() . 'backend/advertisements/index');
        $data['advertise'] = $this->advertise_model->get_row('advertisements', array('id' => $ad_id));
        if (empty($data['advertise']))
            redirect(base_url() . 'backend/advertisements/index');

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        if (!empty($_FILES['advertise_image']['name'])):
            $this->form_validation->set_rules('advertise_image', 'Advertise image', 'callback_advertise_image_check');
        endif;
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $ad_data = array(
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'url' => $this->input->post('url'),
                'slug' => url_title($this->input->Post('title'), '-', TRUE),
                'role' => $this->input->post('popup_image'),
                'status' => $this->input->post('status'),
                'created' => date('Y-m-d h:i:s')
            );
            if ($this->session->userdata('advertise_image')):
                $advertise_image = $this->session->userdata('advertise_image');
                $ad_data['file_path'] = $advertise_image['image'];
                $ad_data['thumb_path'] = $advertise_image['thumb_image'];
            endif;
            if ($this->advertise_model->update('advertisements', $ad_data, array('id' => $ad_id))) {
                if ($this->session->userdata('advertise_image')):
                    $this->session->unset_userdata('advertise_image');
                    @unlink($data['advertise']->file_path);
                    @unlink($data['advertise']->thumb_path);
                endif;
                $this->session->set_flashdata('msg_success', 'Advertisement updated successfully.');
                redirect('backend/advertisements/index');
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/advertisements/edit/' . $ad_id);
            }
        }
        $data['template'] = 'backend/advertise/edit';
        $this->load->view('templates/backend/layout', $data);
    }

    public function advertise_image_check($str) {
        if ($this->session->userdata('advertise_image')) {
            return TRUE;
        } else {
            $param = array(
                'file_name' => 'advertise_image',
                'upload_path' => './assets/uploads/advertise/',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'image_resize' => TRUE,
                'source_image' => './assets/uploads/advertise/',
                'new_image' => './assets/uploads/advertise/thumb/',
                'resize_width' => 320,
                'resize_height' => 300,
                'encrypt_name' => TRUE);

            $upload_file = upload_file($param);
            if ($upload_file['STATUS']) {
                // $filepath='./assets/uploads/blog/';
                // $configs = array();
                // //$configs[] = array('source_image' => $upload_file['UPLOAD_DATA']['file_name'], 'new_image' => 'medium/'.$upload_file['UPLOAD_DATA']['file_name'], 'width' => 325, 'height' => 325,'maintain_ratio' =>TRUE);
                // $configs[] = array('source_image' => $upload_file['UPLOAD_DATA']['file_name'], 'new_image' => 'thumb/'.$upload_file['UPLOAD_DATA']['file_name'], 'width' => 325, 'height' => 225,'maintain_ratio' => TRUE);
                // $this->load->library('image_lib');
                // foreach ($configs as $config) {
                //     $this->image_lib->thumb($config,$filepath);
                // }      
                $this->session->set_userdata('advertise_image', array('image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'], 'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']));
                return TRUE;
            } else {
                $this->form_validation->set_message('advertise_image_check', $upload_file['FILE_ERROR']);
                return FALSE;
            }
        }
    }

    public function delete($ad_id = '') {
        _check_superadmin_login(); //check login authentication
        if (empty($ad_id))
            redirect(base_url() . 'backend/advertisements/index');
        if ($this->advertise_model->delete('advertisements', array('id' => $ad_id))) {
            $this->session->set_flashdata('msg_success', 'Advertisement deleted successfully.');
            redirect('backend/advertisements/index');
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/advertisements/index');
        }
    }

    public function changestatus($id = "", $status = "", $offset = "") {

        if (!empty($id)) {
            $this->advertise_model->changestatus($id, $status, $offset, "faqs");
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/faqs/index');
        }
    }

}
