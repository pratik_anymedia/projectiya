<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Slider_images extends CI_Controller {

    public function __construct() {

        parent::__construct();

        clear_cache();

        $this->load->model('slider_images_modal');
    }

    public function index($offset = 0) {
        _check_superadmin_login(); //check login authentication
        $per_page = 10;
        $data['slider_images'] = $this->slider_images_modal->slider_images($offset, $per_page);
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'backend/slider_images/index/';
        $config['total_rows'] = $this->slider_images_modal->slider_images(0, 0);
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = "backend/slider_images/index";
        $this->load->view('templates/backend/layout', $data);
    }

    public function add() {
        _check_superadmin_login(); //check login authentication

        $this->form_validation->set_rules('slider_image', 'Slider Image', 'callback_slider_image_add');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        $this->form_validation->set_rules('slider_image_desc', 'Description', 'trim');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == TRUE) {
            $image_data = array();
            if ($this->session->userdata('slider_image')):
                $userfile = $this->session->userdata('slider_image');
                $image_data['path'] = $userfile['image'];
                $image_data['image_type'] = 2;
                $image_data['status'] = $this->input->post('status');
                $image_data['description'] = $this->input->post('slider_image_desc');
            endif;

            if ($this->session->userdata('slider_image')):
                $this->session->unset_userdata('slider_image');
            endif;

            if ($image_id = $this->slider_images_modal->insert('image_setting', $image_data)) {
                $this->session->set_flashdata('msg_success', 'Image added successfully.');
                redirect('backend/slider_images');
            }
        }

        $data['template'] = "backend/slider_images/add";

        $this->load->view('templates/backend/layout', $data);
    }

    /**
     * 
     * @param type $slider_image_id
     */
    public function edit($slider_image_id = '') {
        _check_superadmin_login(); //check login authentication
        if (empty($slider_image_id)) {
            redirect('backend/slider_images');
        }

        $data['slider_image'] = $this->slider_images_modal->get_row('image_setting', array('id' => $slider_image_id, 'image_type' => 2));

        $this->form_validation->set_rules('status', 'Status', 'trim|required');

        if (!empty($_FILES['slider_image']['name'])) {
            $this->form_validation->set_rules('slider_image', 'Slider Image', 'callback_slider_image_add');
        }

        $this->form_validation->set_rules('slider_image_desc', 'Description', 'trim');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');


        if ($this->form_validation->run() == TRUE) {

            $image_data = array();
            if ($this->session->userdata('slider_image')):
                $userfile = $this->session->userdata('slider_image');
                $image_data['path'] = $userfile['image'];

                if (!empty($data['slider_image'])) {
                    $new_file = $data['slider_image']->path;
                    @unlink($new_file);
                }
            endif;

            $image_data['image_type'] = 2;
            $image_data['status'] = $this->input->post('status');
            $image_data['description'] = $this->input->post('slider_image_desc');

            if ($this->session->userdata('slider_image')):
                $this->session->unset_userdata('slider_image');
            endif;


            if (!empty($image_data) && $this->slider_images_modal->update('image_setting', $image_data, array('id' => $slider_image_id))) {
                $this->session->set_flashdata('msg_success', 'Slider Image updated successfully.');
                redirect('backend/slider_images');
            } else {
                $this->session->set_flashdata('msg_error', 'Slider Image update failed, Please try again.');
                redirect('backend/slider_images');
            }
        }

//         echo validation_errors();
//        die('dfdfds');
        $data['template'] = "backend/slider_images/edit";
        $this->load->view('templates/backend/layout', $data);
    }

    public function view($template_id = '') {

        _check_superadmin_login(); //check login authentication

        if (empty($template_id))
            redirect('backend/email_templates');



        $data['email_template'] = $this->email_templates_model->get_row('email_templates', array('id' => $template_id));



        $data['template'] = "backend/email_templates/view";

        $this->load->view('templates/backend/layout', $data);
    }

    public function delete($slider_image_id = '') {
        _check_superadmin_login(); //check login authentication
        if (empty($slider_image_id))
            redirect('backend/slider_images');

        if ($this->slider_images_modal->delete('image_setting', array('id' => $slider_image_id))) {
            $this->session->set_flashdata('msg_success', 'slider image deleted successfully.');
            redirect('backend/slider_images');
        } else {
            $this->session->set_flashdata('msg_error', 'Delete failed, Please try again.');
            redirect('backend/slider_images');
        }
    }

    public function slider_image_add($str) {

        if ($this->session->userdata('slider_image')) {

            return TRUE;
        } else {
            $param = array(
                'file_name' => 'slider_image',
                'upload_path' => './assets/backend/images/slider_images/',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'image_resize' => TRUE,
                'source_image' => './assets/backend/images/slider_images/',
                'new_image' => './assets/backend/images/slider_images/thumb/',
                'resize_width' => 200,
                'resize_height' => 200,
                'encrypt_name' => TRUE,
            );

            $upload_file = upload_file($param);
//            var_dump($upload_file);
//            die('dfdf');
            if ($upload_file['STATUS']) {
                $this->session->set_userdata('slider_image', array('image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'],
                        //'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']
                ));
                return TRUE;
            } else {
                $this->form_validation->set_message('slider_image_add', $upload_file['FILE_ERROR']);
                return FALSE;
            }
        }
    }

}
