<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Projects extends CI_Controller {

    public function __construct() {
        parent::__construct();
        clear_cache();
        $this->load->model('project_model');
    }

    public function index($sort_by = 'id', $sort_order = 'desc', $offset = 0) {
        _check_superadmin_login(); //check login authentication
        $per_page = 5;
        $data['projects'] = $this->project_model->projects($per_page, $offset, $sort_by, $sort_order);
        $data['offset'] = $offset;
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'backend/projects/index/' . $sort_by . '/' . $sort_order . '/';
        $config['total_rows'] = $this->project_model->projects(0, 0, $sort_by, $sort_order);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 6;
        if (!empty($_SERVER['QUERY_STRING'])) {
            $config['suffix'] = "?" . $_SERVER['QUERY_STRING'];
        }
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        $data['technology'] = $this->project_model->get_result('technologies', array('status' => 1));
        $data['template'] = 'backend/project/index';
        $this->load->view('templates/backend/layout', $data);
    }
    
    function requests($sort_by = 'prid', $sort_order = 'desc', $offset = 0){
        _check_superadmin_login(); //check login authentication
        $per_page = 25;
        $data['requests'] = $this->project_model->projectRequests($per_page, $offset, $sort_by, $sort_order);
        $data['offset'] = $offset;
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'backend/projects/requests/' . $sort_by . '/' . $sort_order . '/';
        $config['total_rows'] = $this->project_model->projectRequests(0, 0, $sort_by, $sort_order);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 6;
        if (!empty($_SERVER['QUERY_STRING'])) {
            $config['suffix'] = "?" . $_SERVER['QUERY_STRING'];
        }
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        $data['template'] = 'backend/project/requests';
        $this->load->view('templates/backend/layout', $data);
    }
    
    public function add() {
        _check_superadmin_login(); //check login authentication		
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('short_description', 'Short Description', 'trim|required');
        $this->form_validation->set_rules('price', 'Price', 'trim|required|numeric');
        $this->form_validation->set_rules('form_pages', 'Form Pages', 'trim|required|numeric');
        $this->form_validation->set_rules('no_of_tables', 'No Of Tables', 'trim|required|numeric');
        $this->form_validation->set_rules('backend_technology', 'Backend Technology', 'trim|required');
        $this->form_validation->set_rules('technology[]', 'technology', 'trim|required');
        $this->form_validation->set_rules('course[]', 'Course', 'trim|required');
        $this->form_validation->set_rules('userfile', 'userfile', 'callback_userfile_check_add');
        $this->form_validation->set_rules('featured_image', 'Freatured Image', 'callback_featured_file_add');
        $this->form_validation->set_rules('doc_file', 'Document File', 'callback_docfile_check_add');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {

            $technology = "#" . implode('#,#', $this->input->post('technology')) . "#";
            $course = "#" . implode('#,#', $this->input->post('course')) . "#";

            $project = array(
                'user_id' => 0,
                'title' => $this->input->post('title'),
                'slug' => url_title($this->input->post('title'), '-', TRUE),
                'technology' => $technology,
                'course_type' => $course,
                'short_description' => $this->input->post('short_description'),
                'description' => htmlspecialchars($this->input->post('description')),
                'price' => $this->input->post('price'),
                'available_item' => $this->input->post('available_item'),
                'video_url' => $this->input->post('video_url'),
                'backend_technology' => $this->input->post('backend_technology'),
                'form_pages' => $this->input->post('form_pages'),
                'no_of_tables' => $this->input->post('no_of_tables'),
                //'discount_item'=>$this->input->post('discount_item'),
                'featured_project' => $this->input->post('featured_item'),
                'status' => $this->input->post('status'),
                'created' => date('Y-m-d h:i:s')
            );


            if ($this->session->userdata('userfile')):
                $userfile = $this->session->userdata('userfile');
                $project['main_file'] = $userfile['image'];
            endif;
            if ($this->session->userdata('doc_file')):
                $doc_file = $this->session->userdata('doc_file');
                $project['doc_file'] = $doc_file['image'];
            endif;

            if ($this->session->userdata('featured_image')):
                $featured_image = $this->session->userdata('featured_image');
                $project['main_image'] = $featured_image['image'];
                $project['thumb_image'] = $featured_image['thumb_image'];
            endif;


            if ($project_id = $this->project_model->insert('projects', $project)) {
                if ($this->session->userdata('userfile')):
                    $this->session->unset_userdata('userfile');
                endif;
                if ($this->session->userdata('doc_file')):
                    $this->session->unset_userdata('doc_file');
                endif;
                if ($this->session->userdata('featured_image')):
                    $this->session->unset_userdata('featured_image');
                endif;
                $this->session->set_flashdata('msg_success', 'Project added successfully.');
                redirect('backend/projects');
            }
        }

        $data['technology'] = $this->project_model->get_result('technologies', array('status' => 1));
        $data['courses'] = $this->project_model->get_result('courses', array('status' => 1));
        $data['template'] = 'backend/project/pro_add';
        $this->load->view('templates/backend/layout', $data);
    }

    public function userfile_check_add() {
        if ($this->session->userdata('userfile')) {
            return TRUE;
        } else {
            $param = array(
                'file_name' => 'userfile',
                'upload_path' => './assets/uploads/projects/',
                'allowed_types' => 'zip|rar|pdf|doc|docx|ppt|pptx|xml|ppt',
                'source_image' => './assets/uploads/projects/',
                'max_size' => '10000000000000',
                'encrypt_name' => TRUE,
            );
            $this->load->library('upload', $param);
            if (!$this->upload->do_upload('userfile')) {
                $this->form_validation->set_message('userfile_check_add', $this->upload->display_errors());
                return FALSE;
            } else {
                $upload_file = $this->upload->data();
                $this->session->set_userdata('userfile', array('image' => $param['upload_path'] . $upload_file['file_name']));
                return TRUE;
            }
        }
    }

    public function docfile_check_add() {
        if ($this->session->userdata('doc_file')) {
            return TRUE;
        } else {
            $param1 = array(
                'file_name' => 'doc_file',
                'upload_path' => './assets/uploads/projects/',
                'allowed_types' => 'pdf|doc|docx|ppt',
                'source_image' => './assets/uploads/projects/',
                'max_size' => '10000000000000',
                'encrypt_name' => TRUE,
            );
            $this->load->library('upload', $param1);
            if (!$this->upload->do_upload('doc_file')) {
                $this->form_validation->set_message('docfile_check_add', $this->upload->display_errors());
                return FALSE;
            } else {
                $upload_file = $this->upload->data();
                $this->session->set_userdata('doc_file', array('image' => $param1['upload_path'] . $upload_file['file_name']));
                return TRUE;
            }
        }
    }

    public function featured_file_add($str) {

        if ($this->session->userdata('featured_image')) {
            return TRUE;
        } else {
            $param = array(
                'file_name' => 'featured_image',
                'upload_path' => './assets/uploads/project_files/',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'image_resize' => TRUE,
                'source_image' => './assets/uploads/project_files/',
                'new_image' => './assets/uploads/project_files/thumb/',
                'resize_width' => 200,
                'resize_height' => 200,
                'encrypt_name' => TRUE,
            );
            $upload_file = upload_file($param);
            if ($upload_file['STATUS']) {
                $this->session->set_userdata('featured_image', array('image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'], 'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']));
                return TRUE;
            } else {
                $this->form_validation->set_message('featured_file_add', $upload_file['FILE_ERROR']);
                return FALSE;
            }
        }
    }

    public function edit($pro_id = '') {
        _check_superadmin_login(); //check login authentication
        if (empty($pro_id))
            redirect('backend/projects/');
        $data['project'] = $this->project_model->get_row('projects', array('id' => $pro_id));
        
        if (empty($data['project']))
            redirect('backend/projects/');
        $this->form_validation->set_rules('title', 'Project Title', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('short_description', 'Short Description', 'trim|required');
        $this->form_validation->set_rules('form_pages', 'Form Pages', 'trim|required|numeric');
        $this->form_validation->set_rules('no_of_tables', 'No Of Tables', 'trim|required|numeric');
        $this->form_validation->set_rules('backend_technology', 'Backend Technology', 'trim|required');

        $this->form_validation->set_rules('price', 'Price', 'trim|required|numeric');
        $this->form_validation->set_rules('technology[]', 'technology', 'trim|required');
        $this->form_validation->set_rules('course[]', 'Course', 'trim|required');
        if (!empty($_FILES['userfile']['name'])) {
            $this->form_validation->set_rules('userfile', 'image', 'callback_userfile_check_add');
        }
        if (!empty($_FILES['featured_image']['name'])) {
            $this->form_validation->set_rules('featured_image', 'Freatured Image', 'callback_featured_file_add');
        }
        if (!empty($_FILES['doc_file']['name'])) {
            $this->form_validation->set_rules('doc_file', 'Document File', 'callback_docfile_check_add');
        }
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $technology = "#" . implode('#,#', $this->input->post('technology')) . "#";
            $course = "#" . implode('#,#', $this->input->post('course')) . "#";
            $project = array(
                'title' => $this->input->post('title'),
                'slug' => url_title($this->input->post('title'), '-', TRUE),
                'technology' => $technology,
                'course_type' => $course,
                'description' => htmlspecialchars($this->input->post('description')),
                'short_description' => $this->input->post('short_description'),
                'price' => $this->input->post('price'),
                'available_item' => $this->input->post('available_item'),
                'video_url' => $this->input->post('video_url'),
                'backend_technology' => $this->input->post('backend_technology'),
                'form_pages' => $this->input->post('form_pages'),
                'no_of_tables' => $this->input->post('no_of_tables'),
                // 'discount_item'=>$this->input->post('discount_item'),
                'featured_project' => $this->input->post('featured_item'),
                'status' => $this->input->post('status'),
                'modified' => date('Y-m-d h:i:s')
            );

            if ($this->session->userdata('userfile')):
                $userfile = $this->session->userdata('userfile');
                $project['main_file'] = $userfile['image'];
                if (!empty($data['project'])) {
                    $new_file = $data['project']->main_file;
                    @unlink($new_file);
                }
            endif;

            if ($this->session->userdata('doc_file')):
                $doc_file = $this->session->userdata('doc_file');
                $project['doc_file'] = $doc_file['image'];
                if (!empty($data['project'])) {
                    $new_file = $data['project']->doc_file;
                    @unlink($new_file);
                }
            endif;

            if ($this->session->userdata('featured_image')):
                $featured_image = $this->session->userdata('featured_image');
                $project['main_image'] = $featured_image['image'];
                $project['thumb_image'] = $featured_image['thumb_image'];
                if (!empty($data['project'])) {
                    $new_file = $data['project']->main_image;
                    $thumb_file = $data['project']->thumb_image;
                    @unlink($new_file);
                    @unlink($thumb_file);
                }
            endif;

            if ($this->project_model->update('projects', $project, array('id' => $data['project']->id))) {
                if ($this->session->userdata('userfile')):
                    $this->session->unset_userdata('userfile');
                endif;
                if ($this->session->userdata('doc_file')):
                    $this->session->unset_userdata('doc_file');
                endif;
                if ($this->session->userdata('featured_image')):
                    $this->session->unset_userdata('featured_image');
                endif;
                $this->session->set_flashdata('msg_success', 'Project Upadted successfully.');
                redirect('backend/projects');
            }
        }
        $data['technology'] = $this->project_model->get_result('technologies', array('status' => 1));
        $data['courses'] = $this->project_model->get_result('courses', array('status' => 1));
        $data['template'] = 'backend/project/pro_edit';
        $this->load->view('templates/backend/layout', $data);
    }

    public function clinical_check_edit($str) {
        if (!empty($_FILES['clinical_image']['name'])) {
            if ($this->session->userdata('clinical_image')) {
                return TRUE;
            } else {
                $param = array(
                    'file_name' => 'clinical_image',
                    'upload_path' => './assets/uploads/clinical_images/',
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'image_resize' => TRUE,
                    'source_image' => './assets/uploads/clinical_images/',
                    'new_image' => './assets/uploads/clinical_images/thumb/',
                    'resize_width' => 200,
                    'resize_height' => 200,
                    'encrypt_name' => TRUE,
                );
                $upload_file = upload_file($param);
                if ($upload_file['STATUS']) {
                    $this->session->set_userdata('clinical_image', array('image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'], 'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']));
                    return TRUE;
                } else {
                    $this->form_validation->set_message('clinical_check_edit', $upload_file['FILE_ERROR']);
                    return FALSE;
                }
            }
        }
    }

    public function download_doc_file($pro_id = '') {
        _check_superadmin_login(); //check login authentication
        $this->load->helper('download');
        if (empty($pro_id))
            redirect('backend/projects/');
        $data['project'] = $this->project_model->get_row('projects', array('id' => $pro_id));
        if (empty($data['project']))
            redirect('backend/projects/');
        $file_path = base_url(str_replace('./', '', $data['project']->doc_file));
        $file_array = explode('/', str_replace('./', '', $data['project']->doc_file));
        $data = file_get_contents($file_path); // Read the file's contents
        $name = $file_array[3];
        if (force_download($name, $data)) {
            redirect('backend/projects/edit/' . $pro_id);
        }
    }

    public function download_source_file($pro_id = '') {
        _check_superadmin_login(); //check login authentication
        $this->load->helper('download');
        if (empty($pro_id))
            redirect('backend/projects/');
        $data['project'] = $this->project_model->get_row('projects', array('id' => $pro_id));
        if (empty($data['project']))
            redirect('backend/projects/');
        $file_path = base_url(str_replace('./', '', $data['project']->main_file));
        $file_array = explode('/', str_replace('./', '', $data['project']->main_file));
        $data = file_get_contents($file_path); // Read the file's contents
        $name = $file_array[3];
        if (force_download($name, $data)) {
            redirect('backend/projects/edit/' . $pro_id);
        }
    }

    public function delete($pro_id = '') {
        _check_superadmin_login(); //check login authentication
        if (empty($pro_id))
            redirect('backend/projects');
        $data['project'] = $this->project_model->get_row('projects', array('id' => $pro_id));
        if (empty($data['project']))
            redirect('backend/projects/');
        if (!empty($data['project'])) {
            $new_file = $data['project']->main_file;
            @unlink($new_file);
        }
        if (!empty($data['project'])) {
            $new_file = $data['project']->doc_file;
            @unlink($new_file);
        }
        if (!empty($data['project'])) {
            $new_file = $data['project']->main_image;
            $thumb_file = $data['project']->thumb_image;
            @unlink($new_file);
            @unlink($thumb_file);
        }
        $this->project_model->delete('projects', array('id' => $pro_id));
        $this->session->set_flashdata('msg_success', 'project has been deleted successfully.');
        redirect('backend/projects');
    }
    
    public function deleteRequest($id = '') {
        _check_superadmin_login(); //check login authentication
        if (empty($id))
            redirect('backend/projects/requests');
        $data['request'] = $this->project_model->get_row('project_request', array('prid' => $id));
        if (empty($data['request']))
            redirect('backend/projects/requests');
        if (!empty($data['request']) && $data['request']->filename != '') {
            $new_file = FCPATH. '/assets/uploads/requestproject/'.$data['request']->filename;
            @unlink($new_file);
        }
        
        $this->project_model->delete('project_request', array('prid' => $id));
        $this->session->set_flashdata('msg_success', 'Project Request has been deleted successfully.');
        redirect('backend/projects/requests');
    }
    
    public function reviews($project_id = '') {
        _check_superadmin_login(); //check login authentication
        if (empty($project_id))
            redirect('backend/projects/');
        $data['project_info'] = $this->project_model->get_row('projects', array('id' => $project_id));
        if (empty($data['project_info']))
            redirect('backend/projects/');
        $data['reviews'] = $this->project_model->get_result('project_reviews', array('project_id' => $project_id));
        $data['template'] = 'backend/project/reviews';
        $this->load->view('templates/backend/layout', $data);
    }

    public function review_delete($id = '', $project_id = '') {
        _check_superadmin_login(); //check login authentication
        if (empty($project_id))
            redirect('backend/projects/');
        if (empty($id))
            redirect('backend/projects/reviews/' . $project_id);
        $data['review_info'] = $this->project_model->get_row('project_reviews', array('id' => $id));
        $this->project_model->delete('project_reviews', array('id' => $id));
        $this->project_model->delete('product_rating', array('user_id' => $data['review_info']->user_id, 'project_id' => $data['review_info']->project_id));
        $this->session->set_flashdata('msg_success', 'Review has been Deleted successfully.');
        redirect('backend/projects/reviews/' . $project_id);
    }

    public function review_view($id = '', $project_id = '') {
        _check_superadmin_login(); //check login authentication
        if (empty($project_id))
            redirect('backend/projects/');
        $data['project_info'] = $this->project_model->get_row('projects', array('id' => $project_id));
        if (empty($data['project_info']))
            redirect('backend/projects/');
        if (empty($id))
            redirect('backend/projects/reviews/' . $project_id);
        $data['review_info'] = $this->project_model->get_row('project_reviews', array('id' => $id));

        $data['template'] = 'backend/project/review_view';
        $this->load->view('templates/backend/layout', $data);
    }

    public function change_review_status($id = '', $project_id = '', $status = '') {

        if (empty($project_id))
            redirect('backend/projects/');
        if (empty($id))
            redirect('backend/projects/reviews/' . $project_id);

        if ($status == 0) {
            $cat_status = 1;
        }
        if ($status == 1) {
            $cat_status = 0;
        }
        $data = array('status' => $cat_status);
        if ($this->project_model->update('project_reviews', $data, array('id' => $id))) {
            $this->session->set_flashdata('msg_success', 'Product Review status has been updated successfully.');
            redirect('backend/projects/reviews/' . $project_id);
        } else {
            redirect('backend/projects/reviews/' . $project_id);
        }
    }

}
