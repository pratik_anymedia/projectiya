<?php



if (!defined('BASEPATH')) exit('No direct script access allowed');



class Newsletter extends CI_Controller{



    public function __construct() {



        parent::__construct();

        $this->load->model('newsletter_model');

    }



    public function index($offset=0){

         _check_superadmin_login(); //check login authentication

        $per_page = 100;



        $this->form_validation->set_rules('checkall[]','checked', 'required');

        $this->form_validation->set_rules('subject', 'Subject', 'required');

        $this->form_validation->set_rules('message', 'Message', 'required');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if($this->form_validation->run() == TRUE){

            $admin_ids = $this->input->post('checkall[]');



            for($i=0; $i<count($admin_ids); $i++) { 

                $array = explode('_',$admin_ids[$i]);

                $id = $array[1];

                $test = $array[0];

                if($array[0]=='customer'){

                    $user = $this->newsletter_model->get_result('users',array('status'=>1,'id'=>$id));

                }else if($array[0]=='subscribe'){

                    $user = $this->newsletter_model->get_result('users',array('status'=>1,'id'=>$id));

                } 

            }



            if(empty($admin_ids)) {

                $this->session->set_flashdata('msg_error', 'Recievers info is missing.');

                redirect('backend/newsletter');

            }

          

           // $admins = $this->newsletter_model->get_subscriber_for_msg($admin_ids, $all_admins);

          

              $s_subject = $this->input->post('subject');

             $message = $this->input->post('message');
 

            foreach ($admins as $admin) {

               /// $name = $admin->first_name." ".$admin->last_name;

                $subject = $s_subject;  // Subject for email

            

              $i=4;


              $this->load->library('developer_email');

              $email_template=$this->developer_email->get_email_template($i);

              $param=array(

                 'template'  =>  array(

                      'temp'  =>  $email_template->template_body,

                      'var_name'  =>  array(

                              'full_name'  => 'Dear Customer',

                              'site_name'   => SITE_NAME,

                              'site_url'    => base_url(),

                             // 'email_address'  => $user_info->email,

                              'message'=>$message

                            ),

                        ),

                  'email' =>  array(

                  'to'    =>   $admin->email,

                  'from'  =>   NO_REPLY_EMAIL,

                  'from_name' => SITE_NAME,

                  'subject' =>    $subject

                )

              );



              $status=$this->developer_email->send_mail($param);

           

             }



                $this->session->set_flashdata('msg_success',"Message Sent successfully.");

                redirect('backend/newsletter/message_center');

            }

        $data['newsletters'] = $this->newsletter_model->get_subscribers();

        $data['customers'] = $this->newsletter_model->get_customers();

        $data['groups'] = $this->newsletter_model->get_result('groups',array('status'=>1));

        // $config = backend_pagination();

        // $config['base_url'] = base_url().'backend/newsletter/';

        // $config['total_rows'] = $this->newsletter_model->newsletters(0, 0);

        // $config['per_page'] = $per_page;

        // $this->pagination->initialize($config);

        // $data['pagination'] = $this->pagination->create_links();

        // $data['email_template'] = $this->newsletter_model->get_row('email_templates', array('id'=>6));

        $data['template']='backend/newsletter/index';

        $this->load->view('templates/backend/layout', $data);

    }



    // public function add($offset = 0,$page = 'index'){



    //     _check_superadmin_login();  //check login authentication



    //     $this->form_validation->set_rules('first_name', 'Name', 'required');

    //     $this->form_validation->set_rules('last_name', 'Last Name', 'required');

    //     $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check["#"]');



    //     $this->form_validation->set_error_delimiters('<div class="error">', '</div>');



    //     if ($this->form_validation->run() == TRUE) {

    //         $user_data = array(

    //                             'first_name' => $this->input->post('first_name'),

    //                             'last_name' => $this->input->post('last_name'),

    //                             'email' => $this->input->post('email'),

    //                             'status'=>$this->input->post('status'),

    //                             'subscriber_date'=>date('Y-m-d- h:i:s')

    //                         );

    //         if ($this->newsletter_model->insert('newsletters',$user_data)) {

    //             $this->session->set_flashdata('msg_success', 'Newsletter added successfully.');

    //             redirect('backend/newsletter/' . $page . '/' . $offset);

    //         } else {

    //             $this->session->set_flashdata('msg_error', 'Update failed, Please try again.');

    //             redirect('backend/newsletter/' . $page . '/' . $offset);

    //         }

    //     }



    //     $data['template']='backend/newsletter/add';

    //     $this->load->view('templates/backend/layout', $data);

    // }



    public function email_check($new,$old){

        $str_new=strtoupper(trim($new));

        $str_old=strtoupper(trim($old));

        if($str_old==$str_new){

            return TRUE;

        }else{

            if($this->newsletter_model->get_row('newsletters',array('email'=>$str_new))){

                    $this->form_validation->set_message('email_check', 'This %s already exists.');

            return FALSE;

            }else{

                return TRUE;

            }

        }

    }





    public function status($id="",$status="",$offset="")

    {

        _check_superadmin_login(); //check login authentication

        if(empty($id)) redirect('backend/newsletter/');

        if($status==0){

            $cat_status=1;

        }

        if($status==1){

            $cat_status=0;

        }       

        $data = array('status'=>$cat_status);

        if($this->newsletter_model->update('newsletters',$data ,array('id'=>$id)))

        {

           $this->session->set_flashdata('msg_success','Product status has been updated successfully.');

           redirect('backend/newsletter/index/'.$offset);   

        }

    }



    public function sub_status($id="",$status="",$offset="")

    {

        _check_superadmin_login(); //check login authentication



        if(empty($id)) redirect('backend/newsletter/');

        if($status==0){

            $cat_status=1;

        }

        if($status==1){

            $cat_status=0;

        }       

        $data = array('newsletter'=>$cat_status);

        if($this->newsletter_model->update('users',$data ,array('id'=>$id)))

        {

           $this->session->set_flashdata('msg_success','Product status has been updated successfully.');

           redirect('backend/newsletter/index/'.$offset);   

        }

    }



    // public function edit($user_id = '',$offset = 0,$page = 'index'){



    //     _check_superadmin_login();  //check login authentication

    //     if (empty($user_id)) redirect('backend/newsletter/index/'.$offset);



    //     $data['user'] = $this->newsletter_model->get_row('newsletters', array('id' => $user_id));

    //     if($data['user']==FALSE) redirect('backend/newsletter/index/'.$offset);



    //     $this->form_validation->set_rules('first_name', 'Name', 'required');

    //     $this->form_validation->set_rules('last_name', 'Last Name', 'required');

    //     $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check[' . $data['user']->email . ']');



    //     $this->form_validation->set_error_delimiters('<div class="error">', '</div>');



    //     if ($this->form_validation->run() == TRUE) {

    //         $user_data = array(



    //             'first_name' => $this->input->post('first_name'),

    //             'last_name' => $this->input->post('last_name'),

    //             'email' => $this->input->post('email'),

    //             'status'=>$this->input->post('status'),





    //         );

    //         if($_POST['status']==0){

    //             $user_data['unsubscriber_date']=date('Y-m-d- h:i:s');

    //         }



    //         if ($this->newsletter_model->update('newsletters', $user_data, array('id' => $user_id))) {

    //             $this->session->set_flashdata('msg_success', 'Newsletter updated successfully.');

    //             redirect('backend/newsletter/' . $page . '/' . $offset);

    //         } else {

    //             $this->session->set_flashdata('msg_error', 'Update failed, Please try again.');

    //             redirect('backend/newsletter/' . $page . '/' . $offset);

    //         }

    //     }







    //     $data['template']='backend/newsletter/edit';

    //     $this->load->view('templates/backend/layout', $data);

    // }





    public function delete_subscribers()

    {

        _check_superadmin_login(); //check Professor login authentication

        if($_POST){

            $this->form_validation->set_rules('sub_id[]','Customer','trim|required');

            $this->form_validation->set_error_delimiters('<div style="color:red;" class="error">','</div>');

            if($this->form_validation->run() == TRUE){

                $sub_id_array= $this->input->post('sub_id');

                $FLAG=TRUE;

                // print_r($student_id_array);

                // die();

                //$status = $_POST['all_status'];

                $count_sub = count($sub_id_array);

                for($i=0; $i<$count_sub ; $i++){ 

                    $sub_status = $this->newsletter_model->delete('newsletters',array('id'=>$sub_id_array[$i]));   

                    //$this->customer_model->delete('user_shipping_info',array('customer_id'=>$cust_id_array[$i]));

                    if(!$sub_status){

                        $FLAG=FALSE;

                    }

                }           

                if($FLAG){

                    $this->session->set_flashdata('msg_success',"Subscriber have been Deleted successfully.");

                    redirect('backend/newsletter');

                }

            }else{

                $this->session->set_flashdata('msg_error',"Subscriber delete Operation Failed.");

                redirect('backend/newsletter');

            }

        }else{

            $this->session->set_flashdata('msg_error',"Subscriber delete Operation Failed...");

            redirect('backend/newsletter');

        } 

    }



  public function delete($user_id = '', $offset = 0){

        _check_superadmin_login();  //check login authentication

        if (empty($user_id)) redirect('backend/newsletter/index/'.$offset);



        if ($this->newsletter_model->delete('newsletters', array('id' => $user_id))) {

            $this->session->set_flashdata('msg_success', 'Newsletter deleted successfully.');

            redirect('backend/newsletter/index/'.$offset);

        } else {

            $this->session->set_flashdata('msg_error', 'News letter delete failed, Please try again.');

            redirect('backend/newsletter/index/'.$offset);

        }

    }



     public function delete_message($id = '', $offset = 0){

        _check_superadmin_login();  //check login authentication

        if (empty($id)) redirect('backend/newsletter/message_center/'.$offset);



        if ($this->newsletter_model->delete('messages', array('id' => $id))) {

            $this->session->set_flashdata('msg_success', 'Newsletter deleted successfully.');

            redirect('backend/newsletter/message_center/'.$offset);

        } else {

            $this->session->set_flashdata('msg_error', 'Message delete failed, Please try again.');

            redirect('backend/newsletter/message_center/'.$offset);

        }

    }







    public function message_center($offset = 0){

              

        _check_superadmin_login();  //check login authentication     

        $per_page = 10;

        $data['offset'] = $offset;

        $data['messages'] = $this->newsletter_model->message_center($offset, $per_page);

        $config = backend_pagination();

        $config['base_url'] = base_url().'backend/newsletter/message_center/';

        $config['total_rows'] = $this->newsletter_model->message_center(0, 0);

        $config['per_page'] = $per_page;

        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();   

        $data['template']='backend/newsletter/message_center';

        $this->load->view('templates/backend/layout', $data);

    }



    public function message_info($id='',$offset=0)

    {

        $data['msg'] = $this->newsletter_model->get_row('messages', array('id' => $id));

        $data['template']='backend/newsletter/message_info';

        $this->load->view('templates/backend/layout', $data);

    }



}