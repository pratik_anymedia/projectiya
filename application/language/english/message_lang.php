<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//project list
$lang['PROJECT_NAME'] = 'Project Name';
$lang['FEATURED_PROJECT'] = 'Featured Project';
$lang['TECHNOLOGY_USED'] = 'Technology Used';
$lang['FORN_PAGES'] = 'Forms/Pages';
$lang['DATABASE_TABLES'] = 'Database Table';
$lang['CAN_BE_USED_IN'] = 'Can be used in';
$lang['PRICE'] = 'Price';
$lang['COST'] = 'Cost';
$lang['BACKEND_TECHNOLOGY'] = 'Backend Technology';
$lang['DESCRIPTION'] = 'Description';
//$lang['FORN_PAGES'] = 'Forms/Pages';
//$lang['FORN_PAGES'] = 'Forms/Pages';


//project detail page
$lang['LATEST_TECHNOLOGY'] = 'Latest Technology';
$lang['RELATED_PROJECTS'] = 'Related Projects';


$lang['NO_RECORD_FOUND'] = 'No Record Found';

//pages
$lang['ADD_PAGE'] = 'Add Page';
$lang['EDIT_PAGE'] = 'Edit Page';
$lang['PAGES'] = 'Pages';
$lang['TITLE'] = 'Title';
$lang['CONETENT'] = 'Content';
$lang['STATUS'] = 'Status';
$lang['RESOURCEPAGE'] = 'Resource / Page ';
$lang['PUBLISH'] = 'Publish';
$lang['UNPUBLISH'] = 'Unpublish';
$lang['ADD'] = 'Add';
$lang['ADD_PAGE_SUCCESS'] = 'Page added successfully.';
$lang['ADD_PAGE_FAILED'] = 'Page Add Failed.';


//backend order mgmt
$lang['ORDERS'] = 'Orders';
$lang['ORDER_DETAIL'] = 'Order Details';
