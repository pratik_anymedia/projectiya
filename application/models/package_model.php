<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Package_model extends MY_Model { 

    /*     * ******************
     *
     * function constructor
     * 
     * @param		null
     * @return 		null
     */
    
    function package_model() {

        parent::__construct();

        $this->table = 'packages';
        $this->primary_key = 'packages.id';
        
    }
    
    function getById($packageId){
        return $this->db->get_where($this->table , array('id'=>$packageId))->row();
    }
    
}