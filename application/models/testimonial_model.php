<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Testimonial_model extends MY_Model { 

    /*     * ******************
     *
     * function constructor
     * 
     * @param		null
     * @return 		null
     */
    
    function testimonial_model() {

        parent::__construct();

        $this->table = 'testimonial';
        $this->primary_key = 'testimonial.id';
        
    }
    
    function getByUserId($userId){
        return $this->db->get_where($this->table , array('user_id'=>$userId))->row();
    }
    
    public function add($content , $userId){
        if($content ==''){
            return false;
        }
        
        if($this->isExist($userId)){
            $this->db->update($this->table , array('content' => $content), array('user_id' => $userId));
        }else{
            $this->db->insert($this->table , array('content' => $content,'user_id' => $userId,'added_on' => date('Y-m-d h:i:s')));
        }
        return true;
    }
    
    public function isExist($userId){
        if($userId ==''){
            return false;
        }
        return $this->db->get_where($this->table, array('user_id' => $userId))->row();
    }
    
    function getApproved(){
        return $this->db->get_where($this->table, array('is_approved' => 1))->result();
    }
    
    public function getAll($offset = '', $per_page = '', $sort_by, $sort_order) {


        $sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';

        $sort_columns = array('id', 'is_approved');

        $sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'id';



        if (!empty($_GET['search_by'])) {

            if ($_GET['search_by'] != '') {

                $this->db->like($_GET['search_by'], $this->input->get('search_query', TRUE));
            }
        }

        $this->db->select('testimonial.*,users.first_name,users.last_name');
        $this->db->from('testimonial');

        $this->db->join('users', 'users.id = testimonial.user_id', 'left');
        if ($offset >= 0 && $per_page > 0) {

            $this->db->limit($per_page, $offset);

            $this->db->order_by('testimonial.' . $sort_by, $sort_order);


            $query = $this->db->get();

            if ($query->num_rows() > 0)
                return $query->result();
            else
                return FALSE;
        }else {

            return $this->db->count_all_results();
        }
    }
    
    
    
    public function update($table_name = '', $data = '', $id_array = '')

    {

        if (!empty($id_array)):

            foreach ($id_array as $key => $value) {

                $this->db->where($key, $value);

            }

        endif;

        return $this->db->update($table_name, $data);

    }



    public function delete($table_name = '', $id_array = ''){

        return $this->db->delete($table_name, $id_array);

    }
    
    
}