<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
class Project_model extends CI_Model
{

    public function insert($table_name = '', $data = '')
    {
        $query = $this->db->insert($table_name, $data);
        if ($query) return $this->db->insert_id();
        else return FALSE;
    }



    public function get_result($table_name = '', $id_array = '', $columns = array(),$order_by = array())

    {

        if (!empty($columns)):

            $all_columns = implode(",", $columns);

            $this->db->select($all_columns);

        endif;

        if (!empty($id_array)):

            foreach ($id_array as $key => $value) {

                $this->db->where($key, $value);

            }

        endif;

        if (!empty($order_by)):

            $this->db->order_by($order_by[0], $order_by[1]);

        endif;

        $query = $this->db->get($table_name);

        if ($query->num_rows() > 0) return $query->result();

        else return FALSE;

    }



    public function get_row($table_name = '', $id_array = '', $columns = array())

    {

        if (!empty($columns)):

            $all_columns = implode(",", $columns);

            $this->db->select($all_columns);

        endif;

        if (!empty($id_array)):

            foreach ($id_array as $key => $value) {

                $this->db->where($key, $value);

            }

        endif;

        $query = $this->db->get($table_name);

        if ($query->num_rows() > 0) return $query->row();

        else return FALSE;

    }



    public function update($table_name = '', $data = '', $id_array = '')

    {

        if (!empty($id_array)):

            foreach ($id_array as $key => $value) {

                $this->db->where($key, $value);

            }

        endif;

        return $this->db->update($table_name, $data);

    }



    public function delete($table_name = '', $id_array = ''){

        return $this->db->delete($table_name, $id_array);

    }





    public function projects($per_page = '', $offset = '',$sort_by,$sort_order) {

        
        $sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';

        $sort_columns = array('id','status');

        $sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'id';



        if(!empty($_GET['search_by'])){

            if($_GET['search_by']!=''){

                $this->db->like($_GET['search_by'],$this->input->get('search_query',TRUE));

            } 

        }

        if(!empty($_GET['technology'])){

             $this->db->like('technology','#'.$_GET['technology'].'#');

        }  
        
        if(!empty($_GET['searchfor'])){
            $this->db->or_like('title',$_GET['searchfor']);
        }
        
        $this->db->from('projects');

        if ($offset >= 0 && $per_page > 0) {

            $this->db->limit($per_page, $offset);

            $this->db->order_by($sort_by, $sort_order);

            $query = $this->db->get();
            
            if($query->num_rows() > 0)

                return $query->result();

            else

                return FALSE;

        }else {

            return $this->db->count_all_results();

        }

    }
public function projectRequests($per_page = '', $offset = '',$sort_by,$sort_order) {

        
        $sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';

        $sort_columns = array('prid');

        $sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'prid';



        if(!empty($_GET['search_by'])){

            if($_GET['search_by']!=''){

                $this->db->like($_GET['search_by'],$this->input->get('search_query',TRUE));

            } 

        }
        
        $this->db->from('project_request');

        if ($offset >= 0 && $per_page > 0) {

            $this->db->limit($per_page, $offset);

            $this->db->order_by($sort_by, $sort_order);

            $query = $this->db->get();
            
            if($query->num_rows() > 0)

                return $query->result();

            else

                return FALSE;

        }else {

            return $this->db->count_all_results();

        }

    }



  

    public function get_products_info()

    {

        $this->db->select('pr.*,psi.*,pr.id as pro_related_id,psi.id as pro_ship_id,p.*');

        //$this->db->where('user_role',1);

        $this->db->from('products as p');

        $this->db->join('products_related_info as pr','p.id = pr.product_id','left');

        $this->db->join('product_shpping_info as psi','p.id = psi.product_id','left');

        $this->db->order_by('p.id', 'desc');

        $query = $this->db->get();

        if ($query->num_rows() > 0)

            return $query->result();

        else

            return FALSE;

    }


    function getUserProjectCount($userId){
        $this->db->from('projects');
        $this->db->where(array('user_id' => $userId));
        return $this->db->count_all_results();
    }
 



}