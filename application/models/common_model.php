<?php



if (!defined('BASEPATH')) exit('No direct script access allowed');



class Common_model extends CI_Model

{



    public function insert($table_name = '', $data = '')
    {
        $query = $this->db->insert($table_name, $data);
        if ($query) return $this->db->insert_id();
        else return FALSE;
    }


    public function get_result($table_name = '', $id_array = '', $columns = array(), $order_by = array(),$limit='')

    {

        if (!empty($columns)):

            $all_columns = implode(",", $columns);

            $this->db->select($all_columns);

        endif;



        if (!empty($order_by)):

            $this->db->order_by($order_by[0], $order_by[1]);

        endif;



        if(!empty($id_array)):

            foreach ($id_array as $key => $value) {

                $this->db->where($key, $value);

            }

        endif;



        if (!empty($limit)):

            $this->db->limit($limit);

        endif;



        $query = $this->db->get($table_name);

        if ($query->num_rows() > 0) return $query->result();

        else return FALSE;

    }





    public function get_row($table_name = '', $id_array = '', $columns = array(), $order_by = array())

    {

        if (!empty($columns)):

            $all_columns = implode(",", $columns);

            $this->db->select($all_columns);

        endif;

        if(!empty($id_array)):

            foreach ($id_array as $key => $value) {

                $this->db->where($key, $value);

            }

        endif;

        if (!empty($order_by)):

            $this->db->order_by($order_by[0], $order_by[1]);

        endif;

        $query = $this->db->get($table_name);

        if($query->num_rows()>0) return $query->row();

        else return FALSE;

    }



    public function update($table_name = '', $data = '', $id_array = '')

    {

        if(!empty($id_array)):

            foreach($id_array as $key => $value){

                $this->db->where($key, $value);

            }

        endif;

        return $this->db->update($table_name, $data);

    }



    public function delete($table_name = '', $id_array = '')

    {

        return $this->db->delete($table_name, $id_array);

    }





    public function password_check($data = '', $user_id = '')

    {

        $query = $this->db->get_where('users', $data, array('id' => $user_id));

        if ($query->num_rows() > 0) return TRUE;

        else {

            //$this->form_validation->set_message('password_check', 'The %s field can not match');

            return FALSE;

        }

    }



    public function changestatus($id="",$status="",$offset="",$table_name="",$data=array()) {

        if($status==0) $status=1;

        else $status=0;

        if($this->blog_model->update($table_name,$data,array('id'=>$id))){

        $this->session->set_flashdata('msg_success','Status Updated successfully.');

        redirect($_SERVER['HTTP_REFERER']);

        }else{

        $this->session->set_flashdata('msg_error','Status Updated successfully.');

        redirect($_SERVER['HTTP_REFERER']);

        }

    }





    public function page($slug = '')

    {

        if (!empty($slug)) {

            $query = $this->db->get_where('posts', array('post_slug' => $slug, 'post_type' => 'page', 'status' => 1));

            if ($query->num_rows() > 0) return $query->row();

            else return FALSE;

        }else {

            return FALSE;

        }

    }



    public function get_post($slug, $is_slug)

    {

        if ($is_slug) $where = array('id' => $slug, 'post_type' => 'post');

        else $where = array('post_slug' => $slug, 'post_type' => 'post');

        return $this->get_row('posts', $where, array('post_title', 'post_content'));

    }



    public function news_categories($offset = '', $per_page = '')

    { 

        if ($offset >= 0 && $per_page > 0) {

            $this->db->order_by('id', 'desc');

            $this->db->limit($per_page, $offset);

            $query = $this->db->get('news_categories');

            if ($query->num_rows() > 0) return $query->result();

            else return FALSE;

        }else {

            $query = $this->db->get('news_categories');

            return $query->num_rows();

        }

    }









// gallery images counter



     public function image_likes_count($image_id=''){

        $query=$this->db->query("SELECT count(id) as num_rows from  gallery_image_likes where image_id=$image_id");

        return $query->row()->num_rows;

    }



    public function increment_image_count($image_id=''){

        

        $this->db->where('id', $image_id);

        $this->db->set('image_count', 'image_count+1', FALSE);

        $query=$this->db->update('gallary_photos');

    }



//quote like counter



      public function quote_likes_count($quote_id=''){

        $query=$this->db->query("SELECT count(id) as num_rows from  quote_likes where quote_id=$quote_id");

        return $query->row()->num_rows;

    }



    public function increment_quote_count($quote_id=''){

        

        $this->db->where('id', $quote_id);

        $this->db->set('quote_count', 'quote_count+1', FALSE);

        $query=$this->db->update('quotes');

    }



    // News conter 



    public function news_likes_count($news_id=''){

        $query=$this->db->query("SELECT count(id) as num_rows from  news_likes where news_id=$news_id");

        return $query->row()->num_rows;

    }



    public function news_comment_count($news_id=''){

        $query=$this->db->query("SELECT count(id) as num_rows from  news_comments where comment_news_id=$news_id");

        return $query->row()->num_rows;

    }



    public function increment_news_count($news_id=''){

    
        $this->db->where('id', $news_id);

        $this->db->set('news_like_count', 'news_like_count+1', FALSE);

        $query=$this->db->update('news');

    }

   



   public function events($offset = '',$per_page='')

    {   

        if(!empty($_GET['location'])){

            if($_GET['location']!=''){

                $a="address LIKE '%".$this->input->get('location')."%'";

                $b="city LIKE '%".$this->input->get('location')."%'";

                $c="state LIKE '%".$this->input->get('location')."%'";

                $d="country LIKE '%".$this->input->get('location')."%'";

                $e='('.$a .' OR '. $b .' OR '. $c .' OR '. $d.')';

                $this->db->where($e);

            }

        } 



        $this->db->where('status',1);

        if(!empty($_GET['start_date'])&&!empty($_GET['end_date'])){

            $start_date = date('Y-m-d',strtotime($_GET['start_date']));

            $end_date = date('Y-m-d',strtotime($_GET['end_date']));

            $this->db->where('(start_date <="'.$end_date.'" AND end_date >="'.$start_date.'")'); 

        } 



        if(!empty($_GET['event_category'])){

            if($_GET['event_category']!=''){

                $this->db->like('category_id',$this->input->get('event_category'));

            }

        }  

        $this->db->from('events');

      

        if($offset >= 0 && $per_page > 0){

            $this->db->limit($per_page, $offset);

            $this->db->order_by('id', 'desc');

            $query = $this->db->get();

            if ($query->num_rows() > 0)

                return $query->result();

            else

                return FALSE;

        } else {

            return $this->db->count_all_results();

        }

    } 



    public function get_user_info()

    {

        $this->db->select('us.*,usi.id as shipping_id,usi.same_to_bill as same_to_bill,usi.company_name as ship_company_name,usi.fax as ship_fax,usi.first_name as ship_first_name,usi.last_name as ship_last_name,usi.email as ship_email,usi.phone as ship_phone,usi.zip_code as ship_zip_code,usi.address as ship_address,usi.city as ship_city,usi.country as ship_country,usi.state as ship_state,usi.free_shipping as ship_free_shipping,usi.tax_free as ship_tax_free,usi.discount as ship_discount,usi.address1 as ship_address1');

        $this->db->where('us.id',user_id());

        $this->db->where('us.status',1);

        $this->db->where('us.user_role',1);

        $this->db->from('users as us');

        $this->db->join('user_shipping_info as usi','us.id = usi.customer_id','left'); 

        $query = $this->db->get();  

        if ($query->num_rows()>0) 

            return $query->row();

        else

            return 0;

    }  



    public function zone_method_info($con_slug='',$sta_slug='')

    {

        if(!empty($con_slug)){

            $this->db->like('countries',$con_slug);

        }else{

            $this->db->where("NULLIF(countries,'') IS NULL");

        }

        // if(!empty($sta_slug)){

        //     $this->db->like('states',$sta_slug);

        // }else{

        //      $this->db->where("NULLIF(states,'') IS NULL");

        // }

        $this->db->from('zones');

        $query = $this->db->get();  

        if($query->num_rows()>0) 

            return $query->row();

        else

            return 0;

    }

    public function zone_method_country_info($con_slug='')
    {

        if(!empty($con_slug)){

            $this->db->like('countries',$con_slug);

        }

        // if(!empty($sta_slug)){

        //     $this->db->like('states',$sta_slug);

        // }else{

        //      $this->db->where("NULLIF(states,'') IS NULL");

        // }

        $this->db->from('zones');

        $query = $this->db->get();  

        if($query->num_rows()>0) 

            return $query->row();

        else

            return 0;

    }



    

    public function get_filter_projects($techno_ids='',$course_ids='',$offset=0,$per_page=5){

        if(!empty($techno_ids)){
            for($i=0; $i < count($techno_ids) ; $i++) { 
                $this->db->or_like('technology','#'.$techno_ids[$i].'#'); 
            }    
        }
        if(!empty($course_ids)){
            for($j=0; $j< count($course_ids) ; $j++) { 
                $this->db->like('course_type','#'.$course_ids[$j].'#');
            }
        }

        $this->db->where('status',1);
        $this->db->from('projects');        

        if ($offset >= 0 && $per_page > 0) {
            $this->db->limit($per_page, $offset);
            $query = $this->db->get();
            if ($query->num_rows() > 0)
                return $query->result();
            else 
                return FALSE;
        }else {
            return $this->db->count_all_results();
        }
    }



    public function projects($offset=0,$per_page=0){
        
 
        if(!empty($_GET['searchfor'])){
            $this->db->like('title',$_GET['searchfor']);
        }
        $this->db->where('status',1);
        $this->db->from('projects');
        if ($offset >= 0 && $per_page > 0) {
            $this->db->limit($per_page, $offset);
            $query = $this->db->get();
            if ($query->num_rows() > 0)
                return $query->result();
            else 
                return FALSE;
        }else {
            return $this->db->count_all_results();
        }

    }







    public function product_detail($slug='')

    {

        $this->db->select('pri.*,pri.id as pri_id,p.*');

        $this->db->where('p.status',1);

        $this->db->where('p.slug',$slug);

        $this->db->from('products as p');

        $this->db->join('products_related_info as pri', 'p.id = pri.product_id', 'left');    

        $this->db->order_by('p.id','desc');

        $query = $this->db->get();

        if($query->num_rows() > 0)

          return $query->row();

        else

          return $this->db->count_all_results();

       

    }









    public function get_valid_coupon_code($code='')

    {

        $today =date('Y-m-d');

        $this->db->where('code',$code);

        //$priceRange = "(trainer_pay_session.value BETWEEN ".$from." AND ".$to.")";   

        $this->db->where('end_date >=',$today);

        $this->db->from('coupons');

        $query = $this->db->get();

        if($query->num_rows() > 0) 

            return $query->row();

        else 

            return FALSE;

    }



    public function get_code_result($code_id='',$code='')

    {

        $today =date('Y-m-d');

        $this->db->where('coupon_code_id',$code_id);

        $this->db->where('coupon_code',$code);

        $this->db->from('coupon_code_used_data');

        $query = $this->db->get();

        if($query->num_rows() > 0) 

            return $query->num_rows();

        else 

            return FALSE;

    }







    



    public function get_recent_comment($offset=0,$per_page=3)

    {

  

        $this->db->where('comment_approved',1);

        $this->db->group_by('comment_blog_id');

        $this->db->from('comments');

        if($offset >= 0 && $per_page > 0) {



            $this->db->limit($per_page, $offset);



            $this->db->order_by('id', 'desc');  



            $query = $this->db->get();



            if ($query->num_rows() > 0) return $query->result();



            else return FALSE;



        }else {



            return $this->db->count_all_results();



        }

    }



   



    

    





    public function testimonials($offset = '',$per_page='')

    {        

        $this->db->from('testimonials');

        $this->db->where('status',1);

        if($offset >= 0 && $per_page > 0){

            $this->db->limit($per_page, $offset);

            $this->db->order_by('id', 'desc');

            $query = $this->db->get();

            if ($query->num_rows() > 0)

                return $query->result();

            else

                return FALSE;

        } else {

            return $this->db->count_all_results();

        }

    }   



   

   

    public function get_shipping_zone_price($total='',$method='',$id='')

    {            

        $this->db->where('zone_id',$id);

        $this->db->where('(low_price <="'.$total.'" AND high_price >="'.$total.'")'); 

        $this->db->from('price_configurtion');

        $query = $this->db->get();

        if ($query->num_rows() > 0)

            return $query->row();

        else

            return FALSE;

    }  



    public function get_all_tags()

    {

        $this->db->select('blogs.tags');

        $this->db->where(array('blog_status'=>'publish'));

        $this->db->from('blogs');

        $query = $this->db->get();

        if($query->num_rows()>0){

            return $query->result();

        }else{

            return FALSE;

        }

    } 



    public function blog_favorite($offset=0, $limit=3){

        if($offset>=0 && $limit>0){

            $this->db->limit($limit,$offset);

            $this->db->order_by('comment_count','desc');

            $this->db->where('blog_status',1);

            $query = $this->db->get('blogs');

            if($query->num_rows()>0)

                return $query->result();

            else

                return FALSE;

        }else{

            $query = $this->db->get('blogs');

            return $query->num_rows();          

        }       

    }

    public function blogs($offset=0, $limit=''){
        $this->db->where('blog_status',1);
        $this->db->from('blogs');
        if ($offset >= 0 && $limit > 0) {
            $this->db->limit($limit,$offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            if ($query->num_rows() > 0)
                return $query->result();
            else
                return FALSE;
        }else {
            return $this->db->count_all_results();
        }

    }


    public function blog_comments($blog_id=''){
        $this->db->where('comment_blog_id',$blog_id);
        $this->db->where('comment_approved',1);
        $this->db->from('comments');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE; 
    }



    public function get_manage_chart($trainer_id=0){

        $this->db->from('manage_chart');

        $this->db->where('status',1);

        $this->db->where('trainer_id',$trainer_id);

        $this->db->order_by('avail_day', 'ASC');         

        $this->db->order_by('start_time', 'ASC');

        $query = $this->db->get();

        if($query->num_rows()>0):          

            $arrWeek = get_day();

            foreach ($arrWeek as $key => $value) {

               // $start_time=date('h:i A',strtotime($row->start_time));  

                $chart_arr[$key] = array(

                      'day_number'=>$key,

                      'day'=>$value,

                      'data'=>$this->get_data_of_week($key,$trainer_id)

                );

            }

            //print_r($chart_arr); die;

            return $chart_arr;

        else:

        return FALSE;

        endif;

       /* $sql="SELECT id,start_time FROM `manage_chart` WHERE trainer_id=$trainer_id GROUP BY start_time  ORDER BY `start_time` ASC;";

        $query = $this->db->query($sql);



        if ($query->num_rows() > 0):

            foreach ($query->result() as $row) {

                //$start_time=date('h:i A',strtotime($row->start_time));

                //$chart_arr['time']= $start_time;

                $chart_arr['days']= array();

            }*/



       /* else:

        return FALSE;

        endif;

        */

    }

    public function get_data_of_week($day,$trainer_id){    

        $this->db->from('manage_chart');

        $this->db->where('status',1);

        $this->db->where('avail_day',$day);

        $this->db->where('trainer_id',$trainer_id);      

        $this->db->order_by('start_time', 'ASC');

        $query = $this->db->get();

        if($query->num_rows()>0)

            return $query->result();

        else

            return FALSE;

    }    



    public function check_rating($plan_user='',$planid='',$userid='',$plan_type=''){

        

        //$this->db->query('SELECT count(*) as num_row');

        $this->db->where('plan_user_id',$plan_user);

        $this->db->where('plan_id',$planid);

        $this->db->where('user_id',$userid);

        $this->db->where('plan_type',$plan_type);

        //$query2 = $this->db->get('plan_rating0');        

        return $this->db->count_all_results('plan_rating');        

    }



    public function increment_plan_rating($score='',$id='',$plan_type=''){

        

        $this->db->where('id', $id);

        $this->db->set('rating_count', 'rating_count+1', FALSE);

        $this->db->set('sum_rating', 'sum_rating+'.$score, FALSE);

        if($plan_type=='nutrition'){

        return $this->db->update('nutritionplans');

        }



        if($plan_type=='work'){

        return $this->db->update('workoutplans');

        }



        if($plan_type=='sup'){

            return $this->db->update('supplements');

        }



        if($plan_type=='video'){

            return $this->db->update('videoes');

        }



    }





    public function left_bar_nut_rating($trainer_id='')

    {

         $query11=$this->db->query("SELECT count(id) as total, sum(rating_count) as ratingcount,sum(sum_rating) as sumrating  FROM nutritionplans where trainer_id=$trainer_id");

         return $query11->row();



    }



     public function left_bar_work_rating($trainer_id='')

    {

         $query11=$this->db->query("SELECT count(id) as total, sum(rating_count) as ratingcount,sum(sum_rating) as sumrating  FROM workoutplans where trainer_id=$trainer_id");

         return $query11->row();



    }



     public function left_bar_sup_rating($trainer_id='')

    {

         $query11=$this->db->query("SELECT count(id) as total, sum(rating_count) as ratingcount,sum(sum_rating) as sumrating  FROM supplements where trainer_id=$trainer_id");

         return $query11->row();



    }



     public function left_bar_video_rating($trainer_id='')

    {

         $query11=$this->db->query("SELECT count(id) as total, sum(rating_count) as ratingcount,sum(sum_rating) as sumrating  FROM videoes where trainer_id=$trainer_id");

         return $query11->row();



    }









    public function get_nutritional_plan()

    {

        

        $nutri = array(); 

        $this->db->from('nutritionplans');

        $this->db->where('status',1);

        $this->db->order_by('id','desc');

        $query = $this->db->get();

        if($query->num_rows()>0){

            $array =  $query->result();

            $j=0;

            foreach ($array as $value){

                $rating = $this->get_result('plan_rating',array('plan_id'=>$value->id,'plan_type'=>'nutrition'));

                if(!empty($rating)){

                    $i=0; $total_percentage='';

                    $rate=0;

                    foreach($rating as $row){

                        $rate = $rate + $row->rating_score;   

                        $i++;

                    }            

                    $t=$i*5;

                    $total_percentage = ($rate/$t)*100;                   

                }

                $myarray = (array) $value;

                $myarray['total']=$total_percentage;

                $nutri[$j]=$myarray;

                $j++;

            }

            return  $nutri;

                 

        } else {

            return FALSE;

        }

    }



    public function get_rated_trainers()

    {

        $this->db->where('status',1);

        $this->db->where('user_role',1);

        $this->db->from('users');

        $this->db->order_by('overall_rating','desc');

        $this->db->order_by('trainer_status','desc');

        $query = $this->db->get();

        if($query->num_rows()>0){

            return $query->result();

        }else{

            return FALSE;

        }    



    }

    public function resource($offset=0, $limit=3){

        if($offset>=0 && $limit>0){

            $this->db->limit($limit,$offset);
            $this->db->order_by('page_type','desc');
            $this->db->from('posts');
            $this->db->where('page_type','resource');
            

            $query = $this->db->get();

            if($query->num_rows()>0)

                return $query->result();

            else

                return FALSE;

        }else{

            $query = $this->db->get('posts');

            return $query->num_rows();          

        }       

    }
    
    public function resources($offset=0, $limit=''){
        $this->db->where('page_type','resource');
        $this->db->from('posts');
        if ($offset >= 0 && $limit > 0) {
            $this->db->limit($limit,$offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            if ($query->num_rows() > 0)
                return $query->result();
            else
                return FALSE;
        }else {
            return $this->db->count_all_results();
        }

    }
    



}

