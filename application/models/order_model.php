<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends CI_Model
{



    public function insert($table_name = '', $data = '')
    {
        $query = $this->db->insert($table_name, $data);
        if ($query) return $this->db->insert_id();
        else return FALSE;
    }

    public function get_result($table_name = '', $id_array = '', $columns = array(),$order_by = array())
    {

        if (!empty($columns)):
            $all_columns = implode(",", $columns);
            $this->db->select($all_columns);
        endif;

        if (!empty($id_array)):

            foreach ($id_array as $key => $value) {

                $this->db->where($key, $value);

            }

        endif;

        if (!empty($order_by)):

            $this->db->order_by($order_by[0], $order_by[1]);

        endif;

        $query = $this->db->get($table_name);

        if ($query->num_rows() > 0) return $query->result();

        else return FALSE;

    }



    public function get_row($table_name = '', $id_array = '', $columns = array())

    {

        if (!empty($columns)):

            $all_columns = implode(",", $columns);

            $this->db->select($all_columns);

        endif;

        if (!empty($id_array)):

            foreach ($id_array as $key => $value) {

                $this->db->where($key, $value);

            }

        endif;

        $query = $this->db->get($table_name);

        if ($query->num_rows() > 0) return $query->row();

        else return FALSE;

    }



    public function update($table_name = '', $data = '', $id_array = '')

    {

        if (!empty($id_array)):

            foreach ($id_array as $key => $value) {

                $this->db->where($key, $value);

            }

        endif;

        return $this->db->update($table_name, $data);

    }



    public function delete($table_name = '', $id_array = ''){

        return $this->db->delete($table_name, $id_array);

    }





    public function orders($offset = '',$per_page = '',$sort_by,$sort_order){

        
        $sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';

        $sort_columns = array('id','status');

        $sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'id';



        if(!empty($_GET['search_by'])){

            if($_GET['search_by']!=''){

                $this->db->like($_GET['search_by'],$this->input->get('search_query',TRUE));

            } 

        }

        $this->db->select('orders.*,users.first_name,users.last_name');
        $this->db->from('orders');
        
        $this->db->join('users', 'users.id = orders.user_id','left');
        if($offset >= 0 && $per_page > 0) {

            $this->db->limit($per_page, $offset);
            
            $this->db->order_by('orders.'.$sort_by, $sort_order);


            $query = $this->db->get();

            if ($query->num_rows() > 0)

                return $query->result();

            else

                return FALSE;

        }else {

            return $this->db->count_all_results();

        }

    }



    public function search_customers($value='')

    {

        if($_POST){

        $this->db->select('u.*,us.id as shipping_id,us.company_name as ship_company_name,us.fax as ship_fax,us.first_name as ship_first_name,us.last_name as ship_last_name,us.email as ship_email,us.phone as ship_phone,us.zip_code as ship_zip_code,us.address as ship_address,us.city as ship_city,us.country as ship_country,us.state as ship_state,us.free_shipping as ship_free_shipping,us.tax_free as ship_tax_free,us.discount as ship_discount');

        if(!empty($_POST['customer_id'])){

            $this->db->where('u.customer_id',$_POST['customer_id']);

        }

        if(!empty($_POST['customer_first_name'])){

            $this->db->like('u.first_name', $_POST['customer_first_name']);

        }

        if(!empty($_POST['customer_last_name'])){

            $this->db->like('u.last_name', $_POST['customer_last_name']);

        }        

        if(!empty($_POST['customer_email'])){

            $this->db->like('u.email', $_POST['customer_email']);

        }

        if(!empty($_POST['customer_company_name'])){

            $this->db->like('u.company_name', $_POST['customer_company_name']);

        }



        $this->db->where('user_role',1);

        $this->db->from('users as u');

        $this->db->join('user_shipping_info as us', 'u.id = us.customer_id','left');

        $this->db->order_by('u.id', 'desc');

        $query = $this->db->get();

        if ($query->num_rows() > 0)

            return $query->result();

        else

            return FALSE;

        }else{



              return FALSE;

        }

    }



    public function search_cust_info($id='')

    {

        $this->db->select('u.*,us.id as shipping_id,us.company_name as ship_company_name,us.fax as ship_fax,us.first_name as ship_first_name,us.last_name as ship_last_name,us.email as ship_email,us.phone as ship_phone,us.zip_code as ship_zip_code,us.address as ship_address,us.city as ship_city,us.country as ship_country,us.state as ship_state,us.free_shipping as ship_free_shipping,us.tax_free as ship_tax_free,us.discount as ship_discount,us.same_to_bill as sametobill');        

        $this->db->where('u.id',$id);

        $this->db->from('users as u');

        $this->db->join('user_shipping_info as us','u.id = us.customer_id','left');

        $query = $this->db->get();

        if($query->num_rows() > 0)

            return $query->row();

        else

            return FALSE;

        

    }



    public function get_orders_info($array='')

    {

        if(!empty($array)){

            $this->db->where_in('o.id',$array);

        }

        $this->db->select('oi.*,oi.id as info_id,o.*');

        $this->db->from('orders as o');

        $this->db->join('order_info as oi', 'o.id = oi.order_id','left');

        $this->db->order_by('o.id', 'desc');

        $query = $this->db->get();

        if ($query->num_rows() > 0)

            return $query->result();

        else

            return FALSE;

    }



    





 



}