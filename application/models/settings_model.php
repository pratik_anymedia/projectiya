<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings_model extends MY_Model { 

    /*     * ******************
     *
     * function constructor
     * 
     * @param		null
     * @return 		null
     */
    
    function settings_model() {

        parent::__construct();

        $this->table = 'settings';
        $this->primary_key = 'settings.id';
        
    }
    
    public function setSetting($key , $value){
        if($key ==''){
            return false;
        }
        
        if($this->isSettingExist($key)){
            $this->db->update('settings' , array('value' => $value), array('key' => $key));
        }else{
            $this->db->insert('settings' , array('key' => $key,'value' => $value));
        }
    }
    
    public function isSettingExist($key){
        if($key ==''){
            return false;
        }
        return $this->db->get_where('settings', array('key' => $key))->row();
    }
    
    function getSettings(){
        return $this->db->get('settings')->result();
    }
    
}