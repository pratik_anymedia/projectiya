<?php



if (!defined('BASEPATH')) exit('No direct script access allowed');



/**

 *

 * 	clear cache

 *

 */



if (!function_exists('clear_cache')) {

    function clear_cache()

    {


        $CI = & get_instance();

        $CI->output->set_header('Expires: Wed, 11 Jan 1984 05:00:00 GMT');

        $CI->output->set_header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . 'GMT');

        $CI->output->set_header("Cache-Control: no-cache, no-store, must-revalidate");

        $CI->output->set_header("Pragma: no-cache");



    }







}

/**

 *

 *  check superadmin logged in

 *

 */



if (!function_exists('superadmin_logged_in')) {

    function superadmin_logged_in()
    {

        $CI = & get_instance();

        $superadmin_info = $CI->session->userdata('superadmin_info');

        if ($superadmin_info['logged_in'] === TRUE && $superadmin_info['user_role'] == 0) return TRUE;

        else return FALSE;

    }

}



/**

 *

 * 	check user logged in

 *

 */


if (!function_exists('user_logged_in')) {

    function user_logged_in()
    {

        $CI = & get_instance();

        $superadmin_info = $CI->session->userdata('user_info');

        if($superadmin_info['logged_in'] === TRUE && $superadmin_info['user_role'] == 1) return TRUE;

        else return FALSE;

    }

}



/**

 *

 * 	get superadmin id

 *
 
 */



if (!function_exists('superadmin_id')) {



    function superadmin_id()

    {



        $CI = & get_instance();



        $superadmin_info = $CI->session->userdata('superadmin_info');



        return $superadmin_info['id'];



    }



}





/**



 *



 *  check user role


 *


 */



if (!function_exists('user_role')) {



    function user_role()

    {



        $CI = & get_instance();



        $user_info = $CI->session->userdata('user_info');



        if($user_info['logged_in'] === TRUE && $user_info['user_role'] == 2){



            return $user_info['user_role'];



        } else { 



            return FALSE; 

        }



    }







}



/**



 *



 *  check trainer logged in



 *



 */



if (!function_exists('user_id')) {



    function user_id()

    {



        $CI = & get_instance();



        $user_info = $CI->session->userdata('user_info');



        if($user_info['logged_in'] === TRUE && $user_info['user_role'] == 1){



            return $user_info['id'];



        } else { 



            return FALSE; 

        }



    }







}







/**



 *



 * 	superadmin login information



 *



 */



if (!function_exists('superadmin_name')) {







    function superadmin_name()



    {



        $CI = & get_instance();



        $superadmin_info = $CI->session->userdata('superadmin_info');



        if ($superadmin_info['logged_in'] === TRUE) return $superadmin_info['first_name']." ".$superadmin_info['last_name'];



        else return FALSE;



    }







}















/**



 *



 * 	backend pagination



 *



 */



if (!function_exists('backend_pagination')) {







    function backend_pagination()



    {



        $data = array();



        $data['full_tag_open'] = '<ul class="pagination">';



        $data['full_tag_close'] = '</ul>';



        $data['first_tag_open'] = '<li>';



        $data['first_tag_close'] = '</li>';



        $data['num_tag_open'] = '<li>';



        $data['num_tag_close'] = '</li>';



        $data['last_tag_open'] = '<li>';



        $data['last_tag_close'] = '</li>';



        $data['next_tag_open'] = '<li>';



        $data['next_tag_close'] = '</li>';



        $data['prev_tag_open'] = '<li>';



        $data['prev_tag_close'] = '</li>';



        $data['cur_tag_open'] = '<li class="active"><a href="#">';



        $data['cur_tag_close'] = '</a></li>';



        return $data;



    }







}



/**



 *



 *  frontend pagination



 *



 */



if (!function_exists('frontend_pagination')) {





    function frontend_pagination()

    {



        $data = array();



        $data['full_tag_open'] = '<ul class="pagination pull-right">';



        $data['full_tag_close'] = '</ul>';



        $data['first_tag_open'] = '<li>';



        $data['first_tag_close'] = '</li>';



        $data['num_tag_open'] = '<li>';



        $data['num_tag_close'] = '</li>';



        $data['last_tag_open'] = '<li>';



        $data['last_tag_close'] = '</li>';



        $data['next_tag_open'] = '<li>';



        $data['next_tag_close'] = '</li>';



        $data['prev_tag_open'] = '<li>';



        $data['prev_tag_close'] = '</li>';



        $data['cur_tag_open'] = '<li class="active"><a href="#">';



        $data['cur_tag_close'] = '</a></li>';



        return $data;



    }







}







/**



 *



 * 	thisis  back end helper



 *



 */



if (!function_exists('msg_alert')) {







    function msg_alert_backend()



    {



        $CI = & get_instance();







        ?>



        <?php if ($CI->session->flashdata('msg_success')): ?>



            <div class="alert alert-success">



                <button type="button" class="close" data-dismiss="alert">&times;</button>



                  <!-- <strong>Success :</strong> <br> --> <?php echo $CI->session->flashdata('msg_success'); ?>



            </div>



        <?php endif; ?>



        <?php if ($CI->session->flashdata('msg_info')): ?>



            <div class="alert alert-info">



                <button type="button" class="close" data-dismiss="alert">&times;</button>



                <!-- <strong>Info :</strong> <br> --> <?php echo $CI->session->flashdata('msg_info'); ?>



            </div>



        <?php endif; ?>



        <?php if ($CI->session->flashdata('msg_warning')): ?>



            <div class="alert alert-warning">



                <button type="button" class="close" data-dismiss="alert">&times;</button>



                  <!--  <strong>Warning :</strong> <br> --> <?php echo $CI->session->flashdata('msg_warning'); ?>



            </div>



        <?php endif; ?>



        <?php if ($CI->session->flashdata('msg_error')): ?>



            <div class="alert alert-danger">



                <button type="button" class="close" data-dismiss="alert">&times;</button>



                 <!-- <strong>Error :</strong> <br> --> <?php echo $CI->session->flashdata('msg_error'); ?>



            </div>



        <?php endif; ?>



        <?php







    }







}



/**

 *

 * 	thisis  back end helper

 *

 */



if (!function_exists('msg_alert_frontend')) {

    function msg_alert_frontend()
    {

        $CI = & get_instance();

 ?>



        <?php if ($CI->session->flashdata('msg_success')): ?>



            <div class="alert alert-success">



                <!--  <button type="button" class="close" data-dismiss="alert">&times;</button> -->



                 <!-- <strong>Success :</strong> <br> --> <?php echo $CI->session->flashdata('msg_success'); ?>



            </div>



        <?php endif; ?>



        <?php if ($CI->session->flashdata('msg_info')): ?>



            <div class="alert alert-info">



                <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->



                <!-- <strong>Info :</strong> <br> --> <?php echo $CI->session->flashdata('msg_info'); ?>



            </div>



        <?php endif; ?>



        <?php if ($CI->session->flashdata('msg_warning')): ?>



            <div class="alert alert-warning">



                <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->



            <!--  <strong>Warning :</strong> <br> --> <?php echo $CI->session->flashdata('msg_warning'); ?>



            </div>



        <?php endif; ?>



        <?php if ($CI->session->flashdata('msg_error')): ?>



            <div class="alert alert-danger">



                <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->



                 <!-- <strong>Error :</strong> <br> --> <?php echo $CI->session->flashdata('msg_error'); ?>



            </div>



        <?php endif; ?>



        <?php







    }







}















/**



 *



 * 	Menu Information



 *



 */





if (!function_exists('upload_file')) {



    function upload_file($param = null) {



        $CI = & get_instance();



        $config['upload_path'] = './assets/uploads/';



        $config['allowed_types'] = 'gif|jpg|png|xls|xlsx|csv|jpeg|pdf|doc|docx';



        $config['max_size'] = 1024 * 90;



        $config['image_resize'] = TRUE;

        

        $config['resize_width'] = 200;



        $config['resize_height'] = 200;

 

        if ($param) {



            $config = $param + $config;



        }



        $CI->load->library('upload');



        $CI->upload->initialize($config);



        if (!empty($config['file_name'])) $file_Status = $CI->upload->do_upload($config['file_name']);



        else $file_Status = $CI->upload->do_upload();



        if (!$file_Status) {



            return array('STATUS' => FALSE, 'FILE_ERROR' => $CI->upload->display_errors());



        } else {





        $uplaod_data = $CI->upload->data();



            // if(empty($param['resize_width'])&&empty($param['resize_height'])){

            //      // $original_height = $uplaod_data['image_height']; 

            //      // $original_width =  $uplaod_data['image_width']; 

            //         $config['resize_width']=175; 

            //         $config['resize_height']=150;

            //  } 



            $upload_file = explode('.', $uplaod_data['file_name']);





            if ($config['image_resize'] && in_array($upload_file[1], array('gif', 'jpeg', 'jpg', 'png', 'bmp', 'jpe'))) {



                $param2 = array(



                    'source_image' => $config['source_image'] . $uplaod_data['file_name'],



                    'new_image' => $config['new_image'] . $uplaod_data['file_name'],



                    'create_thumb' => FALSE,



                    'maintain_ratio' => TRUE,



                    'width' => $config['resize_width'],



                    'height' => $config['resize_height'],



                );





                image_resize($param2);



            }



            if (empty($config['image_resize']) && in_array($upload_file[1], array('gif', 'jpeg', 'jpg', 'png', 'bmp', 'jpe'))) {



                $param3 = array(

                    'file_name' => $uplaod_data['file_name'],



                    'source_image' => $config['source_image'] . $uplaod_data['file_name'],



                    'new_image' => $config['new_image'] . $uplaod_data['file_name'],



                    'maintain_ratio' => 0,



                    'width' => $config['resize_width'],



                    'height' => $config['resize_height'],



                );



               create_frontend_thumbnail($param3);



            }

            return array('STATUS' => TRUE, 'UPLOAD_DATA' => $uplaod_data);



        }



    }







}











    function create_frontend_thumbnail($config_img = '')



    {



     

        $CI = & get_instance();



        $config_image['image_library'] = 'gd2';



        $config_image['source_image'] = $config_img['source_image'];



        $config_image['file_name'] = $config_img['file_name'];



        $config_image['new_image'] = $config_img['new_image'];



        $config_image['height'] = $config_img['height'];



        $config_image['width'] = $config_img['width'];





        list($width, $height, $type, $attr) = getimagesize($config_image['source_image']);



        if ($width < $height) {



            $cal = $width / $height;



            $config_image['width'] = $config_img['width'] * $cal;



        }



        if ($height < $width) {



            $cal = $height / $width;



            $config_image['height'] = $config_img['height'] * $cal;



        }



        $CI->load->library('image_lib');

        $CI->image_lib->initialize($config_image);

        if (!$CI->image_lib->resize()) return array('status' => FALSE, 'error_msg' => $CI->image_lib->display_errors());



        else return array('status' => TRUE, 'file_name' => $config_image['file_name']);



    }







/**



 *



 *  thumbnail image



 *



 */



if (!function_exists('create_thumbnail')) {







    function create_thumbnail($config_img = '')



    {



     

        $CI = & get_instance();



        $config_image['image_library'] = 'gd2';



        $config_image['source_image'] = $config_img['source_path'] . $config_img['file_name'];



        $config_image['encrypt_name'] = TRUE;



        $config_image['new_image'] = $config_img['destination_path'] . $config_img['file_name'];



        $config_image['maintain_ratio'] = FALSE;



        $config_image['height'] = $config_img['height'];



        $config_image['width'] = $config_img['width'];



    if (!empty($config_image['image_resize'])) {

        list($width, $height, $type, $attr) = getimagesize($config_img['source_path'] . $config_img['file_name']);



        if ($width < $height) {



            $cal = $width / $height;



            $config_image['width'] = $config_img['width'] * $cal;



        }



        if ($height < $width) {



            $cal = $height / $width;



            $config_image['height'] = $config_img['height'] * $cal;



        }

    }

       





       

        $CI->load->library('image_lib');

        $CI->image_lib->initialize($config_image);

        if (!$CI->image_lib->resize()) return array('status' => FALSE, 'error_msg' => $CI->image_lib->display_errors());



        else return array('status' => TRUE, 'file_name' => $config_img['file_name']);



    }







}



function check_image_type($image,$type){

     switch ($type) {

           case IMAGETYPE_JPEG:

             $source = imagecreatefromjpeg($path);

             break;

             

           case IMAGETYPE_GIF:

             $source = imagecreatefromgif($path);

           

           case IMAGETYPE_PNG:

             $source = imagecreatefrompng($path);

           }

           return $source;

   }

















if (!function_exists('convert_to_gry')) {

    function convert_to_gry($image,$des)

    {

       

       

      $imageInfo = getimagesize($image);



 switch ($imageInfo[2]) {

           case IMAGETYPE_JPEG:

             $source = imagecreatefromjpeg($image);

             break;

             

           case IMAGETYPE_GIF:

             $source = imagecreatefromgif($image);

           

           case IMAGETYPE_PNG:

             $source = imagecreatefrompng($image);

           }



            imagecreatetruecolor(imagesx($source), imagesy($source));



        if($source && imagefilter($source, IMG_FILTER_GRAYSCALE))

        {

            imagecolorallocate($source,255,255,255);

        $transColor = imagecolorallocatealpha($source, 0, 0, 0, 127);

            imagecolortransparent($source, $transColor);   

            imagepng($source,$des);

            // echo'Conversion to grayscale Done';

        }

        else

        {

            echo 'Conversion to grayscale failed.';

        }

    }

}



/**



 *



 * 	image resize



 *



 */



if (!function_exists('image_resize')) {







    function image_resize($param = null)



    {



        $CI = & get_instance();



        $CI->load->library('image_lib');



        $CI->image_lib->clear();



        $config['image_library'] = 'gd2';



        $config['source_image'] = './assets/uploads/';



        $config['new_image'] = './assets/uploads/';



        $config['create_thumb'] = FALSE;



        $config['maintain_ratio'] = FALSE;



        $config['width'] = 150;



        $config['height'] = 150;



        if($param){



            $config = $param + $config;

        

        }

    

        $CI->image_lib->initialize($config);



        if (!$CI->image_lib->resize()) {



            //return array('STATUS'=>TRUE,'MESSAGE'=>$CI->image_lib->display_errors());



            die($CI->image_lib->display_errors());



        } else {



            

            return array('STATUS' => TRUE, 'MESSAGE' => 'Image resized.');



        }



    }







}



/**



 *



 * 	image delete



 *



 */


if (!function_exists('file_delete')) {
    function file_delete($param = null)
    {
        $config['file_path'] = './assets/uploads/';
        $config['file_thumb_path'] = './assets/uploads/';
        if($param){
            $config = $param + $config;
        }      
        if (file_exists($config['file_path'])) {
            @unlink($config['file_path']);
        }
        if (file_exists($config['file_thumb_path'])) {
            @unlink($config['file_thumb_path']);
        }
    }
}



/**

 *

 *  Menu Information

 *

 */

if(!function_exists('get_nav_menu')) {
    function get_nav_menu($slug = '', $is_location = FALSE)
    {
        $CI = & get_instance();
        $CI->load->model('common_model');
        if ($menu = $CI->common_model->get_nav_menu($slug, $is_location)) return $menu;
        else return FALSE;
    }
}


/**

 *

 *  User information 

 *

 */


if(!function_exists('get_user_info')){
    function get_user_info()
    {
        $CI = & get_instance();
        $CI->load->model('common_model');
        if($menu = $CI->common_model->get_row('users',array('status'=>1,'user_role'=>1,'id'=>user_id()))){ 
            return $menu;
        } else { 
            return FALSE; 
        }
    }
}

if(!function_exists('get_user_order_info')) {
    function get_user_order_info($user_id='')
    {
        $CI = & get_instance();
        $CI->load->model('common_model');
        if($menu = $CI->common_model->get_row('users',array('status'=>1,'user_role'=>1,'id'=>$user_id))){ 
            return $menu;
        } else { 
            return FALSE; 
        }

    }
}



if(!function_exists('get_user_name')){
    function get_user_name($user_id='')
    {
        $CI = & get_instance();
        $CI->load->model('common_model');
        if($menu = $CI->common_model->get_row('users',array('id'=>$user_id ,'user_role'=>1))){ 
            return $name =  $menu->first_name.' '.$menu->last_name;
        } else { 
            return FALSE; 
        }
    }
}


if(!function_exists('get_user_project_count')){
    function get_user_project_count($user_id='')
    {
        $CI = & get_instance();
        $CI->db->where('user_id',$user_id);
        $CI->db->where('status',1);        
        $CI->db->from('projects');
        $query = $CI->db->get();  
        if ($query->num_rows()>0) 
            return $query->num_rows();
        else
            return 0;
    }
}

if(!function_exists('get_product_status_info')){
    function get_product_status_info($id='')
    {
        $CI = & get_instance();
        $CI->load->model('common_model');
        if($menu = $CI->common_model->get_row('products',array('id'=>$id))){ 
            return $menu;
        } else { 
            return FALSE; 
        }
    }
}

if(!function_exists('get_user_information')){
    function get_user_information($user_id='')
    {
        $CI = & get_instance();
        $CI->load->model('common_model');
        if($menu = $CI->common_model->get_row('users',array('status'=>1,'user_role'=>1,'id'=>$user_id))){ 
            return $menu;
        } else { 
            return FALSE; 
        }
    }
}

if(!function_exists('get_user_rate')){
    function get_user_rate($pro_id='',$user_id='')
    {
        $CI = &get_instance();
        $CI->load->model('common_model');
        if($menu = $CI->common_model->get_row('product_rating',array('status'=>1,'product_id'=>$pro_id,'user_id'=>$user_id))){ 
            return $menu;
        } else { 
            return FALSE; 
        }
    }
}

if(!function_exists('get_product_image')){
    function get_product_image($pro_id='')
    {
        $CI = & get_instance();
        $CI->load->model('common_model');
        if($menu = $CI->common_model->get_row('products_photos',array('random_background'=>1,'products_id'=>$pro_id))){ 
            return $menu;
        } else { 
            return FALSE; 
        }
    }
}

/**

 *

 *  User information by id

 *

 */

if (!function_exists('get_no_of_uers')){
    function get_no_of_uers()
    {

        $CI = &get_instance();      
        $CI->db->where('user_role',1);
        $CI->db->from('users');
        $query = $CI->db->get();  
        if ($query->num_rows()>0) 
            return $query->num_rows();
        else
            return 0;
    }     
}



if (!function_exists('get_no_of_products')) {
    function get_no_of_products()
    {
        $CI = &get_instance();
        $CI->db->from('projects');
        $query = $CI->db->get();  
        if ($query->num_rows()>0) 
            return $query->num_rows();
        else
            return 0;
    }     
}

if(!function_exists('get_project_title')){
    function get_project_title($pro_id='')
    {
        $CI = &get_instance();
        $CI->db->where('id',$pro_id);
        $CI->db->from('projects');
        $query = $CI->db->get();  
        if($query->num_rows()>0) 
            return $query->row()->title;
        else
            return 0;
    }     
}

if(!function_exists('get_pending_contact_msg')){
    function get_pending_contact_msg()
    {
        $CI = &get_instance();
        $CI->db->where('status',0);
        $CI->db->from('contact_us');
        $query = $CI->db->get();  
        if($query->num_rows()>0) 
            return $query->num_rows();
        else
            return 0;
    }     
}


if (!function_exists('get_pro_information')) {



    function get_pro_information($pro_id='')

    {

        $CI = &get_instance();

        $CI->db->select('pp.*,pp.id as photo_id,p.*');     

        $CI->db->where('p.id',$pro_id);

        $CI->db->where('p.status',1);

        $CI->db->where('pp.random_background',1);  

        $CI->db->from('products as p');

        $CI->db->join('products_photos as pp', 'p.id = pp.products_id', 'left');        

        $query = $CI->db->get();

        if($query->num_rows() > 0){ 

            return $query->row();

        }else{ 

            return FALSE;

        }    

    }     





}



if (!function_exists('get_category_products')) {



    function get_category_products($cat_id='')

    {

        $CI = &get_instance();

        $CI->db->like('category','#'.$cat_id.'#');

        $CI->db->from('products');

        $query = $CI->db->get();

        if($query->num_rows() > 0){ 

            return $query->result();

        }else{ 

            return FALSE;

        }    

    }     





}

/**



 *



 * 	Get YouTube video ID from URL



 *



 */



if (!function_exists('get_youtube_id_from_url')) {



    function get_youtube_thumbnail($youtube_url = '', $alt = TRUE,$number=1) {



        $youtubeId = preg_replace('/^[^v]+v.(.{11}).*/', '$1', $youtube_url);



        if ($alt) $alt = 'alt="AA' . $youtubeId . '"';



        else $alt = '';



        return'<img style="border-radius: 0px !important; transition: none 0s ease 0s;" class="img-rounded img-responsive" src="http://img.youtube.com/vi/' . $youtubeId . '/mqdefault.jpg" ' . $alt . '>';



    }



}





//for option



if (!function_exists('get_option_value')) {



    function get_option_value($key = FALSE) {



        $CI = & get_instance();



        if ($option = $CI->getoption->get_option_value($key)) 

            return $option;

        else

             return FALSE;



    }



}



/**   

 Sub-Services  

**/



if (!function_exists('get_product_images')) {



    function get_product_images($pro_id=''){



        $CI = &get_instance();

        $CI->db->where('products_id',$pro_id);

        $CI->db->from('products_photos');

        $query = $CI->db->get();  

        if ($query->num_rows()>0) 

            return $query->result();

        else

            return 0;

    }



}



/**   

 Sub-Services  

**/



if (!function_exists('get_prices_type')) {

    function get_prices_type($pro_id=''){

        $CI = &get_instance();

        $CI->db->where('product_id',$pro_id);

        $CI->db->from('multiple_price');

        $query = $CI->db->get();  

        if ($query->num_rows()>0) 

            return $query->result();

        else

            return 0;

    }

}

if (!function_exists('get_price_value')) {

    function get_price_value($pri_id=''){

        $CI = &get_instance();

        $CI->db->where('id',$pri_id);

        $CI->db->from('multiple_price');

        $query = $CI->db->get();  

        if($query->num_rows()>0) 

            return $query->row()->price_for;

        else

            return 0;

    }

}



/**   

All-Sub-Services  

**/



if (!function_exists('get_all_sub_services')) {



    function get_all_sub_services($service_id=''){



        $CI = & get_instance();

        $CI->db->where('service_id',$service_id);

        $CI->db->from('services');

        $query = $CI->db->get();  

        if ($query->num_rows()>0) 

            return $query->result();

        else

            return 0;

    }



}

if (!function_exists('get_country_state')) {



    function get_country_state($con_sulg=''){



        $CI = & get_instance();

        $Country = $CI->common_model->get_row('countries',array('slug'=>$con_sulg));

        if(!empty($Country)){

            $CI->db->where('con_id',$Country->id);

            $CI->db->from('states');

            $query = $CI->db->get();  

            if ($query->num_rows()>0) 

                return $query->result();

            else

                return 0;

        }else{

            return 0;

        }

    }



}



/**   

  Related Info

**/



if (!function_exists('get_related_info')) {

    function get_related_info($pro_id=''){

        $CI = & get_instance();
        $CI->db->where('product_id',$pro_id);
        $CI->db->from('products_related_info');
        $query = $CI->db->get();  
        if ($query->num_rows()>0) 
            return $query->row();
        else
            return 0;
    }
}


if(!function_exists('get_project_reviews')){

    function get_project_reviews($pro_id=''){

        $CI = & get_instance();
        $CI->db->where('status',1);
        $CI->db->where('project_id',$pro_id);
        $CI->db->from('project_reviews');
        $query = $CI->db->get();  
        if($query->num_rows()>0) 
            return $query->result();
        else
            return 0;
    }

}



/**   
   Service  
**/



if (!function_exists('get_category_name')) {



    function get_category_name($cat_id=''){



        $CI = & get_instance();

        $resp='';

        $product = $CI->common_model->get_row('products',array('id'=>$cat_id));

        if(!empty($product->category)){

            $Array=explode('#,#',trim($product->category,'#'));

           if(!empty($Array)): 

            for($i=0; $i < count($Array); $i++){ 

                $cats = $CI->common_model->get_row('categories',array('id'=>$Array[$i]));

                $resp.=$cats->category.',';

            } 

           endif;

              

        } 

        return $resp;

    }



}

if (!function_exists('get_category_id')) {



    function get_category_id($slug=''){



        $CI = & get_instance();

        $CI->db->where('slug',$slug);

        $CI->db->from('categories');

        $query = $CI->db->get();  

        if ($query->num_rows()>0) 

            return $query->row()->id;

        else

            return 0;

    }



}

/**   

  Service  

**/



if (!function_exists('get_group_name')) {



    function get_group_name($gro_id=''){



        $CI = & get_instance();

        $CI->db->where('id',$gro_id);

        $CI->db->from('groups');

        $query = $CI->db->get();  

        if ($query->num_rows()>0) 

            return $query->row()->group_name;

        else

            return 0;

    }



}

if (!function_exists('get_country_info')) {



    function get_country_info($cat_slug=''){



        $CI = & get_instance();

        $CI->db->where('slug',$cat_slug);

        $CI->db->from('countries');

        $query = $CI->db->get();  

        if ($query->num_rows()>0) 

            return $query->row();

        else

            return 0;

    }



}



if (!function_exists('get_shiping_info')) {

    function get_shiping_info($pro_id=''){

        $CI = & get_instance();

        $CI->db->where('product_id',$pro_id);

        $CI->db->from('product_shpping_info');

        $query = $CI->db->get();  

        if ($query->num_rows()>0) 

            return $query->row();

        else

            return 0;

    }
}



if (!function_exists('get_post_category')) {
    function get_post_category($blog_cat_id = FALSE) {
        $CI = & get_instance();
        $CI->db->where('category_id',$blog_cat_id);
        $CI->db->from('blogs');
        $query = $CI->db->get();  
        if ($query->num_rows()>0) 
            return $query->num_rows();
        else
            return 0;
    }
}


if (!function_exists('get_recent_blog')) {
    function get_recent_blog($offset=0,$limit=3) {
          $CI = &get_instance();
         if($offset>=0 && $limit>0){

            $CI->db->limit($limit,$offset);

            $CI->db->where('blog_status',1);

            $CI->db->order_by('id', 'desc');

            $query = $CI->db->get('blogs');

            if($query->num_rows()>0)

                return $query->result();

            else

                return FALSE;

        }else{

            $query = $CI->db->get('blogs');

            return $query->num_rows();          

        }       

    }
}


if(!function_exists('get_latest_blogs')) {
    function get_latest_blogs(){
        $CI = & get_instance();
        return $CI->common_model->blog_favorite(0,3);
    }
}


if ( ! function_exists('increase_comments')) {
    function increase_comments($id,$comments){
        $CI =& get_instance();
        // if($type==1){
            $count=$comments+1;
            $arrayName = array('comment_count' =>$count);          
        //}
        // else
        // {
        //     $count=$comments+1;
        //     $arrayName = array('comment_count' =>$count);           
        // }
        $CI->db->where('id', $id);
        
        return $query = $CI->db->update('blogs',$arrayName);

            // return $query = $CI->db->update('events',$arrayName);
    }                   
}


if ( ! function_exists('increase_download_count')) {
    function increase_download_count($id,$download_count){
        $CI =& get_instance();
        $count=$download_count+1;
        $arrayName = array('download_count' =>$count);          
        $CI->db->where('unique_id', $id);
        return $query = $CI->db->update('orders',$arrayName);
        // return $query = $CI->db->update('events',$arrayName);
    }                   
}


if ( ! function_exists('decrease_comments')) {
    function decrease_comments($id,$comments){
        $CI =& get_instance();
            $count=$comments-1;
            $arrayName = array('comment_count' =>$count);           
      
        $CI->db->where('id', $id);
        
        return $query = $CI->db->update('blogs',$arrayName);

           
    }                   
}


if (!function_exists('get_blog_by_id')) {



    function get_blog_by_id($blog_id = FALSE) {



        $CI = &get_instance();



        $CI->db->where('id',$blog_id);

        $CI->db->from('blogs');

        $query = $CI->db->get();  

        if ($query->num_rows()>0) 

            return $query->row();

        else

            return 0;

    }



}



if (!function_exists('get_blog_count')){
    function get_blog_count($category_id = 0) {
        $CI = & get_instance();
        return $CI->common_model->blog_count($category_id);
    }
}

if(!function_exists('get_blog_comments')) {
    function get_blog_comments($blog_id = 0){
        $CI = & get_instance();
        return $CI->common_model->blog_comments($blog_id);
    }
}

if(!function_exists('get_pending_comments')) {
    function get_pending_comments($blog_id=0){
        $CI = & get_instance();
        $CI->db->where('comment_blog_id',$blog_id);
        $CI->db->where('comment_approved',0);
        $CI->db->from('comments');
        $query = $CI->db->get();
        if ($query->num_rows() > 0)
            return $query->num_rows();
        else
            return FALSE; 
    }
}

if(!function_exists('get_total_like_of_project')) {
    function get_total_like_of_project($pro_id=''){
        $CI = & get_instance();
        $CI->db->where('project_id',$pro_id);
        $CI->db->from('favorite_project');
        $query = $CI->db->get();
        if ($query->num_rows() > 0)
            return $query->num_rows();
        else
            return FALSE; 
    }
}

if(!function_exists('product_rating_count')) {
    function product_rating_count($pro_id=0){
        $CI = & get_instance();
        $total_rate=0;
        $CI->db->where('project_id',$pro_id);
        $CI->db->from('project_rating');
        $query = $CI->db->get();
        if ($query->num_rows() > 0){
            $no_of_rate=$query->num_rows();
            $ratings = $query->result();
            if(!empty($ratings)){
                foreach ($ratings as $value) {
                    $total_rate += $value->rate; 
                }
                if(!empty($total_rate)){
                  return  $total_rating= $total_rate/$no_of_rate;
                }
            }
            return  0;
        }else{
            return FALSE; 
        }
    }
}

        

        

if (!function_exists('get_blog_month')) {

    function get_blog_month() {

        $CI = & get_instance();

        $CI->db->group_by('MONTH(blogs.blog_created)');

        $CI->db->select('*');

        $CI->db->from('blogs');

        $query = $CI->db->get();  

        if ($query->num_rows()>0) 

            return $query->result();

        else

            return FALSE;

    }



}







if (!function_exists('file_download')) {



    function file_download($title = FALSE, $data = FALSE)  {



        $data = str_replace('./', '', $data);



        $CI = & get_instance();



        $CI->load->helper('download');



        if (!empty($title) && !empty($data)):



            $title = url_title($title, '-', TRUE);



            if ($file = file_get_contents($data)) {



                $extend = end(explode('.', $data));



                $file_name = $title . '.' . $extend;



                force_download($file_name, $file);



            } else {



                return FALSE;



            }



        endif;



    }



}









if(!function_exists('gettwitterfeeds')){

    function gettwitterfeeds()

    {

        $CI = & get_instance();

        //include APPPATH.'libraries/twitter/api.php';

        //$CI->api = new Api();

        $twitter_feed = $CI->api->get_user_timeline(array('screen_name' => 'testchapter', 'count' => 3));

        if(!empty($twitter_feed)){

            echo "<li class='twitt'>";

            echo "<p>@" . $twitter_feed[0]['text'].'</p>';

            $timespan = explode(',', timespan(strtotime($twitter_feed[0]['created_at']), time()), 2);

            echo "<span>Posted " . $timespan[0] . " ago </span>";

            echo "</li>";

        }

    }

}



if (!function_exists('get_fb_feed')) {



    function get_fb_feed()



    {



        $CI = & get_instance();



        $fb_post = $CI->fb_api->get_fb_post_timeline();



        if (!empty($fb_post)) {



            //print_r($fb_post);



            echo $fb_post['data'][0]['message'];



            echo "<br>";



            $timespan1 = explode(',', timespan(strtotime($fb_post['data'][0]['created_time']), time()), 2);



            echo "<span>Posted " . $timespan1[0] . " ago </span>";



        }



    }







}

if(!function_exists('get_user_name_id')){
    function get_user_name_id()
    {
        $CI = &get_instance();
        $CI->load->model('common_model');
        $user_id_name = array();
        if($menu = $CI->common_model->get_result('users',array())){ 
           
            foreach ($menu as $key => $value) {
                
                $user_id_name[$value->id] = $value->first_name.' '.$value->last_name;
            }
            return $user_id_name;
        } else { 
            return FALSE; 
        }
    }
}



 /**



 *



 * 	Get All Country



 *



 */





if ( ! function_exists('get_country_array'))

    {   

        function get_country_array()

        {

            

            return array(

                            "AF"=>"Afghanistan",

                            "AX"=>"Aland Islands",

                            "AL"=>"Albania",

                            "DZ"=>"Algeria",

                            "AS"=>"American Samoa",

                            "AD"=>"Andorra",

                            "AO"=>"Angola",

                            "AI"=>"Anguilla",

                            "AQ"=>"Antarctica",

                            "AG"=>"Antigua and Barbuda",

                            "AR"=>"Argentina",

                            "AM"=>"Armenia",

                            "AW"=>"Aruba",

                            "AU"=>"Australia",

                            "AT"=>"Austria",

                            "AZ"=>"Azerbaijan",

                            "BS"=>"Bahamas",

                            "BH"=>"Bahrain",

                            "BD"=>"Bangladesh",

                            "BB"=>"Barbados",

                            "BY"=>"Belarus",

                            "BE"=>"Belgium",

                            "BZ"=>"Belize",

                            "BJ"=>"Benin",

                            "BM"=>"Bermuda",

                            "BT"=>"Bhutan",

                            "BO"=>"Bolivia, Plurinational State of",

                            "BQ"=>"Bonaire, Sint Eustatius and Saba",

                            "BA"=>"Bosnia and Herzegovina",

                            "BW"=>"Botswana",

                            "BV"=>"Bouvet Island",

                            "BR"=>"Brazil",

                            "IO"=>"British Indian Ocean Territory",

                            "BN"=>"Brunei Darussalam",

                            "BG"=>"Bulgaria",

                            "BF"=>"Burkina Faso",

                            "BI"=>"Burundi",

                            "KH"=>"Cambodia",

                            "CM"=>"Cameroon",

                            "CA"=>"Canada",

                            "CV"=>"Cape Verde",

                            "KY"=>"Cayman Islands",

                            "CF"=>"Central African Republic",

                            "TD"=>"Chad",

                            "CL"=>"Chile",

                            "CN"=>"China",

                            "CX"=>"Christmas Island",

                            "CC"=>"Cocos (Keeling) Islands",

                            "CO"=>"Colombia",

                            "KM"=>"Comoros",

                            "CG"=>"Congo",

                            "CD"=>"Congo, The Democratic Republic of the",

                            "CK"=>"Cook Islands",

                            "CR"=>"Costa Rica",

                            "CI"=>"Cote D'Ivoire",

                            "HR"=>"Croatia",

                            "CU"=>"Cuba",

                            "CW"=>"Curaçao",

                            "CY"=>"Cyprus",

                            "CZ"=>"Czech Republic",

                            "DK"=>"Denmark",

                            "DJ"=>"Djibouti",

                            "DM"=>"Dominica",

                            "DO"=>"Dominican Republic",

                            "EC"=>"Ecuador",

                            "EG"=>"Egypt",

                            "SV"=>"El Salvador",

                            "GQ"=>"Equatorial Guinea",

                            "ER"=>"Eritrea",

                            "EE"=>"Estonia",

                            "ET"=>"Ethiopia",

                            "FK"=>"Falkland Islands (Malvinas)",

                            "FO"=>"Faroe Islands",

                            "FJ"=>"Fiji",

                            "FI"=>"Finland",

                            "FR"=>"France",

                            "GF"=>"French Guiana",

                            "PF"=>"French Polynesia",

                            "TF"=>"French Southern Territories",

                            "GA"=>"Gabon",

                            "GM"=>"Gambia",

                            "GE"=>"Georgia",

                            "DE"=>"Germany",

                            "GH"=>"Ghana",

                            "GI"=>"Gibraltar",

                            "GR"=>"Greece",

                            "GL"=>"Greenland",

                            "GD"=>"Grenada",

                            "GP"=>"Guadeloupe",

                            "GU"=>"Guam",

                            "GT"=>"Guatemala",

                            "GG"=>"Guernsey",

                            "GN"=>"Guinea",

                            "GW"=>"Guinea-Bissau",

                            "GY"=>"Guyana",

                            "HT"=>"Haiti",

                            "HM"=>"Heard Island and McDonald Islands",

                            "VA"=>"Holy See (Vatican City State)",

                            "HN"=>"Honduras",

                            "HK"=>"Hong Kong",

                            "HU"=>"Hungary",

                            "IS"=>"Iceland",

                            "IN"=>"India",

                            "ID"=>"Indonesia",

                            "IR"=>"Iran, Islamic Republic of",

                            "IQ"=>"Iraq",

                            "IE"=>"Ireland",

                            "IM"=>"Isle of Man",

                            "IL"=>"Israel",

                            "IT"=>"Italy",

                            "JM"=>"Jamaica",

                            "JP"=>"Japan",

                            "JE"=>"Jersey",

                            "JO"=>"Jordan",

                            "KZ"=>"Kazakhstan",

                            "KE"=>"Kenya",

                            "KI"=>"Kiribati",

                            "KP"=>"Korea, Democratic People's Republic of",

                            "KR"=>"Korea, Republic of",

                            "KW"=>"Kuwait",

                            "KG"=>"Kyrgyzstan",

                            "LA"=>"Lao People's Democratic Republic",

                            "LV"=>"Latvia",

                            "LB"=>"Lebanon",

                            "LS"=>"Lesotho",

                            "LR"=>"Liberia",

                            "LY"=>"Libya",

                            "LI"=>"Liechtenstein",

                            "LT"=>"Lithuania",

                            "LU"=>"Luxembourg",

                            "MO"=>"Macao",

                            "MK"=>"Macedonia, The Former Yugoslav Republic of",

                            "MG"=>"Madagascar",

                            "MW"=>"Malawi",

                            "MY"=>"Malaysia",

                            "MV"=>"Maldives",

                            "ML"=>"Mali",

                            "MT"=>"Malta",

                            "MH"=>"Marshall Islands",

                            "MQ"=>"Martinique",

                            "MR"=>"Mauritania",

                            "MU"=>"Mauritius",

                            "YT"=>"Mayotte",

                            "MX"=>"Mexico",

                            "FM"=>"Micronesia, Federated States of",

                            "MD"=>"Moldova, Republic of",

                            "MC"=>"Monaco",

                            "MN"=>"Mongolia",

                            "ME"=>"Montenegro",

                            "MS"=>"Montserrat",

                            "MA"=>"Morocco",

                            "MZ"=>"Mozambique",

                            "MM"=>"Myanmar",

                            "NA"=>"Namibia",

                            "NR"=>"Nauru",

                            "NP"=>"Nepal",

                            "NL"=>"Netherlands",

                            "NC"=>"New Caledonia",

                            "NZ"=>"New Zealand",

                            "NI"=>"Nicaragua",

                            "NE"=>"Niger",

                            "NG"=>"Nigeria",

                            "NU"=>"Niue",

                            "NF"=>"Norfolk Island",

                            "MP"=>"Northern Mariana Islands",

                            "NO"=>"Norway",

                            "OM"=>"Oman",

                            "PK"=>"Pakistan",

                            "PW"=>"Palau",

                            "PS"=>"Palestinian Territory, Occupied",

                            "PA"=>"Panama",

                            "PG"=>"Papua New Guinea",

                            "PY"=>"Paraguay",

                            "PE"=>"Peru",

                            "PH"=>"Philippines",

                            "PN"=>"Pitcairn",

                            "PL"=>"Poland",

                            "PT"=>"Portugal",

                            "PR"=>"Puerto Rico",

                            "QA"=>"Qatar",

                            "RE"=>"Reunion",

                            "RO"=>"Romania",

                            "RU"=>"Russian Federation",

                            "RW"=>"Rwanda",

                            "BL"=>"Saint Barthelemy",

                            "SH"=>"Saint Helena, Ascension and Tristan Da Cunha",

                            "KN"=>"Saint Kitts and Nevis",

                            "LC"=>"Saint Lucia",

                            "MF"=>"Saint Martin (French part)",

                            "PM"=>"Saint Pierre and Miquelon",

                            "VC"=>"Saint Vincent and the Grenadines",

                            "WS"=>"Samoa",

                            "SM"=>"San Marino",

                            "ST"=>"Sao Tome and Principe",

                            "SA"=>"Saudi Arabia",

                            "SN"=>"Senegal",

                            "RS"=>"Serbia",

                            "SC"=>"Seychelles",

                            "SL"=>"Sierra Leone",

                            "SG"=>"Singapore",

                            "SX"=>"Sint Maarten (Dutch part)",

                            "SK"=>"Slovakia",

                            "SI"=>"Slovenia",

                            "SB"=>"Solomon Islands",

                            "SO"=>"Somalia",

                            "ZA"=>"South Africa",

                            "GS"=>"South Georgia and the South Sandwich Islands",

                            "SS"=>"South Sudan",

                            "ES"=>"Spain",

                            "LK"=>"Sri Lanka",

                            "SD"=>"Sudan",

                            "SR"=>"Suriname",

                            "SJ"=>"Svalbard and Jan Mayen",

                            "SZ"=>"Swaziland",

                            "SE"=>"Sweden",

                            "CH"=>"Switzerland",

                            "SY"=>"Syrian Arab Republic",

                            "TW"=>"Taiwan, Province of China",

                            "TJ"=>"Tajikistan",

                            "TZ"=>"Tanzania, United Republic of",

                            "TH"=>"Thailand",

                            "TL"=>"Timor-Leste",

                            "TG"=>"Togo",

                            "TK"=>"Tokelau",

                            "TO"=>"Tonga",

                            "TT"=>"Trinidad and Tobago",

                            "TN"=>"Tunisia",

                            "TR"=>"Turkey",

                            "TM"=>"Turkmenistan",

                            "TC"=>"Turks and Caicos Islands",

                            "TV"=>"Tuvalu",

                            "UG"=>"Uganda",

                            "UA"=>"Ukraine",

                            "AE"=>"United Arab Emirates",

                            "GB"=>"United Kingdom",

                            "US"=>"United States",

                            "UM"=>"United States Minor Outlying Islands",

                            "UY"=>"Uruguay",

                            "UZ"=>"Uzbekistan",

                            "VU"=>"Vanuatu",

                            "VE"=>"Venezuela, Bolivarian Republic of",

                            "VN"=>"Viet Nam",

                            "VG"=>"Virgin Islands, British",

                            "VI"=>"Virgin Islands, U.S.",

                            "WF"=>"Wallis and Futuna",

                            "EH"=>"Western Sahara",

                            "YE"=>"Yemen",

                            "ZM"=>"Zambia",

                            "ZW"=>"Zimbabwe"

                        );

        }

    }





















function _check_superadmin_login() {



        if(superadmin_logged_in() === FALSE) redirect('superadmin/login');

}



function _check_trainer_login() {



        if(trainer_logged_in()=== FALSE) redirect('user/login');

}



function _check_trainer_pain_login() {



        if(trainer_pain_login()=== FALSE) redirect('user/login');

}



function _check_user_login() {



        if(user_logged_in()=== FALSE) redirect('user/login');

}



function _check_provider_login(){



        if(provider_logged_in()=== FALSE) redirect('user/login');



}



